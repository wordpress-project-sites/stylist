$(document).ready(function() {

	$('#pagepiling').pagepiling({
		menu: '#topmenu',
		anchors: ['price', 'contacts'],
		navigation: {
	    	'textColor': '#f2f2f2',
	        'bulletsColor': 'white',
	        'position': 'right'
	    }
	}); /*End pagepiling*/

	function get_animation_hair() {
	    if ($('body').hasClass('pp-viewing-hair')) {
	        $('.hair_block-left').addClass('AnimationLeft');
	        $('.hair_block-right').addClass('AnimationRight');
	    }
	    else {
	        setTimeout(get_animation_hair, 100);
	    }
	}

	function get_animation_cosmetolog() {
	    if ($('body').hasClass('pp-viewing-cosmetolog')) {
	        $('.cosmetolog_block').addClass('AnimationTop');
	    }
	    else {
	        setTimeout(get_animation_cosmetolog, 100);
	    }
	}

	get_animation_hair();
	get_animation_cosmetolog();

}); /*End ready*/