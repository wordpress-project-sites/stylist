$(document).ready(function() {
	$('#pagepiling').pagepiling({
		menu: '#topmenu',
		anchors: ['calendar', 'note', 'contacts'],
		navigation: {
	    	'textColor': '#f2f2f2',
	        'bulletsColor': 'white',
	        'position': 'right'
	    }
	}); /*End pagepiling*/

	/*Уберем событие клика*/
	$('a.ai1ec-event-container').click(function() {return false;});

}); /*End ready*/