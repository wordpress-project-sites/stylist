$(document).ready(function() {

	$('#pagepiling').pagepiling({
		menu: '#topmenu',
		anchors: ['home', 'services', 'entry', 'gallery', 'about', 'contacts'],
		navigation: {
	        'textColor': '#f2f2f2',
	        'bulletsColor': 'white',
	        'position': 'right'
	    }
	}); /*End pagepiling*/
	
}); /*End ready*/