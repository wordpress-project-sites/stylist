$(document).ready(function() {
	
	$('#pagepiling').pagepiling({
		menu: '#topmenu',
		anchors: ['greeting', 'aboutMe', 'contacts'],
		navigation: {
	    	'textColor': '#f2f2f2',
	        'bulletsColor': 'white',
	        'position': 'right'
	    }
	}); /*End pagepiling*/

	function get_animation_aboutMe() {
	    if ($('body').hasClass('pp-viewing-aboutMe')) {
	        $('.aboutMe_block').addClass('AnimationTop');
	        $('.aboutMe_block-img').addClass('AnimationDown');  
	    }
	    else {
	        setTimeout(get_animation_aboutMe, 100);
	    }
	}



	get_animation_aboutMe();

}); /*End ready*/