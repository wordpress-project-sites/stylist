$(document).ready(function() {

	/*Управление слайдером страниц*/
	$('#pagepiling').pagepiling({
			menu: '#topmenu',
			anchors: ['portfolio', 'contacts'],
			navigation: {
	            'textColor': '#f2f2f2',
	            'bulletsColor': 'white',
	            'position': 'right'
	        }
	}); /*End pagepiling*/

}); /*End ready*/