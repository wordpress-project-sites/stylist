<!-- preloader -->
<style type="text/css">
    .preloader_bg { position: fixed; background: white; width: 100%; height: 100%; top: 0; left: 0; z-index: 200; }
    .preloader_content { position: fixed; left: 50%; top: 50%; transform: translate(-50%,-50%); z-index: 201; font-size: 14px; }
    .preloader_content span { display: block; margin: auto; text-align: center; text-transform: uppercase; color: rgba(225,225,225,1);}

    .loader-one__svg {
  display: none;
}
.loader-one {
  -webkit-filter: url(#goo);
          filter: url(#goo);
  width: 200px;
  height: 200px;
  margin: auto;
  position: relative;
  overflow: hidden;
  border-radius: 70px;
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;
}

.loader-one__center {
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;
  position: absolute;
  background: #2196F3;
  top: 50%;
  left: 50%;
  width: 30px;
  height: 30px;
  -webkit-transform-origin: left top;
          transform-origin: left top;
  -webkit-transform: scale(0.9) translate(-50%, -50%);
          transform: scale(0.9) translate(-50%, -50%);
  -webkit-animation: blob-grow linear 3.4s infinite;
          animation: blob-grow linear 3.4s infinite;
  border-radius: 50%;
  -webkit-box-shadow: 0 -10px 40px -5px #2196F3;
          box-shadow: 0 -10px 40px -5px #2196F3;
}

.loader-one__item {
  position: absolute;
  background: #2196F3;
  top: 50%;
  left: 50%;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  -webkit-animation: blobs ease-out 3.4s infinite;
          animation: blobs ease-out 3.4s infinite;
  -webkit-transform: scale(0.9) translate(-50%, -50%);
          transform: scale(0.9) translate(-50%, -50%);
  -webkit-transform-origin: center top;
          transform-origin: center top;
  opacity: 0;
}

.loader-one__item:nth-child(1) {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s;
}

.loader-one__item:nth-child(2) {
  -webkit-animation-delay: 0.4s;
          animation-delay: 0.4s;
}

.loader-one__item:nth-child(3) {
  -webkit-animation-delay: 0.6s;
          animation-delay: 0.6s;
}

.loader-one__item:nth-child(4) {
  -webkit-animation-delay: 0.8s;
          animation-delay: 0.8s;
}

.loader-one__item:nth-child(5) {
  -webkit-animation-delay: 1s;
          animation-delay: 1s;
}

@-webkit-keyframes blobs {
  0% {
    opacity: 0;
    -webkit-transform: scale(0) translate(calc(-330px - 50%), -50%);
            transform: scale(0) translate(calc(-330px - 50%), -50%);
  }
  1% {
    opacity: 1;
  }
  35%, 65% {
    opacity: 1;
    -webkit-transform: scale(0.9) translate(-50%, -50%);
            transform: scale(0.9) translate(-50%, -50%);
  }
  99% {
    opacity: 1;
  }
  100% {
    opacity: 0;
    -webkit-transform: scale(0) translate(calc(330px - 50%), -50%);
            transform: scale(0) translate(calc(330px - 50%), -50%);
  }
}

@keyframes blobs {
  0% {
    opacity: 0;
    -webkit-transform: scale(0) translate(calc(-330px - 50%), -50%);
            transform: scale(0) translate(calc(-330px - 50%), -50%);
  }
  1% {
    opacity: 1;
  }
  35%, 65% {
    opacity: 1;
    -webkit-transform: scale(0.9) translate(-50%, -50%);
            transform: scale(0.9) translate(-50%, -50%);
  }
  99% {
    opacity: 1;
  }
  100% {
    opacity: 0;
    -webkit-transform: scale(0) translate(calc(330px - 50%), -50%);
            transform: scale(0) translate(calc(330px - 50%), -50%);
  }
}

@-webkit-keyframes blob-grow {
  0%, 39% {
    -webkit-transform: scale(0) translate(-50%, -50%);
            transform: scale(0) translate(-50%, -50%);
  }
  40%, 42% {
    -webkit-transform: scale(1, 0.9) translate(-50%, -50%);
            transform: scale(1, 0.9) translate(-50%, -50%);
  }
  43%, 44% {
    -webkit-transform: scale(1.2, 1.1) translate(-50%, -50%);
            transform: scale(1.2, 1.1) translate(-50%, -50%);
  }
  45%, 46% {
    -webkit-transform: scale(1.3, 1.2) translate(-50%, -50%);
            transform: scale(1.3, 1.2) translate(-50%, -50%);
  }
  47%, 48% {
    -webkit-transform: scale(1.4, 1.3) translate(-50%, -50%);
            transform: scale(1.4, 1.3) translate(-50%, -50%);
  }
  52% {
    -webkit-transform: scale(1.5, 1.4) translate(-50%, -50%);
            transform: scale(1.5, 1.4) translate(-50%, -50%);
  }
  54% {
    -webkit-transform: scale(1.7, 1.6) translate(-50%, -50%);
            transform: scale(1.7, 1.6) translate(-50%, -50%);
  }
  58% {
    -webkit-transform: scale(1.8, 1.7) translate(-50%, -50%);
            transform: scale(1.8, 1.7) translate(-50%, -50%);
  }
  68%, 70% {
    -webkit-transform: scale(1.7, 1.5) translate(-50%, -50%);
            transform: scale(1.7, 1.5) translate(-50%, -50%);
  }
  78% {
    -webkit-transform: scale(1.6, 1.4) translate(-50%, -50%);
            transform: scale(1.6, 1.4) translate(-50%, -50%);
  }
  80%, 81% {
    -webkit-transform: scale(1.5, 1.4) translate(-50%, -50%);
            transform: scale(1.5, 1.4) translate(-50%, -50%);
  }
  82%, 83% {
    -webkit-transform: scale(1.4, 1.3) translate(-50%, -50%);
            transform: scale(1.4, 1.3) translate(-50%, -50%);
  }
  84%, 85% {
    -webkit-transform: scale(1.3, 1.2) translate(-50%, -50%);
            transform: scale(1.3, 1.2) translate(-50%, -50%);
  }
  86%, 87% {
    -webkit-transform: scale(1.2, 1.1) translate(-50%, -50%);
            transform: scale(1.2, 1.1) translate(-50%, -50%);
  }
  90%, 91% {
    -webkit-transform: scale(1, 0.9) translate(-50%, -50%);
            transform: scale(1, 0.9) translate(-50%, -50%);
  }
  92%, 100% {
    -webkit-transform: scale(0) translate(-50%, -50%);
            transform: scale(0) translate(-50%, -50%);
  }
}

@keyframes blob-grow {
  0%, 39% {
    -webkit-transform: scale(0) translate(-50%, -50%);
            transform: scale(0) translate(-50%, -50%);
  }
  40%, 42% {
    -webkit-transform: scale(1, 0.9) translate(-50%, -50%);
            transform: scale(1, 0.9) translate(-50%, -50%);
  }
  43%, 44% {
    -webkit-transform: scale(1.2, 1.1) translate(-50%, -50%);
            transform: scale(1.2, 1.1) translate(-50%, -50%);
  }
  45%, 46% {
    -webkit-transform: scale(1.3, 1.2) translate(-50%, -50%);
            transform: scale(1.3, 1.2) translate(-50%, -50%);
  }
  47%, 48% {
    -webkit-transform: scale(1.4, 1.3) translate(-50%, -50%);
            transform: scale(1.4, 1.3) translate(-50%, -50%);
  }
  52% {
    -webkit-transform: scale(1.5, 1.4) translate(-50%, -50%);
            transform: scale(1.5, 1.4) translate(-50%, -50%);
  }
  54% {
    -webkit-transform: scale(1.7, 1.6) translate(-50%, -50%);
            transform: scale(1.7, 1.6) translate(-50%, -50%);
  }
  58% {
    -webkit-transform: scale(1.8, 1.7) translate(-50%, -50%);
            transform: scale(1.8, 1.7) translate(-50%, -50%);
  }
  68%, 70% {
    -webkit-transform: scale(1.7, 1.5) translate(-50%, -50%);
            transform: scale(1.7, 1.5) translate(-50%, -50%);
  }
  78% {
    -webkit-transform: scale(1.6, 1.4) translate(-50%, -50%);
            transform: scale(1.6, 1.4) translate(-50%, -50%);
  }
  80%, 81% {
    -webkit-transform: scale(1.5, 1.4) translate(-50%, -50%);
            transform: scale(1.5, 1.4) translate(-50%, -50%);
  }
  82%, 83% {
    -webkit-transform: scale(1.4, 1.3) translate(-50%, -50%);
            transform: scale(1.4, 1.3) translate(-50%, -50%);
  }
  84%, 85% {
    -webkit-transform: scale(1.3, 1.2) translate(-50%, -50%);
            transform: scale(1.3, 1.2) translate(-50%, -50%);
  }
  86%, 87% {
    -webkit-transform: scale(1.2, 1.1) translate(-50%, -50%);
            transform: scale(1.2, 1.1) translate(-50%, -50%);
  }
  90%, 91% {
    -webkit-transform: scale(1, 0.9) translate(-50%, -50%);
            transform: scale(1, 0.9) translate(-50%, -50%);
  }
  92%, 100% {
    -webkit-transform: scale(0) translate(-50%, -50%);
            transform: scale(0) translate(-50%, -50%);
  }
}
</style>
<script type="text/javascript">
$(function(){
    $('.preloader_bg, .preloader_content').fadeIn(0);
    $(window).load(function(){
        $('.preloader_bg').delay(250).fadeOut(1500);
        $('.preloader_content').delay(250).fadeOut(750);
    });
});
</script>
<div class="preloader_bg"></div>
<div class="preloader_content">
    <div class="loader-one">
    <div class="loader-one__center"></div>
    <div class="loader-one__item"></div>
    <div class="loader-one__item"></div>
    <div class="loader-one__item"></div>
    <div class="loader-one__item"></div>
    <div class="loader-one__item"></div>
    <div class="loader-one__item"></div>
</div>
<svg class="loader-one__svg" xmlns="http://www.w3.org/2000/svg" version="1.1">
  <defs>
    <filter id="goo">
      <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
      <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />
      <feBlend in="SourceGraphic" in2="goo" />
    </filter>
  </defs>
</svg>
</div>
<noscript>
    <style>
        html, body { opacity: 1 !important; }
        .preloader_bg, .preloader_content { display: none !important; }
    </style>
</noscript>
<!-- /preloader -->