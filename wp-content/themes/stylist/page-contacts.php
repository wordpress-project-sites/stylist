<?php 
	get_header();
	wp_reset_postdata();
?>

<body>	
<?php include 'preloader.php'; ?>

	<div id="pagepiling">
	    <div class="section" id="information" data-anchor="information">
	      <div class="container-fluid">
	      	<div class="row">
	      		<div class="col-md-12">
	      			<h1>Обращайтесь! Мы всегда рады помочь</h1>
	      		</div>	
	      	</div>

	        <div class="row">
	          <div class="offset-md-1 col-md-4">
	          	<div class="AnimationLeft">

	          		<div class="contacts-text">
	          			<?php the_content(); ?>
	          		</div>
	
					<div class="contacts_block-right">
		          		<ul class="contacts_list">
		          			<li>
		          				<a href="mailto:<?=do_shortcode('[userEmail]');?>">
		          					<i class="fas fa-envelope"></i> <?=do_shortcode('[userEmail]');?>
		          				</a>
		          			</li>
		          			<li>
		          				<i class="fas fa-home"></i> <?=do_shortcode('[userAddress]');?>
		          			</li>
		          			<li>
		          				<i class="fas fa-phone-square"></i> <?=do_shortcode('[userPhone]');?>
		          			</li>
		          		</ul>	
		          	</div>

	          		<ul class="list-inline social_list">
	          			<li class="list-inline-item">
	          				<a target="_blank" href="<?=do_shortcode('[userInstagram]');?>">
	          					<img src="<?php echo get_template_directory_uri();?>/assets/images/instagram.png" alt="instagram">
	          				</a>
	          			</li>
	          			
	          			<li class="list-inline-item">
	          				<a href="<?=do_shortcode('[userTelegram]');?>">
	          					<img src="<?php echo get_template_directory_uri();?>/assets/images/telegram.png" alt="telegram">
	          				</a>
	          			</li>
	          		</ul>
	          	</div>	
	          </div>

	          <div class="offset-md-1 col-md-5">
	          	<div class="map AnimationRight">
					<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Add97f15f29a0dac7007e81337d9c2c3ce9978847053dcc41aaba1ea9b1bd93f5&amp;width=100%&amp;height=400&amp;lang=ru_RU&amp;scroll=false"></script>
		    	</div>
	          </div>

	        </div> <!-- End row -->
	      </div> <!-- End container -->
	    </div>
<?php 
	/*Переменная для верхней конпки футера*/
	$link = 'information'; 
?>
	 
<?php include 'footer.php'; ?>

<script>
function getMap() {
	$('.map_shadow').css('display', 'none');
    $('.map_button').css('display', 'none');
}

function comeBack() {
	$('.map_shadow').css('display', 'block');
	$('.map_button').css('display', 'block');
}

$('html').keyup(function() {
    if(event.keyCode == 17 || event.keyCode == 27) {
    	comeBack();      
    }
});
</script>