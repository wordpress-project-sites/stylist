<?php 
	get_header();
	wp_reset_postdata();
?>

<body>
<?php include 'preloader.php'; ?>

	<div id="pagepiling">
	    <div class="section h-100" id="page404" data-anchor="page404">
			<div class="cont404">
				<h1>Запрашиваемая страница не найдена...</h1>
			</div>
	    </div>
<?php 
	/*Переменная для верхней конпки футера*/
	$link = 'page404'; 
?>

<?php include 'footer.php'; ?>