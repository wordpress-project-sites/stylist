<?php 
	get_header();
	wp_reset_postdata();
?>

<body>
<?php include 'preloader.php'; ?>

	<div id="pagepiling">
	    <div class="section" id="portfolio" data-anchor="portfolio">
	    	<div class="container-fluid">
	    		<div class="row mt-3">   
                    <div class="col-md-3">
						<img class="portfolioImg" src="<?=get_template_directory_uri();?>/assets/images/portfolio/1.jpg" alt="img">
					</div> 

					<div class="col-md-3">
						<img class="portfolioImg" src="<?=get_template_directory_uri();?>/assets/images/portfolio/2.jpg" alt="img">
					</div> 

					<div class="col-md-3">
						<img class="portfolioImg" src="<?=get_template_directory_uri();?>/assets/images/portfolio/3.jpg" alt="img">
					</div> 

					<div class="col-md-3">
						<img class="portfolioImg" src="<?=get_template_directory_uri();?>/assets/images/portfolio/4.jpg" alt="img">
					</div> 
                </div> 

				<div class="row mt-3">   
                    <div class="col-md-3">
						<img class="portfolioImg" src="<?=get_template_directory_uri();?>/assets/images/portfolio/5.jpg" alt="img">
					</div> 

					<div class="col-md-3">
						<img class="portfolioImg" src="<?=get_template_directory_uri();?>/assets/images/portfolio/6.jpg" alt="img">
					</div> 

					<div class="col-md-3">
						<img class="portfolioImg" src="<?=get_template_directory_uri();?>/assets/images/portfolio/7.jpg" alt="img">
					</div> 

					<div class="col-md-3">
						<img class="portfolioImg" src="<?=get_template_directory_uri();?>/assets/images/portfolio/8.jpg" alt="img">
					</div> 
                </div>

				<div class="row">
					<a class="services_button button mt-3" target="_blank" href="https://www.instagram.com/kaza4enkova/">Посмотреть все фото</a>
				</div>
	    	</div>
	    </div>

<?php 
    /*Переменная для верхней конпки футера*/
    $link = 'portfolio'; 
?>
        
<?php include 'footer.php'; ?>