-- ---------------------------------------------------------
-- Backup with BackWPup ver.: 3.6.3
-- http://backwpup.com/
-- Blog Name: stylist
-- Blog URL: http://stylist/
-- Blog ABSPATH: C:/OSPanel/domains/stylist/
-- Blog Charset: UTF-8
-- Table Prefix: wp_
-- Database Name: stylist
-- Backup on: 2020-07-07 22:23.46
-- ---------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='SYSTEM' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Table structure for `wp_alm`
--

DROP TABLE IF EXISTS `wp_alm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_alm` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `repeaterDefault` longtext NOT NULL,
  `repeaterType` text NOT NULL,
  `pluginVersion` text NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_alm`
--

LOCK TABLES `wp_alm` WRITE;
/*!40000 ALTER TABLE `wp_alm` DISABLE KEYS */;
INSERT INTO `wp_alm` (`id`, `name`, `repeaterDefault`, `repeaterType`, `pluginVersion`) VALUES 
(1, 'default', '<li <?php if (!has_post_thumbnail()) { ?> class=\"no-img\"<?php } ?>>\r\n   <?php if ( has_post_thumbnail() ) { the_post_thumbnail(\'alm-thumbnail\'); }?>\r\n   <h3><a href=\"<?php the_permalink(); ?>\" title=\"<?php the_title(); ?>\"><?php the_title(); ?></a></h3>\r\n   <p class=\"entry-meta\"><?php the_time(\"F d, Y\"); ?></p>\r\n   <?php the_excerpt(); ?>\r\n</li>', 'default', '3.5.1');
/*!40000 ALTER TABLE `wp_alm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_booking`
--

DROP TABLE IF EXISTS `wp_booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_booking` (
  `booking_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `trash` bigint(10) NOT NULL DEFAULT 0,
  `sync_gid` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `is_new` bigint(10) NOT NULL DEFAULT 1,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `sort_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `form` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `booking_type` bigint(10) NOT NULL DEFAULT 1,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_booking`
--

LOCK TABLES `wp_booking` WRITE;
/*!40000 ALTER TABLE `wp_booking` DISABLE KEYS */;
INSERT INTO `wp_booking` (`booking_id`, `trash`, `sync_gid`, `is_new`, `status`, `sort_date`, `modification_date`, `form`, `booking_type`) VALUES 
(1, 0, '', 0, '', 0x323031382d30382d31322030303a30303a3030, 0x323031382d30382d31302031343a31373a3538, 'text^name1^Jony~text^secondname1^Smith~text^email1^example-free@wpbookingcalendar.com~text^phone1^458-77-77~textarea^details1^Reserve a room with sea view', 1),
(2, 0, '', 0, '', 0x323031382d30382d33312030303a30303a3030, 0x323031382d30382d31312031373a35393a3339, 'text^name1^jjjj~text^secondname1^jjjjjjj~email^email1^55@mail.ru~text^phone1^909090~textarea^details1^99999999999999', 1),
(3, 0, '', 0, '', 0x323031382d31322d32312030303a30303a3030, 0x323031382d31312d32372030373a31393a3131, 'text^name1^vvv~text^secondname1^vvv~email^email1^tsapin89@gmail.com~text^phone1^~textarea^details1^', 1),
(4, 0, '', 1, '', 0x323031382d31322d31342030303a30303a3030, 0x323031382d31312d32372030373a32353a3138, 'text^name1^vvv~text^secondname1^vvv~email^email1^tsapin89@gmail.com~text^phone1^~textarea^details1^', 1);
/*!40000 ALTER TABLE `wp_booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_bookingdates`
--

DROP TABLE IF EXISTS `wp_bookingdates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_bookingdates` (
  `booking_id` bigint(20) unsigned NOT NULL,
  `booking_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `approved` bigint(20) unsigned NOT NULL DEFAULT 0,
  UNIQUE KEY `booking_id_dates` (`booking_id`,`booking_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_bookingdates`
--

LOCK TABLES `wp_bookingdates` WRITE;
/*!40000 ALTER TABLE `wp_bookingdates` DISABLE KEYS */;
INSERT INTO `wp_bookingdates` (`booking_id`, `booking_date`, `approved`) VALUES 
(1, 0x323031382d30382d31322030303a30303a3030, 1),
(1, 0x323031382d30382d31332030303a30303a3030, 1),
(1, 0x323031382d30382d31342030303a30303a3030, 1),
(2, 0x323031382d30382d33312030303a30303a3030, 1),
(2, 0x323031382d30392d31312030303a30303a3030, 1),
(3, 0x323031382d31322d32312030303a30303a3030, 1),
(4, 0x323031382d31322d31342030303a30303a3030, 0);
/*!40000 ALTER TABLE `wp_bookingdates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_bwg_album`
--

DROP TABLE IF EXISTS `wp_bwg_album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_bwg_album` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `preview_image` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `random_preview_image` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order` bigint(20) NOT NULL,
  `author` bigint(20) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `modified_date` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_bwg_album`
--

LOCK TABLES `wp_bwg_album` WRITE;
/*!40000 ALTER TABLE `wp_bwg_album` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_bwg_album` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_bwg_album_gallery`
--

DROP TABLE IF EXISTS `wp_bwg_album_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_bwg_album_gallery` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `album_id` bigint(20) NOT NULL,
  `is_album` tinyint(1) NOT NULL,
  `alb_gal_id` bigint(20) NOT NULL,
  `order` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_bwg_album_gallery`
--

LOCK TABLES `wp_bwg_album_gallery` WRITE;
/*!40000 ALTER TABLE `wp_bwg_album_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_bwg_album_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_bwg_gallery`
--

DROP TABLE IF EXISTS `wp_bwg_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_bwg_gallery` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `page_link` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `preview_image` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `random_preview_image` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order` bigint(20) NOT NULL,
  `author` bigint(20) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `gallery_type` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `gallery_source` varchar(256) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autogallery_image_number` int(4) NOT NULL,
  `update_flag` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `modified_date` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_bwg_gallery`
--

LOCK TABLES `wp_bwg_gallery` WRITE;
/*!40000 ALTER TABLE `wp_bwg_gallery` DISABLE KEYS */;
INSERT INTO `wp_bwg_gallery` (`id`, `name`, `slug`, `description`, `page_link`, `preview_image`, `random_preview_image`, `order`, `author`, `published`, `gallery_type`, `gallery_source`, `autogallery_image_number`, `update_flag`, `modified_date`) VALUES 
(1, 'Галерея', '%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b5%d1%8f', '', '', '/thumb/Mining.jpg', '', 0, 1, 1, '', '', 12, '', 1531946268);
/*!40000 ALTER TABLE `wp_bwg_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_bwg_image`
--

DROP TABLE IF EXISTS `wp_bwg_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_bwg_image` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `gallery_id` bigint(20) NOT NULL,
  `slug` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `image_url` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `thumb_url` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `alt` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `size` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `filetype` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `resolution` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `author` bigint(20) NOT NULL,
  `order` bigint(20) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `comment_count` bigint(20) NOT NULL,
  `avg_rating` float NOT NULL,
  `rate_count` bigint(20) NOT NULL,
  `hit_count` bigint(20) NOT NULL,
  `redirect_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pricelist_id` bigint(20) NOT NULL,
  `modified_date` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_bwg_image`
--

LOCK TABLES `wp_bwg_image` WRITE;
/*!40000 ALTER TABLE `wp_bwg_image` DISABLE KEYS */;
INSERT INTO `wp_bwg_image` (`id`, `gallery_id`, `slug`, `filename`, `image_url`, `thumb_url`, `description`, `alt`, `date`, `size`, `filetype`, `resolution`, `author`, `order`, `published`, `comment_count`, `avg_rating`, `rate_count`, `hit_count`, `redirect_url`, `pricelist_id`, `modified_date`) VALUES 
(1, 1, 'Mining', 'Mining', '/Mining.jpg', '/thumb/Mining.jpg', '', 'Mining', '18 July 2018, 20:38', '131 KB', 'jpg', '800 x 600 px', 1, 13, 1, 0, 0, 0, 0, '', 0, 0),
(3, 1, 'regnum picture 1484072057 big', 'regnum picture 1484072057 big', '/regnum_picture_1484072057_big.jpg', '/thumb/regnum_picture_1484072057_big.jpg', '', 'regnum picture 1484072057 big', '18 July 2018, 20:38', '119 KB', 'jpg', '359 x 420 px', 1, 11, 1, 0, 0, 0, 0, '', 0, 0),
(4, 1, 'maxresdefault (1)', 'maxresdefault (1)', '/maxresdefault_(1).jpg', '/thumb/maxresdefault_(1).jpg', '', 'maxresdefault (1)', '18 July 2018, 20:38', '97 KB', 'jpg', '1200 x 675 px', 1, 10, 1, 0, 0, 0, 0, '', 0, 0),
(5, 1, 'maxresdefault', 'maxresdefault', '/maxresdefault.jpg', '/thumb/maxresdefault.jpg', '', 'maxresdefault', '18 July 2018, 20:38', '51 KB', 'jpg', '1200 x 675 px', 1, 9, 1, 0, 0, 0, 0, '', 0, 0),
(6, 1, '670px-Vitalik-buterin-ethereum-796x437', '670px-Vitalik-buterin-ethereum-796x437', '/670px-Vitalik-buterin-ethereum-796x437.jpg', '/thumb/670px-Vitalik-buterin-ethereum-796x437.jpg', '', '670px-Vitalik-buterin-ethereum-796x437', '18 July 2018, 20:37', '113 KB', 'jpg', '670 x 368 px', 1, 8, 1, 0, 0, 0, 0, '', 0, 0),
(7, 1, 'pank-kultura-raznovidnosti-zhanra', 'pank-kultura-raznovidnosti-zhanra', '/pank-kultura-raznovidnosti-zhanra.jpg', '/thumb/pank-kultura-raznovidnosti-zhanra.jpg', '', 'pank-kultura-raznovidnosti-zhanra', '18 July 2018, 20:37', '42 KB', 'jpg', '500 x 369 px', 1, 7, 1, 0, 0, 0, 0, '', 0, 0),
(8, 1, '445961.700xp', '445961.700xp', '/445961.700xp.jpg', '/thumb/445961.700xp.jpg', '', '445961.700xp', '18 July 2018, 20:37', '56 KB', 'jpg', '700 x 467 px', 1, 6, 1, 0, 0, 0, 0, '', 0, 0),
(9, 1, '01689b ce4c29b304414092b4e69f6cc50a2300 mv2', '01689b ce4c29b304414092b4e69f6cc50a2300 mv2', '/01689b_ce4c29b304414092b4e69f6cc50a2300_mv2.png', '/thumb/01689b_ce4c29b304414092b4e69f6cc50a2300_mv2.png', '', '01689b ce4c29b304414092b4e69f6cc50a2300 mv2', '18 July 2018, 20:37', '263 KB', 'png', '556 x 391 px', 1, 5, 1, 0, 0, 0, 0, '', 0, 0),
(10, 1, '5b4cc0cf183561a4388b45db', '5b4cc0cf183561a4388b45db', '/5b4cc0cf183561a4388b45db.jpg', '/thumb/5b4cc0cf183561a4388b45db.jpg', '', '5b4cc0cf183561a4388b45db', '18 July 2018, 20:37', '99 KB', 'jpg', '827 x 465 px', 1, 4, 1, 0, 0, 0, 0, '', 0, 0),
(11, 1, '47422be535dbaeea93b63db6a8eb8683', '47422be535dbaeea93b63db6a8eb8683', '/47422be535dbaeea93b63db6a8eb8683.jpg', '/thumb/47422be535dbaeea93b63db6a8eb8683.jpg', '', '47422be535dbaeea93b63db6a8eb8683', '18 July 2018, 20:37', '27 KB', 'jpg', '720 x 400 px', 1, 3, 1, 0, 0, 0, 0, '', 0, 0),
(12, 1, '5b4bbba518356135598b460c', '5b4bbba518356135598b460c', '/5b4bbba518356135598b460c.jpeg', '/thumb/5b4bbba518356135598b460c.jpeg', '', '5b4bbba518356135598b460c', '18 July 2018, 20:37', '151 KB', 'jpeg', '827 x 465 px', 1, 2, 1, 0, 0, 0, 0, '', 0, 0),
(13, 1, '5b4bbba518356135598b460c (1)', '5b4bbba518356135598b460c (1)', '/5b4bbba518356135598b460c_(1).jpeg', '/thumb/5b4bbba518356135598b460c_(1).jpeg', '', '5b4bbba518356135598b460c (1)', '18 July 2018, 20:37', '151 KB', 'jpeg', '827 x 465 px', 1, 1, 1, 0, 0, 0, 0, '', 0, 0);
/*!40000 ALTER TABLE `wp_bwg_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_bwg_image_comment`
--

DROP TABLE IF EXISTS `wp_bwg_image_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_bwg_image_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `url` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `mail` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_bwg_image_comment`
--

LOCK TABLES `wp_bwg_image_comment` WRITE;
/*!40000 ALTER TABLE `wp_bwg_image_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_bwg_image_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_bwg_image_rate`
--

DROP TABLE IF EXISTS `wp_bwg_image_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_bwg_image_rate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image_id` bigint(20) NOT NULL,
  `rate` float NOT NULL,
  `ip` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_bwg_image_rate`
--

LOCK TABLES `wp_bwg_image_rate` WRITE;
/*!40000 ALTER TABLE `wp_bwg_image_rate` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_bwg_image_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_bwg_image_tag`
--

DROP TABLE IF EXISTS `wp_bwg_image_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_bwg_image_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(20) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  `gallery_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_bwg_image_tag`
--

LOCK TABLES `wp_bwg_image_tag` WRITE;
/*!40000 ALTER TABLE `wp_bwg_image_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_bwg_image_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_bwg_shortcode`
--

DROP TABLE IF EXISTS `wp_bwg_shortcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_bwg_shortcode` (
  `id` bigint(20) NOT NULL,
  `tagtext` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_bwg_shortcode`
--

LOCK TABLES `wp_bwg_shortcode` WRITE;
/*!40000 ALTER TABLE `wp_bwg_shortcode` DISABLE KEYS */;
INSERT INTO `wp_bwg_shortcode` (`id`, `tagtext`) VALUES 
(1, ' use_option_defaults=\"1\" type=\"gallery\" theme_id=\"1\" gallery_id=\"1\" tag=\"0\" gallery_type=\"thumbnails\"');
/*!40000 ALTER TABLE `wp_bwg_shortcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_bwg_theme`
--

DROP TABLE IF EXISTS `wp_bwg_theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_bwg_theme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `options` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `default_theme` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_bwg_theme`
--

LOCK TABLES `wp_bwg_theme` WRITE;
/*!40000 ALTER TABLE `wp_bwg_theme` DISABLE KEYS */;
INSERT INTO `wp_bwg_theme` (`id`, `name`, `options`, `default_theme`) VALUES 
(1, 'Light', '{\"thumb_margin\":\"4\",\"container_margin\":\"1\",\"thumb_padding\":\"0\",\"thumb_border_radius\":\"0\",\"thumb_border_width\":0,\"thumb_border_style\":\"none\",\"thumb_border_color\":\"CCCCCC\",\"thumb_bg_color\":\"000000\",\"thumbs_bg_color\":\"FFFFFF\",\"thumb_bg_transparent\":0,\"thumb_box_shadow\":\"\",\"thumb_transparent\":100,\"thumb_align\":\"center\",\"thumb_hover_effect\":\"zoom\",\"thumb_hover_effect_value\":\"1.08\",\"thumb_transition\":1,\"thumb_title_margin\":\"2px\",\"thumb_title_font_style\":\"Ubuntu\",\"thumb_title_pos\":\"bottom\",\"thumb_title_font_color\":\"323A45\",\"thumb_title_font_color_hover\":\"FFFFFF\",\"thumb_title_shadow\":\"\",\"thumb_title_font_size\":16,\"thumb_title_font_weight\":\"bold\",\"thumb_gal_title_font_color\":\"000000\",\"thumb_gal_title_font_style\":\"Ubuntu\",\"thumb_gal_title_font_size\":18,\"thumb_gal_title_font_weight\":\"bold\",\"thumb_gal_title_margin\":\"2px\",\"thumb_gal_title_shadow\":\"\",\"thumb_gal_title_align\":\"center\",\"page_nav_position\":\"bottom\",\"page_nav_align\":\"center\",\"page_nav_number\":0,\"page_nav_font_size\":12,\"page_nav_font_style\":\"segoe ui\",\"page_nav_font_color\":\"666666\",\"page_nav_font_weight\":\"bold\",\"page_nav_border_width\":1,\"page_nav_border_style\":\"solid\",\"page_nav_border_color\":\"E3E3E3\",\"page_nav_border_radius\":\"0\",\"page_nav_margin\":\"0\",\"page_nav_padding\":\"3px 6px\",\"page_nav_button_bg_color\":\"FFFFFF\",\"page_nav_button_bg_transparent\":100,\"page_nav_box_shadow\":\"0\",\"page_nav_button_transition\":1,\"page_nav_button_text\":0,\"lightbox_ctrl_btn_pos\":\"bottom\",\"lightbox_ctrl_btn_align\":\"center\",\"lightbox_ctrl_btn_height\":20,\"lightbox_ctrl_btn_margin_top\":10,\"lightbox_ctrl_btn_margin_left\":7,\"lightbox_ctrl_btn_transparent\":100,\"lightbox_ctrl_btn_color\":\"808080\",\"lightbox_toggle_btn_height\":20,\"lightbox_toggle_btn_width\":100,\"lightbox_ctrl_cont_bg_color\":\"FFFFFF\",\"lightbox_ctrl_cont_border_radius\":4,\"lightbox_ctrl_cont_transparent\":85,\"lightbox_close_btn_bg_color\":\"FFFFFF\",\"lightbox_close_btn_border_radius\":\"16px\",\"lightbox_close_btn_border_width\":2,\"lightbox_close_btn_border_style\":\"none\",\"lightbox_close_btn_border_color\":\"FFFFFF\",\"lightbox_close_btn_box_shadow\":\"0\",\"lightbox_close_btn_color\":\"808080\",\"lightbox_close_btn_size\":20,\"lightbox_close_btn_width\":30,\"lightbox_close_btn_height\":30,\"lightbox_close_btn_top\":\"-20\",\"lightbox_close_btn_right\":\"-15\",\"lightbox_close_btn_full_color\":\"000000\",\"lightbox_close_btn_transparent\":60,\"lightbox_rl_btn_bg_color\":\"FFFFFF\",\"lightbox_rl_btn_transparent\":\"60\",\"lightbox_rl_btn_border_radius\":\"20px\",\"lightbox_rl_btn_border_width\":0,\"lightbox_rl_btn_border_style\":\"none\",\"lightbox_rl_btn_border_color\":\"FFFFFF\",\"lightbox_rl_btn_box_shadow\":\"\",\"lightbox_rl_btn_color\":\"ADADAD\",\"lightbox_rl_btn_height\":35,\"lightbox_rl_btn_width\":35,\"lightbox_rl_btn_size\":25,\"lightbox_close_rl_btn_hover_color\":\"808080\",\"lightbox_comment_pos\":\"left\",\"lightbox_comment_width\":350,\"lightbox_comment_bg_color\":\"FFFFFF\",\"lightbox_comment_font_color\":\"7A7A7A\",\"lightbox_comment_font_style\":\"Ubuntu\",\"lightbox_comment_font_size\":12,\"lightbox_comment_button_bg_color\":\"2F2F2F\",\"lightbox_comment_button_border_color\":\"666666\",\"lightbox_comment_button_border_width\":1,\"lightbox_comment_button_border_style\":\"none\",\"lightbox_comment_button_border_radius\":\"7px\",\"lightbox_comment_button_padding\":\"10px 10px\",\"lightbox_comment_input_bg_color\":\"F7F8F9\",\"lightbox_comment_input_border_color\":\"EBEBEB\",\"lightbox_comment_input_border_width\":2,\"lightbox_comment_input_border_style\":\"none\",\"lightbox_comment_input_border_radius\":\"7px\",\"lightbox_comment_input_padding\":\"5px\",\"lightbox_comment_separator_width\":20,\"lightbox_comment_separator_style\":\"none\",\"lightbox_comment_separator_color\":\"383838\",\"lightbox_comment_author_font_size\":14,\"lightbox_comment_date_font_size\":10,\"lightbox_comment_body_font_size\":12,\"lightbox_comment_share_button_color\":\"808080\",\"lightbox_filmstrip_rl_bg_color\":\"EBEBEB\",\"lightbox_filmstrip_rl_btn_size\":20,\"lightbox_filmstrip_rl_btn_color\":\"808080\",\"lightbox_filmstrip_thumb_margin\":\"0 1px\",\"lightbox_filmstrip_thumb_border_width\":1,\"lightbox_filmstrip_thumb_border_style\":\"none\",\"lightbox_filmstrip_thumb_border_color\":\"000000\",\"lightbox_filmstrip_thumb_border_radius\":\"0\",\"lightbox_filmstrip_thumb_deactive_transparent\":80,\"lightbox_filmstrip_pos\":\"bottom\",\"lightbox_filmstrip_thumb_active_border_width\":0,\"lightbox_filmstrip_thumb_active_border_color\":\"FFFFFF\",\"lightbox_overlay_bg_transparent\":60,\"lightbox_bg_color\":\"FFFFFF\",\"lightbox_overlay_bg_color\":\"EEEEEE\",\"lightbox_rl_btn_style\":\"fa-angle\",\"lightbox_bg_transparent\":100,\"blog_style_margin\":\"2px\",\"blog_style_padding\":\"0\",\"blog_style_border_radius\":\"0\",\"blog_style_border_width\":1,\"blog_style_border_style\":\"solid\",\"blog_style_border_color\":\"F5F5F5\",\"blog_style_bg_color\":\"FFFFFF\",\"blog_style_transparent\":80,\"blog_style_box_shadow\":\"\",\"blog_style_align\":\"center\",\"blog_style_share_buttons_margin\":\"5px auto 10px auto\",\"blog_style_share_buttons_border_radius\":\"0\",\"blog_style_share_buttons_border_width\":0,\"blog_style_share_buttons_border_style\":\"none\",\"blog_style_share_buttons_border_color\":\"000000\",\"blog_style_share_buttons_bg_color\":\"FFFFFF\",\"blog_style_share_buttons_align\":\"right\",\"blog_style_img_font_size\":16,\"blog_style_img_font_family\":\"segoe ui\",\"blog_style_img_font_color\":\"000000\",\"blog_style_share_buttons_font_size\":20,\"blog_style_share_buttons_color\":\"B3AFAF\",\"blog_style_share_buttons_bg_transparent\":0,\"blog_style_gal_title_font_color\":\"CCCCCC\",\"blog_style_gal_title_font_style\":\"segoe ui\",\"blog_style_gal_title_font_size\":16,\"blog_style_gal_title_font_weight\":\"bold\",\"blog_style_gal_title_margin\":\"2px\",\"blog_style_gal_title_shadow\":\"0px 0px 0px #888888\",\"blog_style_gal_title_align\":\"center\",\"image_browser_margin\":\"2px auto\",\"image_browser_padding\":\"4px\",\"image_browser_border_radius\":\"0\",\"image_browser_border_width\":1,\"image_browser_border_style\":\"none\",\"image_browser_border_color\":\"F5F5F5\",\"image_browser_bg_color\":\"EBEBEB\",\"image_browser_box_shadow\":\"\",\"image_browser_transparent\":80,\"image_browser_align\":\"center\",\"image_browser_image_description_margin\":\"0px 5px 0px 5px\",\"image_browser_image_description_padding\":\"8px 8px 8px 8px\",\"image_browser_image_description_border_radius\":\"0\",\"image_browser_image_description_border_width\":1,\"image_browser_image_description_border_style\":\"none\",\"image_browser_image_description_border_color\":\"FFFFFF\",\"image_browser_image_description_bg_color\":\"EBEBEB\",\"image_browser_image_description_align\":\"center\",\"image_browser_img_font_size\":15,\"image_browser_img_font_family\":\"Ubuntu\",\"image_browser_img_font_color\":\"000000\",\"image_browser_full_padding\":\"4px\",\"image_browser_full_border_radius\":\"0\",\"image_browser_full_border_width\":2,\"image_browser_full_border_style\":\"none\",\"image_browser_full_border_color\":\"F7F7F7\",\"image_browser_full_bg_color\":\"F5F5F5\",\"image_browser_full_transparent\":90,\"image_browser_image_title_align\":\"top\",\"image_browser_gal_title_font_color\":\"CCCCCC\",\"image_browser_gal_title_font_style\":\"segoe ui\",\"image_browser_gal_title_font_size\":16,\"image_browser_gal_title_font_weight\":\"bold\",\"image_browser_gal_title_margin\":\"2px\",\"image_browser_gal_title_shadow\":\"0px 0px 0px #888888\",\"image_browser_gal_title_align\":\"center\",\"album_compact_title_margin\":\"2px\",\"album_compact_thumb_margin\":2,\"album_compact_back_padding\":\"0\",\"album_compact_thumb_padding\":0,\"album_compact_thumb_border_radius\":\"0\",\"album_compact_thumb_border_width\":0,\"album_compact_title_font_style\":\"segoe ui\",\"album_compact_back_font_color\":\"000000\",\"album_compact_title_font_color\":\"FFFFFF\",\"album_compact_title_shadow\":\"0px 0px 0px #888888\",\"album_compact_thumb_bg_transparent\":0,\"album_compact_thumb_box_shadow\":\"0px 0px 0px #888888\",\"album_compact_thumb_transition\":1,\"album_compact_thumb_border_style\":\"none\",\"album_compact_thumb_border_color\":\"CCCCCC\",\"album_compact_thumb_bg_color\":\"FFFFFF\",\"album_compact_back_font_weight\":\"bold\",\"album_compact_back_font_size\":16,\"album_compact_back_font_style\":\"segoe ui\",\"album_compact_thumb_title_pos\":\"bottom\",\"album_compact_thumbs_bg_color\":\"FFFFFF\",\"album_compact_title_font_size\":16,\"album_compact_title_font_weight\":\"bold\",\"album_compact_thumb_align\":\"center\",\"album_compact_thumb_hover_effect\":\"scale\",\"album_compact_thumb_transparent\":100,\"album_compact_thumb_hover_effect_value\":\"1.08\",\"album_compact_gal_title_font_color\":\"CCCCCC\",\"album_compact_gal_title_font_style\":\"segoe ui\",\"album_compact_gal_title_font_size\":16,\"album_compact_gal_title_font_weight\":\"bold\",\"album_compact_gal_title_margin\":\"2px\",\"album_compact_gal_title_shadow\":\"0px 0px 0px #888888\",\"album_compact_gal_title_align\":\"center\",\"album_extended_thumb_margin\":2,\"album_extended_thumb_padding\":0,\"album_extended_thumb_border_radius\":\"0\",\"album_extended_thumb_border_width\":0,\"album_extended_thumb_border_style\":\"none\",\"album_extended_thumb_border_color\":\"CCCCCC\",\"album_extended_thumb_bg_color\":\"FFFFFF\",\"album_extended_thumbs_bg_color\":\"FFFFFF\",\"album_extended_thumb_bg_transparent\":0,\"album_extended_thumb_box_shadow\":\"\",\"album_extended_thumb_transparent\":100,\"album_extended_thumb_align\":\"left\",\"album_extended_thumb_hover_effect\":\"scale\",\"album_extended_thumb_hover_effect_value\":\"1.08\",\"album_extended_thumb_transition\":1,\"album_extended_back_font_color\":\"000000\",\"album_extended_back_font_style\":\"segoe ui\",\"album_extended_back_font_size\":20,\"album_extended_back_font_weight\":\"bold\",\"album_extended_back_padding\":\"0\",\"album_extended_div_bg_color\":\"FFFFFF\",\"album_extended_div_bg_transparent\":0,\"album_extended_div_border_radius\":\"0 0 0 0\",\"album_extended_div_margin\":\"0 0 5px 0\",\"album_extended_div_padding\":10,\"album_extended_div_separator_width\":1,\"album_extended_div_separator_style\":\"solid\",\"album_extended_div_separator_color\":\"E0E0E0\",\"album_extended_thumb_div_bg_color\":\"FFFFFF\",\"album_extended_thumb_div_border_radius\":\"0\",\"album_extended_thumb_div_border_width\":1,\"album_extended_thumb_div_border_style\":\"solid\",\"album_extended_thumb_div_border_color\":\"E8E8E8\",\"album_extended_thumb_div_padding\":\"5px\",\"album_extended_text_div_bg_color\":\"FFFFFF\",\"album_extended_text_div_border_radius\":\"0\",\"album_extended_text_div_border_width\":1,\"album_extended_text_div_border_style\":\"solid\",\"album_extended_text_div_border_color\":\"E8E8E8\",\"album_extended_text_div_padding\":\"5px\",\"album_extended_title_span_border_width\":1,\"album_extended_title_span_border_style\":\"none\",\"album_extended_title_span_border_color\":\"CCCCCC\",\"album_extended_title_font_color\":\"000000\",\"album_extended_title_font_style\":\"segoe ui\",\"album_extended_title_font_size\":16,\"album_extended_title_font_weight\":\"bold\",\"album_extended_title_margin_bottom\":2,\"album_extended_title_padding\":\"2px\",\"album_extended_desc_span_border_width\":1,\"album_extended_desc_span_border_style\":\"none\",\"album_extended_desc_span_border_color\":\"CCCCCC\",\"album_extended_desc_font_color\":\"000000\",\"album_extended_desc_font_style\":\"segoe ui\",\"album_extended_desc_font_size\":14,\"album_extended_desc_font_weight\":\"normal\",\"album_extended_desc_padding\":\"2px\",\"album_extended_desc_more_color\":\"F2D22E\",\"album_extended_desc_more_size\":12,\"album_extended_gal_title_font_color\":\"CCCCCC\",\"album_extended_gal_title_font_style\":\"segoe ui\",\"album_extended_gal_title_font_size\":16,\"album_extended_gal_title_font_weight\":\"bold\",\"album_extended_gal_title_margin\":\"2px\",\"album_extended_gal_title_shadow\":\"0px 0px 0px #888888\",\"album_extended_gal_title_align\":\"center\",\"slideshow_cont_bg_color\":\"F2F2F2\",\"slideshow_close_btn_transparent\":100,\"slideshow_rl_btn_bg_color\":\"FFFFFF\",\"slideshow_rl_btn_border_radius\":\"20px\",\"slideshow_rl_btn_border_width\":0,\"slideshow_rl_btn_border_style\":\"none\",\"slideshow_rl_btn_border_color\":\"FFFFFF\",\"slideshow_rl_btn_box_shadow\":\"\",\"slideshow_rl_btn_color\":\"D6D6D6\",\"slideshow_rl_btn_height\":37,\"slideshow_rl_btn_size\":12,\"slideshow_rl_btn_width\":37,\"slideshow_close_rl_btn_hover_color\":\"BABABA\",\"slideshow_filmstrip_pos\":\"bottom\",\"slideshow_filmstrip_thumb_border_width\":0,\"slideshow_filmstrip_thumb_border_style\":\"none\",\"slideshow_filmstrip_thumb_border_color\":\"000000\",\"slideshow_filmstrip_thumb_border_radius\":\"0\",\"slideshow_filmstrip_thumb_margin\":\"0px 2px 0 0 \",\"slideshow_filmstrip_thumb_active_border_width\":0,\"slideshow_filmstrip_thumb_active_border_color\":\"FFFFFF\",\"slideshow_filmstrip_thumb_deactive_transparent\":100,\"slideshow_filmstrip_rl_bg_color\":\"F2F2F2\",\"slideshow_filmstrip_rl_btn_color\":\"BABABA\",\"slideshow_filmstrip_rl_btn_size\":20,\"slideshow_title_font_size\":16,\"slideshow_title_font\":\"segoe ui\",\"slideshow_title_color\":\"FFFFFF\",\"slideshow_title_opacity\":70,\"slideshow_title_border_radius\":\"5px\",\"slideshow_title_background_color\":\"000000\",\"slideshow_title_padding\":\"0 0 0 0\",\"slideshow_description_font_size\":14,\"slideshow_description_font\":\"segoe ui\",\"slideshow_description_color\":\"FFFFFF\",\"slideshow_description_opacity\":70,\"slideshow_description_border_radius\":\"0\",\"slideshow_description_background_color\":\"000000\",\"slideshow_description_padding\":\"5px 10px 5px 10px\",\"slideshow_dots_width\":12,\"slideshow_dots_height\":12,\"slideshow_dots_border_radius\":\"5px\",\"slideshow_dots_background_color\":\"F2D22E\",\"slideshow_dots_margin\":3,\"slideshow_dots_active_background_color\":\"FFFFFF\",\"slideshow_dots_active_border_width\":1,\"slideshow_dots_active_border_color\":\"000000\",\"slideshow_play_pause_btn_size\":35,\"slideshow_rl_btn_style\":\"fa-chevron\",\"masonry_thumb_padding\":\"2\",\"masonry_thumb_border_radius\":\"0\",\"masonry_thumb_border_width\":\"0\",\"masonry_thumb_border_style\":\"none\",\"masonry_thumb_border_color\":\"CCCCCC\",\"masonry_thumbs_bg_color\":\"FFFFFF\",\"masonry_thumb_bg_transparent\":\"0\",\"masonry_thumb_transparent\":\"100\",\"masonry_thumb_align\":\"center\",\"masonry_thumb_hover_effect\":\"scale\",\"masonry_thumb_hover_effect_value\":\"1.08\",\"masonry_thumb_transition\":\"1\",\"masonry_thumb_gal_title_font_color\":\"CCCCCC\",\"masonry_thumb_gal_title_font_style\":\"segoe ui\",\"masonry_thumb_gal_title_font_size\":16,\"masonry_thumb_gal_title_font_weight\":\"bold\",\"masonry_thumb_gal_title_margin\":\"2px\",\"masonry_thumb_gal_title_shadow\":\"0px 0px 0px #888888\",\"masonry_thumb_gal_title_align\":\"center\",\"mosaic_thumb_padding\":\"2\",\"mosaic_thumb_border_radius\":\"0\",\"mosaic_thumb_border_width\":\"0\",\"mosaic_thumb_border_style\":\"none\",\"mosaic_thumb_border_color\":\"CCCCCC\",\"mosaic_thumbs_bg_color\":\"FFFFFF\",\"mosaic_thumb_bg_transparent\":\"0\",\"mosaic_thumb_transparent\":\"100\",\"mosaic_thumb_align\":\"center\",\"mosaic_thumb_hover_effect\":\"scale\",\"mosaic_thumb_hover_effect_value\":\"1.08\",\"mosaic_thumb_title_margin\":\"2px\",\"mosaic_thumb_title_font_style\":\"segoe ui\",\"mosaic_thumb_title_font_color\":\"CCCCCC\",\"mosaic_thumb_title_shadow\":\"0px 0px 0px #888888\",\"mosaic_thumb_title_font_size\":16,\"mosaic_thumb_title_font_weight\":\"bold\",\"mosaic_thumb_gal_title_font_color\":\"CCCCCC\",\"mosaic_thumb_gal_title_font_style\":\"segoe ui\",\"mosaic_thumb_gal_title_font_size\":16,\"mosaic_thumb_gal_title_font_weight\":\"bold\",\"mosaic_thumb_gal_title_margin\":\"2px\",\"mosaic_thumb_gal_title_shadow\":\"0px 0px 0px #888888\",\"mosaic_thumb_gal_title_align\":\"center\",\"lightbox_info_pos\":\"bottom\",\"lightbox_info_align\":\"left\",\"lightbox_info_bg_color\":\"FFFFFF\",\"lightbox_info_bg_transparent\":\"70\",\"lightbox_info_border_width\":\"1\",\"lightbox_info_border_style\":\"none\",\"lightbox_info_border_color\":\"000000\",\"lightbox_info_border_radius\":\"0px\",\"lightbox_info_padding\":\"10px 7px 44px 10px\",\"lightbox_info_margin\":\"10px 10px -5px 10px\",\"lightbox_title_color\":\"808080\",\"lightbox_title_font_style\":\"Ubuntu\",\"lightbox_title_font_weight\":\"bold\",\"lightbox_title_font_size\":\"16\",\"lightbox_description_color\":\"B0B0B0\",\"lightbox_description_font_style\":\"Ubuntu\",\"lightbox_description_font_weight\":\"bold\",\"lightbox_description_font_size\":\"13\",\"lightbox_rate_pos\":\"top\",\"lightbox_rate_align\":\"left\",\"lightbox_rate_icon\":\"star\",\"lightbox_rate_color\":\"F9D062\",\"lightbox_rate_size\":\"20\",\"lightbox_rate_stars_count\":\"5\",\"lightbox_rate_padding\":\"15px\",\"lightbox_rate_hover_color\":\"F7B50E\",\"lightbox_hit_pos\":\"bottom\",\"lightbox_hit_align\":\"left\",\"lightbox_hit_bg_color\":\"000000\",\"lightbox_hit_bg_transparent\":\"70\",\"lightbox_hit_border_width\":\"1\",\"lightbox_hit_border_style\":\"none\",\"lightbox_hit_border_color\":\"000000\",\"lightbox_hit_border_radius\":\"5px\",\"lightbox_hit_padding\":\"5px\",\"lightbox_hit_margin\":\"0 5px\",\"lightbox_hit_color\":\"FFFFFF\",\"lightbox_hit_font_style\":\"segoe ui\",\"lightbox_hit_font_weight\":\"normal\",\"lightbox_hit_font_size\":\"14\",\"masonry_description_font_size\":12,\"masonry_description_color\":\"CCCCCC\",\"masonry_description_font_style\":\"segoe ui\",\"album_masonry_back_font_color\":\"000000\",\"album_masonry_back_font_style\":\"segoe ui\",\"album_masonry_back_font_size\":16,\"album_masonry_back_font_weight\":\"bold\",\"album_masonry_back_padding\":\"0\",\"album_masonry_title_font_color\":\"CCCCCC\",\"album_masonry_title_font_style\":\"segoe ui\",\"album_masonry_thumb_title_pos\":\"bottom\",\"album_masonry_title_font_size\":16,\"album_masonry_title_font_weight\":\"bold\",\"album_masonry_title_margin\":\"\",\"album_masonry_title_shadow\":\"0px 0px 0px #888888\",\"album_masonry_thumb_margin\":0,\"album_masonry_thumb_padding\":0,\"album_masonry_thumb_border_radius\":\"0\",\"album_masonry_thumb_border_width\":0,\"album_masonry_thumb_border_style\":\"none\",\"album_masonry_thumb_border_color\":\"CCCCCC\",\"album_masonry_thumb_bg_color\":\"FFFFFF\",\"album_masonry_thumbs_bg_color\":\"FFFFFF\",\"album_masonry_thumb_bg_transparent\":0,\"album_masonry_thumb_box_shadow\":\"\",\"album_masonry_thumb_transparent\":100,\"album_masonry_thumb_align\":\"center\",\"album_masonry_thumb_hover_effect\":\"scale\",\"album_masonry_thumb_hover_effect_value\":\"1.08\",\"album_masonry_thumb_transition\":1,\"album_masonry_gal_title_font_color\":\"CCCCCC\",\"album_masonry_gal_title_font_style\":\"segoe ui\",\"album_masonry_gal_title_font_size\":16,\"album_masonry_gal_title_font_weight\":\"bold\",\"album_masonry_gal_title_margin\":\"2px\",\"album_masonry_gal_title_shadow\":\"0px 0px 0px #888888\",\"album_masonry_gal_title_align\":\"center\",\"carousel_cont_bg_color\":\"000000\",\"carousel_cont_btn_transparent\":0,\"carousel_close_btn_transparent\":50,\"carousel_rl_btn_bg_color\":\"FFFFFF\",\"carousel_rl_btn_border_radius\":\"20px\",\"carousel_rl_btn_border_width\":0,\"carousel_rl_btn_border_style\":\"none\",\"carousel_rl_btn_border_color\":\"FFFFFF\",\"carousel_rl_btn_color\":\"303030\",\"carousel_rl_btn_height\":35,\"carousel_rl_btn_size\":15,\"carousel_play_pause_btn_size\":25,\"carousel_rl_btn_width\":35,\"carousel_close_rl_btn_hover_color\":\"191919\",\"carousel_rl_btn_style\":\"fa-chevron\",\"carousel_mergin_bottom\":\"0.5\",\"carousel_font_family\":\"arial\",\"carousel_feature_border_width\":2,\"carousel_feature_border_style\":\"none\",\"carousel_feature_border_color\":\"5D204F\",\"carousel_caption_background_color\":\"000000\",\"carousel_caption_bottom\":0,\"carousel_caption_p_mergin\":0,\"carousel_caption_p_pedding\":5,\"carousel_caption_p_font_weight\":\"bold\",\"carousel_caption_p_font_size\":14,\"carousel_caption_p_color\":\"FFFFFF\",\"carousel_title_opacity\":100,\"carousel_title_border_radius\":\"5px\",\"mosaic_thumb_transition\":\"1\"}', 1),
(2, 'Dark', '{\"thumb_margin\":\"4\",\"container_margin\":\"1\",\"thumb_padding\":\"0\",\"thumb_border_radius\":\"0\",\"thumb_border_width\":5,\"thumb_border_style\":\"none\",\"thumb_border_color\":\"FFFFFF\",\"thumb_bg_color\":\"000000\",\"thumbs_bg_color\":\"FFFFFF\",\"thumb_bg_transparent\":0,\"thumb_box_shadow\":\"\",\"thumb_transparent\":100,\"thumb_align\":\"center\",\"thumb_hover_effect\":\"zoom\",\"thumb_hover_effect_value\":\"1.08\",\"thumb_transition\":1,\"thumb_title_font_color\":\"323A45\",\"thumb_title_font_color_hover\":\"FFFFFF\",\"thumb_title_font_style\":\"Ubuntu\",\"thumb_title_pos\":\"bottom\",\"thumb_title_font_size\":16,\"thumb_title_font_weight\":\"bold\",\"thumb_title_margin\":\"5px\",\"thumb_title_shadow\":\"\",\"thumb_gal_title_font_color\":\"000000\",\"thumb_gal_title_font_style\":\"Ubuntu\",\"thumb_gal_title_font_size\":18,\"thumb_gal_title_font_weight\":\"bold\",\"thumb_gal_title_margin\":\"2px\",\"thumb_gal_title_shadow\":\"\",\"thumb_gal_title_align\":\"center\",\"page_nav_position\":\"bottom\",\"page_nav_align\":\"center\",\"page_nav_number\":0,\"page_nav_font_size\":12,\"page_nav_font_style\":\"segoe ui\",\"page_nav_font_color\":\"666666\",\"page_nav_font_weight\":\"bold\",\"page_nav_border_width\":1,\"page_nav_border_style\":\"none\",\"page_nav_border_color\":\"E3E3E3\",\"page_nav_border_radius\":\"0\",\"page_nav_margin\":\"0\",\"page_nav_padding\":\"3px 6px\",\"page_nav_button_bg_color\":\"FCFCFC\",\"page_nav_button_bg_transparent\":100,\"page_nav_box_shadow\":\"0\",\"page_nav_button_transition\":1,\"page_nav_button_text\":0,\"lightbox_overlay_bg_color\":\"000000\",\"lightbox_overlay_bg_transparent\":70,\"lightbox_bg_color\":\"000000\",\"lightbox_ctrl_btn_pos\":\"bottom\",\"lightbox_ctrl_btn_align\":\"center\",\"lightbox_ctrl_btn_height\":20,\"lightbox_ctrl_btn_margin_top\":10,\"lightbox_ctrl_btn_margin_left\":7,\"lightbox_ctrl_btn_transparent\":80,\"lightbox_ctrl_btn_color\":\"FFFFFF\",\"lightbox_toggle_btn_height\":14,\"lightbox_toggle_btn_width\":100,\"lightbox_ctrl_cont_bg_color\":\"000000\",\"lightbox_ctrl_cont_transparent\":80,\"lightbox_ctrl_cont_border_radius\":4,\"lightbox_close_btn_transparent\":95,\"lightbox_close_btn_bg_color\":\"000000\",\"lightbox_close_btn_border_width\":0,\"lightbox_close_btn_border_radius\":\"16px\",\"lightbox_close_btn_border_style\":\"none\",\"lightbox_close_btn_border_color\":\"FFFFFF\",\"lightbox_close_btn_box_shadow\":\"\",\"lightbox_close_btn_color\":\"FFFFFF\",\"lightbox_close_btn_size\":10,\"lightbox_close_btn_width\":20,\"lightbox_close_btn_height\":20,\"lightbox_close_btn_top\":\"-10\",\"lightbox_close_btn_right\":\"-10\",\"lightbox_close_btn_full_color\":\"FFFFFF\",\"lightbox_rl_btn_bg_color\":\"000000\",\"lightbox_rl_btn_border_radius\":\"20px\",\"lightbox_rl_btn_border_width\":2,\"lightbox_rl_btn_border_style\":\"none\",\"lightbox_rl_btn_border_color\":\"FFFFFF\",\"lightbox_rl_btn_box_shadow\":\"\",\"lightbox_rl_btn_color\":\"FFFFFF\",\"lightbox_rl_btn_height\":40,\"lightbox_rl_btn_width\":40,\"lightbox_rl_btn_size\":20,\"lightbox_close_rl_btn_hover_color\":\"FFFFFF\",\"lightbox_comment_pos\":\"left\",\"lightbox_comment_width\":400,\"lightbox_comment_bg_color\":\"000000\",\"lightbox_comment_font_color\":\"CCCCCC\",\"lightbox_comment_font_style\":\"segoe ui\",\"lightbox_comment_font_size\":12,\"lightbox_comment_button_bg_color\":\"333333\",\"lightbox_comment_button_border_color\":\"666666\",\"lightbox_comment_button_border_width\":1,\"lightbox_comment_button_border_style\":\"none\",\"lightbox_comment_button_border_radius\":\"3px\",\"lightbox_comment_button_padding\":\"3px 10px\",\"lightbox_comment_input_bg_color\":\"333333\",\"lightbox_comment_input_border_color\":\"666666\",\"lightbox_comment_input_border_width\":1,\"lightbox_comment_input_border_style\":\"none\",\"lightbox_comment_input_border_radius\":\"0\",\"lightbox_comment_input_padding\":\"3px\",\"lightbox_comment_separator_width\":1,\"lightbox_comment_separator_style\":\"solid\",\"lightbox_comment_separator_color\":\"2B2B2B\",\"lightbox_comment_author_font_size\":14,\"lightbox_comment_date_font_size\":10,\"lightbox_comment_body_font_size\":12,\"lightbox_comment_share_button_color\":\"FFFFFF\",\"lightbox_filmstrip_pos\":\"top\",\"lightbox_filmstrip_rl_bg_color\":\"2B2B2B\",\"lightbox_filmstrip_rl_btn_size\":20,\"lightbox_filmstrip_rl_btn_color\":\"FFFFFF\",\"lightbox_filmstrip_thumb_margin\":\"0 1px\",\"lightbox_filmstrip_thumb_border_width\":1,\"lightbox_filmstrip_thumb_border_style\":\"none\",\"lightbox_filmstrip_thumb_border_color\":\"000000\",\"lightbox_filmstrip_thumb_border_radius\":\"0\",\"lightbox_filmstrip_thumb_deactive_transparent\":80,\"lightbox_filmstrip_thumb_active_border_width\":0,\"lightbox_filmstrip_thumb_active_border_color\":\"FFFFFF\",\"lightbox_rl_btn_style\":\"fa-chevron\",\"lightbox_rl_btn_transparent\":80,\"lightbox_bg_transparent\":100,\"album_compact_back_font_color\":\"000000\",\"album_compact_back_font_style\":\"segoe ui\",\"album_compact_back_font_size\":14,\"album_compact_back_font_weight\":\"normal\",\"album_compact_back_padding\":\"0\",\"album_compact_title_font_color\":\"CCCCCC\",\"album_compact_title_font_style\":\"segoe ui\",\"album_compact_thumb_title_pos\":\"bottom\",\"album_compact_title_font_size\":16,\"album_compact_title_font_weight\":\"bold\",\"album_compact_title_margin\":\"5px\",\"album_compact_title_shadow\":\"\",\"album_compact_thumb_margin\":4,\"album_compact_thumb_padding\":4,\"album_compact_thumb_border_radius\":\"0\",\"album_compact_thumb_border_width\":1,\"album_compact_thumb_border_style\":\"none\",\"album_compact_thumb_border_color\":\"000000\",\"album_compact_thumb_bg_color\":\"E8E8E8\",\"album_compact_thumbs_bg_color\":\"FFFFFF\",\"album_compact_thumb_bg_transparent\":100,\"album_compact_thumb_box_shadow\":\"\",\"album_compact_thumb_transparent\":100,\"album_compact_thumb_align\":\"center\",\"album_compact_thumb_hover_effect\":\"rotate\",\"album_compact_thumb_hover_effect_value\":\"2deg\",\"album_compact_thumb_transition\":1,\"album_compact_gal_title_font_color\":\"CCCCCC\",\"album_compact_gal_title_font_style\":\"segoe ui\",\"album_compact_gal_title_font_size\":16,\"album_compact_gal_title_font_weight\":\"bold\",\"album_compact_gal_title_margin\":\"2px\",\"album_compact_gal_title_shadow\":\"0px 0px 0px #888888\",\"album_compact_gal_title_align\":\"center\",\"album_extended_thumb_margin\":2,\"album_extended_thumb_padding\":4,\"album_extended_thumb_border_radius\":\"0\",\"album_extended_thumb_border_width\":4,\"album_extended_thumb_border_style\":\"none\",\"album_extended_thumb_border_color\":\"E8E8E8\",\"album_extended_thumb_bg_color\":\"E8E8E8\",\"album_extended_thumbs_bg_color\":\"FFFFFF\",\"album_extended_thumb_bg_transparent\":100,\"album_extended_thumb_box_shadow\":\"\",\"album_extended_thumb_transparent\":100,\"album_extended_thumb_align\":\"left\",\"album_extended_thumb_hover_effect\":\"rotate\",\"album_extended_thumb_hover_effect_value\":\"2deg\",\"album_extended_thumb_transition\":0,\"album_extended_back_font_color\":\"000000\",\"album_extended_back_font_style\":\"segoe ui\",\"album_extended_back_font_size\":16,\"album_extended_back_font_weight\":\"bold\",\"album_extended_back_padding\":\"0\",\"album_extended_div_bg_color\":\"FFFFFF\",\"album_extended_div_bg_transparent\":0,\"album_extended_div_border_radius\":\"0\",\"album_extended_div_margin\":\"0 0 5px 0\",\"album_extended_div_padding\":10,\"album_extended_div_separator_width\":1,\"album_extended_div_separator_style\":\"none\",\"album_extended_div_separator_color\":\"CCCCCC\",\"album_extended_thumb_div_bg_color\":\"FFFFFF\",\"album_extended_thumb_div_border_radius\":\"0\",\"album_extended_thumb_div_border_width\":0,\"album_extended_thumb_div_border_style\":\"none\",\"album_extended_thumb_div_border_color\":\"CCCCCC\",\"album_extended_thumb_div_padding\":\"0\",\"album_extended_text_div_bg_color\":\"FFFFFF\",\"album_extended_text_div_border_radius\":\"0\",\"album_extended_text_div_border_width\":1,\"album_extended_text_div_border_style\":\"none\",\"album_extended_text_div_border_color\":\"CCCCCC\",\"album_extended_text_div_padding\":\"5px\",\"album_extended_title_span_border_width\":1,\"album_extended_title_span_border_style\":\"none\",\"album_extended_title_span_border_color\":\"CCCCCC\",\"album_extended_title_font_color\":\"000000\",\"album_extended_title_font_style\":\"segoe ui\",\"album_extended_title_font_size\":16,\"album_extended_title_font_weight\":\"bold\",\"album_extended_title_margin_bottom\":2,\"album_extended_title_padding\":\"2px\",\"album_extended_desc_span_border_width\":1,\"album_extended_desc_span_border_style\":\"none\",\"album_extended_desc_span_border_color\":\"CCCCCC\",\"album_extended_desc_font_color\":\"000000\",\"album_extended_desc_font_style\":\"segoe ui\",\"album_extended_desc_font_size\":14,\"album_extended_desc_font_weight\":\"normal\",\"album_extended_desc_padding\":\"2px\",\"album_extended_desc_more_color\":\"FFC933\",\"album_extended_desc_more_size\":12,\"album_extended_gal_title_font_color\":\"CCCCCC\",\"album_extended_gal_title_font_style\":\"segoe ui\",\"album_extended_gal_title_font_size\":16,\"album_extended_gal_title_font_weight\":\"bold\",\"album_extended_gal_title_margin\":\"2px\",\"album_extended_gal_title_shadow\":\"0px 0px 0px #888888\",\"album_extended_gal_title_align\":\"center\",\"masonry_thumb_padding\":4,\"masonry_thumb_border_radius\":\"2px\",\"masonry_thumb_border_width\":1,\"masonry_thumb_border_style\":\"none\",\"masonry_thumb_border_color\":\"CCCCCC\",\"masonry_thumbs_bg_color\":\"FFFFFF\",\"masonry_thumb_bg_transparent\":0,\"masonry_thumb_transparent\":80,\"masonry_thumb_align\":\"center\",\"masonry_thumb_hover_effect\":\"rotate\",\"masonry_thumb_hover_effect_value\":\"2deg\",\"masonry_thumb_transition\":0,\"masonry_thumb_gal_title_font_color\":\"CCCCCC\",\"masonry_thumb_gal_title_font_style\":\"segoe ui\",\"masonry_thumb_gal_title_font_size\":16,\"masonry_thumb_gal_title_font_weight\":\"bold\",\"masonry_thumb_gal_title_margin\":\"2px\",\"masonry_thumb_gal_title_shadow\":\"0px 0px 0px #888888\",\"masonry_thumb_gal_title_align\":\"center\",\"slideshow_cont_bg_color\":\"000000\",\"slideshow_close_btn_transparent\":100,\"slideshow_rl_btn_bg_color\":\"000000\",\"slideshow_rl_btn_border_radius\":\"20px\",\"slideshow_rl_btn_border_width\":0,\"slideshow_rl_btn_border_style\":\"none\",\"slideshow_rl_btn_border_color\":\"FFFFFF\",\"slideshow_rl_btn_box_shadow\":\"\",\"slideshow_rl_btn_color\":\"FFFFFF\",\"slideshow_rl_btn_height\":40,\"slideshow_rl_btn_size\":20,\"slideshow_rl_btn_width\":40,\"slideshow_close_rl_btn_hover_color\":\"DBDBDB\",\"slideshow_filmstrip_pos\":\"bottom\",\"slideshow_filmstrip_thumb_border_width\":1,\"slideshow_filmstrip_thumb_border_style\":\"none\",\"slideshow_filmstrip_thumb_border_color\":\"000000\",\"slideshow_filmstrip_thumb_border_radius\":\"0\",\"slideshow_filmstrip_thumb_margin\":\"0 1px\",\"slideshow_filmstrip_thumb_active_border_width\":0,\"slideshow_filmstrip_thumb_active_border_color\":\"FFFFFF\",\"slideshow_filmstrip_thumb_deactive_transparent\":80,\"slideshow_filmstrip_rl_bg_color\":\"303030\",\"slideshow_filmstrip_rl_btn_color\":\"FFFFFF\",\"slideshow_filmstrip_rl_btn_size\":20,\"slideshow_title_font_size\":16,\"slideshow_title_font\":\"segoe ui\",\"slideshow_title_color\":\"FFFFFF\",\"slideshow_title_opacity\":70,\"slideshow_title_border_radius\":\"5px\",\"slideshow_title_background_color\":\"000000\",\"slideshow_title_padding\":\"5px 10px 5px 10px\",\"slideshow_description_font_size\":14,\"slideshow_description_font\":\"segoe ui\",\"slideshow_description_color\":\"FFFFFF\",\"slideshow_description_opacity\":70,\"slideshow_description_border_radius\":\"0\",\"slideshow_description_background_color\":\"000000\",\"slideshow_description_padding\":\"5px 10px 5px 10px\",\"slideshow_dots_width\":10,\"slideshow_dots_height\":10,\"slideshow_dots_border_radius\":\"10px\",\"slideshow_dots_background_color\":\"292929\",\"slideshow_dots_margin\":1,\"slideshow_dots_active_background_color\":\"292929\",\"slideshow_dots_active_border_width\":2,\"slideshow_dots_active_border_color\":\"FFC933\",\"slideshow_play_pause_btn_size\":60,\"slideshow_rl_btn_style\":\"fa-chevron\",\"blog_style_margin\":\"2px\",\"blog_style_padding\":\"4px\",\"blog_style_border_radius\":\"0\",\"blog_style_border_width\":1,\"blog_style_border_style\":\"none\",\"blog_style_border_color\":\"CCCCCC\",\"blog_style_bg_color\":\"E8E8E8\",\"blog_style_transparent\":70,\"blog_style_box_shadow\":\"\",\"blog_style_align\":\"center\",\"blog_style_share_buttons_margin\":\"5px auto 10px auto\",\"blog_style_share_buttons_border_radius\":\"0\",\"blog_style_share_buttons_border_width\":0,\"blog_style_share_buttons_border_style\":\"none\",\"blog_style_share_buttons_border_color\":\"000000\",\"blog_style_share_buttons_bg_color\":\"FFFFFF\",\"blog_style_share_buttons_align\":\"right\",\"blog_style_img_font_size\":16,\"blog_style_img_font_family\":\"segoe ui\",\"blog_style_img_font_color\":\"000000\",\"blog_style_share_buttons_color\":\"A1A1A1\",\"blog_style_share_buttons_bg_transparent\":0,\"blog_style_share_buttons_font_size\":20,\"blog_style_image_title_align\":\"top\",\"blog_style_gal_title_font_color\":\"CCCCCC\",\"blog_style_gal_title_font_style\":\"segoe ui\",\"blog_style_gal_title_font_size\":16,\"blog_style_gal_title_font_weight\":\"bold\",\"blog_style_gal_title_margin\":\"2px\",\"blog_style_gal_title_shadow\":\"0px 0px 0px #888888\",\"blog_style_gal_title_align\":\"center\",\"image_browser_margin\":\"2px auto\",\"image_browser_padding\":\"4px\",\"image_browser_border_radius\":\"2px\",\"image_browser_border_width\":1,\"image_browser_border_style\":\"none\",\"image_browser_border_color\":\"E8E8E8\",\"image_browser_bg_color\":\"E8E8E8\",\"image_browser_box_shadow\":\"\",\"image_browser_transparent\":80,\"image_browser_align\":\"center\",\"image_browser_image_description_margin\":\"24px 0px 0px 0px\",\"image_browser_image_description_padding\":\"8px 8px 8px 8px\",\"image_browser_image_description_border_radius\":\"0\",\"image_browser_image_description_border_width\":1,\"image_browser_image_description_border_style\":\"none\",\"image_browser_image_description_border_color\":\"FFFFFF\",\"image_browser_image_description_bg_color\":\"E8E8E8\",\"image_browser_image_description_align\":\"center\",\"image_browser_img_font_size\":14,\"image_browser_img_font_family\":\"segoe ui\",\"image_browser_img_font_color\":\"000000\",\"image_browser_full_padding\":\"4px\",\"image_browser_full_border_radius\":\"0\",\"image_browser_full_border_width\":1,\"image_browser_full_border_style\":\"solid\",\"image_browser_full_border_color\":\"EDEDED\",\"image_browser_full_bg_color\":\"FFFFFF\",\"image_browser_full_transparent\":90,\"image_browser_image_title_align\":\"top\",\"image_browser_gal_title_font_color\":\"CCCCCC\",\"image_browser_gal_title_font_style\":\"segoe ui\",\"image_browser_gal_title_font_size\":16,\"image_browser_gal_title_font_weight\":\"bold\",\"image_browser_gal_title_margin\":\"2px\",\"image_browser_gal_title_shadow\":\"0px 0px 0px #888888\",\"image_browser_gal_title_align\":\"center\",\"lightbox_info_pos\":\"top\",\"lightbox_info_align\":\"right\",\"lightbox_info_bg_color\":\"000000\",\"lightbox_info_bg_transparent\":70,\"lightbox_info_border_width\":1,\"lightbox_info_border_style\":\"none\",\"lightbox_info_border_color\":\"000000\",\"lightbox_info_border_radius\":\"5px\",\"lightbox_info_padding\":\"5px\",\"lightbox_info_margin\":\"15px\",\"lightbox_title_color\":\"FFFFFF\",\"lightbox_title_font_style\":\"segoe ui\",\"lightbox_title_font_weight\":\"bold\",\"lightbox_title_font_size\":18,\"lightbox_description_color\":\"FFFFFF\",\"lightbox_description_font_style\":\"segoe ui\",\"lightbox_description_font_weight\":\"normal\",\"lightbox_description_font_size\":14,\"lightbox_rate_pos\":\"bottom\",\"lightbox_rate_align\":\"right\",\"lightbox_rate_icon\":\"star\",\"lightbox_rate_color\":\"F9D062\",\"lightbox_rate_size\":20,\"lightbox_rate_stars_count\":5,\"lightbox_rate_padding\":\"15px\",\"lightbox_rate_hover_color\":\"F7B50E\",\"lightbox_hit_pos\":\"bottom\",\"lightbox_hit_align\":\"left\",\"lightbox_hit_bg_color\":\"000000\",\"lightbox_hit_bg_transparent\":70,\"lightbox_hit_border_width\":1,\"lightbox_hit_border_style\":\"none\",\"lightbox_hit_border_color\":\"000000\",\"lightbox_hit_border_radius\":\"5px\",\"lightbox_hit_padding\":\"5px\",\"lightbox_hit_margin\":\"0 5px\",\"lightbox_hit_color\":\"FFFFFF\",\"lightbox_hit_font_style\":\"segoe ui\",\"lightbox_hit_font_weight\":\"normal\",\"lightbox_hit_font_size\":14,\"masonry_description_font_size\":12,\"masonry_description_color\":\"CCCCCC\",\"masonry_description_font_style\":\"segoe ui\",\"album_masonry_back_font_color\":\"000000\",\"album_masonry_back_font_style\":\"segoe ui\",\"album_masonry_back_font_size\":14,\"album_masonry_back_font_weight\":\"normal\",\"album_masonry_back_padding\":\"0\",\"album_masonry_title_font_color\":\"CCCCCC\",\"album_masonry_title_font_style\":\"segoe ui\",\"album_masonry_thumb_title_pos\":\"bottom\",\"album_masonry_title_font_size\":16,\"album_masonry_title_font_weight\":\"bold\",\"album_masonry_title_margin\":\"5px\",\"album_masonry_title_shadow\":\"\",\"album_masonry_thumb_margin\":4,\"album_masonry_thumb_padding\":4,\"album_masonry_thumb_border_radius\":\"0\",\"album_masonry_thumb_border_width\":1,\"album_masonry_thumb_border_style\":\"none\",\"album_masonry_thumb_border_color\":\"000000\",\"album_masonry_thumb_bg_color\":\"E8E8E8\",\"album_masonry_thumbs_bg_color\":\"FFFFFF\",\"album_masonry_thumb_bg_transparent\":100,\"album_masonry_thumb_box_shadow\":\"\",\"album_masonry_thumb_transparent\":100,\"album_masonry_thumb_align\":\"center\",\"album_masonry_thumb_hover_effect\":\"rotate\",\"album_masonry_thumb_hover_effect_value\":\"2deg\",\"album_masonry_thumb_transition\":1,\"album_masonry_gal_title_font_color\":\"CCCCCC\",\"album_masonry_gal_title_font_style\":\"segoe ui\",\"album_masonry_gal_title_font_size\":16,\"album_masonry_gal_title_font_weight\":\"bold\",\"album_masonry_gal_title_margin\":\"2px\",\"album_masonry_gal_title_shadow\":\"0px 0px 0px #888888\",\"album_masonry_gal_title_align\":\"center\",\"mosaic_thumb_padding\":4,\"mosaic_thumb_border_radius\":\"2px\",\"mosaic_thumb_border_width\":1,\"mosaic_thumb_border_style\":\"none\",\"mosaic_thumb_border_color\":\"CCCCCC\",\"mosaic_thumbs_bg_color\":\"FFFFFF\",\"mosaic_thumb_bg_transparent\":0,\"mosaic_thumb_transparent\":80,\"mosaic_thumb_align\":\"center\",\"mosaic_thumb_hover_effect\":\"rotate\",\"mosaic_thumb_hover_effect_value\":\"2deg\",\"mosaic_thumb_title_font_color\":\"CCCCCC\",\"mosaic_thumb_title_font_style\":\"segoe ui\",\"mosaic_thumb_title_font_weight\":\"bold\",\"mosaic_thumb_title_margin\":\"2px\",\"mosaic_thumb_title_shadow\":\"0px 0px 0px #888888\",\"mosaic_thumb_title_font_size\":16,\"mosaic_thumb_gal_title_font_color\":\"CCCCCC\",\"mosaic_thumb_gal_title_font_style\":\"segoe ui\",\"mosaic_thumb_gal_title_font_size\":16,\"mosaic_thumb_gal_title_font_weight\":\"bold\",\"mosaic_thumb_gal_title_margin\":\"2px\",\"mosaic_thumb_gal_title_shadow\":\"0px 0px 0px #888888\",\"mosaic_thumb_gal_title_align\":\"center\",\"carousel_cont_bg_color\":\"000000\",\"carousel_cont_btn_transparent\":0,\"carousel_close_btn_transparent\":100,\"carousel_rl_btn_bg_color\":\"000000\",\"carousel_rl_btn_border_radius\":\"20px\",\"carousel_rl_btn_border_width\":0,\"carousel_rl_btn_border_style\":\"none\",\"carousel_rl_btn_border_color\":\"FFFFFF\",\"carousel_rl_btn_color\":\"FFFFFF\",\"carousel_rl_btn_height\":40,\"carousel_rl_btn_size\":20,\"carousel_play_pause_btn_size\":20,\"carousel_rl_btn_width\":40,\"carousel_close_rl_btn_hover_color\":\"CCCCCC\",\"carousel_rl_btn_style\":\"fa-chevron\",\"carousel_mergin_bottom\":\"0.5\",\"carousel_font_family\":\"segoe ui\",\"carousel_feature_border_width\":2,\"carousel_feature_border_style\":\"solid\",\"carousel_feature_border_color\":\"5D204F\",\"carousel_caption_background_color\":\"000000\",\"carousel_caption_bottom\":0,\"carousel_caption_p_mergin\":0,\"carousel_caption_p_pedding\":5,\"carousel_caption_p_font_weight\":\"bold\",\"carousel_caption_p_font_size\":14,\"carousel_caption_p_color\":\"FFFFFF\",\"carousel_title_opacity\":100,\"carousel_title_border_radius\":\"5px\",\"mosaic_thumb_transition\":1}', 0);
/*!40000 ALTER TABLE `wp_bwg_theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_commentmeta`
--

LOCK TABLES `wp_commentmeta` WRITE;
/*!40000 ALTER TABLE `wp_commentmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_commentmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT 0,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_comments`
--

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_cpappbk_forms`
--

DROP TABLE IF EXISTS `wp_cpappbk_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_cpappbk_forms` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `form_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `form_structure` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `calendar_language` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `date_format` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `product_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `pay_later_label` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fp_from_email` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fp_destination_emails` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `fp_subject` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fp_inc_additional_info` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fp_return_page` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fp_message` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `fp_emailformat` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fp_emailtomethod` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fp_destination_emails_field` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cu_enable_copy_to_user` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cu_user_email_field` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cu_subject` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cu_message` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `cu_emailformat` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `fp_emailfrommethod` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vs_text_maxapp` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vs_text_is_required` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vs_text_is_email` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vs_text_datemmddyyyy` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vs_text_dateddmmyyyy` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vs_text_number` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vs_text_digits` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vs_text_max` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vs_text_min` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vs_text_pageof` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vs_text_submitbtn` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vs_text_previousbtn` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `vs_text_nextbtn` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cp_user_access` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `rep_enable` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `rep_days` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `rep_hour` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `rep_emails` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `rep_subject` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `rep_emailformat` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `rep_message` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `cv_enable_captcha` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cv_width` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cv_height` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cv_chars` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cv_font` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cv_min_font_size` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cv_max_font_size` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cv_noise` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cv_noise_length` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cv_background` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cv_border` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `cv_text_enter_valid_captcha` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_cpappbk_forms`
--

LOCK TABLES `wp_cpappbk_forms` WRITE;
/*!40000 ALTER TABLE `wp_cpappbk_forms` DISABLE KEYS */;
INSERT INTO `wp_cpappbk_forms` (`id`, `form_name`, `form_structure`, `calendar_language`, `date_format`, `product_name`, `pay_later_label`, `fp_from_email`, `fp_destination_emails`, `fp_subject`, `fp_inc_additional_info`, `fp_return_page`, `fp_message`, `fp_emailformat`, `fp_emailtomethod`, `fp_destination_emails_field`, `cu_enable_copy_to_user`, `cu_user_email_field`, `cu_subject`, `cu_message`, `cu_emailformat`, `fp_emailfrommethod`, `vs_text_maxapp`, `vs_text_is_required`, `vs_text_is_email`, `vs_text_datemmddyyyy`, `vs_text_dateddmmyyyy`, `vs_text_number`, `vs_text_digits`, `vs_text_max`, `vs_text_min`, `vs_text_pageof`, `vs_text_submitbtn`, `vs_text_previousbtn`, `vs_text_nextbtn`, `cp_user_access`, `rep_enable`, `rep_days`, `rep_hour`, `rep_emails`, `rep_subject`, `rep_emailformat`, `rep_message`, `cv_enable_captcha`, `cv_width`, `cv_height`, `cv_chars`, `cv_font`, `cv_min_font_size`, `cv_max_font_size`, `cv_noise`, `cv_noise_length`, `cv_background`, `cv_border`, `cv_text_enter_valid_captcha`) VALUES 
(1, 'Form 1', '[[{\"form_identifier\":\"\",\"name\":\"fieldname1\",\"shortlabel\":\"\",\"index\":0,\"ftype\":\"fapp\",\"userhelp\":\"\",\"userhelpTooltip\":false,\"csslayout\":\"\",\"title\":\"Appointment\",\"services\":[{\"name\":\"Service 1\",\"price\":1,\"duration\":60}],\"openhours\":[{\"type\":\"all\",\"d\":\"\",\"h1\":8,\"m1\":0,\"h2\":17,\"m2\":0}],\"dateFormat\":\"mm/dd/yy\",\"showDropdown\":false,\"dropdownRange\":\"-10:+10\",\"working_dates\":[true,true,true,true,true,true,true],\"numberOfMonths\":1,\"firstDay\":0,\"minDate\":\"0\",\"maxDate\":\"\",\"defaultDate\":\"\",\"invalidDates\":\"\",\"required\":true,\"fBuild\":{}},{\"form_identifier\":\"\",\"name\":\"email\",\"shortlabel\":\"\",\"index\":1,\"ftype\":\"femail\",\"userhelp\":\"\",\"userhelpTooltip\":false,\"csslayout\":\"\",\"title\":\"Email\",\"predefined\":\"\",\"predefinedClick\":false,\"required\":true,\"size\":\"medium\",\"equalTo\":\"\",\"fBuild\":{}}],[{\"title\":\"\",\"description\":\"\",\"formlayout\":\"top_aligned\",\"formtemplate\":\"\",\"evalequations\":1,\"autocomplete\":1}]]', '', '', 'Booking', 'Pay later', 'capitan.flin@yandex.ru', 'capitan.flin@yandex.ru', 'Contact from the blog...', 'true', 'http://stylist', 'The following contact message has been sent:\n\n<%INFO%>\n\n', 'text', '', '', 'true', '', 'Confirmation: Message received...', 'Thank you for your message. We will reply you as soon as possible.\n\nThis is a copy of the data sent:\n\n<%INFO%>\n\nBest Regards.', 'text', '', 'Please select a max of  {0} appointments per customer.', 'This field is required.', 'Please enter a valid email address.', 'Please enter a valid date with this format(mm/dd/yyyy)', 'Please enter a valid date with this format(dd/mm/yyyy)', 'Please enter a valid number.', 'Please enter only digits.', 'Please enter a value less than or equal to {0}.', 'Please enter a value greater than or equal to {0}.', 'Page {0} of {0}', 'Submit', 'Previous', 'Next', NULL, 'no', '1', '0', '', 'Submissions report...', 'text', 'Attached you will find the data from the form submissions.', 'true', '180', '60', '5', 'font-1.ttf', '25', '35', '200', '4', 'ffffff', '000000', 'Please enter a valid captcha code.');
/*!40000 ALTER TABLE `wp_cpappbk_forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_cpappbk_messages`
--

DROP TABLE IF EXISTS `wp_cpappbk_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_cpappbk_messages` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `formid` int(11) NOT NULL,
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ipaddr` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `notifyto` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `posted_data` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `whoadded` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_cpappbk_messages`
--

LOCK TABLES `wp_cpappbk_messages` WRITE;
/*!40000 ALTER TABLE `wp_cpappbk_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_cpappbk_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_flag_album`
--

DROP TABLE IF EXISTS `wp_flag_album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_flag_album` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `previewpic` bigint(20) NOT NULL DEFAULT 0,
  `albumdesc` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `categories` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_flag_album`
--

LOCK TABLES `wp_flag_album` WRITE;
/*!40000 ALTER TABLE `wp_flag_album` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_flag_album` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_flag_comments`
--

DROP TABLE IF EXISTS `wp_flag_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_flag_comments` (
  `cid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ownerid` int(11) unsigned NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `website` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `inmoderation` int(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`cid`),
  KEY `ownerid` (`ownerid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_flag_comments`
--

LOCK TABLES `wp_flag_comments` WRITE;
/*!40000 ALTER TABLE `wp_flag_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_flag_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_flag_gallery`
--

DROP TABLE IF EXISTS `wp_flag_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_flag_gallery` (
  `gid` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `path` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `title` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `galdesc` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `previewpic` bigint(20) DEFAULT 0,
  `sortorder` bigint(20) NOT NULL DEFAULT 0,
  `author` bigint(20) NOT NULL DEFAULT 0,
  `status` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`gid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_flag_gallery`
--

LOCK TABLES `wp_flag_gallery` WRITE;
/*!40000 ALTER TABLE `wp_flag_gallery` DISABLE KEYS */;
INSERT INTO `wp_flag_gallery` (`gid`, `name`, `path`, `title`, `galdesc`, `previewpic`, `sortorder`, `author`, `status`) VALUES 
(1, '18-07-19_12-14-55', 'wp-content/flagallery/18-07-19_12-14-55', 'Галерея', '', 1, 0, 1, 0);
/*!40000 ALTER TABLE `wp_flag_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_flag_pictures`
--

DROP TABLE IF EXISTS `wp_flag_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_flag_pictures` (
  `pid` bigint(20) NOT NULL AUTO_INCREMENT,
  `galleryid` bigint(20) NOT NULL DEFAULT 0,
  `filename` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `alttext` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `imagedate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `exclude` tinyint(4) DEFAULT 0,
  `sortorder` bigint(20) NOT NULL DEFAULT 0,
  `location` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `city` tinytext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `state` tinytext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `country` tinytext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `credit` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `copyright` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `commentson` int(1) unsigned NOT NULL DEFAULT 1,
  `hitcounter` int(11) unsigned DEFAULT 0,
  `total_value` int(11) unsigned DEFAULT 0,
  `total_votes` int(11) unsigned DEFAULT 0,
  `used_ips` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_data` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_flag_pictures`
--

LOCK TABLES `wp_flag_pictures` WRITE;
/*!40000 ALTER TABLE `wp_flag_pictures` DISABLE KEYS */;
INSERT INTO `wp_flag_pictures` (`pid`, `galleryid`, `filename`, `description`, `alttext`, `link`, `imagedate`, `modified`, `exclude`, `sortorder`, `location`, `city`, `state`, `country`, `credit`, `copyright`, `commentson`, `hitcounter`, `total_value`, `total_votes`, `used_ips`, `meta_data`) VALUES 
(1, 1, '11-5.jpg', 'mmmmmmmmmmmmmmmmmm', '', '', 0x323031382d30372d31392030303a31363a3238, 0x323031382d30372d31392030333a32363a3332, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 0, NULL, 'a:18:{i:0;b:0;s:8:\"aperture\";b:0;s:6:\"credit\";b:0;s:6:\"camera\";b:0;s:7:\"caption\";b:0;s:17:\"created_timestamp\";b:0;s:9:\"copyright\";b:0;s:12:\"focal_length\";b:0;s:3:\"iso\";b:0;s:13:\"shutter_speed\";b:0;s:5:\"flash\";b:0;s:5:\"title\";b:0;s:8:\"keywords\";b:0;s:5:\"width\";i:1280;s:6:\"height\";i:1024;s:5:\"saved\";b:1;s:9:\"thumbnail\";a:2:{s:5:\"width\";i:400;s:6:\"height\";i:320;}s:7:\"webview\";a:7:{i:0;i:1280;i:1;i:1024;i:2;i:2;i:3;s:26:\"width=\"1280\" height=\"1024\"\";s:4:\"bits\";i:8;s:8:\"channels\";i:3;s:4:\"mime\";s:10:\"image/jpeg\";}}');
/*!40000 ALTER TABLE `wp_flag_pictures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_gdgallerygalleries`
--

DROP TABLE IF EXISTS `wp_gdgallerygalleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_gdgallerygalleries` (
  `id_gallery` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT 0,
  `order_by` int(1) NOT NULL DEFAULT 0,
  `sort_by` int(1) NOT NULL DEFAULT 0,
  `show_title` int(1) NOT NULL DEFAULT 0,
  `display_type` int(1) NOT NULL DEFAULT 0,
  `view_type` int(1) NOT NULL DEFAULT 0,
  `position` enum('center','left','right') DEFAULT 'center',
  `hover_effect` int(1) NOT NULL DEFAULT 0,
  `items_per_page` int(3) NOT NULL DEFAULT 5,
  `custom_css` text DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_gallery`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_gdgallerygalleries`
--

LOCK TABLES `wp_gdgallerygalleries` WRITE;
/*!40000 ALTER TABLE `wp_gdgallerygalleries` DISABLE KEYS */;
INSERT INTO `wp_gdgallerygalleries` (`id_gallery`, `name`, `description`, `ordering`, `order_by`, `sort_by`, `show_title`, `display_type`, `view_type`, `position`, `hover_effect`, `items_per_page`, `custom_css`, `ctime`) VALUES 
(1, 'My First Gallery', NULL, 0, 0, 0, 0, 0, 0, 'center', 0, 5, '#gdgallery_container_1{}', 0x323031382d30372d31392030323a32373a3435),
(2, 'New Created Gallery', NULL, 0, 0, 0, 0, 0, 4, 'center', 0, 5, '#gdgallery_container_2 { background-color: transparent; margin-top: 20px; }', 0x323031382d30372d31392030323a33313a3036);
/*!40000 ALTER TABLE `wp_gdgallerygalleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_gdgalleryimages`
--

DROP TABLE IF EXISTS `wp_gdgalleryimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_gdgalleryimages` (
  `id_image` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_gallery` int(11) unsigned NOT NULL,
  `id_post` int(11) unsigned DEFAULT 0,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT 0,
  `link` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `target` enum('_blank','_self','_top','_parent') DEFAULT '_blank',
  `type` enum('image','youtube','vimeo') DEFAULT 'image',
  `ctime` timestamp NOT NULL DEFAULT current_timestamp(),
  `video_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_image`),
  KEY `id_gallery` (`id_gallery`),
  CONSTRAINT `wp_gdgalleryimages_ibfk_1` FOREIGN KEY (`id_gallery`) REFERENCES `wp_gdgallerygalleries` (`id_gallery`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_gdgalleryimages`
--

LOCK TABLES `wp_gdgalleryimages` WRITE;
/*!40000 ALTER TABLE `wp_gdgalleryimages` DISABLE KEYS */;
INSERT INTO `wp_gdgalleryimages` (`id_image`, `id_gallery`, `id_post`, `name`, `description`, `ordering`, `link`, `url`, `target`, `type`, `ctime`, `video_id`) VALUES 
(1, 1, 0, '1', NULL, 1, NULL, 'http://wsite/wp-content/plugins/photo-gallery-image/resources/assets/images/project/1.jpg', '_blank', 'image', 0x323031382d30372d31392030323a32373a3435, NULL),
(2, 1, 0, '2', NULL, 2, NULL, 'http://wsite/wp-content/plugins/photo-gallery-image/resources/assets/images/project/2.jpg', '_blank', 'image', 0x323031382d30372d31392030323a32373a3435, NULL),
(3, 1, 0, '3', NULL, 3, NULL, 'http://wsite/wp-content/plugins/photo-gallery-image/resources/assets/images/project/3.jpg', '_blank', 'image', 0x323031382d30372d31392030323a32373a3435, NULL),
(4, 1, 0, '4', NULL, 4, NULL, 'http://wsite/wp-content/plugins/photo-gallery-image/resources/assets/images/project/4.jpg', '_blank', 'image', 0x323031382d30372d31392030323a32373a3435, NULL),
(5, 1, 0, '5', NULL, 5, NULL, 'http://wsite/wp-content/plugins/photo-gallery-image/resources/assets/images/project/5.jpg', '_blank', 'image', 0x323031382d30372d31392030323a32373a3435, NULL),
(6, 1, 0, '6', NULL, 6, NULL, 'http://wsite/wp-content/plugins/photo-gallery-image/resources/assets/images/project/6.jpg', '_blank', 'image', 0x323031382d30372d31392030323a32373a3435, NULL),
(7, 1, 0, '7', NULL, 7, NULL, 'http://wsite/wp-content/plugins/photo-gallery-image/resources/assets/images/project/7.jpg', '_blank', 'image', 0x323031382d30372d31392030323a32373a3435, NULL),
(8, 1, 0, '8', NULL, 8, NULL, 'http://wsite/wp-content/plugins/photo-gallery-image/resources/assets/images/project/8.jpg', '_blank', 'image', 0x323031382d30372d31392030323a32373a3435, NULL),
(9, 1, 0, '9', NULL, 9, NULL, 'http://wsite/wp-content/plugins/photo-gallery-image/resources/assets/images/project/9.jpg', '_blank', 'image', 0x323031382d30372d31392030323a32373a3435, NULL),
(10, 1, 0, '10', NULL, 10, NULL, 'http://wsite/wp-content/plugins/photo-gallery-image/resources/assets/images/project/10.jpg', '_blank', 'image', 0x323031382d30372d31392030323a32373a3435, NULL),
(11, 1, 0, '11', NULL, 11, NULL, 'http://wsite/wp-content/plugins/photo-gallery-image/resources/assets/images/project/11.jpg', '_blank', 'image', 0x323031382d30372d31392030323a32373a3435, NULL),
(12, 1, 0, '12', NULL, 12, NULL, 'http://wsite/wp-content/plugins/photo-gallery-image/resources/assets/images/project/12.jpg', '_blank', 'image', 0x323031382d30372d31392030323a32373a3435, NULL),
(13, 1, 0, '13', NULL, 13, NULL, 'http://wsite/wp-content/plugins/photo-gallery-image/resources/assets/images/project/13.jpg', '_blank', 'image', 0x323031382d30372d31392030323a32373a3435, NULL),
(14, 2, 40, 'regnum_picture_1484072057_big', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/regnum_picture_1484072057_big.jpg', '_blank', 'image', 0x323031382d30372d31392030323a33313a3233, NULL),
(15, 2, 39, 'pank-kultura-raznovidnosti-zhanra', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/pank-kultura-raznovidnosti-zhanra.jpg', '_blank', 'image', 0x323031382d30372d31392030323a33313a3239, NULL),
(16, 2, 38, 'Mining', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/Mining.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(17, 2, 52, '11 (1)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-1.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(18, 2, 53, '11 (2)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-2.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(19, 2, 54, '11 (3)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-3.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(20, 2, 55, '11 (4)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-4.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(21, 2, 56, '11 (5)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-5.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(22, 2, 57, '11 (6)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-6.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(23, 2, 58, '11 (7)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-7.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(24, 2, 59, 'Photo of water splash on a black background.', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-8.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(25, 2, 60, '11 (9)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-9.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(26, 2, 61, '11 (10)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-10.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(27, 2, 62, '11 (11)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-11.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(28, 2, 63, '11 (12)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-12.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(29, 2, 64, '11 (13)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-13.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(30, 2, 65, '11 (14)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-14.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(31, 2, 66, '11 (15)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-15.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(32, 2, 67, '11 (16)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-16.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(33, 2, 68, '11 (17)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-17.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(34, 2, 69, '11 (18)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-18.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(35, 2, 70, '11 (19)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-19.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(36, 2, 71, '11 (20)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-20.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(37, 2, 72, '11 (21)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-21.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(38, 2, 73, '11 (22)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-22.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(39, 2, 74, '11 (23)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-23.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(40, 2, 75, '11 (24)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-24.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(41, 2, 76, '11 (25)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-25.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(42, 2, 77, '11 (26)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-26.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(43, 2, 78, '11 (27)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-27.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(44, 2, 79, '11 (28)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-28.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(45, 2, 80, 'OLYMPUS DIGITAL CAMERA', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-29.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(46, 2, 81, '11 (30)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-30.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(47, 2, 82, '11 (31)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-31.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(48, 2, 83, '11 (32)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-32.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(49, 2, 84, '11 (33)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-33.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(50, 2, 85, '11 (34)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-34.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(51, 2, 86, '11 (35)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-35.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(52, 2, 87, '11 (36)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-36.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(53, 2, 88, '11 (37)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-37.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(54, 2, 89, '11 (38)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-38.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(55, 2, 90, '11 (39)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-39.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(56, 2, 91, '11 (40)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-40.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(57, 2, 92, '11 (41)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-41.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(58, 2, 93, '11 (42)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-42.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(59, 2, 94, '11 (43)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-43.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(60, 2, 95, '11 (44)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-44.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(61, 2, 96, '11 (45)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-45.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(62, 2, 97, '11 (46)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-46.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(63, 2, 98, '11 (47)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-47.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(64, 2, 99, '11 (48)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-48.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(65, 2, 100, '11 (49)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-49.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL),
(66, 2, 101, '11 (50)', NULL, 0, NULL, 'http://wsite/wp-content/uploads/2018/07/11-50.jpg', '_blank', 'image', 0x323031382d30372d31392030323a34343a3132, NULL);
/*!40000 ALTER TABLE `wp_gdgalleryimages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_gdgallerysettings`
--

DROP TABLE IF EXISTS `wp_gdgallerysettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_gdgallerysettings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `option_key` varchar(200) NOT NULL,
  `option_value` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `option_key` (`option_key`)
) ENGINE=InnoDB AUTO_INCREMENT=277 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_gdgallerysettings`
--

LOCK TABLES `wp_gdgallerysettings` WRITE;
/*!40000 ALTER TABLE `wp_gdgallerysettings` DISABLE KEYS */;
INSERT INTO `wp_gdgallerysettings` (`id`, `option_key`, `option_value`) VALUES 
(1, 'RemoveTablesUninstall', 'off'),
(2, 'lightbox_type_justified', 'wide'),
(3, 'show_title_justified', '0'),
(4, 'title_position_justified', 'center'),
(5, 'title_vertical_position_justified', 'inside_bottom'),
(6, 'title_appear_type_justified', 'slide'),
(7, 'title_size_justified', '16'),
(8, 'title_color_justified', 'FFFFFF'),
(9, 'title_background_color_justified', '333333'),
(10, 'title_background_opacity_justified', '70'),
(11, 'margin_justified', '10'),
(12, 'border_width_justified', '0'),
(13, 'border_color_justified', '333333'),
(14, 'border_radius_justified', '0'),
(15, 'on_hover_overlay_justified', 'b:1;'),
(16, 'show_icons_justified', 'b:1;'),
(17, 'show_link_icon_justified', 'b:1;'),
(18, 'item_as_link_justified', 'b:0;'),
(19, 'link_new_tab_justified', 'b:1;'),
(20, 'image_hover_effect_justified', 'blur'),
(21, 'image_hover_effect_reverse_justified', 'b:0;'),
(22, 'shadow_justified', 'b:0;'),
(23, 'load_more_text_justified', 'Load More'),
(24, 'load_more_position_justified', 'center'),
(25, 'load_more_font_size_justified', '15'),
(26, 'load_more_vertical_padding_justified', '8'),
(27, 'load_more_horisontal_padding_justified', '13'),
(28, 'load_more_border_width_justified', '1'),
(29, 'load_more_border_radius_justified', '0'),
(30, 'load_more_color_justified', 'FFFFFF'),
(31, 'load_more_background_color_justified', '333333'),
(32, 'load_more_border_color_justified', '333333'),
(33, 'load_more_font_family_justified', 'monospace'),
(34, 'load_more_hover_color_justified', '333333'),
(35, 'load_more_hover_background_color_justified', 'FFFFFF'),
(36, 'load_more_hover_border_color_justified', '333333'),
(37, 'load_more_loader_justified', 'b:1;'),
(38, 'load_more_loader_color_justified', '333333'),
(39, 'pagination_position_justified', 'center'),
(40, 'pagination_font_size_justified', '15'),
(41, 'pagination_vertical_padding_justified', '8'),
(42, 'pagination_horisontal_padding_justified', '13'),
(43, 'pagination_margin_justified', '3'),
(44, 'pagination_border_width_justified', '1'),
(45, 'pagination_border_radius_justified', '0'),
(46, 'pagination_border_color_justified', '333333'),
(47, 'pagination_color_justified', '333333'),
(48, 'pagination_background_color_justified', 'FFFFFF'),
(49, 'pagination_font_family_justified', 'monospace'),
(50, 'pagination_hover_border_color_justified', '333333'),
(51, 'pagination_hover_color_justified', 'FFFFFF'),
(52, 'pagination_hover_background_color_justified', '333333'),
(53, 'pagination_nav_type_justified', '0'),
(54, 'pagination_nav_text_justified', 'first,prev,next,last'),
(55, 'pagination_nearby_pages_justified', '2'),
(56, 'lightbox_type_tiles', 'wide'),
(57, 'show_title_tiles', '0'),
(58, 'title_position_tiles', 'center'),
(59, 'title_vertical_position_tiles', 'inside_bottom'),
(60, 'title_appear_type_tiles', 'slide'),
(61, 'title_size_tiles', '16'),
(62, 'title_color_tiles', 'FFFFFF'),
(63, 'title_background_color_tiles', '333333'),
(64, 'title_background_opacity_tiles', '70'),
(65, 'margin_tiles', '10'),
(66, 'col_width_tiles', '250'),
(67, 'min_col_tiles', '2'),
(68, 'border_width_tiles', '0'),
(69, 'border_color_tiles', '333333'),
(70, 'border_radius_tiles', '0'),
(71, 'on_hover_overlay_tiles', 'b:1;'),
(72, 'show_icons_tiles', 'b:1;'),
(73, 'show_link_icon_tiles', 'b:1;'),
(74, 'item_as_link_tiles', 'b:0;'),
(75, 'link_new_tab_tiles', 'b:1;'),
(76, 'image_hover_effect_tiles', 'blur'),
(77, 'image_hover_effect_reverse_tiles', 'b:0;'),
(78, 'shadow_tiles', 'b:0;'),
(79, 'load_more_text_tiles', 'Load More'),
(80, 'load_more_position_tiles', 'center'),
(81, 'load_more_font_size_tiles', '15'),
(82, 'load_more_vertical_padding_tiles', '8'),
(83, 'load_more_horisontal_padding_tiles', '13'),
(84, 'load_more_border_width_tiles', '1'),
(85, 'load_more_border_radius_tiles', '0'),
(86, 'load_more_color_tiles', 'FFFFFF'),
(87, 'load_more_background_color_tiles', '333333'),
(88, 'load_more_border_color_tiles', '333333'),
(89, 'load_more_font_family_tiles', 'monospace'),
(90, 'load_more_hover_color_tiles', '333333'),
(91, 'load_more_hover_background_color_tiles', 'FFFFFF'),
(92, 'load_more_hover_border_color_tiles', '333333'),
(93, 'load_more_loader_tiles', 'b:1;'),
(94, 'load_more_loader_color_tiles', '333333'),
(95, 'pagination_position_tiles', 'center'),
(96, 'pagination_font_size_tiles', '15'),
(97, 'pagination_vertical_padding_tiles', '8'),
(98, 'pagination_horisontal_padding_tiles', '13'),
(99, 'pagination_margin_tiles', '3'),
(100, 'pagination_border_width_tiles', '1'),
(101, 'pagination_border_radius_tiles', '0'),
(102, 'pagination_border_color_tiles', '333333'),
(103, 'pagination_color_tiles', '333333'),
(104, 'pagination_background_color_tiles', 'FFFFFF'),
(105, 'pagination_font_family_tiles', 'monospace'),
(106, 'pagination_hover_border_color_tiles', '333333'),
(107, 'pagination_hover_color_tiles', 'FFFFFF'),
(108, 'pagination_hover_background_color_tiles', '333333'),
(109, 'pagination_nav_type_tiles', '0'),
(110, 'pagination_nav_text_tiles', 'first,prev,next,last'),
(111, 'pagination_nearby_pages_tiles', '2'),
(112, 'lightbox_type_carousel', 'wide'),
(113, 'show_title_carousel', '0'),
(114, 'title_position_carousel', 'center'),
(115, 'title_vertical_position_carousel', 'inside_bottom'),
(116, 'title_appear_type_carousel', 'slide'),
(117, 'title_size_carousel', '16'),
(118, 'title_color_carousel', 'FFFFFF'),
(119, 'title_background_color_carousel', '333333'),
(120, 'title_background_opacity_carousel', '70'),
(121, 'width_carousel', '200'),
(122, 'height_carousel', '200'),
(123, 'margin_carousel', '10'),
(124, 'position_carousel', 'center'),
(125, 'show_background_carousel', 'b:0;'),
(126, 'background_color_carousel', 'FFFFFF'),
(127, 'border_width_carousel', '0'),
(128, 'border_color_carousel', '333333'),
(129, 'border_radius_carousel', '0'),
(130, 'on_hover_overlay_carousel', 'b:1;'),
(131, 'show_icons_carousel', 'b:1;'),
(132, 'show_link_icon_carousel', 'b:1;'),
(133, 'item_as_link_carousel', 'b:0;'),
(134, 'link_new_tab_carousel', 'b:1;'),
(135, 'image_hover_effect_carousel', 'blur'),
(136, 'image_hover_effect_reverse_carousel', 'b:0;'),
(137, 'shadow_carousel', 'b:0;'),
(138, 'nav_num_carousel', '1'),
(139, 'scroll_duration_carousel', '500'),
(140, 'autoplay_carousel', 'b:1;'),
(141, 'autoplay_timeout_carousel', '3000'),
(142, 'autoplay_direction_carousel', 'right'),
(143, 'autoplay_pause_hover_carousel', 'b:1;'),
(144, 'enable_nav_carousel', 'b:1;'),
(145, 'nav_vertical_position_carousel', 'bottom'),
(146, 'nav_horisontal_position_carousel', 'center'),
(147, 'play_icon_carousel', 'b:1;'),
(148, 'icon_space_carousel', '20'),
(149, 'lightbox_type_grid', 'wide'),
(150, 'width_grid', '200'),
(151, 'height_grid', '200'),
(152, 'space_cols_grid', '20'),
(153, 'space_rows_grid', '20'),
(154, 'gallery_width_grid', '100'),
(155, 'gallery_bg_grid', 'b:1;'),
(156, 'gallery_bg_color_grid', 'FFFFFF'),
(157, 'num_rows_grid', '3'),
(158, 'show_title_grid', '1'),
(159, 'title_position_grid', 'left'),
(160, 'title_vertical_position_grid', 'bottom'),
(161, 'title_appear_type_grid', 'slide'),
(162, 'title_size_grid', '16'),
(163, 'title_color_grid', 'FFFFFF'),
(164, 'title_background_color_grid', '333333'),
(165, 'title_background_opacity_grid', '70'),
(166, 'border_width_grid', '1'),
(167, 'border_color_grid', '333333'),
(168, 'border_radius_grid', '3'),
(169, 'on_hover_overlay_grid', 'b:1;'),
(170, 'show_icons_grid', 'b:1;'),
(171, 'show_link_icon_grid', 'b:1;'),
(172, 'item_as_link_grid', 'b:0;'),
(173, 'link_new_tab_grid', 'b:1;'),
(174, 'image_hover_effect_grid', 'blur'),
(175, 'image_hover_effect_reverse_grid', 'b:0;'),
(176, 'shadow_grid', 'b:1;'),
(177, 'nav_type_grid', 'bullets'),
(178, 'bullets_margin_grid', '50'),
(179, 'bullets_color_grid', 'gray'),
(180, 'bullets_space_between_grid', '15'),
(181, 'arrows_margin_grid', '50'),
(182, 'arrows_space_between_grid', '20'),
(183, 'nav_position_grid', 'center'),
(184, 'nav_offset_grid', '0'),
(185, 'width_slider', '900'),
(186, 'height_slider', '500'),
(187, 'autoplay_slider', 'b:1;'),
(188, 'play_interval_slider', '5000'),
(189, 'pause_on_hover_slider', 'b:1;'),
(190, 'scale_mode_slider', 'fill'),
(191, 'transition_slider', 'slide'),
(192, 'transition_speed_slider', '1500'),
(193, 'zoom_slider', 'b:1;'),
(194, 'loader_type_slider', '1'),
(195, 'loader_color_slider', 'white'),
(196, 'bullets_slider', 'b:1;'),
(197, 'bullets_horisontal_position_slider', 'center'),
(198, 'bullets_vertical_position_slider', 'bottom'),
(199, 'arrows_slider', 'b:1;'),
(200, 'progress_indicator_slider', 'b:1;'),
(201, 'progress_indicator_type_slider', 'pie'),
(202, 'progress_indicator_horisontal_position_slider', 'right'),
(203, 'progress_indicator_vertical_position_slider', 'top'),
(204, 'play_slider', 'b:0;'),
(205, 'play_horizontal_position_slider', 'left'),
(206, 'play_vertical_position_slider', 'top'),
(207, 'fullscreen_slider', 'b:0;'),
(208, 'fullscreen_horisontal_position_slider', 'left'),
(209, 'fullscreen_vertical_position_slider', 'top'),
(210, 'zoom_panel_slider', 'b:0;'),
(211, 'zoom_horisontal_panel_position_slider', 'left'),
(212, 'zoom_vertical_panel_position_slider', 'top'),
(213, 'controls_always_on_slider', 'b:0;'),
(214, 'video_play_type_slider', 'round'),
(215, 'text_panel_slider', 'b:1;'),
(216, 'text_panel_always_on_slider', 'b:0;'),
(217, 'text_title_slider', 'b:1;'),
(218, 'text_description_slider', 'b:1;'),
(219, 'text_panel_bg_slider', 'b:1;'),
(220, 'carousel_slider', 'b:1;'),
(221, 'text_panel_bg_color_slider', '000000'),
(222, 'text_panel_bg_opacity_slider', '70'),
(223, 'text_panel_title_size_slider', '17'),
(224, 'text_panel_title_color_slider', 'FFFFFF'),
(225, 'text_panel_desc_size_slider', '14'),
(226, 'text_panel_desc_color_slider', 'FFFFFF'),
(227, 'playlist_slider', 'b:0;'),
(228, 'thumb_width_slider', '88'),
(229, 'thumb_height_slider', '50'),
(230, 'playlist_bg_slider', '000000'),
(231, 'arrows_offset_wide', '0'),
(232, 'overlay_color_wide', '000000'),
(233, 'overlay_opacity_wide', '100'),
(234, 'top_panel_bg_color_wide', '000000'),
(235, 'top_panel_opacity_wide', '100'),
(236, 'show_numbers_wide', 'b:1;'),
(237, 'number_size_wide', '15'),
(238, 'number_color_wide', 'FFFFFF'),
(239, 'image_border_width_wide', '0'),
(240, 'image_border_color_wide', 'FFFFFF'),
(241, 'image_border_radius_wide', '0'),
(242, 'image_shadow_wide', 'b:1;'),
(243, 'swipe_control_wide', 'b:1;'),
(244, 'zoom_control_wide', 'b:1;'),
(245, 'show_text_panel_wide', 'b:1;'),
(246, 'enable_title_wide', 'b:1;'),
(247, 'enable_desc_wide', 'b:0;'),
(248, 'texpanel_paddind_vert_wide', '5'),
(249, 'texpanel_paddind_hor_wide', '5'),
(250, 'text_position_wide', 'center'),
(251, 'title_color_wide', 'FFFFFF'),
(252, 'title_font_size_wide', '16'),
(253, 'desc_color_wide', 'FFFFFF'),
(254, 'desc_font_size_wide', '14'),
(255, 'arrows_offset_compact', '0'),
(256, 'overlay_color_compact', '000000'),
(257, 'overlay_opacity_compact', '50'),
(258, 'show_numbers_compact', 'b:1;'),
(259, 'number_size_compact', '15'),
(260, 'number_color_compact', 'FFFFFF'),
(261, 'image_border_width_compact', '0'),
(262, 'image_border_color_compact', 'FFFFFF'),
(263, 'image_border_radius_compact', '0'),
(264, 'image_shadow_compact', 'b:1;'),
(265, 'swipe_control_compact', 'b:1;'),
(266, 'zoom_control_compact', 'b:1;'),
(267, 'show_text_panel_compact', 'b:1;'),
(268, 'enable_title_compact', 'b:1;'),
(269, 'enable_desc_compact', 'b:0;'),
(270, 'texpanel_paddind_vert_compact', '5'),
(271, 'texpanel_paddind_hor_compact', '5'),
(272, 'text_position_compact', 'left'),
(273, 'title_color_compact', '333333'),
(274, 'title_font_size_compact', '16'),
(275, 'desc_color_compact', '333333'),
(276, 'desc_font_size_compact', '14');
/*!40000 ALTER TABLE `wp_gdgallerysettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_gv_responsive_slider`
--

DROP TABLE IF EXISTS `wp_gv_responsive_slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_gv_responsive_slider` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `image_name` varchar(500) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `createdon` datetime NOT NULL,
  `custom_link` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `post_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_gv_responsive_slider`
--

LOCK TABLES `wp_gv_responsive_slider` WRITE;
/*!40000 ALTER TABLE `wp_gv_responsive_slider` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gv_responsive_slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_links`
--

LOCK TABLES `wp_links` WRITE;
/*!40000 ALTER TABLE `wp_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_mp_timetable_data`
--

DROP TABLE IF EXISTS `wp_mp_timetable_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_mp_timetable_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `column_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `event_start` time NOT NULL,
  `event_end` time NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_mp_timetable_data`
--

LOCK TABLES `wp_mp_timetable_data` WRITE;
/*!40000 ALTER TABLE `wp_mp_timetable_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_mp_timetable_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_my_calendar`
--

DROP TABLE IF EXISTS `wp_my_calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_my_calendar` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_begin` date NOT NULL,
  `event_end` date NOT NULL,
  `event_title` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_desc` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_short` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_registration` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_tickets` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_time` time DEFAULT NULL,
  `event_endtime` time DEFAULT NULL,
  `event_recur` char(3) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_repeats` int(3) DEFAULT NULL,
  `event_status` int(1) NOT NULL DEFAULT 1,
  `event_author` bigint(20) unsigned DEFAULT NULL,
  `event_host` bigint(20) unsigned DEFAULT NULL,
  `event_category` bigint(20) unsigned NOT NULL DEFAULT 1,
  `event_link` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_post` bigint(20) unsigned NOT NULL DEFAULT 0,
  `event_link_expires` tinyint(1) NOT NULL,
  `event_location` bigint(20) unsigned NOT NULL DEFAULT 0,
  `event_label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_street` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_street2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_postcode` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_region` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_url` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_longitude` float(10,6) NOT NULL DEFAULT 0.000000,
  `event_latitude` float(10,6) NOT NULL DEFAULT 0.000000,
  `event_zoom` int(2) NOT NULL DEFAULT 14,
  `event_phone` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_phone2` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `event_access` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_group_id` int(11) NOT NULL DEFAULT 0,
  `event_span` int(1) NOT NULL DEFAULT 0,
  `event_approved` int(1) NOT NULL DEFAULT 1,
  `event_flagged` int(1) NOT NULL DEFAULT 0,
  `event_hide_end` int(1) NOT NULL DEFAULT 0,
  `event_holiday` int(1) NOT NULL DEFAULT 0,
  `event_fifth_week` int(1) NOT NULL DEFAULT 1,
  `event_image` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_added` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`event_id`),
  KEY `event_category` (`event_category`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_my_calendar`
--

LOCK TABLES `wp_my_calendar` WRITE;
/*!40000 ALTER TABLE `wp_my_calendar` DISABLE KEYS */;
INSERT INTO `wp_my_calendar` (`event_id`, `event_begin`, `event_end`, `event_title`, `event_desc`, `event_short`, `event_registration`, `event_tickets`, `event_time`, `event_endtime`, `event_recur`, `event_repeats`, `event_status`, `event_author`, `event_host`, `event_category`, `event_link`, `event_post`, `event_link_expires`, `event_location`, `event_label`, `event_street`, `event_street2`, `event_city`, `event_state`, `event_postcode`, `event_region`, `event_country`, `event_url`, `event_longitude`, `event_latitude`, `event_zoom`, `event_phone`, `event_phone2`, `event_access`, `event_group_id`, `event_span`, `event_approved`, `event_flagged`, `event_hide_end`, `event_holiday`, `event_fifth_week`, `event_image`, `event_added`) VALUES 
(7, 0x323031382d31312d3238, 0x323031382d31312d3238, 'Lorem Ipsum', 'Lorem Ipsum - это текст-\\\"рыба\\\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \\\"рыбой\\\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.', '', '', '', 0x31313a30303a3030, 0x31313a34353a3030, 'S1', 0, 1, 1, 1, 1, '', 537, 0, 0, '', '', '', '', '', '', '', '', '', 0.000000, 0.000000, 16, '', '', '', 0, 0, 1, 0, 0, 0, 1, '', 0x323031382d31312d32372030383a35383a3339),
(8, 0x323031382d31312d3239, 0x323031382d31312d3239, 'Стрижка у Васи', 'Побрить Васю на лысо.', '', '', '', 0x30383a30303a3030, 0x30393a30303a3030, 'S1', 0, 1, 1, 1, 1, '', 538, 0, 0, '', '', '', '', '', '', '', '', '', 0.000000, 0.000000, 16, '', '', '', 0, 0, 1, 0, 0, 0, 1, 'http://stylist/wp-content/uploads/2018/07/s-l300.jpg', 0x323031382d31312d32372030393a31363a3039),
(9, 0x323031382d31322d3133, 0x323031382d31322d3133, 'Постричь Майкла', 'Майкла надо постричь.', '', '', '', 0x31303a34353a3030, 0x31313a33303a3030, 'S1', 0, 1, 1, 1, 1, '', 539, 0, 0, '', '', '', '', '', '', '', '', '', 0.000000, 0.000000, 16, '', '', '', 0, 0, 1, 0, 0, 0, 1, 'http://stylist/wp-content/uploads/2018/07/300px-Negr.jpg', 0x323031382d31312d32372030393a32363a3335);
/*!40000 ALTER TABLE `wp_my_calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_my_calendar_categories`
--

DROP TABLE IF EXISTS `wp_my_calendar_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_my_calendar_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `category_color` varchar(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `category_icon` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `category_private` int(1) NOT NULL DEFAULT 0,
  `category_term` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_my_calendar_categories`
--

LOCK TABLES `wp_my_calendar_categories` WRITE;
/*!40000 ALTER TABLE `wp_my_calendar_categories` DISABLE KEYS */;
INSERT INTO `wp_my_calendar_categories` (`category_id`, `category_name`, `category_color`, `category_icon`, `category_private`, `category_term`) VALUES 
(1, 'General', '#ffffcc', 'event.png', 0, 14);
/*!40000 ALTER TABLE `wp_my_calendar_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_my_calendar_category_relationships`
--

DROP TABLE IF EXISTS `wp_my_calendar_category_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_my_calendar_category_relationships` (
  `relationship_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`relationship_id`),
  KEY `event_id` (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_my_calendar_category_relationships`
--

LOCK TABLES `wp_my_calendar_category_relationships` WRITE;
/*!40000 ALTER TABLE `wp_my_calendar_category_relationships` DISABLE KEYS */;
INSERT INTO `wp_my_calendar_category_relationships` (`relationship_id`, `event_id`, `category_id`) VALUES 
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1);
/*!40000 ALTER TABLE `wp_my_calendar_category_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_my_calendar_events`
--

DROP TABLE IF EXISTS `wp_my_calendar_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_my_calendar_events` (
  `occur_id` int(11) NOT NULL AUTO_INCREMENT,
  `occur_event_id` int(11) NOT NULL,
  `occur_begin` datetime NOT NULL,
  `occur_end` datetime NOT NULL,
  `occur_group_id` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`occur_id`),
  KEY `occur_event_id` (`occur_event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_my_calendar_events`
--

LOCK TABLES `wp_my_calendar_events` WRITE;
/*!40000 ALTER TABLE `wp_my_calendar_events` DISABLE KEYS */;
INSERT INTO `wp_my_calendar_events` (`occur_id`, `occur_event_id`, `occur_begin`, `occur_end`, `occur_group_id`) VALUES 
(7, 7, 0x323031382d31312d32382031313a30303a3030, 0x323031382d31312d32382031313a34353a3030, 0),
(8, 8, 0x323031382d31312d32392030383a30303a3030, 0x323031382d31312d32392030393a30303a3030, 0),
(9, 9, 0x323031382d31322d31332031303a34353a3030, 0x323031382d31322d31332031313a33303a3030, 0);
/*!40000 ALTER TABLE `wp_my_calendar_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_my_calendar_locations`
--

DROP TABLE IF EXISTS `wp_my_calendar_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_my_calendar_locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_street` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_street2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_city` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_state` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_postcode` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_region` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_url` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `location_country` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_longitude` float(10,6) NOT NULL DEFAULT 0.000000,
  `location_latitude` float(10,6) NOT NULL DEFAULT 0.000000,
  `location_zoom` int(2) NOT NULL DEFAULT 14,
  `location_phone` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_phone2` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_access` text COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_my_calendar_locations`
--

LOCK TABLES `wp_my_calendar_locations` WRITE;
/*!40000 ALTER TABLE `wp_my_calendar_locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_my_calendar_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_ngg_album`
--

DROP TABLE IF EXISTS `wp_ngg_album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_ngg_album` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `previewpic` bigint(20) NOT NULL DEFAULT 0,
  `albumdesc` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `sortorder` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pageid` bigint(20) NOT NULL DEFAULT 0,
  `extras_post_id` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `extras_post_id_key` (`extras_post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_ngg_album`
--

LOCK TABLES `wp_ngg_album` WRITE;
/*!40000 ALTER TABLE `wp_ngg_album` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_ngg_album` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_ngg_gallery`
--

DROP TABLE IF EXISTS `wp_ngg_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_ngg_gallery` (
  `gid` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `path` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `title` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `galdesc` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `pageid` bigint(20) NOT NULL DEFAULT 0,
  `previewpic` bigint(20) NOT NULL DEFAULT 0,
  `author` bigint(20) NOT NULL DEFAULT 0,
  `extras_post_id` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`gid`),
  KEY `extras_post_id_key` (`extras_post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_ngg_gallery`
--

LOCK TABLES `wp_ngg_gallery` WRITE;
/*!40000 ALTER TABLE `wp_ngg_gallery` DISABLE KEYS */;
INSERT INTO `wp_ngg_gallery` (`gid`, `name`, `slug`, `path`, `title`, `galdesc`, `pageid`, `previewpic`, `author`, `extras_post_id`) VALUES 
(3, '%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b5%d1%8f', '%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b5%d1%8f', 'wp-content\\gallery\\d0b3d0b0d0bbd0b5d180d0b5d18f', 'Галерея ', '', 0, 0, 1, 0);
/*!40000 ALTER TABLE `wp_ngg_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_ngg_pictures`
--

DROP TABLE IF EXISTS `wp_ngg_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_ngg_pictures` (
  `pid` bigint(20) NOT NULL AUTO_INCREMENT,
  `image_slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) NOT NULL DEFAULT 0,
  `galleryid` bigint(20) NOT NULL DEFAULT 0,
  `filename` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `alttext` mediumtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `imagedate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `exclude` tinyint(4) DEFAULT 0,
  `sortorder` bigint(20) NOT NULL DEFAULT 0,
  `meta_data` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `extras_post_id` bigint(20) NOT NULL DEFAULT 0,
  `updated_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`pid`),
  KEY `extras_post_id_key` (`extras_post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_ngg_pictures`
--

LOCK TABLES `wp_ngg_pictures` WRITE;
/*!40000 ALTER TABLE `wp_ngg_pictures` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_ngg_pictures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=81718 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_options`
--

LOCK TABLES `wp_options` WRITE;
/*!40000 ALTER TABLE `wp_options` DISABLE KEYS */;
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES 
(1, 'siteurl', 'http://stylist/', 'yes'),
(2, 'home', 'http://stylist/', 'yes'),
(3, 'blogname', 'stylist', 'yes'),
(4, 'blogdescription', 'Создай свой новый стиль', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'capitan.flin@yandex.ru', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'd.m.Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'd.m.Y H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:10:{i:0;s:43:\"all-in-one-seo-pack/all_in_one_seo_pack.php\";i:1;s:21:\"backwpup/backwpup.php\";i:2;s:55:\"contact-form-7-datepicker/contact-form-7-datepicker.php\";i:3;s:36:\"contact-form-7/wp-contact-form-7.php\";i:4;s:22:\"cyr3lat/cyr-to-lat.php\";i:5;s:45:\"disable-wordpress-updates/disable-updates.php\";i:7;s:27:\"my-calendar/my-calendar.php\";i:8;s:58:\"podamibe-custom-user-gravatar/pod-custom-user-gravatar.php\";i:9;s:25:\"tablepress/tablepress.php\";i:10;s:18:\"userdata/index.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'stylist', 'yes'),
(41, 'stylesheet', 'stylist', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:5:{s:28:\"flash-album-gallery/flag.php\";a:2:{i:0;s:10:\"flagLoader\";i:1;s:9:\"uninstall\";}s:41:\"booking-activities/booking-activities.php\";s:18:\"bookacti_uninstall\";s:29:\"mp-timetable/mp-timetable.php\";a:2:{i:0;s:13:\"Mp_Time_Table\";i:1;s:12:\"on_uninstall\";}s:40:\"wp-smart-preloader/inc/wsp_uninstall.php\";a:2:{i:0;s:18:\"WP_smart_preloader\";i:1;s:13:\"wsp_uninstall\";}s:18:\"userdata/index.php\";s:12:\"AT_uninstall\";}', 'no'),
(82, 'timezone_string', 'Europe/Kaliningrad', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '450', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'initial_db_version', '38590', 'yes'),
(93, 'wp_user_roles', 'a:8:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:137:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:19:\"NextGEN Manage tags\";b:1;s:29:\"NextGEN Manage others gallery\";b:1;s:13:\"FlAG overview\";b:1;s:16:\"FlAG Use TinyMCE\";b:1;s:18:\"FlAG Upload images\";b:1;s:18:\"FlAG Import folder\";b:1;s:19:\"FlAG Manage gallery\";b:1;s:26:\"FlAG Manage others gallery\";b:1;s:16:\"FlAG Change skin\";b:1;s:14:\"FlAG Add skins\";b:1;s:17:\"FlAG Delete skins\";b:1;s:19:\"FlAG Change options\";b:1;s:17:\"FlAG Manage music\";b:1;s:17:\"FlAG Manage video\";b:1;s:19:\"FlAG Manage banners\";b:1;s:16:\"FlAG iFrame page\";b:1;s:31:\"read_private_aggregator-records\";b:1;s:23:\"edit_aggregator-records\";b:1;s:30:\"edit_others_aggregator-records\";b:1;s:31:\"edit_private_aggregator-records\";b:1;s:33:\"edit_published_aggregator-records\";b:1;s:25:\"delete_aggregator-records\";b:1;s:32:\"delete_others_aggregator-records\";b:1;s:33:\"delete_private_aggregator-records\";b:1;s:35:\"delete_published_aggregator-records\";b:1;s:26:\"publish_aggregator-records\";b:1;s:8:\"backwpup\";b:1;s:13:\"backwpup_jobs\";b:1;s:18:\"backwpup_jobs_edit\";b:1;s:19:\"backwpup_jobs_start\";b:1;s:16:\"backwpup_backups\";b:1;s:25:\"backwpup_backups_download\";b:1;s:23:\"backwpup_backups_delete\";b:1;s:13:\"backwpup_logs\";b:1;s:20:\"backwpup_logs_delete\";b:1;s:17:\"backwpup_settings\";b:1;s:16:\"backwpup_restore\";b:1;s:24:\"manage_events_categories\";b:1;s:13:\"mc_add_events\";b:1;s:17:\"mc_approve_events\";b:1;s:16:\"mc_manage_events\";b:1;s:12:\"mc_edit_cats\";b:1;s:14:\"mc_edit_styles\";b:1;s:17:\"mc_edit_behaviors\";b:1;s:17:\"mc_edit_templates\";b:1;s:16:\"mc_edit_settings\";b:1;s:17:\"mc_edit_locations\";b:1;s:12:\"mc_view_help\";b:1;s:22:\"tablepress_edit_tables\";b:1;s:24:\"tablepress_delete_tables\";b:1;s:22:\"tablepress_list_tables\";b:1;s:21:\"tablepress_add_tables\";b:1;s:22:\"tablepress_copy_tables\";b:1;s:24:\"tablepress_import_tables\";b:1;s:24:\"tablepress_export_tables\";b:1;s:32:\"tablepress_access_options_screen\";b:1;s:30:\"tablepress_access_about_screen\";b:1;s:29:\"tablepress_import_tables_wptr\";b:1;s:23:\"tablepress_edit_options\";b:1;s:16:\"aiosp_manage_seo\";b:1;s:16:\"chronosly_author\";b:1;s:14:\"edit_chronosly\";b:1;s:14:\"read_chronosly\";b:1;s:16:\"delete_chronosly\";b:1;s:17:\"delete_chronoslys\";b:1;s:15:\"edit_chronoslys\";b:1;s:22:\"edit_others_chronoslys\";b:1;s:25:\"edit_published_chronoslys\";b:1;s:27:\"delete_published_chronoslys\";b:1;s:25:\"delete_private_chronoslys\";b:1;s:24:\"delete_others_chronoslys\";b:1;s:18:\"publish_chronoslys\";b:1;s:23:\"read_private_chronoslys\";b:1;s:17:\"chronosly_options\";b:1;s:17:\"chronosly_license\";b:1;s:23:\"edit_private_chronoslys\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:70:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:31:\"read_private_aggregator-records\";b:1;s:23:\"edit_aggregator-records\";b:1;s:30:\"edit_others_aggregator-records\";b:1;s:31:\"edit_private_aggregator-records\";b:1;s:33:\"edit_published_aggregator-records\";b:1;s:25:\"delete_aggregator-records\";b:1;s:32:\"delete_others_aggregator-records\";b:1;s:33:\"delete_private_aggregator-records\";b:1;s:35:\"delete_published_aggregator-records\";b:1;s:26:\"publish_aggregator-records\";b:1;s:24:\"manage_events_categories\";b:1;s:22:\"tablepress_edit_tables\";b:1;s:24:\"tablepress_delete_tables\";b:1;s:22:\"tablepress_list_tables\";b:1;s:21:\"tablepress_add_tables\";b:1;s:22:\"tablepress_copy_tables\";b:1;s:24:\"tablepress_import_tables\";b:1;s:24:\"tablepress_export_tables\";b:1;s:32:\"tablepress_access_options_screen\";b:1;s:30:\"tablepress_access_about_screen\";b:1;s:16:\"chronosly_author\";b:1;s:14:\"edit_chronosly\";b:1;s:14:\"read_chronosly\";b:1;s:16:\"delete_chronosly\";b:1;s:17:\"delete_chronoslys\";b:1;s:15:\"edit_chronoslys\";b:1;s:22:\"edit_others_chronoslys\";b:1;s:25:\"edit_published_chronoslys\";b:1;s:27:\"delete_published_chronoslys\";b:1;s:25:\"delete_private_chronoslys\";b:1;s:24:\"delete_others_chronoslys\";b:1;s:18:\"publish_chronoslys\";b:1;s:23:\"read_private_chronoslys\";b:1;s:17:\"chronosly_options\";b:1;s:17:\"chronosly_license\";b:1;s:23:\"edit_private_chronoslys\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:25:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:23:\"edit_aggregator-records\";b:1;s:33:\"edit_published_aggregator-records\";b:1;s:25:\"delete_aggregator-records\";b:1;s:35:\"delete_published_aggregator-records\";b:1;s:26:\"publish_aggregator-records\";b:1;s:24:\"manage_events_categories\";b:1;s:22:\"tablepress_edit_tables\";b:1;s:24:\"tablepress_delete_tables\";b:1;s:22:\"tablepress_list_tables\";b:1;s:21:\"tablepress_add_tables\";b:1;s:22:\"tablepress_copy_tables\";b:1;s:24:\"tablepress_import_tables\";b:1;s:24:\"tablepress_export_tables\";b:1;s:32:\"tablepress_access_options_screen\";b:1;s:30:\"tablepress_access_about_screen\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:7:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:23:\"edit_aggregator-records\";b:1;s:25:\"delete_aggregator-records\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:14:\"backwpup_admin\";a:2:{s:4:\"name\";s:14:\"BackWPup Admin\";s:12:\"capabilities\";a:12:{s:4:\"read\";b:1;s:8:\"backwpup\";b:1;s:13:\"backwpup_jobs\";b:1;s:18:\"backwpup_jobs_edit\";b:1;s:19:\"backwpup_jobs_start\";b:1;s:16:\"backwpup_backups\";b:1;s:25:\"backwpup_backups_download\";b:1;s:23:\"backwpup_backups_delete\";b:1;s:13:\"backwpup_logs\";b:1;s:20:\"backwpup_logs_delete\";b:1;s:17:\"backwpup_settings\";b:1;s:16:\"backwpup_restore\";b:1;}}s:14:\"backwpup_check\";a:2:{s:4:\"name\";s:21:\"BackWPup jobs checker\";s:12:\"capabilities\";a:12:{s:4:\"read\";b:1;s:8:\"backwpup\";b:1;s:13:\"backwpup_jobs\";b:1;s:18:\"backwpup_jobs_edit\";b:0;s:19:\"backwpup_jobs_start\";b:0;s:16:\"backwpup_backups\";b:1;s:25:\"backwpup_backups_download\";b:0;s:23:\"backwpup_backups_delete\";b:0;s:13:\"backwpup_logs\";b:1;s:20:\"backwpup_logs_delete\";b:0;s:17:\"backwpup_settings\";b:0;s:16:\"backwpup_restore\";b:0;}}s:15:\"backwpup_helper\";a:2:{s:4:\"name\";s:20:\"BackWPup jobs helper\";s:12:\"capabilities\";a:12:{s:4:\"read\";b:1;s:8:\"backwpup\";b:1;s:13:\"backwpup_jobs\";b:1;s:18:\"backwpup_jobs_edit\";b:0;s:19:\"backwpup_jobs_start\";b:1;s:16:\"backwpup_backups\";b:1;s:25:\"backwpup_backups_download\";b:1;s:23:\"backwpup_backups_delete\";b:1;s:13:\"backwpup_logs\";b:1;s:20:\"backwpup_logs_delete\";b:1;s:17:\"backwpup_settings\";b:0;s:16:\"backwpup_restore\";b:0;}}}', 'yes'),
(94, 'fresh_site', '0', 'yes'),
(95, 'WPLANG', 'ru_RU', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'cron', 'a:7:{i:1594153485;a:1:{s:13:\"backwpup_cron\";a:1:{s:32:\"4ef54d4ada20caa38401a05f7ccc9311\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:1:{s:2:\"id\";s:7:\"restart\";}}}}i:1594154731;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1594158346;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1594158771;a:2:{s:22:\"backwpup_check_cleanup\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:23:\"backwpup_update_message\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1594201804;a:1:{s:24:\"tribe_common_log_cleanup\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1594234747;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(112, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1531756197;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(131, 'can_compress_scripts', '1', 'no'),
(142, 'dismissed_update_core', 'a:1:{s:11:\"4.9.7|ru_RU\";b:1;}', 'no'),
(145, 'recently_activated', 'a:2:{s:31:\"insta-gallery/insta-gallery.php\";i:1594123937;s:28:\"telsender_ver 0.65/index.php\";i:1594119122;}', 'yes'),
(157, 'current_theme', 'stylist', 'yes'),
(158, 'theme_mods_stylist', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1531945167;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(159, 'theme_switched', '', 'yes'),
(195, 'ngg_run_freemius', '1', 'yes'),
(196, 'fs_active_plugins', 'O:8:\"stdClass\":3:{s:7:\"plugins\";a:1:{s:19:\"foogallery/freemius\";O:8:\"stdClass\":4:{s:7:\"version\";s:5:\"2.0.1\";s:4:\"type\";s:6:\"plugin\";s:9:\"timestamp\";i:1532020046;s:11:\"plugin_path\";s:25:\"foogallery/foogallery.php\";}}s:7:\"abspath\";s:28:\"C:\\OpenServer\\domains\\wsite/\";s:6:\"newest\";O:8:\"stdClass\":5:{s:11:\"plugin_path\";s:25:\"foogallery/foogallery.php\";s:8:\"sdk_path\";s:19:\"foogallery/freemius\";s:7:\"version\";s:5:\"2.0.1\";s:13:\"in_activation\";b:1;s:9:\"timestamp\";i:1532020046;}}', 'yes'),
(197, 'fs_debug_mode', '', 'yes'),
(198, 'fs_accounts', 'a:8:{s:11:\"plugin_data\";a:2:{s:15:\"nextgen-gallery\";a:16:{s:16:\"plugin_main_file\";O:8:\"stdClass\":1:{s:4:\"path\";s:76:\"C:/OpenServer/domains/wsite/wp-content/plugins/nextgen-gallery/nggallery.php\";}s:17:\"install_timestamp\";i:1531942385;s:16:\"sdk_last_version\";N;s:11:\"sdk_version\";s:7:\"1.2.1.5\";s:16:\"sdk_upgrade_mode\";b:1;s:18:\"sdk_downgrade_mode\";b:0;s:19:\"plugin_last_version\";N;s:14:\"plugin_version\";s:5:\"3.0.1\";s:19:\"plugin_upgrade_mode\";b:1;s:21:\"plugin_downgrade_mode\";b:0;s:21:\"is_plugin_new_install\";b:1;s:17:\"connectivity_test\";a:6:{s:12:\"is_connected\";b:1;s:4:\"host\";s:5:\"wsite\";s:9:\"server_ip\";s:9:\"127.0.0.1\";s:9:\"is_active\";b:1;s:9:\"timestamp\";i:1531942385;s:7:\"version\";s:5:\"3.0.1\";}s:17:\"was_plugin_loaded\";b:1;s:15:\"prev_is_premium\";b:0;s:22:\"install_sync_timestamp\";i:1532020074;s:12:\"is_anonymous\";a:3:{s:2:\"is\";b:1;s:9:\"timestamp\";i:1531957563;s:7:\"version\";s:5:\"3.0.1\";}}s:10:\"foogallery\";a:15:{s:16:\"plugin_main_file\";O:8:\"stdClass\":1:{s:4:\"path\";s:25:\"foogallery/foogallery.php\";}s:17:\"install_timestamp\";i:1531945594;s:16:\"sdk_last_version\";N;s:11:\"sdk_version\";s:5:\"2.0.1\";s:16:\"sdk_upgrade_mode\";b:1;s:18:\"sdk_downgrade_mode\";b:0;s:19:\"plugin_last_version\";N;s:14:\"plugin_version\";s:6:\"1.4.31\";s:19:\"plugin_upgrade_mode\";b:1;s:21:\"plugin_downgrade_mode\";b:0;s:21:\"is_plugin_new_install\";b:1;s:17:\"connectivity_test\";a:6:{s:12:\"is_connected\";b:1;s:4:\"host\";s:5:\"wsite\";s:9:\"server_ip\";s:9:\"127.0.0.1\";s:9:\"is_active\";b:1;s:9:\"timestamp\";i:1531945594;s:7:\"version\";s:6:\"1.4.31\";}s:17:\"was_plugin_loaded\";b:1;s:15:\"prev_is_premium\";b:0;s:12:\"is_anonymous\";a:3:{s:2:\"is\";b:1;s:9:\"timestamp\";i:1531961119;s:7:\"version\";s:6:\"1.4.31\";}}}s:13:\"file_slug_map\";a:2:{s:29:\"nextgen-gallery/nggallery.php\";s:15:\"nextgen-gallery\";s:25:\"foogallery/foogallery.php\";s:10:\"foogallery\";}s:7:\"plugins\";a:2:{s:15:\"nextgen-gallery\";O:9:\"FS_Plugin\":18:{s:16:\"parent_plugin_id\";N;s:5:\"title\";s:15:\"NextGEN Gallery\";s:4:\"slug\";s:15:\"nextgen-gallery\";s:4:\"type\";N;s:4:\"file\";s:29:\"nextgen-gallery/nggallery.php\";s:7:\"version\";s:5:\"3.0.1\";s:11:\"auto_update\";N;s:4:\"info\";N;s:10:\"is_premium\";b:0;s:7:\"is_live\";b:1;s:10:\"public_key\";s:32:\"pk_009356711cd548837f074e1ef60a4\";s:10:\"secret_key\";N;s:2:\"id\";s:3:\"266\";s:7:\"updated\";N;s:7:\"created\";N;s:22:\"\0FS_Entity\0_is_updated\";b:0;s:20:\"affiliate_moderation\";N;s:19:\"is_wp_org_compliant\";b:1;}s:10:\"foogallery\";O:9:\"FS_Plugin\":18:{s:16:\"parent_plugin_id\";N;s:5:\"title\";s:10:\"FooGallery\";s:4:\"slug\";s:10:\"foogallery\";s:4:\"type\";s:6:\"plugin\";s:4:\"file\";s:25:\"foogallery/foogallery.php\";s:7:\"version\";s:6:\"1.4.31\";s:11:\"auto_update\";N;s:4:\"info\";N;s:10:\"is_premium\";b:0;s:7:\"is_live\";b:1;s:10:\"public_key\";s:32:\"pk_d87616455a835af1d0658699d0192\";s:10:\"secret_key\";N;s:2:\"id\";s:3:\"843\";s:7:\"updated\";N;s:7:\"created\";N;s:22:\"\0FS_Entity\0_is_updated\";b:0;s:20:\"affiliate_moderation\";b:0;s:19:\"is_wp_org_compliant\";b:1;}}s:9:\"unique_id\";s:32:\"606db3db508caed45d92ebb3f081bced\";s:13:\"admin_notices\";a:2:{s:15:\"nextgen-gallery\";a:0:{}s:10:\"foogallery\";a:0:{}}s:21:\"id_slug_type_path_map\";a:1:{i:843;a:3:{s:4:\"slug\";s:10:\"foogallery\";s:4:\"type\";s:6:\"plugin\";s:4:\"path\";s:25:\"foogallery/foogallery.php\";}}s:11:\"all_plugins\";O:8:\"stdClass\":3:{s:9:\"timestamp\";i:1532020074;s:3:\"md5\";s:32:\"70ffc1034413ae1bc2cc88932f757190\";s:7:\"plugins\";a:8:{s:29:\"nextgen-gallery/nggallery.php\";a:5:{s:4:\"slug\";s:15:\"nextgen-gallery\";s:7:\"version\";s:5:\"3.0.1\";s:5:\"title\";s:15:\"NextGEN Gallery\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:33:\"ajax-load-more/ajax-load-more.php\";a:5:{s:4:\"slug\";s:14:\"ajax-load-more\";s:7:\"version\";s:5:\"3.5.1\";s:5:\"title\";s:14:\"Ajax Load More\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:47:\"category-to-pages-wud/category-to-pages-wud.php\";a:5:{s:4:\"slug\";s:21:\"category-to-pages-wud\";s:7:\"version\";s:5:\"2.4.7\";s:5:\"title\";s:21:\"Category to Pages WUD\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:55:\"lazyest-gallery-hide-menu/lazyest-gallery-hide-menu.php\";a:5:{s:4:\"slug\";s:25:\"lazyest-gallery-hide-menu\";s:7:\"version\";s:5:\"1.2.2\";s:5:\"title\";s:25:\"Lazyest Gallery Hide Menu\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:29:\"photo-gallery-image/index.php\";a:5:{s:4:\"slug\";s:19:\"photo-gallery-image\";s:7:\"version\";s:5:\"1.0.5\";s:5:\"title\";s:15:\"GrandWP Gallery\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:28:\"robo-gallery/robogallery.php\";a:5:{s:4:\"slug\";s:12:\"robo-gallery\";s:7:\"version\";s:5:\"2.8.1\";s:5:\"title\";s:12:\"Robo Gallery\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:56:\"tiled-gallery-carousel-without-jetpack/tiled-gallery.php\";a:5:{s:4:\"slug\";s:38:\"tiled-gallery-carousel-without-jetpack\";s:7:\"version\";s:3:\"3.0\";s:5:\"title\";s:40:\"Tiled Galleries Carousel Without Jetpack\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:27:\"wp-pagenavi/wp-pagenavi.php\";a:5:{s:4:\"slug\";s:11:\"wp-pagenavi\";s:7:\"version\";s:4:\"2.92\";s:5:\"title\";s:11:\"WP-PageNavi\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}}}s:10:\"all_themes\";O:8:\"stdClass\":3:{s:9:\"timestamp\";i:1532020074;s:3:\"md5\";s:32:\"c9ffd65161e6aabedfb58d78ff09fcca\";s:6:\"themes\";a:3:{s:7:\"stylist\";a:5:{s:4:\"slug\";s:7:\"stylist\";s:7:\"version\";s:0:\"\";s:5:\"title\";s:7:\"stylist\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:15:\"twentyseventeen\";a:5:{s:4:\"slug\";s:15:\"twentyseventeen\";s:7:\"version\";s:3:\"1.6\";s:5:\"title\";s:16:\"Twenty Seventeen\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:13:\"twentysixteen\";a:5:{s:4:\"slug\";s:13:\"twentysixteen\";s:7:\"version\";s:3:\"1.5\";s:5:\"title\";s:14:\"Twenty Sixteen\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}}}}', 'yes'),
(199, 'fs_api_cache', 'a:0:{}', 'yes'),
(201, 'widget_ngg-images', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(202, 'widget_ngg-mrssw', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(203, 'widget_slideshow', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(204, 'ngg_transient_groups', 'a:7:{s:9:\"__counter\";i:7;s:3:\"MVC\";a:2:{s:2:\"id\";i:2;s:7:\"enabled\";b:1;}s:15:\"col_in_wp_posts\";a:2:{s:2:\"id\";i:3;s:7:\"enabled\";b:1;}s:21:\"col_in_wp_ngg_gallery\";a:2:{s:2:\"id\";i:4;s:7:\"enabled\";b:1;}s:22:\"col_in_wp_ngg_pictures\";a:2:{s:2:\"id\";i:5;s:7:\"enabled\";b:1;}s:27:\"displayed_gallery_rendering\";a:2:{s:2:\"id\";i:6;s:7:\"enabled\";b:1;}s:19:\"col_in_wp_ngg_album\";a:2:{s:2:\"id\";i:7;s:7:\"enabled\";b:1;}}', 'yes'),
(205, 'ngg_options', 'a:72:{s:11:\"gallerypath\";s:19:\"wp-content\\gallery\\\";s:11:\"wpmuCSSfile\";s:13:\"nggallery.css\";s:9:\"wpmuStyle\";b:0;s:9:\"wpmuRoles\";b:0;s:16:\"wpmuImportFolder\";b:0;s:13:\"wpmuZipUpload\";b:0;s:14:\"wpmuQuotaCheck\";b:0;s:17:\"datamapper_driver\";s:22:\"custom_post_datamapper\";s:21:\"gallerystorage_driver\";s:25:\"ngglegacy_gallery_storage\";s:20:\"maximum_entity_count\";i:500;s:17:\"router_param_slug\";s:9:\"nggallery\";s:22:\"router_param_separator\";s:2:\"--\";s:19:\"router_param_prefix\";s:0:\"\";s:9:\"deleteImg\";b:1;s:9:\"swfUpload\";b:1;s:13:\"usePermalinks\";b:0;s:13:\"permalinkSlug\";s:9:\"nggallery\";s:14:\"graphicLibrary\";s:2:\"gd\";s:14:\"imageMagickDir\";s:15:\"/usr/local/bin/\";s:11:\"useMediaRSS\";b:0;s:18:\"galleries_in_feeds\";b:0;s:12:\"activateTags\";i:0;s:10:\"appendType\";s:4:\"tags\";s:9:\"maxImages\";i:7;s:14:\"relatedHeading\";s:51:\"<h3>Связанные изображения:</h3>\";s:10:\"thumbwidth\";i:240;s:11:\"thumbheight\";i:160;s:8:\"thumbfix\";b:1;s:12:\"thumbquality\";i:100;s:8:\"imgWidth\";i:800;s:9:\"imgHeight\";i:600;s:10:\"imgQuality\";i:100;s:9:\"imgBackup\";b:1;s:13:\"imgAutoResize\";b:0;s:9:\"galImages\";s:2:\"20\";s:17:\"galPagedGalleries\";i:0;s:10:\"galColumns\";i:0;s:12:\"galShowSlide\";b:1;s:12:\"galTextSlide\";s:35:\"[Показать слайдшоу]\";s:14:\"galTextGallery\";s:31:\"[Показать эскизы]\";s:12:\"galShowOrder\";s:7:\"gallery\";s:7:\"galSort\";s:9:\"sortorder\";s:10:\"galSortDir\";s:3:\"ASC\";s:10:\"galNoPages\";b:1;s:13:\"galImgBrowser\";i:0;s:12:\"galHiddenImg\";i:0;s:10:\"galAjaxNav\";i:0;s:11:\"thumbEffect\";s:8:\"fancybox\";s:9:\"thumbCode\";s:41:\"class=\"ngg-fancybox\" rel=\"%GALLERY_NAME%\"\";s:18:\"thumbEffectContext\";s:14:\"nextgen_images\";s:5:\"wmPos\";s:9:\"midCenter\";s:6:\"wmXpos\";i:15;s:6:\"wmYpos\";i:5;s:6:\"wmType\";s:4:\"text\";s:6:\"wmPath\";s:0:\"\";s:6:\"wmFont\";s:9:\"arial.ttf\";s:6:\"wmSize\";i:30;s:6:\"wmText\";s:7:\"stylist\";s:7:\"wmColor\";s:6:\"ffffff\";s:8:\"wmOpaque\";s:2:\"33\";s:7:\"slideFX\";s:4:\"fade\";s:7:\"irWidth\";i:600;s:8:\"irHeight\";i:400;s:12:\"irRotatetime\";i:10;s:11:\"activateCSS\";i:1;s:7:\"CSSfile\";s:13:\"nggallery.css\";s:28:\"always_enable_frontend_logic\";b:0;s:22:\"dynamic_thumbnail_slug\";s:13:\"nextgen-image\";s:23:\"dynamic_stylesheet_slug\";s:12:\"nextgen-dcss\";s:11:\"installDate\";i:1531942419;s:13:\"gallery_count\";i:1;s:40:\"gallery_created_after_reviews_introduced\";b:1;}', 'yes'),
(207, 'photocrati_auto_update_admin_update_list', '', 'yes'),
(208, 'photocrati_auto_update_admin_check_date', '', 'yes'),
(209, 'ngg_db_version', '1.8.1', 'yes'),
(212, 'pope_module_list', 'a:1:{i:21;s:35:\"photocrati-dynamic_stylesheet|3.0.0\";}', 'yes'),
(240, 'envira_gallery_116', '1', 'yes'),
(241, 'envira_gallery_121', '1', 'yes'),
(242, '_amn_envira-lite_last_checked', '1531958400', 'yes'),
(243, 'envira_gallery_review', 'a:2:{s:4:\"time\";i:1531943025;s:9:\"dismissed\";b:0;}', 'yes'),
(249, 'theme_mods_twentyfifteen', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1531945194;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-1\";a:0:{}}}}', 'yes'),
(256, 'foogallery_extensions_slugs', 'a:10:{i:0;s:17:\"default_templates\";i:1;s:6:\"albums\";i:2;s:8:\"foovideo\";i:3;s:6:\"foobox\";i:4;s:15:\"custom_branding\";i:5;s:7:\"nextgen\";i:6;s:24:\"foogallery-zoom-template\";i:7;s:21:\"foobox-image-lightbox\";i:8;s:32:\"foogallery-owl-carousel-template\";i:9;s:10:\"media_menu\";}', 'yes'),
(259, 'foogallery_extensions_activated', 'a:1:{s:17:\"default_templates\";s:38:\"FooGallery_Default_Templates_Extension\";}', 'yes'),
(260, 'foogallery_extensions_auto_activated', '1', 'yes'),
(263, 'foogallery-version', '1.4.31', 'no'),
(266, 'widget_foogallery_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(267, 'foogallery-thumb-test', 'a:2:{s:3:\"key\";s:25:\"php$(5.6.23}-http://wsite\";s:7:\"results\";a:4:{s:7:\"success\";b:1;s:5:\"thumb\";s:82:\"http://wsite/wp-content/uploads/cache/plugins/foogallery/assets/logo/546443246.png\";s:5:\"error\";s:0:\"\";s:9:\"file_info\";a:2:{s:3:\"ext\";s:3:\"png\";s:4:\"type\";s:9:\"image/png\";}}}', 'yes'),
(272, 'wd_bwg_version', '1.4.15', 'no'),
(273, 'wd_bwg_theme_version', '1.0.0', 'no'),
(274, 'widget_bwp_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(275, 'widget_bwp_gallery_slideshow', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(277, 'bwg_admin_notice', 'a:1:{s:15:\"two_week_review\";a:2:{s:5:\"start\";s:8:\"8/1/2018\";s:3:\"int\";i:14;}}', 'yes'),
(278, 'bwg_subscribe_done', '1', 'yes'),
(302, 'tiled_galleries', '', 'yes'),
(303, 'carousel_enable_it', '1', 'yes'),
(304, 'carousel_background_color', 'black', 'yes'),
(305, 'carousel_display_exif', '1', 'yes'),
(306, 'comments_display', '1', 'yes'),
(307, 'fullsize_display', '1', 'yes'),
(321, 'gdgallery_version', '1.0.5', 'yes'),
(322, 'widget_gdgallery_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(328, 'gallery_categories_children', 'a:0:{}', 'yes'),
(369, 'flag_options', 'a:31:{s:11:\"galleryPath\";s:22:\"wp-content/flagallery/\";s:9:\"swfUpload\";b:0;s:9:\"deleteImg\";b:1;s:9:\"deepLinks\";b:1;s:10:\"access_key\";s:0:\"\";s:11:\"license_key\";s:0:\"\";s:12:\"license_name\";s:0:\"\";s:11:\"useMediaRSS\";b:0;s:9:\"gp_jscode\";s:0:\"\";s:7:\"albSort\";s:5:\"title\";s:10:\"albSortDir\";s:3:\"ASC\";s:10:\"albPerPage\";s:2:\"50\";s:7:\"galSort\";s:9:\"sortorder\";s:10:\"galSortDir\";s:3:\"ASC\";s:11:\"skinsDirABS\";s:64:\"C:/OpenServer/domains/wsite/wp-content/plugins/flagallery-skins/\";s:11:\"skinsDirURL\";s:49:\"http://wsite/wp-content/plugins/flagallery-skins/\";s:9:\"flashSkin\";s:7:\"phantom\";s:10:\"flashWidth\";s:4:\"100%\";s:11:\"flashHeight\";s:3:\"500\";s:8:\"imgWidth\";i:2200;s:9:\"imgHeight\";i:2200;s:10:\"imgQuality\";i:87;s:10:\"thumbWidth\";i:400;s:11:\"thumbHeight\";i:400;s:12:\"thumbQuality\";i:100;s:10:\"mpAutoplay\";s:5:\"false\";s:10:\"vmAutoplay\";s:5:\"false\";s:7:\"vmWidth\";s:3:\"640\";s:8:\"vmHeight\";s:3:\"480\";s:8:\"advanced\";b:0;s:11:\"installDate\";i:1531959265;}', 'yes'),
(370, 'flag_db_version', '5.0', 'yes'),
(371, 'flagVersion', '5.2.4', 'yes'),
(372, 'flag_plugin_error', '', 'yes'),
(373, 'widget_flag-grandpages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(374, 'widget_flag-banner', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(375, 'widget_flag-images', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(376, 'widget_flag-video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(377, 'widget_flag-music', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(380, 'photonic_options', 'a:1:{s:14:\"disable_editor\";s:2:\"on\";}', 'yes'),
(382, 'widget_rbs_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(383, 'RoboGalleryInstallDate', '1533909065', 'yes'),
(384, 'RoboGalleryInstallVersion', '2.8.2', 'yes'),
(385, 'rbs_gallery_db_version', '2.8.2', 'yes'),
(405, 'do_activate', '0', 'yes'),
(422, 'my_responsive_photo_gallery_slider_settings', 'a:27:{s:16:\"transition_speed\";s:4:\"1000\";s:19:\"transition_interval\";s:4:\"4000\";s:11:\"show_panels\";s:1:\"1\";s:14:\"show_panel_nav\";s:1:\"1\";s:15:\"enable_overlays\";s:1:\"0\";s:11:\"panel_width\";s:3:\"550\";s:12:\"panel_height\";s:3:\"400\";s:15:\"panel_animation\";s:4:\"fade\";s:11:\"panel_scale\";s:4:\"crop\";s:16:\"overlay_position\";s:6:\"bottom\";s:10:\"pan_images\";s:1:\"1\";s:9:\"pan_style\";s:4:\"drag\";s:11:\"start_frame\";s:1:\"1\";s:14:\"show_filmstrip\";s:1:\"1\";s:18:\"show_filmstrip_nav\";s:1:\"0\";s:16:\"enable_slideshow\";s:1:\"1\";s:8:\"autoplay\";s:1:\"1\";s:18:\"filmstrip_position\";s:6:\"bottom\";s:11:\"frame_width\";i:80;s:12:\"frame_height\";i:80;s:13:\"frame_opacity\";d:0.40000000000000002;s:11:\"frame_scale\";s:4:\"crop\";s:15:\"filmstrip_style\";s:6:\"scroll\";s:9:\"frame_gap\";i:1;s:13:\"show_captions\";i:0;s:12:\"show_infobar\";i:0;s:15:\"infobar_opacity\";i:1;}', 'yes'),
(423, 'rjg_settings', 'a:6:{s:15:\"BackgroundColor\";s:7:\"#FFFFFF\";s:11:\"imageheight\";i:160;s:11:\"imageMargin\";i:5;s:9:\"page_size\";i:50;s:18:\"show_hover_caption\";i:1;s:15:\"show_hover_icon\";i:1;}', 'yes'),
(424, 'my_responsive_photo_gallery_slider_settings_messages', 'a:0:{}', 'yes'),
(458, 'jetpack_constants_sync_checksum', 'a:19:{s:16:\"EMPTY_TRASH_DAYS\";i:-1821685917;s:17:\"WP_POST_REVISIONS\";i:-33796979;s:26:\"AUTOMATIC_UPDATER_DISABLED\";i:634125391;s:7:\"ABSPATH\";i:-603043251;s:14:\"WP_CONTENT_DIR\";i:-80664438;s:9:\"FS_METHOD\";i:634125391;s:18:\"DISALLOW_FILE_EDIT\";i:634125391;s:18:\"DISALLOW_FILE_MODS\";i:634125391;s:19:\"WP_AUTO_UPDATE_CORE\";i:634125391;s:22:\"WP_HTTP_BLOCK_EXTERNAL\";i:634125391;s:19:\"WP_ACCESSIBLE_HOSTS\";i:634125391;s:16:\"JETPACK__VERSION\";i:-2118014162;s:12:\"IS_PRESSABLE\";i:634125391;s:15:\"DISABLE_WP_CRON\";i:634125391;s:17:\"ALTERNATE_WP_CRON\";i:634125391;s:20:\"WP_CRON_LOCK_TIMEOUT\";i:-300109018;s:11:\"PHP_VERSION\";i:1910822128;s:15:\"WP_MEMORY_LIMIT\";i:-1229557325;s:19:\"WP_MAX_MEMORY_LIMIT\";i:828812020;}', 'yes'),
(461, 'jetpack_sync_https_history_main_network_site_url', 'a:1:{i:0;s:4:\"http\";}', 'yes'),
(462, 'jetpack_sync_https_history_site_url', 'a:2:{i:0;s:4:\"http\";i:1;s:4:\"http\";}', 'yes'),
(463, 'jetpack_sync_https_history_home_url', 'a:2:{i:0;s:4:\"http\";i:1;s:4:\"http\";}', 'yes'),
(496, 'jetpack_callables_sync_checksum', 'a:30:{s:18:\"wp_max_upload_size\";i:869269328;s:15:\"is_main_network\";i:734881840;s:13:\"is_multi_site\";i:734881840;s:17:\"main_network_site\";i:602382626;s:8:\"site_url\";i:602382626;s:8:\"home_url\";i:602382626;s:16:\"single_user_site\";i:-33796979;s:7:\"updates\";i:-869524094;s:28:\"has_file_system_write_access\";i:-33796979;s:21:\"is_version_controlled\";i:734881840;s:10:\"taxonomies\";i:-978751696;s:10:\"post_types\";i:-1992279330;s:18:\"post_type_features\";i:1401777283;s:10:\"shortcodes\";i:152203370;s:27:\"rest_api_allowed_post_types\";i:-1464794550;s:32:\"rest_api_allowed_public_metadata\";i:223132457;s:24:\"sso_is_two_step_required\";i:734881840;s:26:\"sso_should_hide_login_form\";i:734881840;s:18:\"sso_match_by_email\";i:-33796979;s:21:\"sso_new_user_override\";i:734881840;s:29:\"sso_bypass_default_login_form\";i:734881840;s:10:\"wp_version\";i:-557638140;s:11:\"get_plugins\";i:-1987300090;s:24:\"get_plugins_action_links\";i:223132457;s:14:\"active_modules\";i:223132457;s:16:\"hosting_provider\";i:769900095;s:6:\"locale\";i:1550596449;s:13:\"site_icon_url\";i:734881840;s:5:\"roles\";i:-38612725;s:8:\"timezone\";i:-936608382;}', 'no'),
(497, 'jpsq_sync_checkout', '0:0', 'no'),
(529, 'alm_version', '3.5.1', 'yes'),
(530, 'alm_settings', 'a:10:{s:19:\"_alm_container_type\";s:1:\"1\";s:14:\"_alm_classname\";s:0:\"\";s:16:\"_alm_disable_css\";s:1:\"0\";s:14:\"_alm_btn_color\";s:4:\"grey\";s:18:\"_alm_btn_classname\";s:0:\"\";s:15:\"_alm_inline_css\";s:1:\"1\";s:15:\"_alm_scroll_top\";s:1:\"0\";s:20:\"_alm_disable_dynamic\";s:1:\"0\";s:13:\"_alm_hide_btn\";s:1:\"0\";s:18:\"_alm_error_notices\";s:1:\"1\";}', 'yes'),
(777, 'category_children', 'a:0:{}', 'yes'),
(803, 'widget_akismet_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(804, 'akismet_strictness', '1', 'yes'),
(805, 'akismet_show_user_comments_approved', '1', 'yes'),
(806, 'akismet_comment_form_privacy_notice', 'display', 'yes'),
(813, 'wpcf7', 'a:3:{s:7:\"version\";s:5:\"5.0.5\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1573247599;s:7:\"version\";s:5:\"5.0.5\";s:11:\"count_valid\";i:2;s:13:\"count_invalid\";i:0;}s:9:\"recaptcha\";a:1:{s:40:\"6Lc1cK4ZAAAAAGWIH95tQAVbxILxG2Lz_ApAN1iO\";s:40:\"6Lc1cK4ZAAAAAK4mf5zg-016nYfWP2b0lboTNnF1\";}}', 'yes'),
(879, 'ecwd_old_events', '0', 'yes'),
(880, 'ecwd_default_calendar', '395', 'yes'),
(881, 'ecwd_settings_events', 'a:15:{s:20:\"event_count_per_cell\";s:1:\"3\";s:25:\"events_archive_page_order\";s:1:\"0\";s:36:\"change_events_archive_page_post_date\";s:1:\"0\";s:25:\"event_default_description\";s:84:\"Нет дополнительных деталей для этого события.\";s:11:\"events_date\";s:1:\"1\";s:11:\"long_events\";s:1:\"0\";s:20:\"related_events_count\";s:3:\"100\";s:15:\"events_in_popup\";s:1:\"1\";s:11:\"events_slug\";s:6:\"events\";s:10:\"event_slug\";s:5:\"event\";s:18:\"show_events_detail\";s:1:\"1\";s:14:\"events_new_tab\";s:1:\"0\";s:14:\"related_events\";s:1:\"1\";s:15:\"hide_old_events\";s:1:\"0\";s:19:\"use_custom_template\";s:1:\"0\";}', 'yes'),
(882, 'ecwd_version', '1.1.23', 'yes'),
(883, 'ecwd_scripts_key', '5b5cf219e7aa7', 'yes'),
(884, 'ecwd_settings_general', 'a:13:{s:9:\"time_zone\";s:18:\"Europe/Kaliningrad\";s:14:\"show_time_zone\";s:1:\"1\";s:11:\"date_format\";s:0:\"\";s:11:\"time_format\";s:0:\"\";s:9:\"time_type\";s:0:\"\";s:16:\"list_date_format\";s:5:\"d.F.l\";s:11:\"week_starts\";s:1:\"1\";s:14:\"enable_rewrite\";s:1:\"1\";s:9:\"cpt_order\";s:9:\"post_date\";s:15:\"cat_title_color\";s:1:\"1\";s:16:\"move_first_image\";s:1:\"1\";s:28:\"event_description_max_length\";s:0:\"\";s:13:\"save_settings\";i:1;}', 'yes'),
(885, 'widget_ecwd_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(886, 'ecwd_slug_changed', '0', 'yes'),
(887, 'ecwd_single_slug', 'event', 'yes'),
(888, 'ecwd_slug', 'events', 'yes'),
(889, 'ecwd_cpt_setup', '1', 'yes'),
(890, 'ecwd_settings', '', 'yes'),
(894, 'ecwd_subscribe_done', '1', 'yes'),
(905, 'cf7dp_ui_theme', 'smoothness', 'yes'),
(912, 'tribe_events_calendar_options', 'a:6:{s:14:\"schema-version\";s:3:\"3.9\";s:27:\"recurring_events_are_hidden\";s:6:\"hidden\";s:21:\"previous_ecp_versions\";a:2:{i:0;s:1:\"0\";i:1;s:8:\"4.6.20.1\";}s:18:\"latest_ecp_version\";s:8:\"4.6.26.1\";s:39:\"last-update-message-the-events-calendar\";s:8:\"4.6.20.1\";s:16:\"tribeEnableViews\";a:3:{i:0;s:4:\"list\";i:1;s:5:\"month\";i:2;s:3:\"day\";}}', 'yes'),
(913, 'widget_tribe-events-list-widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(914, 'tribe_last_save_post', '1543292953', 'yes'),
(961, 'backwpup_cfg_hash', '5b67ea', 'no'),
(962, 'backwpup_jobs', 'a:1:{i:2;a:33:{s:5:\"jobid\";i:2;s:10:\"backuptype\";s:7:\"archive\";s:4:\"type\";a:5:{i:0;s:7:\"DBCHECK\";i:1;s:6:\"DBDUMP\";i:2;s:4:\"FILE\";i:3;s:5:\"WPEXP\";i:4;s:8:\"WPPLUGIN\";}s:12:\"destinations\";a:1:{i:0;s:6:\"FOLDER\";}s:4:\"name\";s:6:\"task#1\";s:14:\"mailaddresslog\";s:22:\"capitan.flin@yandex.ru\";s:20:\"mailaddresssenderlog\";s:41:\"BackWPup stylist <capitan.flin@yandex.ru>\";s:13:\"mailerroronly\";b:1;s:13:\"archiveformat\";s:7:\".tar.gz\";s:17:\"archiveencryption\";b:0;s:11:\"archivename\";s:24:\"%Y-%m-%d_%H-%i-%s_%hash%\";s:9:\"backupdir\";s:32:\"uploads/backwpup-5b67ea-backups/\";s:10:\"maxbackups\";i:15;s:18:\"backupsyncnodelete\";b:0;s:7:\"lastrun\";d:1594160625;s:7:\"logfile\";s:111:\"C:/OSPanel/domains/stylist/wp-content/uploads/backwpup-5b67ea-logs/backwpup_log_5b67ea_2020-07-07_22-23-45.html\";s:21:\"lastbackupdownloadurl\";s:0:\"\";s:11:\"lastruntime\";d:26;s:11:\"fileexclude\";s:51:\".DS_Store,.git,.svn,.tmp,/node_modules/,desktop.ini\";s:10:\"dirinclude\";s:0:\"\";s:19:\"backupexcludethumbs\";b:0;s:18:\"backupspecialfiles\";b:1;s:10:\"backuproot\";b:1;s:17:\"backupabsfolderup\";b:0;s:13:\"backupcontent\";b:1;s:13:\"backupplugins\";b:1;s:12:\"backupthemes\";b:1;s:13:\"backupuploads\";b:1;s:21:\"backuprootexcludedirs\";a:0:{}s:24:\"backupcontentexcludedirs\";a:1:{i:0;s:7:\"upgrade\";}s:24:\"backuppluginsexcludedirs\";a:0:{}s:23:\"backupthemesexcludedirs\";a:0:{}s:24:\"backupuploadsexcludedirs\";a:1:{i:0;s:20:\"backwpup-5b67ea-logs\";}}}', 'no'),
(963, 'backwpup_version', '3.6.3', 'no'),
(964, 'backwpup_cfg_showadminbar', '', 'no'),
(965, 'backwpup_cfg_showfoldersize', '', 'no'),
(966, 'backwpup_cfg_protectfolders', '1', 'no'),
(967, 'backwpup_cfg_jobmaxexecutiontime', '30', 'no'),
(968, 'backwpup_cfg_jobstepretry', '3', 'no'),
(969, 'backwpup_cfg_jobrunauthkey', '1858b8f6', 'no'),
(970, 'backwpup_cfg_loglevel', 'normal_translated', 'no'),
(971, 'backwpup_cfg_jobwaittimems', '0', 'no'),
(972, 'backwpup_cfg_jobdooutput', '0', 'no'),
(973, 'backwpup_cfg_windows', '0', 'no'),
(974, 'backwpup_cfg_maxlogs', '30', 'no'),
(975, 'backwpup_cfg_gzlogs', '0', 'no'),
(976, 'backwpup_cfg_logfolder', 'uploads/backwpup-5b67ea-logs/', 'no'),
(977, 'backwpup_cfg_httpauthuser', '', 'no'),
(978, 'backwpup_cfg_httpauthpassword', '', 'no'),
(980, 'backwpup_message_id_en', 'I5SXIIBSGUSSA3TPO4QXY2DUORYHGORPF5UW44DTPFSGKLTDN5WS6ZLOF5ZWC3DFH52XI3K7NVSWI2LVNU6WEYLONZSXEJTVORWV643POVZGGZJ5MFSG22LONZXXI2LDMUTHK5DNL5RWC3LQMFUWO3R5NFSHGMRQEZ2XI3K7MNXW45DFNZ2D2YTBMNVXO4DVOB6HY7D4', 'no'),
(981, 'backwpup_message_content_en', 'Stay independent with BackWPup PRO: Automatic restore and backup encryption! Buy now and get a 25% Independence Day discount! Only valid until July 13th.', 'no'),
(982, 'backwpup_message_button_text_en', 'Get 25% now!', 'no'),
(983, 'backwpup_message_url_en', 'https://inpsyde.com/en/sale?utm_medium=banner&utm_source=adminnotice&utm_campaign=ids20&utm_content=backwpup', 'no'),
(984, 'backwpup_message_id_es', 'YKQU6YTUYOUW4IDVNYQDENJFEBSGKIDEMVZWG5LFNZ2G6IDBNBXXEYJBPRUHI5DQOM5C6L3JNZYHG6LEMUXGG33NF5SW4L3TMFWGKP3VORWV63LFMRUXK3J5MJQW43TFOITHK5DNL5ZW65LSMNST2YLENVUW43TPORUWGZJGOV2G2X3DMFWXAYLJM5XD22LEOMZDAJTVORWV6Y3PNZ2GK3TUHVRGCY3LO5YHK4D4', 'no'),
(985, 'backwpup_message_content_es', 'Mantente independiente con BackWPup PRO: restauración automática y copias de seguridad cifradas. ¡Compra ahora y obtén un descuento del 25% en el Día de la Independencia! Solo válido hasta el 13 de julio.', 'no'),
(986, 'backwpup_message_button_text_es', '¡Obtén un 25% de descuento ahora!', 'no'),
(987, 'backwpup_message_url_es', 'https://inpsyde.com/en/sale?utm_medium=banner&utm_source=adminnotice&utm_campaign=ids20&utm_content=backwpup', 'no'),
(988, 'backwpup_message_id_fr', 'I5SXIIBSGUSSA3TPO4QXY2DUORYHGORPF5UW44DTPFSGKLTDN5WS6ZLOF5ZWC3DFH52XI3K7NVSWI2LVNU6WEYLONZSXEJTVORWV643POVZGGZJ5MFSG22LONZXXI2LDMUTHK5DNL5RWC3LQMFUWO3R5NFSHGMRQEZ2XI3K7MNXW45DFNZ2D2YTBMNVXO4DVOB6HY7D4', 'no'),
(989, 'backwpup_message_content_fr', 'Stay independent with BackWPup PRO: Automatic restore and backup encryption! Buy now and get a 25% Independence Day discount! Only valid until July 13th.', 'no'),
(990, 'backwpup_message_button_text_fr', 'Get 25% now!', 'no'),
(991, 'backwpup_message_url_fr', 'https://inpsyde.com/en/sale?utm_medium=banner&utm_source=adminnotice&utm_campaign=ids20&utm_content=backwpup', 'no'),
(992, 'backwpup_message_id_de', 'JJSXI6TUEAZDKJJAONUWG2DFOJXCC7DIOR2HA4Z2F4XWS3TQON4WIZJOMNXW2L3FNYXXGYLMMU7XK5DNL5WWKZDJOVWT2YTBNZXGK4RGOV2G2X3TN52XEY3FHVQWI3LJNZXG65DJMNSSM5LUNVPWGYLNOBQWSZ3OHVUWI4ZSGATHK5DNL5RW63TUMVXHIPLCMFRWW53QOVYHY7D4', 'no'),
(993, 'backwpup_message_content_de', 'Mit BackWPup PRO unabhängig bleiben: Automatische Datenwiederherstellung und Backup-Verschlüsselung! Jetzt kaufen und 25% Independence Day Rabatt erhalten! Nur gültig bis zum 13. Juli.', 'no'),
(994, 'backwpup_message_button_text_de', 'Jetzt 25% sichern!', 'no'),
(995, 'backwpup_message_url_de', 'https://inpsyde.com/en/sale?utm_medium=banner&utm_source=adminnotice&utm_campaign=ids20&utm_content=backwpup', 'no'),
(996, 'backwpup_message_id_it', 'IFRXC5LJON2GCIDPOJQSAY3PNYQGS3BAGI2SKIDENEQHGY3PNZ2G6IL4NB2HI4DTHIXS62LOOBZXSZDFFZRW63JPMVXC643BNRST65LUNVPW2ZLENF2W2PLCMFXG4ZLSEZ2XI3K7ONXXK4TDMU6WCZDNNFXG433UNFRWKJTVORWV6Y3BNVYGC2LHNY6WSZDTGIYCM5LUNVPWG33OORSW45B5MJQWG23XOB2XA7D4', 'no'),
(997, 'backwpup_message_content_it', 'Resta indipendente con BackWPup PRO: funzionalità di ripristino automatica e criptazione dei tuoi backup! Acquista ora e ottieni uno sconto del 25% per celebrare il Giorno dell\'Indipendenza! Offerta valida solo fino al 13 luglio.', 'no'),
(998, 'backwpup_message_button_text_it', 'Acquista ora con il 25% di sconto!', 'no'),
(999, 'backwpup_message_url_it', 'https://inpsyde.com/en/sale?utm_medium=banner&utm_source=adminnotice&utm_campaign=ids20&utm_content=backwpup', 'no'),
(1000, 'inpsyde-phone-consent-given-BackWPup', 'a:4:{s:6:\"plugin\";s:8:\"BackWPup\";s:10:\"identifier\";s:32:\"9090faafadff1d430bf47633a3bd3603\";s:11:\"php_version\";s:6:\"5.6.30\";s:10:\"wp_version\";s:5:\"4.9.7\";}', 'no'),
(1020, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:22:\"capitan.flin@yandex.ru\";s:7:\"version\";s:6:\"4.9.12\";s:9:\"timestamp\";i:1572970472;}', 'no'),
(1025, 'widget_gce_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1030, 'simple-calendar_version', '3.1.19', 'yes'),
(1064, 'new_admin_email', 'capitan.flin@yandex.ru', 'yes'),
(1081, 'backwpup_messages', 'a:0:{}', 'no'),
(1212, 'wd_seo_notice_status', '1', 'no'),
(1249, 'calendarjsupdated', '0', 'no'),
(1250, 'jswidgetupdated', '0', 'no'),
(1303, 'booking_activation_process', 'Off', 'yes'),
(1304, 'booking_admin_cal_count', '2', 'yes'),
(1305, 'booking_skin', '/css/skins/black.css', 'yes'),
(1306, 'booking_num_per_page', '50', 'yes'),
(1307, 'booking_sort_order', '', 'yes'),
(1308, 'booking_default_toolbar_tab', 'actions', 'yes'),
(1309, 'booking_listing_default_view_mode', 'vm_calendar', 'yes'),
(1310, 'booking_view_days_num', '90', 'yes'),
(1311, 'booking_max_monthes_in_calendar', '1y', 'yes'),
(1312, 'booking_client_cal_count', '1', 'yes'),
(1313, 'booking_start_day_weeek', '0', 'yes'),
(1314, 'booking_title_after_reservation', 'Спасибо за онлайн бронирование.  Мы вышлем Вам подтверждение как можно скорее.', 'yes'),
(1315, 'booking_title_after_reservation_time', '2000', 'yes'),
(1316, 'booking_type_of_thank_you_message', 'message', 'yes'),
(1317, 'booking_thank_you_page_URL', '/thank-you', 'yes'),
(1318, 'booking_is_use_autofill_4_logged_user', 'Off', 'yes'),
(1319, 'booking_date_format', 'd.m.Y', 'yes'),
(1320, 'booking_date_view_type', 'wide', 'yes'),
(1321, 'booking_is_delete_if_deactive', 'Off', 'yes'),
(1322, 'booking_dif_colors_approval_pending', 'On', 'yes'),
(1323, 'booking_is_use_hints_at_admin_panel', 'On', 'yes'),
(1324, 'booking_is_not_load_bs_script_in_client', 'Off', 'yes'),
(1325, 'booking_is_not_load_bs_script_in_admin', 'Off', 'yes'),
(1326, 'booking_is_load_js_css_on_specific_pages', 'Off', 'yes'),
(1327, 'booking_is_show_system_debug_log', 'Off', 'yes'),
(1328, 'booking_pages_for_load_js_css', '', 'yes'),
(1329, 'booking_type_of_day_selections', 'multiple', 'yes'),
(1330, 'booking_timeslot_day_bg_as_available', 'Off', 'yes'),
(1331, 'booking_form_is_using_bs_css', 'On', 'yes'),
(1332, 'booking_form_format_type', 'vertical', 'yes'),
(1333, 'booking_form_field_active1', 'On', 'yes'),
(1334, 'booking_form_field_required1', 'On', 'yes'),
(1335, 'booking_form_field_label1', 'First Name', 'yes'),
(1336, 'booking_form_field_active2', 'On', 'yes'),
(1337, 'booking_form_field_required2', 'On', 'yes'),
(1338, 'booking_form_field_label2', 'Last Name', 'yes'),
(1339, 'booking_form_field_active3', 'On', 'yes'),
(1340, 'booking_form_field_required3', 'On', 'yes'),
(1341, 'booking_form_field_label3', 'Email', 'yes'),
(1342, 'booking_form_field_active4', 'On', 'yes'),
(1343, 'booking_form_field_required4', 'Off', 'yes'),
(1344, 'booking_form_field_label4', 'Phone', 'yes'),
(1345, 'booking_form_field_active5', 'On', 'yes'),
(1346, 'booking_form_field_required5', 'Off', 'yes'),
(1347, 'booking_form_field_label5', 'Details', 'yes'),
(1348, 'booking_form_field_active6', 'Off', 'yes'),
(1349, 'booking_form_field_required6', 'Off', 'yes'),
(1350, 'booking_form_field_label6', 'Visitors', 'yes'),
(1351, 'booking_form_field_values6', '1\n2\n3\n4', 'yes'),
(1352, 'booking_is_days_always_available', 'Off', 'yes'),
(1353, 'booking_is_show_pending_days_as_available', 'Off', 'yes'),
(1354, 'booking_check_on_server_if_dates_free', 'Off', 'yes'),
(1355, 'booking_unavailable_days_num_from_today', '0', 'yes'),
(1356, 'booking_unavailable_day0', 'Off', 'yes'),
(1357, 'booking_unavailable_day1', 'Off', 'yes'),
(1358, 'booking_unavailable_day2', 'Off', 'yes'),
(1359, 'booking_unavailable_day3', 'Off', 'yes'),
(1360, 'booking_unavailable_day4', 'Off', 'yes'),
(1361, 'booking_unavailable_day5', 'Off', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES 
(1362, 'booking_unavailable_day6', 'Off', 'yes'),
(1363, 'booking_menu_position', 'top', 'yes'),
(1364, 'booking_user_role_booking', 'editor', 'yes'),
(1365, 'booking_user_role_addbooking', 'editor', 'yes'),
(1366, 'booking_user_role_resources', 'editor', 'yes'),
(1367, 'booking_user_role_settings', 'administrator', 'yes'),
(1368, 'booking_is_email_reservation_adress', 'On', 'yes'),
(1369, 'booking_email_reservation_adress', '&quot;Booking system&quot; &lt;capitan.flin@yandex.ru&gt;', 'yes'),
(1370, 'booking_email_reservation_from_adress', '[visitoremail]', 'yes'),
(1371, 'booking_email_reservation_subject', 'Новое бронирование', 'yes'),
(1372, 'booking_email_reservation_content', 'Вы должны подтвердить новую бронь [bookingtype] на: [dates]&lt;br/&gt;&lt;br/&gt; Подробная информация о заказчике:&lt;br/&gt; [content]&lt;br/&gt;&lt;br/&gt; В настоящее время новый заказ ждет одобрения! Пожалуйста, посетите панель модерирования! [moderatelink]&lt;br/&gt;&lt;br/&gt;Спасибо, stylist&lt;br/&gt;[siteurl]', 'yes'),
(1373, 'booking_is_email_newbookingbyperson_adress', 'Off', 'yes'),
(1374, 'booking_email_newbookingbyperson_adress', '&quot;Booking system&quot; &lt;capitan.flin@yandex.ru&gt;', 'yes'),
(1375, 'booking_email_newbookingbyperson_subject', 'Новое бронирование', 'yes'),
(1376, 'booking_email_newbookingbyperson_content', 'Ваша бронь [bookingtype] на: [dates] в процессе проверки! Пожалуйста, дождитесь письма, подтверждающего Вашу бронь. &lt;br/&gt;&lt;br/&gt;[content]&lt;br/&gt;&lt;br/&gt;  Спасибо, stylist&lt;br/&gt;[siteurl]', 'yes'),
(1377, 'booking_is_email_approval_adress', 'On', 'yes'),
(1378, 'booking_is_email_approval_send_copy_to_admin', 'Off', 'yes'),
(1379, 'booking_email_approval_adress', '&quot;Booking system&quot; &lt;capitan.flin@yandex.ru&gt;', 'yes'),
(1380, 'booking_email_approval_subject', 'Бронирование подтверждено', 'yes'),
(1381, 'booking_email_approval_content', 'Ваша бронь [bookingtype] на: [dates] была подтверждена. &lt;br/&gt;&lt;br/&gt;[content]&lt;br/&gt;&lt;br/&gt;Спасибо, stylist&lt;br/&gt;[siteurl]', 'yes'),
(1382, 'booking_is_email_deny_adress', 'On', 'yes'),
(1383, 'booking_is_email_deny_send_copy_to_admin', 'Off', 'yes'),
(1384, 'booking_email_deny_adress', '&quot;Booking system&quot; &lt;capitan.flin@yandex.ru&gt;', 'yes'),
(1385, 'booking_email_deny_subject', 'Ваша бронь отклонена', 'yes'),
(1386, 'booking_email_deny_content', 'Ваша бронь [bookingtype] на: [dates] была отклонена.  &lt;br/&gt;[denyreason]&lt;br/&gt;&lt;br/&gt;[content]&lt;br/&gt;&lt;br/&gt;Спасибо, stylist&lt;br/&gt;[siteurl]', 'yes'),
(1387, 'booking_widget_title', 'Форма бронирования', 'yes'),
(1388, 'booking_widget_show', 'booking_form', 'yes'),
(1389, 'booking_widget_type', '1', 'yes'),
(1390, 'booking_widget_calendar_count', '1', 'yes'),
(1391, 'booking_widget_last_field', '', 'yes'),
(1392, 'booking_wpdev_copyright_adminpanel', 'On', 'yes'),
(1393, 'booking_is_show_powered_by_notice', 'On', 'yes'),
(1394, 'booking_is_use_captcha', 'On', 'yes'),
(1395, 'booking_is_show_legend', 'Off', 'yes'),
(1396, 'booking_legend_is_show_item_available', 'On', 'yes'),
(1397, 'booking_legend_text_for_item_available', 'Доступно', 'yes'),
(1398, 'booking_legend_is_show_item_pending', 'On', 'yes'),
(1399, 'booking_legend_text_for_item_pending', 'Ожидающие подтверждения', 'yes'),
(1400, 'booking_legend_is_show_item_approved', 'On', 'yes'),
(1401, 'booking_legend_text_for_item_approved', 'Забронировано', 'yes'),
(1402, 'booking_legend_is_show_numbers', 'Off', 'yes'),
(1403, 'booking_email_new_admin', 'a:15:{s:7:\"enabled\";s:2:\"On\";s:2:\"to\";s:22:\"capitan.flin@yandex.ru\";s:7:\"to_name\";s:14:\"Booking system\";s:4:\"from\";s:22:\"capitan.flin@yandex.ru\";s:9:\"from_name\";s:14:\"Booking system\";s:7:\"subject\";s:35:\"Новое бронирование\";s:7:\"content\";s:414:\"Вы должны подтвердить новую бронь [bookingtype] на: [dates]<br/><br/> Подробная информация о заказчике:<br/> [content]<br/><br/> В настоящее время новый заказ ждет одобрения! Пожалуйста, посетите панель модерирования! [moderatelink]<br/><br/>Спасибо, stylist<br/>[siteurl]\";s:14:\"header_content\";s:0:\"\";s:14:\"footer_content\";s:0:\"\";s:13:\"template_file\";s:5:\"plain\";s:10:\"base_color\";s:7:\"#557da1\";s:16:\"background_color\";s:7:\"#f5f5f5\";s:10:\"body_color\";s:7:\"#fdfdfd\";s:10:\"text_color\";s:7:\"#505050\";s:18:\"email_content_type\";s:4:\"html\";}', 'yes'),
(1404, 'booking_email_new_visitor', 'a:13:{s:7:\"enabled\";s:2:\"On\";s:4:\"from\";s:22:\"capitan.flin@yandex.ru\";s:9:\"from_name\";s:14:\"Booking system\";s:7:\"subject\";s:35:\"Новое бронирование\";s:7:\"content\";s:261:\"Ваша бронь [bookingtype] на: [dates] в процессе проверки! Пожалуйста, дождитесь письма, подтверждающего Вашу бронь. <br/><br/>[content]<br/><br/>  Спасибо, stylist<br/>[siteurl]\";s:14:\"header_content\";s:0:\"\";s:14:\"footer_content\";s:0:\"\";s:13:\"template_file\";s:5:\"plain\";s:10:\"base_color\";s:7:\"#557da1\";s:16:\"background_color\";s:7:\"#f5f5f5\";s:10:\"body_color\";s:7:\"#fdfdfd\";s:10:\"text_color\";s:7:\"#505050\";s:18:\"email_content_type\";s:4:\"html\";}', 'yes'),
(1405, 'booking_email_approved', 'a:16:{s:7:\"enabled\";s:2:\"On\";s:13:\"copy_to_admin\";s:3:\"Off\";s:2:\"to\";s:22:\"capitan.flin@yandex.ru\";s:7:\"to_name\";s:14:\"Booking system\";s:4:\"from\";s:22:\"capitan.flin@yandex.ru\";s:9:\"from_name\";s:14:\"Booking system\";s:7:\"subject\";s:49:\"Бронирование подтверждено\";s:7:\"content\";s:149:\"Ваша бронь [bookingtype] на: [dates] была подтверждена. <br/><br/>[content]<br/><br/>Спасибо, stylist<br/>[siteurl]\";s:14:\"header_content\";s:0:\"\";s:14:\"footer_content\";s:0:\"\";s:13:\"template_file\";s:5:\"plain\";s:10:\"base_color\";s:7:\"#557da1\";s:16:\"background_color\";s:7:\"#f5f5f5\";s:10:\"body_color\";s:7:\"#fdfdfd\";s:10:\"text_color\";s:7:\"#505050\";s:18:\"email_content_type\";s:4:\"html\";}', 'yes'),
(1406, 'booking_email_deleted', 'a:16:{s:7:\"enabled\";s:2:\"On\";s:13:\"copy_to_admin\";s:3:\"Off\";s:2:\"to\";s:22:\"capitan.flin@yandex.ru\";s:7:\"to_name\";s:14:\"Booking system\";s:4:\"from\";s:22:\"capitan.flin@yandex.ru\";s:9:\"from_name\";s:14:\"Booking system\";s:7:\"subject\";s:38:\"Ваша бронь отклонена\";s:7:\"content\";s:161:\"Ваша бронь [bookingtype] на: [dates] была отклонена.  <br/>[denyreason]<br/><br/>[content]<br/><br/>Спасибо, stylist<br/>[siteurl]\";s:14:\"header_content\";s:0:\"\";s:14:\"footer_content\";s:0:\"\";s:13:\"template_file\";s:5:\"plain\";s:10:\"base_color\";s:7:\"#557da1\";s:16:\"background_color\";s:7:\"#f5f5f5\";s:10:\"body_color\";s:7:\"#fdfdfd\";s:10:\"text_color\";s:7:\"#505050\";s:18:\"email_content_type\";s:4:\"html\";}', 'yes'),
(1407, 'booking_email_deny', 'a:16:{s:7:\"enabled\";s:2:\"On\";s:13:\"copy_to_admin\";s:3:\"Off\";s:2:\"to\";s:22:\"capitan.flin@yandex.ru\";s:7:\"to_name\";s:14:\"Booking system\";s:4:\"from\";s:22:\"capitan.flin@yandex.ru\";s:9:\"from_name\";s:14:\"Booking system\";s:7:\"subject\";s:38:\"Ваша бронь отклонена\";s:7:\"content\";s:161:\"Ваша бронь [bookingtype] на: [dates] была отклонена.  <br/>[denyreason]<br/><br/>[content]<br/><br/>Спасибо, stylist<br/>[siteurl]\";s:14:\"header_content\";s:0:\"\";s:14:\"footer_content\";s:0:\"\";s:13:\"template_file\";s:5:\"plain\";s:10:\"base_color\";s:7:\"#557da1\";s:16:\"background_color\";s:7:\"#f5f5f5\";s:10:\"body_color\";s:7:\"#fdfdfd\";s:10:\"text_color\";s:7:\"#505050\";s:18:\"email_content_type\";s:4:\"html\";}', 'yes'),
(1408, 'booking_email_trash', 'a:16:{s:7:\"enabled\";s:2:\"On\";s:13:\"copy_to_admin\";s:3:\"Off\";s:2:\"to\";s:22:\"capitan.flin@yandex.ru\";s:7:\"to_name\";s:14:\"Booking system\";s:4:\"from\";s:22:\"capitan.flin@yandex.ru\";s:9:\"from_name\";s:14:\"Booking system\";s:7:\"subject\";s:38:\"Ваша бронь отклонена\";s:7:\"content\";s:161:\"Ваша бронь [bookingtype] на: [dates] была отклонена.  <br/>[denyreason]<br/><br/>[content]<br/><br/>Спасибо, stylist<br/>[siteurl]\";s:14:\"header_content\";s:0:\"\";s:14:\"footer_content\";s:0:\"\";s:13:\"template_file\";s:5:\"plain\";s:10:\"base_color\";s:7:\"#557da1\";s:16:\"background_color\";s:7:\"#f5f5f5\";s:10:\"body_color\";s:7:\"#fdfdfd\";s:10:\"text_color\";s:7:\"#505050\";s:18:\"email_content_type\";s:4:\"html\";}', 'yes'),
(1409, 'booking_form_structure_type', 'vertical', 'yes'),
(1410, 'booking_menu_go_pro', 'show', 'yes'),
(1411, 'booking_form', '<div class=\"wpbc_booking_form_structure wpbc_vertical\">\n  <div class=\"wpbc_structure_calendar\">\n    [calendar]\n  </div>\n  <div class=\"wpbc_structure_form\">\n     <p>First Name*:<br />[text* name]</p>\n     <p>Last Name*:<br />[text* secondname]</p>\n     <p>Email*:<br />[email* email]</p>\n     <p>Phone:<br />[text phone]</p>\n     <p>Details:<br />[textarea details]</p>\n     <p>[captcha]</p>\n     <p>[submit class:btn \"Send\"]</p>\n  </div>\n</div>\n<div class=\"wpbc_booking_form_footer\"></div>', 'yes'),
(1412, 'booking_form_show', '<div style=\"text-align:left;word-wrap: break-word;\">\n  <strong>First Name</strong>: <span class=\"fieldvalue\">[name]</span><br/>\n  <strong>Last Name</strong>: <span class=\"fieldvalue\">[secondname]</span><br/>\n  <strong>Email</strong>: <span class=\"fieldvalue\">[email]</span><br/>\n  <strong>Phone</strong>: <span class=\"fieldvalue\">[phone]</span><br/>\n  <strong>Details</strong>: <span class=\"fieldvalue\">[details]</span><br/>\n</div>', 'yes'),
(1413, 'booking_form_visual', 'a:9:{i:0;a:2:{s:4:\"type\";s:8:\"calendar\";s:10:\"obligatory\";s:2:\"On\";}i:1;a:6:{s:4:\"type\";s:4:\"text\";s:4:\"name\";s:4:\"name\";s:10:\"obligatory\";s:3:\"Off\";s:6:\"active\";s:2:\"On\";s:8:\"required\";s:2:\"On\";s:5:\"label\";s:10:\"First Name\";}i:2;a:6:{s:4:\"type\";s:4:\"text\";s:4:\"name\";s:10:\"secondname\";s:10:\"obligatory\";s:3:\"Off\";s:6:\"active\";s:2:\"On\";s:8:\"required\";s:2:\"On\";s:5:\"label\";s:9:\"Last Name\";}i:3;a:6:{s:4:\"type\";s:5:\"email\";s:4:\"name\";s:5:\"email\";s:10:\"obligatory\";s:2:\"On\";s:6:\"active\";s:2:\"On\";s:8:\"required\";s:2:\"On\";s:5:\"label\";s:5:\"Email\";}i:4;a:7:{s:4:\"type\";s:6:\"select\";s:4:\"name\";s:8:\"visitors\";s:10:\"obligatory\";s:3:\"Off\";s:6:\"active\";s:3:\"Off\";s:8:\"required\";s:3:\"Off\";s:5:\"label\";s:8:\"Visitors\";s:5:\"value\";s:7:\"1\n2\n3\n4\";}i:5;a:6:{s:4:\"type\";s:4:\"text\";s:4:\"name\";s:5:\"phone\";s:10:\"obligatory\";s:3:\"Off\";s:6:\"active\";s:2:\"On\";s:8:\"required\";s:3:\"Off\";s:5:\"label\";s:5:\"Phone\";}i:6;a:6:{s:4:\"type\";s:8:\"textarea\";s:4:\"name\";s:7:\"details\";s:10:\"obligatory\";s:3:\"Off\";s:6:\"active\";s:2:\"On\";s:8:\"required\";s:3:\"Off\";s:5:\"label\";s:7:\"Details\";}i:7;a:6:{s:4:\"type\";s:7:\"captcha\";s:4:\"name\";s:7:\"captcha\";s:10:\"obligatory\";s:2:\"On\";s:6:\"active\";s:3:\"Off\";s:8:\"required\";s:2:\"On\";s:5:\"label\";s:0:\"\";}i:8;a:6:{s:4:\"type\";s:6:\"submit\";s:4:\"name\";s:6:\"submit\";s:10:\"obligatory\";s:2:\"On\";s:6:\"active\";s:2:\"On\";s:8:\"required\";s:2:\"On\";s:5:\"label\";s:18:\"Отправить\";}}', 'yes'),
(1414, 'booking_gcal_feed', '', 'yes'),
(1415, 'booking_gcal_events_from', 'month-start', 'yes'),
(1416, 'booking_gcal_events_from_offset', '', 'yes'),
(1417, 'booking_gcal_events_from_offset_type', '', 'yes'),
(1418, 'booking_gcal_events_until', 'any', 'yes'),
(1419, 'booking_gcal_events_until_offset', '', 'yes'),
(1420, 'booking_gcal_events_until_offset_type', '', 'yes'),
(1421, 'booking_gcal_events_max', '25', 'yes'),
(1422, 'booking_gcal_api_key', '', 'yes'),
(1423, 'booking_gcal_timezone', '', 'yes'),
(1424, 'booking_gcal_is_send_email', 'Off', 'yes'),
(1425, 'booking_gcal_auto_import_is_active', 'Off', 'yes'),
(1426, 'booking_gcal_auto_import_time', '24', 'yes'),
(1427, 'booking_gcal_events_form_fields', 's:101:\"a:3:{s:5:\"title\";s:9:\"text^name\";s:11:\"description\";s:16:\"textarea^details\";s:5:\"where\";s:5:\"text^\";}\";', 'yes'),
(1428, 'booking_version_num', '8.4.2', 'yes'),
(1431, 'widget_bookingwidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1432, 'booking_activation_redirect_for_version', '8.4.2', 'yes'),
(1465, 'mc_style_vars', 'a:6:{s:14:\"--primary-dark\";s:7:\"#313233\";s:15:\"--primary-light\";s:4:\"#fff\";s:17:\"--secondary-light\";s:4:\"#fff\";s:16:\"--secondary-dark\";s:4:\"#000\";s:16:\"--highlight-dark\";s:4:\"#666\";s:17:\"--highlight-light\";s:7:\"#efefef\";}', 'yes'),
(1478, 'widget_my_calendar_today_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1479, 'widget_my_calendar_upcoming_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1480, 'widget_my_calendar_mini_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1481, 'widget_my_calendar_simple_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1482, 'widget_my_calendar_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1485, 'mc_cpt_base', 'mc-events', 'yes'),
(1777, 'widget_instagal_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1778, 'insta_gallery_items', 'a:1:{i:1;a:24:{s:12:\"insta_source\";s:3:\"tag\";s:12:\"insta_layout\";s:7:\"gallery\";s:14:\"insta_username\";s:0:\"\";s:9:\"insta_tag\";s:11:\"kaza4enkova\";s:11:\"insta_limit\";i:8;s:14:\"insta_gal-cols\";i:4;s:13:\"insta_spacing\";i:0;s:12:\"insta_button\";i:1;s:17:\"insta_button-text\";s:36:\"Посмотреть все фото\";s:23:\"insta_button-background\";s:0:\"\";s:29:\"insta_button-background-hover\";s:0:\"\";s:18:\"insta_car-slidespv\";i:5;s:18:\"insta_car-autoplay\";i:1;s:27:\"insta_car-autoplay-interval\";i:3000;s:19:\"insta_car-navarrows\";i:1;s:25:\"insta_car-navarrows-color\";s:0:\"\";s:20:\"insta_car-pagination\";i:1;s:26:\"insta_car-pagination-color\";s:0:\"\";s:10:\"insta_size\";s:6:\"medium\";s:11:\"insta_hover\";i:1;s:17:\"insta_hover-color\";s:0:\"\";s:11:\"insta_popup\";i:1;s:11:\"insta_likes\";i:1;s:14:\"insta_comments\";i:1;}}', 'no'),
(2270, 'wordpress_api_key', '3c117e4f60d1', 'yes'),
(2271, 'akismet_spam_count', '0', 'yes'),
(2675, 'tablepress_plugin_options', '{\"plugin_options_db_version\":37,\"table_scheme_db_version\":3,\"prev_tablepress_version\":\"1.9\",\"tablepress_version\":\"1.9.1\",\"first_activation\":1534340319,\"message_plugin_update\":false,\"message_donation_nag\":false,\"use_custom_css\":true,\"use_custom_css_file\":true,\"custom_css\":\"\",\"custom_css_minified\":\"\",\"custom_css_version\":0}', 'yes'),
(2684, 'tablepress_tables', '{\"last_id\":1,\"table_post\":{\"1\":433}}', 'yes'),
(4287, 'ts_token', '', 'yes'),
(4288, 'ts_chatid', '', 'yes'),
(4289, 'sent_wcm', '', 'yes'),
(4290, 'sent_key', 'a:1:{s:6:\"ts_key\";s:1:\"1\";}', 'yes'),
(4291, 'ts_newtoken', '51e5c148ee449fceeefda94b1c538d41', 'yes'),
(5300, 'aioseop_options', 'a:80:{s:16:\"aiosp_home_title\";N;s:22:\"aiosp_home_description\";s:0:\"\";s:20:\"aiosp_togglekeywords\";i:1;s:19:\"aiosp_home_keywords\";N;s:26:\"aiosp_use_static_home_info\";i:0;s:9:\"aiosp_can\";i:1;s:30:\"aiosp_no_paged_canonical_links\";i:0;s:31:\"aiosp_customize_canonical_links\";i:0;s:20:\"aiosp_rewrite_titles\";i:1;s:20:\"aiosp_force_rewrites\";i:1;s:24:\"aiosp_use_original_title\";i:0;s:28:\"aiosp_home_page_title_format\";s:12:\"%page_title%\";s:23:\"aiosp_page_title_format\";s:27:\"%page_title% | %blog_title%\";s:23:\"aiosp_post_title_format\";s:27:\"%post_title% | %blog_title%\";s:27:\"aiosp_category_title_format\";s:31:\"%category_title% | %blog_title%\";s:26:\"aiosp_archive_title_format\";s:30:\"%archive_title% | %blog_title%\";s:23:\"aiosp_date_title_format\";s:21:\"%date% | %blog_title%\";s:25:\"aiosp_author_title_format\";s:23:\"%author% | %blog_title%\";s:22:\"aiosp_tag_title_format\";s:20:\"%tag% | %blog_title%\";s:25:\"aiosp_search_title_format\";s:23:\"%search% | %blog_title%\";s:24:\"aiosp_description_format\";s:13:\"%description%\";s:22:\"aiosp_404_title_format\";s:33:\"Nothing found for %request_words%\";s:18:\"aiosp_paged_format\";s:14:\" - Part %page%\";s:17:\"aiosp_enablecpost\";s:2:\"on\";s:17:\"aiosp_cpostactive\";a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}s:19:\"aiosp_cpostadvanced\";i:0;s:18:\"aiosp_cpostnoindex\";a:0:{}s:19:\"aiosp_cpostnofollow\";a:0:{}s:17:\"aiosp_cposttitles\";i:0;s:21:\"aiosp_posttypecolumns\";a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}s:19:\"aiosp_google_verify\";s:0:\"\";s:17:\"aiosp_bing_verify\";s:0:\"\";s:22:\"aiosp_pinterest_verify\";s:0:\"\";s:22:\"aiosp_google_publisher\";s:0:\"\";s:28:\"aiosp_google_disable_profile\";i:0;s:29:\"aiosp_google_sitelinks_search\";N;s:26:\"aiosp_google_set_site_name\";N;s:30:\"aiosp_google_specify_site_name\";N;s:28:\"aiosp_google_author_advanced\";i:0;s:28:\"aiosp_google_author_location\";a:1:{i:0;s:3:\"all\";}s:29:\"aiosp_google_enable_publisher\";s:2:\"on\";s:30:\"aiosp_google_specify_publisher\";N;s:25:\"aiosp_google_analytics_id\";N;s:25:\"aiosp_ga_advanced_options\";s:2:\"on\";s:15:\"aiosp_ga_domain\";N;s:21:\"aiosp_ga_multi_domain\";i:0;s:21:\"aiosp_ga_addl_domains\";N;s:21:\"aiosp_ga_anonymize_ip\";N;s:28:\"aiosp_ga_display_advertising\";N;s:22:\"aiosp_ga_exclude_users\";N;s:29:\"aiosp_ga_track_outbound_links\";i:0;s:25:\"aiosp_ga_link_attribution\";i:0;s:27:\"aiosp_ga_enhanced_ecommerce\";i:0;s:20:\"aiosp_use_categories\";i:0;s:26:\"aiosp_use_tags_as_keywords\";i:1;s:32:\"aiosp_dynamic_postspage_keywords\";i:1;s:22:\"aiosp_category_noindex\";i:1;s:26:\"aiosp_archive_date_noindex\";i:1;s:28:\"aiosp_archive_author_noindex\";i:1;s:18:\"aiosp_tags_noindex\";i:0;s:20:\"aiosp_search_noindex\";i:0;s:17:\"aiosp_404_noindex\";i:0;s:17:\"aiosp_tax_noindex\";a:0:{}s:23:\"aiosp_paginated_noindex\";i:0;s:24:\"aiosp_paginated_nofollow\";i:0;s:27:\"aiosp_generate_descriptions\";i:0;s:18:\"aiosp_skip_excerpt\";i:0;s:20:\"aiosp_run_shortcodes\";i:0;s:33:\"aiosp_hide_paginated_descriptions\";i:0;s:32:\"aiosp_dont_truncate_descriptions\";i:0;s:19:\"aiosp_schema_markup\";i:1;s:20:\"aiosp_unprotect_meta\";i:0;s:33:\"aiosp_redirect_attachement_parent\";i:0;s:14:\"aiosp_ex_pages\";s:0:\"\";s:20:\"aiosp_post_meta_tags\";s:0:\"\";s:20:\"aiosp_page_meta_tags\";s:0:\"\";s:21:\"aiosp_front_meta_tags\";s:0:\"\";s:20:\"aiosp_home_meta_tags\";s:0:\"\";s:12:\"aiosp_do_log\";N;s:19:\"last_active_version\";s:5:\"2.9.1\";}', 'yes'),
(5508, 'mc_db_version', '3.1.1', 'yes'),
(5509, 'mc_version', '3.1.1', 'yes'),
(5510, 'mc_input_options', 'a:3:{s:14:\"event_specials\";s:2:\"on\";s:12:\"event_access\";s:2:\"on\";s:10:\"event_host\";s:2:\"on\";}', 'yes'),
(5511, 'mc_inverse_color', 'true', 'yes'),
(5512, 'mc_use_permalinks', '', 'yes'),
(5513, 'mc_location_controls', 'a:1:{s:11:\"event_state\";N;}', 'yes'),
(5514, 'mc_use_custom_js', '0', 'yes'),
(5515, 'mc_update_notice', '1', 'yes'),
(5516, 'mc_display_more', 'true', 'yes'),
(5517, 'mc_default_direction', 'DESC', 'yes'),
(5518, 'mc_display_author', 'false', 'yes'),
(5519, 'mc_use_styles', 'false', 'yes'),
(5520, 'mc_show_months', '1', 'yes'),
(5521, 'mc_show_map', 'false', 'yes'),
(5522, 'mc_show_address', 'true', 'yes'),
(5523, 'mc_calendar_javascript', '0', 'yes'),
(5524, 'mc_list_javascript', '0', 'yes'),
(5525, 'mc_mini_javascript', '0', 'yes'),
(5526, 'mc_ajax_javascript', '0', 'yes'),
(5527, 'mc_notime_text', 'Весь день', 'yes'),
(5528, 'mc_hide_icons', 'true', 'yes'),
(5529, 'mc_event_link_expires', 'false', 'yes'),
(5530, 'mc_apply_color', 'background', 'yes'),
(5531, 'mc_input_options_administrators', 'false', 'yes'),
(5532, 'mc_multisite', '0', 'no'),
(5533, 'mc_event_mail', 'false', 'yes'),
(5534, 'mc_desc', 'true', 'yes'),
(5535, 'mc_image', 'true', 'yes'),
(5536, 'mc_process_shortcodes', 'false', 'yes'),
(5537, 'mc_short', 'false', 'yes'),
(5538, 'mc_no_fifth_week', 'true', 'yes'),
(5539, 'mc_week_format', 'M j, \'y', 'yes'),
(5540, 'mc_date_format', 'd.m.Y', 'yes'),
(5541, 'mc_templates', 'a:10:{s:5:\"title\";s:15:\"{time}: {title}\";s:10:\"title_list\";s:7:\"{title}\";s:10:\"title_solo\";s:7:\"{title}\";s:4:\"link\";s:16:\"More information\";s:4:\"grid\";s:448:\"<span class=\\\"event-time value-title\\\" title=\\\"{dtstart}\\\">{time}<span class=\\\"time-separator\\\"> - </span>{endtime before=\\\"<span class=\\\'end-time\\\' title=\\\'{dtend}\\\'>\\\" after=\\\"</span>\\\"}</span>\r\n\r\n		<div class=\\\"sub-details\\\">\r\n		{hcard}\r\n		{details before=\\\"<p class=\\\'mc_details\\\'>\\\" after=\\\"</p>\\\"}\r\n		<p><a href=\\\"{linking}\\\" class=\\\"event-link external\\\"><span class=\\\"screen-reader-text\\\">More information about </span>{title}</a></p></div>\";s:4:\"list\";s:504:\"<span class=\\\"event-time value-title\\\" title=\\\"{dtstart}\\\">{time}<span class=\\\"time-separator\\\"> - </span>{endtime before=\\\"<span class=\\\'end-time value-title\\\' title=\\\'{dtend}\\\'>\\\" after=\\\"</span>\\\"}</span>\r\n\r\n		<h3 class=\\\"event-title\\\">{title}</h3>\r\n\r\n		<div class=\\\"sub-details\\\">\r\n		{hcard}\r\n		{details before=\\\"<p class=\\\'mc_details\\\'>\\\" after=\\\"</p>\\\"}\r\n		<p><a href=\\\"{linking}\\\" class=\\\"event-link external\\\"><span class=\\\"screen-reader-text\\\">More information about </span>{title}</a></p></div>\";s:4:\"mini\";s:505:\"<span class=\\\"event-time value-title\\\" title=\\\"{dtstart}\\\">{time}<span class=\\\"time-separator\\\"> - </span>{endtime before=\\\"<span class=\\\'end-time value-title\\\' title=\\\'{dtend}\\\'>\\\" after=\\\"</span>\\\"}</span>\r\n\r\n		<h3 class=\\\"event-title\\\">{title}</h3>\r\n\r\n		<div class=\\\"sub-details\\\">\r\n		{excerpt before=\\\"<div class=\\\'excerpt\\\'>\\\" after=\\\"</div>\\\"}\r\n		{hcard}\r\n		<p><a href=\\\"{linking}\\\" class=\\\"event-link external\\\"><span class=\\\"screen-reader-text\\\">More information about </span>{title}</a></p></div>\";s:3:\"rss\";s:876:\"\n<item>\r\n			<title>{rss_title}: {date}, {time}</title>\r\n			<link>{link}</link>\r\n			<pubDate>{rssdate}</pubDate>\r\n			<dc:creator>{author}</dc:creator>\r\n			<description><![CDATA[{rss_description}]]></description>\r\n			<content:encoded><![CDATA[<div class=\\\'vevent\\\'>\r\n			<h1 class=\\\'summary\\\'>{rss_title}</h1>\r\n			<div class=\\\'description\\\'>{rss_description}</div>\r\n			<p class=\\\'dtstart\\\' title=\\\'{ical_start}\\\'>Begins: {time} on {date}</p>\r\n			<p class=\\\'dtend\\\' title=\\\'{ical_end}\\\'>Ends: {endtime} on {enddate}</p>\r\n			<p>Recurrence: {recurs}</p>\r\n			<p>Repetition: {repeats} times</p>\r\n			<div class=\\\'location\\\'>{rss_hcard}</div>\r\n			{link_title}\r\n			</div>]]></content:encoded>\r\n			<dc:format xmlns:dc=\\\'http://purl.org/dc/elements/1.1/\\\'>text/html</dc:format>\r\n			<dc:source xmlns:dc=\\\'http://purl.org/dc/elements/1.1/\\\'>http://stylist</dc:source>\r\n			{guid}\r\n		</item>\r\n\";s:7:\"details\";s:483:\"<span class=\\\"event-time value-title\\\" title=\\\"{dtstart}\\\">{time}<span class=\\\"time-separator\\\"> - </span><span class=\\\"end-time value-title\\\" title=\\\"{dtend}\\\">{endtime}</span></span>\r\n\r\n		<div class=\\\"sub-details\\\">\r\n		{hcard}\r\n		<div class=\\\"mc-description\\\">{image}{description}</div>\r\n		<p>{ical_html} &bull; {gcal_link}</p>\r\n		{map}\r\n		<p><a href=\\\"{linking}\\\" class=\\\"event-link external\\\"><span class=\\\"screen-reader-text\\\">More information about </span>{title}</a></p></div>\";s:5:\"label\";s:9:\"Read more\";}', 'yes'),
(5542, 'mc_skip_holidays', 'false', 'yes'),
(5543, 'mc_css_file', 'twentyeighteen.css', 'yes'),
(5544, 'mc_time_format', 'H:i', 'yes'),
(5545, 'mc_show_weekends', 'true', 'yes'),
(5546, 'mc_convert', 'true', 'yes'),
(5547, 'mc_show_event_vcal', 'false', 'yes'),
(5548, 'mc_multisite_show', '0', 'yes'),
(5549, 'mc_event_link', 'true', 'yes'),
(5550, 'mc_topnav', 'toggle,timeframe,jump,nav', 'yes'),
(5551, 'mc_bottomnav', 'key,print', 'yes'),
(5552, 'mc_uri', 'http://stylist/my-calendar/', 'yes'),
(5553, 'mc_uri_id', '459', 'yes'),
(5590, 'mc_count_cache', 'a:5:{s:9:\"published\";s:1:\"3\";s:5:\"draft\";s:1:\"0\";s:5:\"trash\";s:1:\"0\";s:7:\"archive\";s:1:\"0\";s:4:\"spam\";s:1:\"0\";}', 'yes'),
(5614, 'mc_event_title_template', '{title} » {date}', 'yes'),
(5615, 'mc_week_caption', '', 'yes'),
(5616, 'mc_next_events', 'Вперед', 'yes'),
(5617, 'mc_previous_events', 'Назад', 'yes'),
(5618, 'mc_caption', '', 'yes'),
(5619, 'mc_multidate_format', 'F j-%d, Y', 'yes'),
(5620, 'mc_month_format', '', 'yes'),
(6601, 'mp_timetable_permalinks', 'a:4:{s:11:\"column_base\";s:16:\"timetable/column\";s:10:\"event_base\";s:15:\"timetable/event\";s:19:\"event_category_base\";s:18:\"timetable/category\";s:14:\"event_tag_base\";s:13:\"timetable/tag\";}', 'yes'),
(6602, 'widget_mp-timetable', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(6617, 'mp_timetable_general', 'a:1:{s:10:\"theme_mode\";s:6:\"plugin\";}', 'yes'),
(6628, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1594119168;s:7:\"checked\";a:1:{s:7:\"stylist\";s:0:\"\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(6637, 'mp-event_category_children', 'a:0:{}', 'yes'),
(6654, 'installed_appointment-hour-booking', '1543291618', 'yes'),
(6656, 'cp_cpappb_last_verified', '2018-11-27 04:10:16', 'yes'),
(6657, 'autoptimize_js_exclude', 'jQuery.stringify.js,jquery.validate.js,seal.js, js/jquery/jquery.js', 'yes'),
(6658, '_transient_codepeople_promote_banner_appointment-hour-booking', '1543291619', 'yes'),
(6695, 'widget_chronosly_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(6698, 'chronosly-mk-addons', 's:63:\"a:1:{s:17:\"addons-downloaded\";a:2:{i:0;s:2:\"t1\";i:1;s:2:\"t2\";}}\";', 'yes'),
(6699, 'chronosly-mk-templates', 's:53:\"a:1:{s:20:\"templates-downloaded\";a:1:{i:0;s:2:\"t1\";}}\";', 'yes'),
(6893, 'rewrite_rules', 'a:88:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:40:\"index.php?&page_id=450&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(6926, 'ecwd_admin_notice', 'a:1:{s:15:\"two_week_review\";a:2:{s:5:\"start\";s:10:\"12/11/2018\";s:3:\"int\";i:14;}}', 'yes'),
(7010, 'sp_calendar_version', '1.5.62', 'yes'),
(7011, 'widget_spider_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(7012, 'widget_upcoming_events', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(7014, 'sp_calendar_admin_notice', 'a:1:{s:15:\"two_week_review\";a:2:{s:5:\"start\";s:10:\"12/11/2018\";s:3:\"int\";i:14;}}', 'yes'),
(7020, 'sp_calendar_subscribe_done', '1', 'yes'),
(7304, 'mc-event-category_children', 'a:0:{}', 'yes'),
(7580, 'wsp-loader-opt', 'a:5:{s:6:\"loader\";s:8:\"Loader 6\";s:16:\"custom_animation\";s:0:\"\";s:10:\"custom_css\";s:33:\"body {\r\n    background: white;\r\n}\";s:5:\"delay\";s:4:\"1000\";s:7:\"fadeout\";s:4:\"2000\";}', 'yes'),
(10962, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:8:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.4.2.zip\";s:6:\"locale\";s:5:\"ru_RU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.4.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.2\";s:7:\"version\";s:5:\"5.4.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.4.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.4.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.2\";s:7:\"version\";s:5:\"5.4.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.4.2.zip\";s:6:\"locale\";s:5:\"ru_RU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.4.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.2\";s:7:\"version\";s:5:\"5.4.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.3.4.zip\";s:6:\"locale\";s:5:\"ru_RU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.3.4.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.3.4\";s:7:\"version\";s:5:\"5.3.4\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:4;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.2.7.zip\";s:6:\"locale\";s:5:\"ru_RU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.2.7.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.7\";s:7:\"version\";s:5:\"5.2.7\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:5;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.1.6.zip\";s:6:\"locale\";s:5:\"ru_RU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.1.6.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.1.6\";s:7:\"version\";s:5:\"5.1.6\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:6;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:66:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.0.10.zip\";s:6:\"locale\";s:5:\"ru_RU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:66:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.0.10.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:6:\"5.0.10\";s:7:\"version\";s:6:\"5.0.10\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:7;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:66:\"https://downloads.wordpress.org/release/ru_RU/wordpress-4.9.15.zip\";s:6:\"locale\";s:5:\"ru_RU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:66:\"https://downloads.wordpress.org/release/ru_RU/wordpress-4.9.15.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:6:\"4.9.15\";s:7:\"version\";s:6:\"4.9.15\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1594119129;s:15:\"version_checked\";s:6:\"4.9.12\";s:12:\"translations\";a:0:{}}', 'no'),
(10965, 'widget_qligg_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(81534, '_transient_timeout_aioseop_feed', '1594162316', 'no'),
(81535, '_transient_aioseop_feed', 'a:4:{i:0;a:4:{s:3:\"url\";s:133:\"https://semperplugins.com/best-free-wordpress-plugins/?utm_source=rss&amp;utm_medium=rss&amp;utm_campaign=best-free-wordpress-plugins\";s:5:\"title\";s:47:\"14 Best Free WordPress Plugins You Need in 2020\";s:4:\"date\";s:13:\"Jun 17th 2020\";s:7:\"content\";s:131:\"Are you looking for the best free WordPress plugins?\n\n\n\nWordPress is the most popular website builder in the world, and it comes...\";}i:1;a:4:{s:3:\"url\";s:143:\"https://semperplugins.com/introducing-google-news-sitemaps/?utm_source=rss&amp;utm_medium=rss&amp;utm_campaign=introducing-google-news-sitemaps\";s:5:\"title\";s:64:\"Introducing News Sitemaps: Submit Articles to Google News Faster\";s:4:\"date\";s:13:\"May 20th 2020\";s:7:\"content\";s:131:\"In March’s release of All In One SEO 3.4, we released a variety of updates to help you improve your website. And since then, w...\";}i:2;a:4:{s:3:\"url\";s:125:\"https://semperplugins.com/all-in-one-seo-pack-3-4/?utm_source=rss&amp;utm_medium=rss&amp;utm_campaign=all-in-one-seo-pack-3-4\";s:5:\"title\";s:58:\"All in One SEO Pack 3.4: Image SEO, Breadcrumbs, and More!\";s:4:\"date\";s:13:\"Mar 25th 2020\";s:7:\"content\";s:131:\"We are excited to announce that version 3.4 of All in One SEO has been released today!\nThis major release adds new features and ...\";}i:3;a:4:{s:3:\"url\";s:189:\"https://semperplugins.com/all-in-one-seo-is-now-part-of-the-awesome-motive-family/?utm_source=rss&amp;utm_medium=rss&amp;utm_campaign=all-in-one-seo-is-now-part-of-the-awesome-motive-family\";s:5:\"title\";s:55:\"All in One SEO is now part of the Awesome Motive Family\";s:4:\"date\";s:12:\"Feb 6th 2020\";s:7:\"content\";s:131:\"With over 2 million active installs, All in One SEO Pack is one of the most popular WordPress SEO plugins in the world.\nToday, I...\";}}', 'no'),
(81537, '_site_transient_timeout_community-events-1aecf33ab8525ff212ebdffbb438372e', '1594162317', 'no'),
(81538, '_site_transient_community-events-1aecf33ab8525ff212ebdffbb438372e', 'a:3:{s:9:\"sandboxed\";b:0;s:8:\"location\";a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}s:6:\"events\";a:0:{}}', 'no'),
(81540, '_transient_timeout_plugin_slugs', '1594210338', 'no'),
(81541, '_transient_plugin_slugs', 'a:11:{i:0;s:43:\"all-in-one-seo-pack/all_in_one_seo_pack.php\";i:1;s:21:\"backwpup/backwpup.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:55:\"contact-form-7-datepicker/contact-form-7-datepicker.php\";i:4;s:22:\"cyr3lat/cyr-to-lat.php\";i:5;s:45:\"disable-wordpress-updates/disable-updates.php\";i:6;s:27:\"my-calendar/my-calendar.php\";i:7;s:58:\"podamibe-custom-user-gravatar/pod-custom-user-gravatar.php\";i:8;s:31:\"insta-gallery/insta-gallery.php\";i:9;s:25:\"tablepress/tablepress.php\";i:10;s:18:\"userdata/index.php\";}', 'no'),
(81547, '_site_transient_timeout_theme_roots', '1594120929', 'no'),
(81548, '_site_transient_theme_roots', 'a:1:{s:7:\"stylist\";s:7:\"/themes\";}', 'no'),
(81553, '_site_transient_update_plugins', 'O:8:\"stdClass\":3:{s:7:\"updates\";a:0:{}s:15:\"version_checked\";s:6:\"4.9.12\";s:12:\"last_checked\";i:1594123943;}', 'no'),
(81560, '_transient_timeout_mc_db_type', '1596711195', 'no'),
(81561, '_transient_mc_db_type', 'InnoDB', 'no'),
(81578, '_site_transient_timeout_available_translations', '1594130323', 'no'),
(81579, '_site_transient_available_translations', 'a:114:{s:2:\"af\";a:8:{s:8:\"language\";s:2:\"af\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-23 12:40:30\";s:12:\"english_name\";s:9:\"Afrikaans\";s:11:\"native_name\";s:9:\"Afrikaans\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/af.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"af\";i:2;s:3:\"afr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Gaan voort\";}}s:2:\"ar\";a:8:{s:8:\"language\";s:2:\"ar\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-22 22:32:51\";s:12:\"english_name\";s:6:\"Arabic\";s:11:\"native_name\";s:14:\"العربية\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/ar.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:2;s:3:\"ara\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:3:\"ary\";a:8:{s:8:\"language\";s:3:\"ary\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:42:35\";s:12:\"english_name\";s:15:\"Moroccan Arabic\";s:11:\"native_name\";s:31:\"العربية المغربية\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.7/ary.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ar\";i:3;s:3:\"ary\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"المتابعة\";}}s:2:\"as\";a:8:{s:8:\"language\";s:2:\"as\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-22 18:59:07\";s:12:\"english_name\";s:8:\"Assamese\";s:11:\"native_name\";s:21:\"অসমীয়া\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/as.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"as\";i:2;s:3:\"asm\";i:3;s:3:\"asm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"az\";a:8:{s:8:\"language\";s:2:\"az\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-06 00:09:27\";s:12:\"english_name\";s:11:\"Azerbaijani\";s:11:\"native_name\";s:16:\"Azərbaycan dili\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/az.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:2;s:3:\"aze\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Davam\";}}s:3:\"azb\";a:8:{s:8:\"language\";s:3:\"azb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-12 20:34:31\";s:12:\"english_name\";s:17:\"South Azerbaijani\";s:11:\"native_name\";s:29:\"گؤنئی آذربایجان\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/azb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"az\";i:3;s:3:\"azb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:3:\"bel\";a:8:{s:8:\"language\";s:3:\"bel\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-29 07:54:22\";s:12:\"english_name\";s:10:\"Belarusian\";s:11:\"native_name\";s:29:\"Беларуская мова\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/translation/core/4.9.12/bel.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"be\";i:2;s:3:\"bel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Працягнуць\";}}s:5:\"bg_BG\";a:8:{s:8:\"language\";s:5:\"bg_BG\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-11-09 14:00:48\";s:12:\"english_name\";s:9:\"Bulgarian\";s:11:\"native_name\";s:18:\"Български\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/bg_BG.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bg\";i:2;s:3:\"bul\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Напред\";}}s:5:\"bn_BD\";a:8:{s:8:\"language\";s:5:\"bn_BD\";s:7:\"version\";s:6:\"4.8.14\";s:7:\"updated\";s:19:\"2017-10-01 12:57:10\";s:12:\"english_name\";s:20:\"Bengali (Bangladesh)\";s:11:\"native_name\";s:15:\"বাংলা\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.8.14/bn_BD.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"bn\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:23:\"এগিয়ে চল.\";}}s:2:\"bo\";a:8:{s:8:\"language\";s:2:\"bo\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-11-12 07:38:11\";s:12:\"english_name\";s:7:\"Tibetan\";s:11:\"native_name\";s:21:\"བོད་ཡིག\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/bo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bo\";i:2;s:3:\"tib\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:33:\"མུ་མཐུད་དུ།\";}}s:5:\"bs_BA\";a:8:{s:8:\"language\";s:5:\"bs_BA\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-25 20:24:41\";s:12:\"english_name\";s:7:\"Bosnian\";s:11:\"native_name\";s:8:\"Bosanski\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/bs_BA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"bs\";i:2;s:3:\"bos\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:2:\"ca\";a:8:{s:8:\"language\";s:2:\"ca\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-20 06:58:38\";s:12:\"english_name\";s:7:\"Catalan\";s:11:\"native_name\";s:7:\"Català\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/ca.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ca\";i:2;s:3:\"cat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:3:\"ceb\";a:8:{s:8:\"language\";s:3:\"ceb\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-02 17:25:51\";s:12:\"english_name\";s:7:\"Cebuano\";s:11:\"native_name\";s:7:\"Cebuano\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/ceb.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"ceb\";i:3;s:3:\"ceb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Padayun\";}}s:5:\"cs_CZ\";a:8:{s:8:\"language\";s:5:\"cs_CZ\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-28 19:06:35\";s:12:\"english_name\";s:5:\"Czech\";s:11:\"native_name\";s:9:\"Čeština\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/cs_CZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cs\";i:2;s:3:\"ces\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Pokračovat\";}}s:2:\"cy\";a:8:{s:8:\"language\";s:2:\"cy\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-22 10:36:32\";s:12:\"english_name\";s:5:\"Welsh\";s:11:\"native_name\";s:7:\"Cymraeg\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/cy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"cy\";i:2;s:3:\"cym\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Parhau\";}}s:5:\"da_DK\";a:8:{s:8:\"language\";s:5:\"da_DK\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-04-02 13:26:35\";s:12:\"english_name\";s:6:\"Danish\";s:11:\"native_name\";s:5:\"Dansk\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/da_DK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"da\";i:2;s:3:\"dan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsæt\";}}s:12:\"de_DE_formal\";a:8:{s:8:\"language\";s:12:\"de_DE_formal\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-19 15:11:46\";s:12:\"english_name\";s:15:\"German (Formal)\";s:11:\"native_name\";s:13:\"Deutsch (Sie)\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/translation/core/4.9.12/de_DE_formal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:14:\"de_CH_informal\";a:8:{s:8:\"language\";s:14:\"de_CH_informal\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-28 11:48:22\";s:12:\"english_name\";s:30:\"German (Switzerland, Informal)\";s:11:\"native_name\";s:21:\"Deutsch (Schweiz, Du)\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/translation/core/4.9.8/de_CH_informal.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_CH\";a:8:{s:8:\"language\";s:5:\"de_CH\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-28 11:47:36\";s:12:\"english_name\";s:20:\"German (Switzerland)\";s:11:\"native_name\";s:17:\"Deutsch (Schweiz)\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/de_CH.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:5:\"de_DE\";a:8:{s:8:\"language\";s:5:\"de_DE\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-19 15:11:37\";s:12:\"english_name\";s:6:\"German\";s:11:\"native_name\";s:7:\"Deutsch\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/de_DE.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"de\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Weiter\";}}s:3:\"dzo\";a:8:{s:8:\"language\";s:3:\"dzo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-06-29 08:59:03\";s:12:\"english_name\";s:8:\"Dzongkha\";s:11:\"native_name\";s:18:\"རྫོང་ཁ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/dzo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"dz\";i:2;s:3:\"dzo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"el\";a:8:{s:8:\"language\";s:2:\"el\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-11-13 12:21:45\";s:12:\"english_name\";s:5:\"Greek\";s:11:\"native_name\";s:16:\"Ελληνικά\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/el.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"el\";i:2;s:3:\"ell\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Συνέχεια\";}}s:5:\"en_GB\";a:8:{s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-19 14:34:43\";s:12:\"english_name\";s:12:\"English (UK)\";s:11:\"native_name\";s:12:\"English (UK)\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/en_GB.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_ZA\";a:8:{s:8:\"language\";s:5:\"en_ZA\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-03-13 07:15:15\";s:12:\"english_name\";s:22:\"English (South Africa)\";s:11:\"native_name\";s:22:\"English (South Africa)\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/en_ZA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_AU\";a:8:{s:8:\"language\";s:5:\"en_AU\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-20 00:25:37\";s:12:\"english_name\";s:19:\"English (Australia)\";s:11:\"native_name\";s:19:\"English (Australia)\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/en_AU.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_CA\";a:8:{s:8:\"language\";s:5:\"en_CA\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-20 22:34:15\";s:12:\"english_name\";s:16:\"English (Canada)\";s:11:\"native_name\";s:16:\"English (Canada)\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/en_CA.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"en_NZ\";a:8:{s:8:\"language\";s:5:\"en_NZ\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-20 22:34:08\";s:12:\"english_name\";s:21:\"English (New Zealand)\";s:11:\"native_name\";s:21:\"English (New Zealand)\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/en_NZ.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"en\";i:2;s:3:\"eng\";i:3;s:3:\"eng\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"eo\";a:8:{s:8:\"language\";s:2:\"eo\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-04-24 14:32:41\";s:12:\"english_name\";s:9:\"Esperanto\";s:11:\"native_name\";s:9:\"Esperanto\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/eo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eo\";i:2;s:3:\"epo\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Daŭrigi\";}}s:5:\"es_MX\";a:8:{s:8:\"language\";s:5:\"es_MX\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-11-12 17:57:39\";s:12:\"english_name\";s:16:\"Spanish (Mexico)\";s:11:\"native_name\";s:19:\"Español de México\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/es_MX.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_VE\";a:8:{s:8:\"language\";s:5:\"es_VE\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-19 23:11:17\";s:12:\"english_name\";s:19:\"Spanish (Venezuela)\";s:11:\"native_name\";s:21:\"Español de Venezuela\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/es_VE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_GT\";a:8:{s:8:\"language\";s:5:\"es_GT\";s:7:\"version\";s:5:\"4.9.9\";s:7:\"updated\";s:19:\"2019-03-02 06:27:10\";s:12:\"english_name\";s:19:\"Spanish (Guatemala)\";s:11:\"native_name\";s:21:\"Español de Guatemala\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.9/es_GT.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CR\";a:8:{s:8:\"language\";s:5:\"es_CR\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-10-01 17:54:52\";s:12:\"english_name\";s:20:\"Spanish (Costa Rica)\";s:11:\"native_name\";s:22:\"Español de Costa Rica\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.8.3/es_CR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_PE\";a:8:{s:8:\"language\";s:5:\"es_PE\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-09 09:36:22\";s:12:\"english_name\";s:14:\"Spanish (Peru)\";s:11:\"native_name\";s:17:\"Español de Perú\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/es_PE.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CL\";a:8:{s:8:\"language\";s:5:\"es_CL\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-12-07 04:02:01\";s:12:\"english_name\";s:15:\"Spanish (Chile)\";s:11:\"native_name\";s:17:\"Español de Chile\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/es_CL.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_ES\";a:8:{s:8:\"language\";s:5:\"es_ES\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-11-17 18:18:53\";s:12:\"english_name\";s:15:\"Spanish (Spain)\";s:11:\"native_name\";s:8:\"Español\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/es_ES.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_AR\";a:8:{s:8:\"language\";s:5:\"es_AR\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-03-21 23:58:09\";s:12:\"english_name\";s:19:\"Spanish (Argentina)\";s:11:\"native_name\";s:21:\"Español de Argentina\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/es_AR.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"es_CO\";a:8:{s:8:\"language\";s:5:\"es_CO\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-05-23 02:23:28\";s:12:\"english_name\";s:18:\"Spanish (Colombia)\";s:11:\"native_name\";s:20:\"Español de Colombia\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/es_CO.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"es\";i:2;s:3:\"spa\";i:3;s:3:\"spa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"et\";a:8:{s:8:\"language\";s:2:\"et\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-11-19 14:11:29\";s:12:\"english_name\";s:8:\"Estonian\";s:11:\"native_name\";s:5:\"Eesti\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/et.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"et\";i:2;s:3:\"est\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Jätka\";}}s:2:\"eu\";a:8:{s:8:\"language\";s:2:\"eu\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2017-12-09 21:12:23\";s:12:\"english_name\";s:6:\"Basque\";s:11:\"native_name\";s:7:\"Euskara\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.2/eu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"eu\";i:2;s:3:\"eus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Jarraitu\";}}s:5:\"fa_IR\";a:8:{s:8:\"language\";s:5:\"fa_IR\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-26 11:04:10\";s:12:\"english_name\";s:7:\"Persian\";s:11:\"native_name\";s:10:\"فارسی\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/fa_IR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fa\";i:2;s:3:\"fas\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:2:\"fi\";a:8:{s:8:\"language\";s:2:\"fi\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-22 14:09:45\";s:12:\"english_name\";s:7:\"Finnish\";s:11:\"native_name\";s:5:\"Suomi\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/fi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fi\";i:2;s:3:\"fin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Jatka\";}}s:5:\"fr_FR\";a:8:{s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-19 15:01:55\";s:12:\"english_name\";s:15:\"French (France)\";s:11:\"native_name\";s:9:\"Français\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/fr_FR.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"fr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_BE\";a:8:{s:8:\"language\";s:5:\"fr_BE\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-01-31 11:16:06\";s:12:\"english_name\";s:16:\"French (Belgium)\";s:11:\"native_name\";s:21:\"Français de Belgique\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/fr_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:5:\"fr_CA\";a:8:{s:8:\"language\";s:5:\"fr_CA\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-20 19:34:46\";s:12:\"english_name\";s:15:\"French (Canada)\";s:11:\"native_name\";s:19:\"Français du Canada\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/fr_CA.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"fr\";i:2;s:3:\"fra\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuer\";}}s:3:\"fur\";a:8:{s:8:\"language\";s:3:\"fur\";s:7:\"version\";s:5:\"4.8.6\";s:7:\"updated\";s:19:\"2018-01-29 17:32:35\";s:12:\"english_name\";s:8:\"Friulian\";s:11:\"native_name\";s:8:\"Friulian\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.6/fur.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"fur\";i:3;s:3:\"fur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:2:\"gd\";a:8:{s:8:\"language\";s:2:\"gd\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-08-23 17:41:37\";s:12:\"english_name\";s:15:\"Scottish Gaelic\";s:11:\"native_name\";s:9:\"Gàidhlig\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/gd.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"gd\";i:2;s:3:\"gla\";i:3;s:3:\"gla\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"Lean air adhart\";}}s:5:\"gl_ES\";a:8:{s:8:\"language\";s:5:\"gl_ES\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-19 22:09:13\";s:12:\"english_name\";s:8:\"Galician\";s:11:\"native_name\";s:6:\"Galego\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/gl_ES.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gl\";i:2;s:3:\"glg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:2:\"gu\";a:8:{s:8:\"language\";s:2:\"gu\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-14 12:33:48\";s:12:\"english_name\";s:8:\"Gujarati\";s:11:\"native_name\";s:21:\"ગુજરાતી\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.8/gu.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"gu\";i:2;s:3:\"guj\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:31:\"ચાલુ રાખવું\";}}s:3:\"haz\";a:8:{s:8:\"language\";s:3:\"haz\";s:7:\"version\";s:5:\"4.4.2\";s:7:\"updated\";s:19:\"2015-12-05 00:59:09\";s:12:\"english_name\";s:8:\"Hazaragi\";s:11:\"native_name\";s:15:\"هزاره گی\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.4.2/haz.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"haz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"ادامه\";}}s:5:\"he_IL\";a:8:{s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-11-05 14:00:18\";s:12:\"english_name\";s:6:\"Hebrew\";s:11:\"native_name\";s:16:\"עִבְרִית\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/he_IL.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"he\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"המשך\";}}s:5:\"hi_IN\";a:8:{s:8:\"language\";s:5:\"hi_IN\";s:7:\"version\";s:5:\"4.9.7\";s:7:\"updated\";s:19:\"2018-06-17 09:33:44\";s:12:\"english_name\";s:5:\"Hindi\";s:11:\"native_name\";s:18:\"हिन्दी\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.7/hi_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hi\";i:2;s:3:\"hin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"जारी\";}}s:2:\"hr\";a:8:{s:8:\"language\";s:2:\"hr\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-28 08:30:56\";s:12:\"english_name\";s:8:\"Croatian\";s:11:\"native_name\";s:8:\"Hrvatski\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/hr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hr\";i:2;s:3:\"hrv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:7:\"Nastavi\";}}s:5:\"hu_HU\";a:8:{s:8:\"language\";s:5:\"hu_HU\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2018-12-16 15:53:35\";s:12:\"english_name\";s:9:\"Hungarian\";s:11:\"native_name\";s:6:\"Magyar\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/hu_HU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hu\";i:2;s:3:\"hun\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Folytatás\";}}s:2:\"hy\";a:8:{s:8:\"language\";s:2:\"hy\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-03 16:21:10\";s:12:\"english_name\";s:8:\"Armenian\";s:11:\"native_name\";s:14:\"Հայերեն\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/hy.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"hy\";i:2;s:3:\"hye\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Շարունակել\";}}s:5:\"id_ID\";a:8:{s:8:\"language\";s:5:\"id_ID\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-07-28 13:16:13\";s:12:\"english_name\";s:10:\"Indonesian\";s:11:\"native_name\";s:16:\"Bahasa Indonesia\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/id_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"id\";i:2;s:3:\"ind\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Lanjutkan\";}}s:5:\"is_IS\";a:8:{s:8:\"language\";s:5:\"is_IS\";s:7:\"version\";s:6:\"4.7.11\";s:7:\"updated\";s:19:\"2018-09-20 11:13:37\";s:12:\"english_name\";s:9:\"Icelandic\";s:11:\"native_name\";s:9:\"Íslenska\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.7.11/is_IS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"is\";i:2;s:3:\"isl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Áfram\";}}s:5:\"it_IT\";a:8:{s:8:\"language\";s:5:\"it_IT\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-19 16:39:23\";s:12:\"english_name\";s:7:\"Italian\";s:11:\"native_name\";s:8:\"Italiano\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/it_IT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"it\";i:2;s:3:\"ita\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continua\";}}s:2:\"ja\";a:8:{s:8:\"language\";s:2:\"ja\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-11-13 18:47:35\";s:12:\"english_name\";s:8:\"Japanese\";s:11:\"native_name\";s:9:\"日本語\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/ja.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"ja\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"次へ\";}}s:5:\"jv_ID\";a:8:{s:8:\"language\";s:5:\"jv_ID\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-24 13:53:29\";s:12:\"english_name\";s:8:\"Javanese\";s:11:\"native_name\";s:9:\"Basa Jawa\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/jv_ID.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"jv\";i:2;s:3:\"jav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Nerusaké\";}}s:5:\"ka_GE\";a:8:{s:8:\"language\";s:5:\"ka_GE\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-11-04 08:16:57\";s:12:\"english_name\";s:8:\"Georgian\";s:11:\"native_name\";s:21:\"ქართული\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/ka_GE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ka\";i:2;s:3:\"kat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"გაგრძელება\";}}s:3:\"kab\";a:8:{s:8:\"language\";s:3:\"kab\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-09-21 14:15:57\";s:12:\"english_name\";s:6:\"Kabyle\";s:11:\"native_name\";s:9:\"Taqbaylit\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.8/kab.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"kab\";i:3;s:3:\"kab\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Kemmel\";}}s:2:\"kk\";a:8:{s:8:\"language\";s:2:\"kk\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-12 08:08:32\";s:12:\"english_name\";s:6:\"Kazakh\";s:11:\"native_name\";s:19:\"Қазақ тілі\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.5/kk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kk\";i:2;s:3:\"kaz\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Жалғастыру\";}}s:2:\"km\";a:8:{s:8:\"language\";s:2:\"km\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-07 02:07:59\";s:12:\"english_name\";s:5:\"Khmer\";s:11:\"native_name\";s:27:\"ភាសាខ្មែរ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/km.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"km\";i:2;s:3:\"khm\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"បន្ត\";}}s:2:\"kn\";a:8:{s:8:\"language\";s:2:\"kn\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-12-04 12:22:34\";s:12:\"english_name\";s:7:\"Kannada\";s:11:\"native_name\";s:15:\"ಕನ್ನಡ\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/kn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"kn\";i:2;s:3:\"kan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"ಮುಂದುವರೆಸಿ\";}}s:5:\"ko_KR\";a:8:{s:8:\"language\";s:5:\"ko_KR\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-11-05 01:54:38\";s:12:\"english_name\";s:6:\"Korean\";s:11:\"native_name\";s:9:\"한국어\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/ko_KR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ko\";i:2;s:3:\"kor\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"계속\";}}s:3:\"ckb\";a:8:{s:8:\"language\";s:3:\"ckb\";s:7:\"version\";s:5:\"4.9.9\";s:7:\"updated\";s:19:\"2018-12-18 14:32:44\";s:12:\"english_name\";s:16:\"Kurdish (Sorani)\";s:11:\"native_name\";s:13:\"كوردی‎\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.9/ckb.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ku\";i:3;s:3:\"ckb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"به‌رده‌وام به‌\";}}s:2:\"lo\";a:8:{s:8:\"language\";s:2:\"lo\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 09:59:23\";s:12:\"english_name\";s:3:\"Lao\";s:11:\"native_name\";s:21:\"ພາສາລາວ\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/lo.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lo\";i:2;s:3:\"lao\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"ຕໍ່​ໄປ\";}}s:5:\"lt_LT\";a:8:{s:8:\"language\";s:5:\"lt_LT\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-19 19:21:11\";s:12:\"english_name\";s:10:\"Lithuanian\";s:11:\"native_name\";s:15:\"Lietuvių kalba\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/lt_LT.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lt\";i:2;s:3:\"lit\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Tęsti\";}}s:2:\"lv\";a:8:{s:8:\"language\";s:2:\"lv\";s:7:\"version\";s:6:\"4.7.15\";s:7:\"updated\";s:19:\"2019-05-10 10:24:08\";s:12:\"english_name\";s:7:\"Latvian\";s:11:\"native_name\";s:16:\"Latviešu valoda\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.15/lv.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"lv\";i:2;s:3:\"lav\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Turpināt\";}}s:5:\"mk_MK\";a:8:{s:8:\"language\";s:5:\"mk_MK\";s:7:\"version\";s:5:\"4.7.7\";s:7:\"updated\";s:19:\"2017-01-26 15:54:41\";s:12:\"english_name\";s:10:\"Macedonian\";s:11:\"native_name\";s:31:\"Македонски јазик\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.7/mk_MK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mk\";i:2;s:3:\"mkd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:16:\"Продолжи\";}}s:5:\"ml_IN\";a:8:{s:8:\"language\";s:5:\"ml_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:43:32\";s:12:\"english_name\";s:9:\"Malayalam\";s:11:\"native_name\";s:18:\"മലയാളം\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ml_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ml\";i:2;s:3:\"mal\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:18:\"തുടരുക\";}}s:2:\"mn\";a:8:{s:8:\"language\";s:2:\"mn\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-12 07:29:35\";s:12:\"english_name\";s:9:\"Mongolian\";s:11:\"native_name\";s:12:\"Монгол\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/mn.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mn\";i:2;s:3:\"mon\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"Үргэлжлүүлэх\";}}s:2:\"mr\";a:8:{s:8:\"language\";s:2:\"mr\";s:7:\"version\";s:6:\"4.8.14\";s:7:\"updated\";s:19:\"2018-02-13 07:38:55\";s:12:\"english_name\";s:7:\"Marathi\";s:11:\"native_name\";s:15:\"मराठी\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.14/mr.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"mr\";i:2;s:3:\"mar\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"सुरु ठेवा\";}}s:5:\"ms_MY\";a:8:{s:8:\"language\";s:5:\"ms_MY\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-08-30 20:27:25\";s:12:\"english_name\";s:5:\"Malay\";s:11:\"native_name\";s:13:\"Bahasa Melayu\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/ms_MY.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ms\";i:2;s:3:\"msa\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Teruskan\";}}s:5:\"my_MM\";a:8:{s:8:\"language\";s:5:\"my_MM\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-26 15:57:42\";s:12:\"english_name\";s:17:\"Myanmar (Burmese)\";s:11:\"native_name\";s:15:\"ဗမာစာ\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.1.20/my_MM.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"my\";i:2;s:3:\"mya\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:54:\"ဆက်လက်လုပ်ဆောင်ပါ။\";}}s:5:\"nb_NO\";a:8:{s:8:\"language\";s:5:\"nb_NO\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-12-12 16:58:33\";s:12:\"english_name\";s:19:\"Norwegian (Bokmål)\";s:11:\"native_name\";s:13:\"Norsk bokmål\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/nb_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nb\";i:2;s:3:\"nob\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Fortsett\";}}s:5:\"ne_NP\";a:8:{s:8:\"language\";s:5:\"ne_NP\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-27 10:30:26\";s:12:\"english_name\";s:6:\"Nepali\";s:11:\"native_name\";s:18:\"नेपाली\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ne_NP.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ne\";i:2;s:3:\"nep\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:43:\"जारी राख्नुहोस्\";}}s:5:\"nl_NL\";a:8:{s:8:\"language\";s:5:\"nl_NL\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-11-14 09:00:04\";s:12:\"english_name\";s:5:\"Dutch\";s:11:\"native_name\";s:10:\"Nederlands\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/nl_NL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:12:\"nl_NL_formal\";a:8:{s:8:\"language\";s:12:\"nl_NL_formal\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-03-13 05:36:09\";s:12:\"english_name\";s:14:\"Dutch (Formal)\";s:11:\"native_name\";s:20:\"Nederlands (Formeel)\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/translation/core/4.9.12/nl_NL_formal.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nl_BE\";a:8:{s:8:\"language\";s:5:\"nl_BE\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-20 07:54:58\";s:12:\"english_name\";s:15:\"Dutch (Belgium)\";s:11:\"native_name\";s:20:\"Nederlands (België)\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/nl_BE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nl\";i:2;s:3:\"nld\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Doorgaan\";}}s:5:\"nn_NO\";a:8:{s:8:\"language\";s:5:\"nn_NO\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-06-19 21:14:43\";s:12:\"english_name\";s:19:\"Norwegian (Nynorsk)\";s:11:\"native_name\";s:13:\"Norsk nynorsk\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/nn_NO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"nn\";i:2;s:3:\"nno\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Hald fram\";}}s:3:\"oci\";a:8:{s:8:\"language\";s:3:\"oci\";s:7:\"version\";s:5:\"4.8.3\";s:7:\"updated\";s:19:\"2017-08-25 10:03:08\";s:12:\"english_name\";s:7:\"Occitan\";s:11:\"native_name\";s:7:\"Occitan\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.8.3/oci.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"oc\";i:2;s:3:\"oci\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Contunhar\";}}s:5:\"pa_IN\";a:8:{s:8:\"language\";s:5:\"pa_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-16 05:19:43\";s:12:\"english_name\";s:7:\"Punjabi\";s:11:\"native_name\";s:18:\"ਪੰਜਾਬੀ\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/pa_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pa\";i:2;s:3:\"pan\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:25:\"ਜਾਰੀ ਰੱਖੋ\";}}s:5:\"pl_PL\";a:8:{s:8:\"language\";s:5:\"pl_PL\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-23 19:49:40\";s:12:\"english_name\";s:6:\"Polish\";s:11:\"native_name\";s:6:\"Polski\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/pl_PL.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pl\";i:2;s:3:\"pol\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Kontynuuj\";}}s:2:\"ps\";a:8:{s:8:\"language\";s:2:\"ps\";s:7:\"version\";s:6:\"4.1.20\";s:7:\"updated\";s:19:\"2015-03-29 22:19:48\";s:12:\"english_name\";s:6:\"Pashto\";s:11:\"native_name\";s:8:\"پښتو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.1.20/ps.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ps\";i:2;s:3:\"pus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"دوام ورکړه\";}}s:5:\"pt_PT\";a:8:{s:8:\"language\";s:5:\"pt_PT\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-12-12 19:56:18\";s:12:\"english_name\";s:21:\"Portuguese (Portugal)\";s:11:\"native_name\";s:10:\"Português\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/pt_PT.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:5:\"pt_BR\";a:8:{s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-19 15:12:37\";s:12:\"english_name\";s:19:\"Portuguese (Brazil)\";s:11:\"native_name\";s:20:\"Português do Brasil\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/pt_BR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"pt\";i:2;s:3:\"por\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:10:\"pt_PT_ao90\";a:8:{s:8:\"language\";s:10:\"pt_PT_ao90\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-03-09 09:30:48\";s:12:\"english_name\";s:27:\"Portuguese (Portugal, AO90)\";s:11:\"native_name\";s:17:\"Português (AO90)\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/translation/core/4.9.5/pt_PT_ao90.zip\";s:3:\"iso\";a:1:{i:1;s:2:\"pt\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuar\";}}s:3:\"rhg\";a:8:{s:8:\"language\";s:3:\"rhg\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-16 13:03:18\";s:12:\"english_name\";s:8:\"Rohingya\";s:11:\"native_name\";s:8:\"Ruáinga\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/rhg.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"rhg\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"ro_RO\";a:8:{s:8:\"language\";s:5:\"ro_RO\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-11-30 11:36:26\";s:12:\"english_name\";s:8:\"Romanian\";s:11:\"native_name\";s:8:\"Română\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/ro_RO.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ro\";i:2;s:3:\"ron\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Continuă\";}}s:5:\"ru_RU\";a:8:{s:8:\"language\";s:5:\"ru_RU\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-19 14:38:33\";s:12:\"english_name\";s:7:\"Russian\";s:11:\"native_name\";s:14:\"Русский\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/ru_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ru\";i:2;s:3:\"rus\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продолжить\";}}s:3:\"sah\";a:8:{s:8:\"language\";s:3:\"sah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-21 02:06:41\";s:12:\"english_name\";s:5:\"Sakha\";s:11:\"native_name\";s:14:\"Сахалыы\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/sah.zip\";s:3:\"iso\";a:2:{i:2;s:3:\"sah\";i:3;s:3:\"sah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Салҕаа\";}}s:5:\"si_LK\";a:8:{s:8:\"language\";s:5:\"si_LK\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-12 06:00:52\";s:12:\"english_name\";s:7:\"Sinhala\";s:11:\"native_name\";s:15:\"සිංහල\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/si_LK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"si\";i:2;s:3:\"sin\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:44:\"දිගටම කරගෙන යන්න\";}}s:5:\"sk_SK\";a:8:{s:8:\"language\";s:5:\"sk_SK\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-03-22 15:02:38\";s:12:\"english_name\";s:6:\"Slovak\";s:11:\"native_name\";s:11:\"Slovenčina\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/sk_SK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sk\";i:2;s:3:\"slk\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Pokračovať\";}}s:5:\"sl_SI\";a:8:{s:8:\"language\";s:5:\"sl_SI\";s:7:\"version\";s:5:\"4.9.2\";s:7:\"updated\";s:19:\"2018-01-04 13:33:13\";s:12:\"english_name\";s:9:\"Slovenian\";s:11:\"native_name\";s:13:\"Slovenščina\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.2/sl_SI.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sl\";i:2;s:3:\"slv\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Nadaljuj\";}}s:2:\"sq\";a:8:{s:8:\"language\";s:2:\"sq\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-20 12:35:07\";s:12:\"english_name\";s:8:\"Albanian\";s:11:\"native_name\";s:5:\"Shqip\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/sq.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sq\";i:2;s:3:\"sqi\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"Vazhdo\";}}s:5:\"sr_RS\";a:8:{s:8:\"language\";s:5:\"sr_RS\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2018-12-19 11:12:50\";s:12:\"english_name\";s:7:\"Serbian\";s:11:\"native_name\";s:23:\"Српски језик\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/sr_RS.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sr\";i:2;s:3:\"srp\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:14:\"Настави\";}}s:5:\"sv_SE\";a:8:{s:8:\"language\";s:5:\"sv_SE\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-19 13:42:49\";s:12:\"english_name\";s:7:\"Swedish\";s:11:\"native_name\";s:7:\"Svenska\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/sv_SE.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"sv\";i:2;s:3:\"swe\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:9:\"Fortsätt\";}}s:3:\"szl\";a:8:{s:8:\"language\";s:3:\"szl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-09-24 19:58:14\";s:12:\"english_name\";s:8:\"Silesian\";s:11:\"native_name\";s:17:\"Ślōnskŏ gŏdka\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/szl.zip\";s:3:\"iso\";a:1:{i:3;s:3:\"szl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:13:\"Kōntynuować\";}}s:5:\"ta_IN\";a:8:{s:8:\"language\";s:5:\"ta_IN\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-27 03:22:47\";s:12:\"english_name\";s:5:\"Tamil\";s:11:\"native_name\";s:15:\"தமிழ்\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/ta_IN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ta\";i:2;s:3:\"tam\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:24:\"தொடரவும்\";}}s:2:\"te\";a:8:{s:8:\"language\";s:2:\"te\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2017-01-26 15:47:39\";s:12:\"english_name\";s:6:\"Telugu\";s:11:\"native_name\";s:18:\"తెలుగు\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/te.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"te\";i:2;s:3:\"tel\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:30:\"కొనసాగించు\";}}s:2:\"th\";a:8:{s:8:\"language\";s:2:\"th\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-03-27 04:30:57\";s:12:\"english_name\";s:4:\"Thai\";s:11:\"native_name\";s:9:\"ไทย\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/th.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"th\";i:2;s:3:\"tha\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:15:\"ต่อไป\";}}s:2:\"tl\";a:8:{s:8:\"language\";s:2:\"tl\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-12-30 02:38:08\";s:12:\"english_name\";s:7:\"Tagalog\";s:11:\"native_name\";s:7:\"Tagalog\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.7.2/tl.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tl\";i:2;s:3:\"tgl\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:10:\"Magpatuloy\";}}s:5:\"tr_TR\";a:8:{s:8:\"language\";s:5:\"tr_TR\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-30 18:44:25\";s:12:\"english_name\";s:7:\"Turkish\";s:11:\"native_name\";s:8:\"Türkçe\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/tr_TR.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tr\";i:2;s:3:\"tur\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:5:\"Devam\";}}s:5:\"tt_RU\";a:8:{s:8:\"language\";s:5:\"tt_RU\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-11-20 20:20:50\";s:12:\"english_name\";s:5:\"Tatar\";s:11:\"native_name\";s:19:\"Татар теле\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.7.2/tt_RU.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"tt\";i:2;s:3:\"tat\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:17:\"дәвам итү\";}}s:3:\"tah\";a:8:{s:8:\"language\";s:3:\"tah\";s:7:\"version\";s:5:\"4.7.2\";s:7:\"updated\";s:19:\"2016-03-06 18:39:39\";s:12:\"english_name\";s:8:\"Tahitian\";s:11:\"native_name\";s:10:\"Reo Tahiti\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.7.2/tah.zip\";s:3:\"iso\";a:3:{i:1;s:2:\"ty\";i:2;s:3:\"tah\";i:3;s:3:\"tah\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:8:\"Continue\";}}s:5:\"ug_CN\";a:8:{s:8:\"language\";s:5:\"ug_CN\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-12 12:31:53\";s:12:\"english_name\";s:6:\"Uighur\";s:11:\"native_name\";s:16:\"ئۇيغۇرچە\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/ug_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ug\";i:2;s:3:\"uig\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:26:\"داۋاملاشتۇرۇش\";}}s:2:\"uk\";a:8:{s:8:\"language\";s:2:\"uk\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-10-03 17:08:48\";s:12:\"english_name\";s:9:\"Ukrainian\";s:11:\"native_name\";s:20:\"Українська\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/uk.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uk\";i:2;s:3:\"ukr\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:20:\"Продовжити\";}}s:2:\"ur\";a:8:{s:8:\"language\";s:2:\"ur\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-03-21 08:17:35\";s:12:\"english_name\";s:4:\"Urdu\";s:11:\"native_name\";s:8:\"اردو\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/ur.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"ur\";i:2;s:3:\"urd\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:19:\"جاری رکھیں\";}}s:5:\"uz_UZ\";a:8:{s:8:\"language\";s:5:\"uz_UZ\";s:7:\"version\";s:5:\"4.9.8\";s:7:\"updated\";s:19:\"2018-10-11 06:46:15\";s:12:\"english_name\";s:5:\"Uzbek\";s:11:\"native_name\";s:11:\"O‘zbekcha\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.8/uz_UZ.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"uz\";i:2;s:3:\"uzb\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:11:\"Davom etish\";}}s:2:\"vi\";a:8:{s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-03-25 02:35:54\";s:12:\"english_name\";s:10:\"Vietnamese\";s:11:\"native_name\";s:14:\"Tiếng Việt\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/4.9.12/vi.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"vi\";i:2;s:3:\"vie\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:12:\"Tiếp tục\";}}s:5:\"zh_TW\";a:8:{s:8:\"language\";s:5:\"zh_TW\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-12-05 18:56:44\";s:12:\"english_name\";s:16:\"Chinese (Taiwan)\";s:11:\"native_name\";s:12:\"繁體中文\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/zh_TW.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_HK\";a:8:{s:8:\"language\";s:5:\"zh_HK\";s:7:\"version\";s:5:\"4.9.5\";s:7:\"updated\";s:19:\"2018-04-09 00:56:52\";s:12:\"english_name\";s:19:\"Chinese (Hong Kong)\";s:11:\"native_name\";s:16:\"香港中文版	\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/translation/core/4.9.5/zh_HK.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"繼續\";}}s:5:\"zh_CN\";a:8:{s:8:\"language\";s:5:\"zh_CN\";s:7:\"version\";s:6:\"4.9.12\";s:7:\"updated\";s:19:\"2019-11-14 00:33:02\";s:12:\"english_name\";s:15:\"Chinese (China)\";s:11:\"native_name\";s:12:\"简体中文\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/translation/core/4.9.12/zh_CN.zip\";s:3:\"iso\";a:2:{i:1;s:2:\"zh\";i:2;s:3:\"zho\";}s:7:\"strings\";a:1:{s:8:\"continue\";s:6:\"继续\";}}}', 'no'),
(81714, '_site_transient_timeout_aioseop_update_check_time', '1594175017', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES 
(81715, '_site_transient_aioseop_update_check_time', '1594153417', 'no');
/*!40000 ALTER TABLE `wp_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=2370 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_postmeta`
--

LOCK TABLES `wp_postmeta` WRITE;
/*!40000 ALTER TABLE `wp_postmeta` DISABLE KEYS */;
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES 
(4, 5, '_edit_last', '1'),
(5, 5, '_edit_lock', '1543216257:1'),
(13, 10, '_edit_last', '1'),
(14, 10, '_edit_lock', '1558377156:1'),
(113, 19, '__defaults_set', '1'),
(114, 19, 'filter', 'raw'),
(115, 19, 'id_field', 'ID'),
(116, 20, '__defaults_set', '1'),
(117, 20, 'filter', 'raw'),
(118, 20, 'id_field', 'ID'),
(131, 23, '__defaults_set', '1'),
(132, 23, 'filter', 'raw'),
(133, 23, 'id_field', 'ID'),
(143, 25, '__defaults_set', '1'),
(144, 25, 'filter', 'raw'),
(145, 25, 'id_field', 'ID'),
(155, 27, '__defaults_set', '1'),
(156, 27, 'filter', 'raw'),
(157, 27, 'id_field', 'ID'),
(167, 29, '__defaults_set', '1'),
(168, 29, 'filter', 'raw'),
(169, 29, 'id_field', 'ID'),
(179, 31, '__defaults_set', '1'),
(180, 31, 'filter', 'raw'),
(181, 31, 'id_field', 'ID'),
(215, 12, 'filter', 'raw'),
(216, 12, 'meta_id', '19'),
(217, 12, 'post_id', '12'),
(218, 12, 'meta_key', 'name'),
(219, 12, 'meta_value', 'photocrati-nextgen_basic_thumbnails'),
(220, 12, 'title', 'NextGEN Basic Thumbnails'),
(221, 12, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_gallery\\static\\thumb_preview.jpg'),
(222, 12, 'default_source', 'galleries'),
(223, 12, 'view_order', '10000'),
(224, 12, 'name', 'photocrati-nextgen_basic_thumbnails'),
(225, 12, 'installed_at_version', '3.0.1'),
(226, 12, 'hidden_from_ui', ''),
(227, 12, 'hidden_from_igw', ''),
(228, 12, '__defaults_set', '1'),
(229, 12, 'entity_types', 'WyJpbWFnZSJd'),
(230, 12, 'aliases', 'WyJiYXNpY190aHVtYm5haWwiLCJiYXNpY190aHVtYm5haWxzIiwibmV4dGdlbl9iYXNpY190aHVtYm5haWxzIl0='),
(231, 12, 'id_field', 'ID'),
(232, 12, 'settings', 'eyJvdmVycmlkZV90aHVtYm5haWxfc2V0dGluZ3MiOiIwIiwidGh1bWJuYWlsX3dpZHRoIjoiMjQwIiwidGh1bWJuYWlsX2hlaWdodCI6IjE2MCIsInRodW1ibmFpbF9jcm9wIjoiMSIsImltYWdlc19wZXJfcGFnZSI6IjIwIiwibnVtYmVyX29mX2NvbHVtbnMiOiIwIiwiYWpheF9wYWdpbmF0aW9uIjoiMSIsInNob3dfYWxsX2luX2xpZ2h0Ym94IjoiMCIsInVzZV9pbWFnZWJyb3dzZXJfZWZmZWN0IjoiMCIsInNob3dfc2xpZGVzaG93X2xpbmsiOiIxIiwic2xpZGVzaG93X2xpbmtfdGV4dCI6IltcdTA0MWZcdTA0M2VcdTA0M2FcdTA0MzBcdTA0MzdcdTA0MzBcdTA0NDJcdTA0NGMgXHUwNDQxXHUwNDNiXHUwNDMwXHUwNDM5XHUwNDM0XHUwNDQ4XHUwNDNlXHUwNDQzXSIsInRlbXBsYXRlIjoiIiwidXNlX2xpZ2h0Ym94X2VmZmVjdCI6dHJ1ZSwiZGlzcGxheV9ub19pbWFnZXNfZXJyb3IiOjEsImRpc2FibGVfcGFnaW5hdGlvbiI6MCwidGh1bWJuYWlsX3F1YWxpdHkiOiIxMDAiLCJ0aHVtYm5haWxfd2F0ZXJtYXJrIjowLCJuZ2dfdHJpZ2dlcnNfZGlzcGxheSI6Im5ldmVyIiwiX2Vycm9ycyI6W119'),
(233, 13, 'filter', 'raw'),
(234, 13, 'meta_id', '33'),
(235, 13, 'post_id', '13'),
(236, 13, 'meta_key', 'name'),
(237, 13, 'meta_value', 'photocrati-nextgen_basic_slideshow'),
(238, 13, 'title', 'NextGEN Basic Slideshow'),
(239, 13, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_gallery\\static\\slideshow_preview.jpg'),
(240, 13, 'default_source', 'galleries'),
(241, 13, 'view_order', '10010'),
(242, 13, 'name', 'photocrati-nextgen_basic_slideshow'),
(243, 13, 'installed_at_version', '3.0.1'),
(244, 13, 'hidden_from_ui', ''),
(245, 13, 'hidden_from_igw', ''),
(246, 13, '__defaults_set', '1'),
(247, 13, 'entity_types', 'WyJpbWFnZSJd'),
(248, 13, 'aliases', 'WyJiYXNpY19zbGlkZXNob3ciLCJuZXh0Z2VuX2Jhc2ljX3NsaWRlc2hvdyJd'),
(249, 13, 'id_field', 'ID'),
(250, 13, 'settings', 'eyJnYWxsZXJ5X3dpZHRoIjoiNjAwIiwiZ2FsbGVyeV9oZWlnaHQiOiI0MDAiLCJjeWNsZV9lZmZlY3QiOiJmYWRlIiwiY3ljbGVfaW50ZXJ2YWwiOiIxMCIsInNob3dfdGh1bWJuYWlsX2xpbmsiOiIxIiwidGh1bWJuYWlsX2xpbmtfdGV4dCI6IltcdTA0MWZcdTA0M2VcdTA0M2FcdTA0MzBcdTA0MzdcdTA0MzBcdTA0NDJcdTA0NGMgXHUwNDRkXHUwNDQxXHUwNDNhXHUwNDM4XHUwNDM3XHUwNDRiXSIsInVzZV9saWdodGJveF9lZmZlY3QiOnRydWUsInRodW1ibmFpbF93aWR0aCI6MjQwLCJ0aHVtYm5haWxfaGVpZ2h0IjoxNjAsImVmZmVjdF9jb2RlIjoiY2xhc3M9XCJuZ2ctZmFuY3lib3hcIiByZWw9XCIlR0FMTEVSWV9OQU1FJVwiIiwidGVtcGxhdGUiOiIiLCJuZ2dfdHJpZ2dlcnNfZGlzcGxheSI6Im5ldmVyIiwiX2Vycm9ycyI6W119'),
(251, 14, 'filter', 'raw'),
(252, 14, 'meta_id', '47'),
(253, 14, 'post_id', '14'),
(254, 14, 'meta_key', 'name'),
(255, 14, 'meta_value', 'photocrati-nextgen_basic_imagebrowser'),
(256, 14, 'title', 'NextGEN Basic ImageBrowser'),
(257, 14, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_imagebrowser\\static\\preview.jpg'),
(258, 14, 'default_source', 'galleries'),
(259, 14, 'view_order', '10020'),
(260, 14, 'name', 'photocrati-nextgen_basic_imagebrowser'),
(261, 14, 'installed_at_version', '3.0.1'),
(262, 14, 'hidden_from_ui', ''),
(263, 14, 'hidden_from_igw', ''),
(264, 14, '__defaults_set', '1'),
(265, 14, 'entity_types', 'WyJpbWFnZSJd'),
(266, 14, 'aliases', 'WyJiYXNpY19pbWFnZWJyb3dzZXIiLCJpbWFnZWJyb3dzZXIiLCJuZXh0Z2VuX2Jhc2ljX2ltYWdlYnJvd3NlciJd'),
(267, 14, 'id_field', 'ID'),
(268, 14, 'settings', 'eyJhamF4X3BhZ2luYXRpb24iOiIwIiwidGVtcGxhdGUiOiIiLCJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJuZ2dfdHJpZ2dlcnNfZGlzcGxheSI6Im5ldmVyIiwiX2Vycm9ycyI6W119'),
(269, 15, 'filter', 'raw'),
(270, 15, 'meta_id', '63'),
(271, 15, 'post_id', '15'),
(272, 15, 'meta_key', 'name'),
(273, 15, 'meta_value', 'photocrati-nextgen_basic_singlepic'),
(274, 15, 'title', 'NextGEN Basic SinglePic'),
(275, 15, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_singlepic\\static\\preview.gif'),
(276, 15, 'default_source', 'galleries'),
(277, 15, 'view_order', '10060'),
(278, 15, 'hidden_from_ui', '1'),
(279, 15, 'hidden_from_igw', '1'),
(280, 15, 'name', 'photocrati-nextgen_basic_singlepic'),
(281, 15, 'installed_at_version', '3.0.1'),
(282, 15, '__defaults_set', '1'),
(283, 15, 'entity_types', 'WyJpbWFnZSJd'),
(284, 15, 'aliases', 'WyJiYXNpY19zaW5nbGVwaWMiLCJzaW5nbGVwaWMiLCJuZXh0Z2VuX2Jhc2ljX3NpbmdsZXBpYyJd'),
(285, 15, 'id_field', 'ID'),
(286, 15, 'settings', 'eyJ3aWR0aCI6IiIsImhlaWdodCI6IiIsImxpbmsiOiIiLCJsaW5rX3RhcmdldCI6Il9ibGFuayIsImZsb2F0IjoiIiwicXVhbGl0eSI6IjEwMCIsImNyb3AiOiIwIiwiZGlzcGxheV93YXRlcm1hcmsiOiIwIiwiZGlzcGxheV9yZWZsZWN0aW9uIjoiMCIsInRlbXBsYXRlIjoiIiwidXNlX2xpZ2h0Ym94X2VmZmVjdCI6dHJ1ZSwibW9kZSI6IiIsIm5nZ190cmlnZ2Vyc19kaXNwbGF5IjoibmV2ZXIiLCJfZXJyb3JzIjpbXX0='),
(287, 16, 'filter', 'raw'),
(288, 16, 'meta_id', '75'),
(289, 16, 'post_id', '16'),
(290, 16, 'meta_key', 'name'),
(291, 16, 'meta_value', 'photocrati-nextgen_basic_tagcloud'),
(292, 16, 'title', 'NextGEN Basic TagCloud'),
(293, 16, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_tagcloud\\static\\preview.gif'),
(294, 16, 'default_source', 'tags'),
(295, 16, 'view_order', '10100'),
(296, 16, 'name', 'photocrati-nextgen_basic_tagcloud'),
(297, 16, 'installed_at_version', '3.0.1'),
(298, 16, 'hidden_from_ui', ''),
(299, 16, 'hidden_from_igw', ''),
(300, 16, '__defaults_set', '1'),
(301, 16, 'entity_types', 'WyJpbWFnZSJd'),
(302, 16, 'aliases', 'WyJiYXNpY190YWdjbG91ZCIsInRhZ2Nsb3VkIiwibmV4dGdlbl9iYXNpY190YWdjbG91ZCJd'),
(303, 16, 'id_field', 'ID'),
(304, 16, 'settings', 'eyJudW1iZXIiOiI0NSIsImdhbGxlcnlfZGlzcGxheV90eXBlIjoicGhvdG9jcmF0aS1uZXh0Z2VuX2Jhc2ljX3RodW1ibmFpbHMiLCJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJuZ2dfdHJpZ2dlcnNfZGlzcGxheSI6Im5ldmVyIiwiX2Vycm9ycyI6W119'),
(305, 17, 'filter', 'raw'),
(306, 17, 'meta_id', '89'),
(307, 17, 'post_id', '17'),
(308, 17, 'meta_key', 'name'),
(309, 17, 'meta_value', 'photocrati-nextgen_basic_compact_album'),
(310, 17, 'title', 'NextGEN Basic Compact Album'),
(311, 17, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_album\\static\\compact_preview.jpg'),
(312, 17, 'default_source', 'albums'),
(313, 17, 'view_order', '10200'),
(314, 17, 'name', 'photocrati-nextgen_basic_compact_album'),
(315, 17, 'installed_at_version', '3.0.1'),
(316, 17, 'hidden_from_ui', ''),
(317, 17, 'hidden_from_igw', ''),
(318, 17, '__defaults_set', '1'),
(319, 17, 'entity_types', 'WyJhbGJ1bSIsImdhbGxlcnkiXQ=='),
(320, 17, 'aliases', 'WyJiYXNpY19jb21wYWN0X2FsYnVtIiwibmV4dGdlbl9iYXNpY19hbGJ1bSIsImJhc2ljX2FsYnVtX2NvbXBhY3QiLCJjb21wYWN0X2FsYnVtIl0='),
(321, 17, 'id_field', 'ID'),
(322, 17, 'settings', 'eyJnYWxsZXJ5X2Rpc3BsYXlfdHlwZSI6InBob3RvY3JhdGktbmV4dGdlbl9iYXNpY190aHVtYm5haWxzIiwiZ2FsbGVyaWVzX3Blcl9wYWdlIjoiMCIsImVuYWJsZV9icmVhZGNydW1icyI6IjEiLCJ0ZW1wbGF0ZSI6IiIsImVuYWJsZV9kZXNjcmlwdGlvbnMiOiIwIiwib3ZlcnJpZGVfdGh1bWJuYWlsX3NldHRpbmdzIjoiMCIsInRodW1ibmFpbF93aWR0aCI6IjI0MCIsInRodW1ibmFpbF9oZWlnaHQiOiIxNjAiLCJ0aHVtYm5haWxfY3JvcCI6IjAiLCJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJkaXNhYmxlX3BhZ2luYXRpb24iOjAsIm9wZW5fZ2FsbGVyeV9pbl9saWdodGJveCI6MCwiZ2FsbGVyeV9kaXNwbGF5X3RlbXBsYXRlIjoiIiwibmdnX3RyaWdnZXJzX2Rpc3BsYXkiOiJuZXZlciIsIl9lcnJvcnMiOltdfQ=='),
(323, 18, 'filter', 'raw'),
(324, 18, 'meta_id', '103'),
(325, 18, 'post_id', '18'),
(326, 18, 'meta_key', 'name'),
(327, 18, 'meta_value', 'photocrati-nextgen_basic_extended_album'),
(328, 18, 'title', 'NextGEN Basic Extended Album'),
(329, 18, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_album\\static\\extended_preview.jpg'),
(330, 18, 'default_source', 'albums'),
(331, 18, 'view_order', '10210'),
(332, 18, 'name', 'photocrati-nextgen_basic_extended_album'),
(333, 18, 'installed_at_version', '3.0.1'),
(334, 18, 'hidden_from_ui', ''),
(335, 18, 'hidden_from_igw', ''),
(336, 18, '__defaults_set', '1'),
(337, 18, 'entity_types', 'WyJhbGJ1bSIsImdhbGxlcnkiXQ=='),
(338, 18, 'aliases', 'WyJiYXNpY19leHRlbmRlZF9hbGJ1bSIsIm5leHRnZW5fYmFzaWNfZXh0ZW5kZWRfYWxidW0iLCJleHRlbmRlZF9hbGJ1bSJd'),
(339, 18, 'id_field', 'ID'),
(340, 18, 'settings', 'eyJnYWxsZXJ5X2Rpc3BsYXlfdHlwZSI6InBob3RvY3JhdGktbmV4dGdlbl9iYXNpY190aHVtYm5haWxzIiwiZ2FsbGVyaWVzX3Blcl9wYWdlIjoiMCIsImVuYWJsZV9icmVhZGNydW1icyI6IjEiLCJ0ZW1wbGF0ZSI6IiIsImVuYWJsZV9kZXNjcmlwdGlvbnMiOiIwIiwib3ZlcnJpZGVfdGh1bWJuYWlsX3NldHRpbmdzIjoiMCIsInRodW1ibmFpbF93aWR0aCI6IjI0MCIsInRodW1ibmFpbF9oZWlnaHQiOiIxNjAiLCJ0aHVtYm5haWxfY3JvcCI6IjEiLCJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJkaXNhYmxlX3BhZ2luYXRpb24iOjAsIm9wZW5fZ2FsbGVyeV9pbl9saWdodGJveCI6MCwidGh1bWJuYWlsX3F1YWxpdHkiOjEwMCwidGh1bWJuYWlsX3dhdGVybWFyayI6MCwiZ2FsbGVyeV9kaXNwbGF5X3RlbXBsYXRlIjoiIiwibmdnX3RyaWdnZXJzX2Rpc3BsYXkiOiJuZXZlciIsIl9lcnJvcnMiOltdfQ=='),
(341, 35, '_edit_last', '1'),
(342, 35, '_edit_lock', '1531958910:1'),
(361, 35, '_eg_in_gallery', 'a:55:{i:0;i:36;i:1;i:37;i:2;i:38;i:3;i:39;i:4;i:40;i:5;i:213;i:6;i:214;i:7;i:215;i:8;i:216;i:9;i:217;i:10;i:218;i:11;i:219;i:12;i:220;i:13;i:221;i:14;i:222;i:15;i:223;i:16;i:224;i:17;i:225;i:18;i:226;i:19;i:227;i:20;i:228;i:21;i:229;i:22;i:230;i:23;i:231;i:24;i:232;i:25;i:233;i:26;i:234;i:27;i:235;i:28;i:236;i:29;i:237;i:30;i:238;i:31;i:239;i:32;i:240;i:33;i:241;i:34;i:242;i:35;i:243;i:36;i:244;i:37;i:245;i:38;i:246;i:39;i:247;i:40;i:248;i:41;i:249;i:42;i:250;i:43;i:251;i:44;i:252;i:45;i:253;i:46;i:254;i:47;i:255;i:48;i:256;i:49;i:257;i:50;i:258;i:51;i:259;i:52;i:260;i:53;i:261;i:54;i:262;}'),
(362, 35, '_eg_gallery_data', 'a:3:{s:2:\"id\";i:35;s:7:\"gallery\";a:55:{i:36;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:59:\"http://wsite/wp-content/uploads/2018/07/maxresdefault-1.jpg\";s:5:\"title\";s:17:\"maxresdefault (1)\";s:4:\"link\";s:59:\"http://wsite/wp-content/uploads/2018/07/maxresdefault-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:37;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:57:\"http://wsite/wp-content/uploads/2018/07/maxresdefault.jpg\";s:5:\"title\";s:13:\"maxresdefault\";s:4:\"link\";s:57:\"http://wsite/wp-content/uploads/2018/07/maxresdefault.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:38;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:50:\"http://wsite/wp-content/uploads/2018/07/Mining.jpg\";s:5:\"title\";s:6:\"Mining\";s:4:\"link\";s:50:\"http://wsite/wp-content/uploads/2018/07/Mining.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:39;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:77:\"http://wsite/wp-content/uploads/2018/07/pank-kultura-raznovidnosti-zhanra.jpg\";s:5:\"title\";s:33:\"pank-kultura-raznovidnosti-zhanra\";s:4:\"link\";s:77:\"http://wsite/wp-content/uploads/2018/07/pank-kultura-raznovidnosti-zhanra.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:40;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:73:\"http://wsite/wp-content/uploads/2018/07/regnum_picture_1484072057_big.jpg\";s:5:\"title\";s:29:\"regnum_picture_1484072057_big\";s:4:\"link\";s:73:\"http://wsite/wp-content/uploads/2018/07/regnum_picture_1484072057_big.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:213;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-1-1.jpg\";s:5:\"title\";s:6:\"11 (1)\";s:4:\"link\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-1-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:214;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-2-1.jpg\";s:5:\"title\";s:6:\"11 (2)\";s:4:\"link\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-2-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:215;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-3-1.jpg\";s:5:\"title\";s:6:\"11 (3)\";s:4:\"link\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-3-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:216;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-4-1.jpg\";s:5:\"title\";s:6:\"11 (4)\";s:4:\"link\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-4-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:217;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-5-1.jpg\";s:5:\"title\";s:6:\"11 (5)\";s:4:\"link\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-5-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:218;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-6-1.jpg\";s:5:\"title\";s:6:\"11 (6)\";s:4:\"link\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-6-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:219;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-7-1.jpg\";s:5:\"title\";s:6:\"11 (7)\";s:4:\"link\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-7-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:220;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-8-1.jpg\";s:5:\"title\";s:44:\"Photo of water splash on a black background.\";s:4:\"link\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-8-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:44:\"Photo of water splash on a black background.\";s:5:\"thumb\";s:0:\"\";}i:221;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-9-1.jpg\";s:5:\"title\";s:6:\"11 (9)\";s:4:\"link\";s:50:\"http://wsite/wp-content/uploads/2018/07/11-9-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:222;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-10-1.jpg\";s:5:\"title\";s:7:\"11 (10)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-10-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:223;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-11-1.jpg\";s:5:\"title\";s:7:\"11 (11)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-11-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:224;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-12-1.jpg\";s:5:\"title\";s:7:\"11 (12)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-12-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:225;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-13-1.jpg\";s:5:\"title\";s:7:\"11 (13)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-13-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:226;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-14-1.jpg\";s:5:\"title\";s:7:\"11 (14)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-14-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:227;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-15-1.jpg\";s:5:\"title\";s:7:\"11 (15)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-15-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:228;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-16-1.jpg\";s:5:\"title\";s:7:\"11 (16)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-16-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:229;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-17-1.jpg\";s:5:\"title\";s:7:\"11 (17)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-17-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:230;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-18-1.jpg\";s:5:\"title\";s:7:\"11 (18)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-18-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:231;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-19-1.jpg\";s:5:\"title\";s:7:\"11 (19)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-19-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:232;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-20-1.jpg\";s:5:\"title\";s:7:\"11 (20)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-20-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:233;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-21-1.jpg\";s:5:\"title\";s:7:\"11 (21)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-21-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:234;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-22-1.jpg\";s:5:\"title\";s:7:\"11 (22)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-22-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:235;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-23-1.jpg\";s:5:\"title\";s:7:\"11 (23)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-23-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:236;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-24-1.jpg\";s:5:\"title\";s:7:\"11 (24)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-24-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:237;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-25-1.jpg\";s:5:\"title\";s:7:\"11 (25)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-25-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:238;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-26-1.jpg\";s:5:\"title\";s:7:\"11 (26)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-26-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:239;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-27-1.jpg\";s:5:\"title\";s:7:\"11 (27)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-27-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:240;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-28-1.jpg\";s:5:\"title\";s:7:\"11 (28)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-28-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:241;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-29-1.jpg\";s:5:\"title\";s:22:\"OLYMPUS DIGITAL CAMERA\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-29-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:22:\"OLYMPUS DIGITAL CAMERA\";s:5:\"thumb\";s:0:\"\";}i:242;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-30-1.jpg\";s:5:\"title\";s:7:\"11 (30)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-30-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:243;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-31-1.jpg\";s:5:\"title\";s:7:\"11 (31)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-31-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:244;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-32-1.jpg\";s:5:\"title\";s:7:\"11 (32)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-32-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:245;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-33-1.jpg\";s:5:\"title\";s:7:\"11 (33)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-33-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:246;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-34-1.jpg\";s:5:\"title\";s:7:\"11 (34)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-34-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:247;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-35-1.jpg\";s:5:\"title\";s:7:\"11 (35)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-35-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:248;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-36-1.jpg\";s:5:\"title\";s:7:\"11 (36)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-36-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:249;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-37-1.jpg\";s:5:\"title\";s:7:\"11 (37)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-37-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:250;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-38-1.jpg\";s:5:\"title\";s:7:\"11 (38)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-38-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:251;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-39-1.jpg\";s:5:\"title\";s:7:\"11 (39)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-39-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:252;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-40-1.jpg\";s:5:\"title\";s:7:\"11 (40)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-40-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:253;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-41-1.jpg\";s:5:\"title\";s:7:\"11 (41)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-41-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:254;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-42-1.jpg\";s:5:\"title\";s:7:\"11 (42)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-42-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:255;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-43-1.jpg\";s:5:\"title\";s:7:\"11 (43)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-43-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:256;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-44-1.jpg\";s:5:\"title\";s:7:\"11 (44)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-44-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:257;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-45-1.jpg\";s:5:\"title\";s:7:\"11 (45)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-45-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:258;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-46-1.jpg\";s:5:\"title\";s:7:\"11 (46)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-46-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:259;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-47-1.jpg\";s:5:\"title\";s:7:\"11 (47)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-47-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:260;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-48-1.jpg\";s:5:\"title\";s:7:\"11 (48)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-48-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:261;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-49-1.jpg\";s:5:\"title\";s:7:\"11 (49)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-49-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}i:262;a:7:{s:6:\"status\";s:6:\"active\";s:3:\"src\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-50-1.jpg\";s:5:\"title\";s:7:\"11 (50)\";s:4:\"link\";s:51:\"http://wsite/wp-content/uploads/2018/07/11-50-1.jpg\";s:3:\"alt\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:5:\"thumb\";s:0:\"\";}}s:6:\"config\";a:21:{s:4:\"type\";s:7:\"default\";s:7:\"columns\";s:1:\"0\";s:13:\"gallery_theme\";s:4:\"base\";s:23:\"justified_gallery_theme\";s:6:\"normal\";s:6:\"gutter\";i:10;s:6:\"margin\";i:10;s:10:\"image_size\";s:5:\"large\";s:10:\"crop_width\";i:640;s:11:\"crop_height\";i:480;s:4:\"crop\";i:0;s:12:\"lazy_loading\";i:0;s:18:\"lazy_loading_delay\";i:500;s:20:\"justified_row_height\";i:150;s:16:\"lightbox_enabled\";i:1;s:14:\"lightbox_theme\";s:4:\"base\";s:19:\"lightbox_image_size\";s:6:\"medium\";s:13:\"title_display\";s:5:\"float\";s:7:\"classes\";a:1:{i:0;s:0:\"\";}s:3:\"rtl\";i:0;s:5:\"title\";s:14:\"Галерея\";s:4:\"slug\";s:0:\"\";}}'),
(363, 42, '_edit_last', '1'),
(364, 42, 'foogallery_attachments', 'a:5:{i:0;s:2:\"36\";i:1;s:2:\"37\";i:2;s:2:\"38\";i:3;s:2:\"39\";i:4;s:2:\"40\";}'),
(365, 42, 'foogallery_template', 'default'),
(366, 42, '_foogallery_settings', 'a:23:{s:21:\"foogallery_items_view\";s:6:\"manage\";s:28:\"default_thumbnail_dimensions\";a:2:{s:5:\"width\";s:3:\"150\";s:6:\"height\";s:3:\"150\";}s:22:\"default_thumbnail_link\";s:4:\"page\";s:16:\"default_lightbox\";s:4:\"none\";s:15:\"default_spacing\";s:12:\"fg-gutter-10\";s:17:\"default_alignment\";s:9:\"fg-center\";s:13:\"default_theme\";s:8:\"fg-light\";s:19:\"default_border_size\";s:14:\"fg-border-thin\";s:23:\"default_rounded_corners\";s:0:\"\";s:19:\"default_drop_shadow\";s:17:\"fg-shadow-outline\";s:20:\"default_inner_shadow\";s:0:\"\";s:20:\"default_loading_icon\";s:18:\"fg-loading-default\";s:21:\"default_loaded_effect\";s:17:\"fg-loaded-fade-in\";s:26:\"default_hover_effect_color\";s:0:\"\";s:26:\"default_hover_effect_scale\";s:0:\"\";s:39:\"default_hover_effect_caption_visibility\";s:16:\"fg-caption-hover\";s:31:\"default_hover_effect_transition\";s:13:\"fg-hover-fade\";s:25:\"default_hover_effect_icon\";s:13:\"fg-hover-zoom\";s:28:\"default_caption_title_source\";s:0:\"\";s:27:\"default_caption_desc_source\";s:0:\"\";s:29:\"default_captions_limit_length\";s:0:\"\";s:19:\"default_paging_type\";s:0:\"\";s:16:\"default_lazyload\";s:0:\"\";}'),
(367, 42, 'foogallery_sort', ''),
(368, 42, '_edit_lock', '1531945755:1'),
(369, 10, '_foogallery_css', 'a:1:{s:15:\"foogallery-core\";a:5:{s:3:\"src\";s:101:\"http://wsite/wp-content/plugins/foogallery/extensions/default-templates/shared/css/foogallery.min.css\";s:4:\"deps\";a:0:{}s:3:\"ver\";s:6:\"1.4.31\";s:5:\"media\";s:3:\"all\";s:4:\"site\";s:12:\"http://wsite\";}}'),
(370, 44, 'title', 'NextGEN Basic Thumbnails'),
(371, 44, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_gallery\\static\\thumb_preview.jpg'),
(372, 44, 'default_source', 'galleries'),
(373, 44, 'view_order', '10000'),
(374, 44, 'name', 'photocrati-nextgen_basic_thumbnails'),
(375, 44, 'installed_at_version', '3.0.1'),
(376, 44, 'hidden_from_ui', ''),
(377, 44, 'hidden_from_igw', ''),
(378, 44, '__defaults_set', '1'),
(379, 44, 'filter', 'raw'),
(380, 44, 'entity_types', 'WyJpbWFnZSJd'),
(381, 44, 'aliases', 'WyJiYXNpY190aHVtYm5haWwiLCJiYXNpY190aHVtYm5haWxzIiwibmV4dGdlbl9iYXNpY190aHVtYm5haWxzIl0='),
(382, 44, 'id_field', 'ID'),
(383, 44, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJpbWFnZXNfcGVyX3BhZ2UiOiIyMCIsIm51bWJlcl9vZl9jb2x1bW5zIjowLCJ0aHVtYm5haWxfd2lkdGgiOjI0MCwidGh1bWJuYWlsX2hlaWdodCI6MTYwLCJzaG93X2FsbF9pbl9saWdodGJveCI6MCwiYWpheF9wYWdpbmF0aW9uIjowLCJ1c2VfaW1hZ2Vicm93c2VyX2VmZmVjdCI6MCwidGVtcGxhdGUiOiIiLCJkaXNwbGF5X25vX2ltYWdlc19lcnJvciI6MSwiZGlzYWJsZV9wYWdpbmF0aW9uIjowLCJzaG93X3NsaWRlc2hvd19saW5rIjoxLCJzbGlkZXNob3dfbGlua190ZXh0IjoiW1x1MDQxZlx1MDQzZVx1MDQzYVx1MDQzMFx1MDQzN1x1MDQzMFx1MDQ0Mlx1MDQ0YyBcdTA0NDFcdTA0M2JcdTA0MzBcdTA0MzlcdTA0MzRcdTA0NDhcdTA0M2VcdTA0NDNdIiwib3ZlcnJpZGVfdGh1bWJuYWlsX3NldHRpbmdzIjowLCJ0aHVtYm5haWxfcXVhbGl0eSI6IjEwMCIsInRodW1ibmFpbF9jcm9wIjoxLCJ0aHVtYm5haWxfd2F0ZXJtYXJrIjowLCJuZ2dfdHJpZ2dlcnNfZGlzcGxheSI6Im5ldmVyIiwiX2Vycm9ycyI6W119'),
(384, 45, 'title', 'NextGEN Basic Slideshow'),
(385, 45, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_gallery\\static\\slideshow_preview.jpg'),
(386, 45, 'default_source', 'galleries'),
(387, 45, 'view_order', '10010'),
(388, 45, 'name', 'photocrati-nextgen_basic_slideshow'),
(389, 45, 'installed_at_version', '3.0.1'),
(390, 45, 'hidden_from_ui', ''),
(391, 45, 'hidden_from_igw', ''),
(392, 45, '__defaults_set', '1'),
(393, 45, 'filter', 'raw'),
(394, 45, 'entity_types', 'WyJpbWFnZSJd'),
(395, 45, 'aliases', 'WyJiYXNpY19zbGlkZXNob3ciLCJuZXh0Z2VuX2Jhc2ljX3NsaWRlc2hvdyJd'),
(396, 45, 'id_field', 'ID'),
(397, 45, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJnYWxsZXJ5X3dpZHRoIjo2MDAsImdhbGxlcnlfaGVpZ2h0Ijo0MDAsInRodW1ibmFpbF93aWR0aCI6MjQwLCJ0aHVtYm5haWxfaGVpZ2h0IjoxNjAsImN5Y2xlX2ludGVydmFsIjoxMCwiY3ljbGVfZWZmZWN0IjoiZmFkZSIsImVmZmVjdF9jb2RlIjoiY2xhc3M9XCJuZ2ctZmFuY3lib3hcIiByZWw9XCIlR0FMTEVSWV9OQU1FJVwiIiwic2hvd190aHVtYm5haWxfbGluayI6MSwidGh1bWJuYWlsX2xpbmtfdGV4dCI6IltcdTA0MWZcdTA0M2VcdTA0M2FcdTA0MzBcdTA0MzdcdTA0MzBcdTA0NDJcdTA0NGMgXHUwNDRkXHUwNDQxXHUwNDNhXHUwNDM4XHUwNDM3XHUwNDRiXSIsInRlbXBsYXRlIjoiIiwibmdnX3RyaWdnZXJzX2Rpc3BsYXkiOiJuZXZlciIsIl9lcnJvcnMiOltdfQ=='),
(398, 46, 'title', 'NextGEN Basic ImageBrowser'),
(399, 46, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_imagebrowser\\static\\preview.jpg'),
(400, 46, 'default_source', 'galleries'),
(401, 46, 'view_order', '10020'),
(402, 46, 'name', 'photocrati-nextgen_basic_imagebrowser'),
(403, 46, 'installed_at_version', '3.0.1'),
(404, 46, 'hidden_from_ui', ''),
(405, 46, 'hidden_from_igw', ''),
(406, 46, '__defaults_set', '1'),
(407, 46, 'filter', 'raw'),
(408, 46, 'entity_types', 'WyJpbWFnZSJd'),
(409, 46, 'aliases', 'WyJiYXNpY19pbWFnZWJyb3dzZXIiLCJpbWFnZWJyb3dzZXIiLCJuZXh0Z2VuX2Jhc2ljX2ltYWdlYnJvd3NlciJd'),
(410, 46, 'id_field', 'ID'),
(411, 46, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJ0ZW1wbGF0ZSI6IiIsIm5nZ190cmlnZ2Vyc19kaXNwbGF5IjoibmV2ZXIiLCJfZXJyb3JzIjpbXX0='),
(412, 47, 'title', 'NextGEN Basic SinglePic'),
(413, 47, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_singlepic\\static\\preview.gif'),
(414, 47, 'default_source', 'galleries'),
(415, 47, 'view_order', '10060'),
(416, 47, 'hidden_from_ui', '1'),
(417, 47, 'hidden_from_igw', '1'),
(418, 47, 'name', 'photocrati-nextgen_basic_singlepic'),
(419, 47, 'installed_at_version', '3.0.1'),
(420, 47, '__defaults_set', '1'),
(421, 47, 'filter', 'raw'),
(422, 47, 'entity_types', 'WyJpbWFnZSJd'),
(423, 47, 'aliases', 'WyJiYXNpY19zaW5nbGVwaWMiLCJzaW5nbGVwaWMiLCJuZXh0Z2VuX2Jhc2ljX3NpbmdsZXBpYyJd'),
(424, 47, 'id_field', 'ID'),
(425, 47, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJ3aWR0aCI6IiIsImhlaWdodCI6IiIsIm1vZGUiOiIiLCJkaXNwbGF5X3dhdGVybWFyayI6MCwiZGlzcGxheV9yZWZsZWN0aW9uIjowLCJmbG9hdCI6IiIsImxpbmsiOiIiLCJsaW5rX3RhcmdldCI6Il9ibGFuayIsInF1YWxpdHkiOjEwMCwiY3JvcCI6MCwidGVtcGxhdGUiOiIiLCJuZ2dfdHJpZ2dlcnNfZGlzcGxheSI6Im5ldmVyIiwiX2Vycm9ycyI6W119'),
(426, 48, 'title', 'NextGEN Basic TagCloud'),
(427, 48, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_tagcloud\\static\\preview.gif'),
(428, 48, 'default_source', 'tags'),
(429, 48, 'view_order', '10100'),
(430, 48, 'name', 'photocrati-nextgen_basic_tagcloud'),
(431, 48, 'installed_at_version', '3.0.1'),
(432, 48, 'hidden_from_ui', ''),
(433, 48, 'hidden_from_igw', ''),
(434, 48, '__defaults_set', '1'),
(435, 48, 'filter', 'raw'),
(436, 48, 'entity_types', 'WyJpbWFnZSJd'),
(437, 48, 'aliases', 'WyJiYXNpY190YWdjbG91ZCIsInRhZ2Nsb3VkIiwibmV4dGdlbl9iYXNpY190YWdjbG91ZCJd'),
(438, 48, 'id_field', 'ID'),
(439, 48, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJnYWxsZXJ5X2Rpc3BsYXlfdHlwZSI6InBob3RvY3JhdGktbmV4dGdlbl9iYXNpY190aHVtYm5haWxzIiwibnVtYmVyIjo0NSwibmdnX3RyaWdnZXJzX2Rpc3BsYXkiOiJuZXZlciIsIl9lcnJvcnMiOltdfQ=='),
(440, 49, 'title', 'NextGEN Basic Compact Album'),
(441, 49, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_album\\static\\compact_preview.jpg'),
(442, 49, 'default_source', 'albums'),
(443, 49, 'view_order', '10200'),
(444, 49, 'name', 'photocrati-nextgen_basic_compact_album'),
(445, 49, 'installed_at_version', '3.0.1'),
(446, 49, 'hidden_from_ui', ''),
(447, 49, 'hidden_from_igw', ''),
(448, 49, '__defaults_set', '1'),
(449, 49, 'filter', 'raw'),
(450, 49, 'entity_types', 'WyJhbGJ1bSIsImdhbGxlcnkiXQ=='),
(451, 49, 'aliases', 'WyJiYXNpY19jb21wYWN0X2FsYnVtIiwibmV4dGdlbl9iYXNpY19hbGJ1bSIsImJhc2ljX2FsYnVtX2NvbXBhY3QiLCJjb21wYWN0X2FsYnVtIl0='),
(452, 49, 'id_field', 'ID'),
(453, 49, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJnYWxsZXJpZXNfcGVyX3BhZ2UiOjAsImVuYWJsZV9icmVhZGNydW1icyI6MSwiZGlzYWJsZV9wYWdpbmF0aW9uIjowLCJlbmFibGVfZGVzY3JpcHRpb25zIjowLCJ0ZW1wbGF0ZSI6IiIsIm9wZW5fZ2FsbGVyeV9pbl9saWdodGJveCI6MCwiZ2FsbGVyeV9kaXNwbGF5X3R5cGUiOiJwaG90b2NyYXRpLW5leHRnZW5fYmFzaWNfdGh1bWJuYWlscyIsImdhbGxlcnlfZGlzcGxheV90ZW1wbGF0ZSI6IiIsIm5nZ190cmlnZ2Vyc19kaXNwbGF5IjoibmV2ZXIiLCJfZXJyb3JzIjpbXX0='),
(454, 50, 'title', 'NextGEN Basic Extended Album'),
(455, 50, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_album\\static\\extended_preview.jpg'),
(456, 50, 'default_source', 'albums'),
(457, 50, 'view_order', '10210'),
(458, 50, 'name', 'photocrati-nextgen_basic_extended_album'),
(459, 50, 'installed_at_version', '3.0.1'),
(460, 50, 'hidden_from_ui', ''),
(461, 50, 'hidden_from_igw', ''),
(462, 50, '__defaults_set', '1'),
(463, 50, 'filter', 'raw'),
(464, 50, 'entity_types', 'WyJhbGJ1bSIsImdhbGxlcnkiXQ=='),
(465, 50, 'aliases', 'WyJiYXNpY19leHRlbmRlZF9hbGJ1bSIsIm5leHRnZW5fYmFzaWNfZXh0ZW5kZWRfYWxidW0iLCJleHRlbmRlZF9hbGJ1bSJd'),
(466, 50, 'id_field', 'ID'),
(467, 50, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJnYWxsZXJpZXNfcGVyX3BhZ2UiOjAsImVuYWJsZV9icmVhZGNydW1icyI6MSwiZGlzYWJsZV9wYWdpbmF0aW9uIjowLCJlbmFibGVfZGVzY3JpcHRpb25zIjowLCJ0ZW1wbGF0ZSI6IiIsIm9wZW5fZ2FsbGVyeV9pbl9saWdodGJveCI6MCwib3ZlcnJpZGVfdGh1bWJuYWlsX3NldHRpbmdzIjowLCJ0aHVtYm5haWxfd2lkdGgiOjI0MCwidGh1bWJuYWlsX2hlaWdodCI6MTYwLCJ0aHVtYm5haWxfcXVhbGl0eSI6MTAwLCJ0aHVtYm5haWxfY3JvcCI6dHJ1ZSwidGh1bWJuYWlsX3dhdGVybWFyayI6MCwiZ2FsbGVyeV9kaXNwbGF5X3R5cGUiOiJwaG90b2NyYXRpLW5leHRnZW5fYmFzaWNfdGh1bWJuYWlscyIsImdhbGxlcnlfZGlzcGxheV90ZW1wbGF0ZSI6IiIsIm5nZ190cmlnZ2Vyc19kaXNwbGF5IjoibmV2ZXIiLCJfZXJyb3JzIjpbXX0='),
(569, 102, 'title', 'NextGEN Basic Thumbnails'),
(570, 102, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_gallery\\static\\thumb_preview.jpg'),
(571, 102, 'default_source', 'galleries'),
(572, 102, 'view_order', '10000'),
(573, 102, 'name', 'photocrati-nextgen_basic_thumbnails'),
(574, 102, 'installed_at_version', '3.0.1'),
(575, 102, 'hidden_from_ui', ''),
(576, 102, 'hidden_from_igw', ''),
(577, 102, '__defaults_set', '1'),
(578, 102, 'filter', 'raw'),
(579, 102, 'entity_types', 'WyJpbWFnZSJd'),
(580, 102, 'aliases', 'WyJiYXNpY190aHVtYm5haWwiLCJiYXNpY190aHVtYm5haWxzIiwibmV4dGdlbl9iYXNpY190aHVtYm5haWxzIl0='),
(581, 102, 'id_field', 'ID'),
(582, 102, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJpbWFnZXNfcGVyX3BhZ2UiOiIyMCIsIm51bWJlcl9vZl9jb2x1bW5zIjowLCJ0aHVtYm5haWxfd2lkdGgiOjI0MCwidGh1bWJuYWlsX2hlaWdodCI6MTYwLCJzaG93X2FsbF9pbl9saWdodGJveCI6MCwiYWpheF9wYWdpbmF0aW9uIjowLCJ1c2VfaW1hZ2Vicm93c2VyX2VmZmVjdCI6MCwidGVtcGxhdGUiOiIiLCJkaXNwbGF5X25vX2ltYWdlc19lcnJvciI6MSwiZGlzYWJsZV9wYWdpbmF0aW9uIjowLCJzaG93X3NsaWRlc2hvd19saW5rIjoxLCJzbGlkZXNob3dfbGlua190ZXh0IjoiW1x1MDQxZlx1MDQzZVx1MDQzYVx1MDQzMFx1MDQzN1x1MDQzMFx1MDQ0Mlx1MDQ0YyBcdTA0NDFcdTA0M2JcdTA0MzBcdTA0MzlcdTA0MzRcdTA0NDhcdTA0M2VcdTA0NDNdIiwib3ZlcnJpZGVfdGh1bWJuYWlsX3NldHRpbmdzIjowLCJ0aHVtYm5haWxfcXVhbGl0eSI6IjEwMCIsInRodW1ibmFpbF9jcm9wIjoxLCJ0aHVtYm5haWxfd2F0ZXJtYXJrIjowLCJuZ2dfdHJpZ2dlcnNfZGlzcGxheSI6Im5ldmVyIiwiX2Vycm9ycyI6W119'),
(583, 103, 'title', 'NextGEN Basic Slideshow'),
(584, 103, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_gallery\\static\\slideshow_preview.jpg'),
(585, 103, 'default_source', 'galleries'),
(586, 103, 'view_order', '10010'),
(587, 103, 'name', 'photocrati-nextgen_basic_slideshow'),
(588, 103, 'installed_at_version', '3.0.1'),
(589, 103, 'hidden_from_ui', ''),
(590, 103, 'hidden_from_igw', ''),
(591, 103, '__defaults_set', '1'),
(592, 103, 'filter', 'raw'),
(593, 103, 'entity_types', 'WyJpbWFnZSJd'),
(594, 103, 'aliases', 'WyJiYXNpY19zbGlkZXNob3ciLCJuZXh0Z2VuX2Jhc2ljX3NsaWRlc2hvdyJd'),
(595, 103, 'id_field', 'ID'),
(596, 103, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJnYWxsZXJ5X3dpZHRoIjo2MDAsImdhbGxlcnlfaGVpZ2h0Ijo0MDAsInRodW1ibmFpbF93aWR0aCI6MjQwLCJ0aHVtYm5haWxfaGVpZ2h0IjoxNjAsImN5Y2xlX2ludGVydmFsIjoxMCwiY3ljbGVfZWZmZWN0IjoiZmFkZSIsImVmZmVjdF9jb2RlIjoiY2xhc3M9XCJuZ2ctZmFuY3lib3hcIiByZWw9XCIlR0FMTEVSWV9OQU1FJVwiIiwic2hvd190aHVtYm5haWxfbGluayI6MSwidGh1bWJuYWlsX2xpbmtfdGV4dCI6IltcdTA0MWZcdTA0M2VcdTA0M2FcdTA0MzBcdTA0MzdcdTA0MzBcdTA0NDJcdTA0NGMgXHUwNDRkXHUwNDQxXHUwNDNhXHUwNDM4XHUwNDM3XHUwNDRiXSIsInRlbXBsYXRlIjoiIiwibmdnX3RyaWdnZXJzX2Rpc3BsYXkiOiJuZXZlciIsIl9lcnJvcnMiOltdfQ=='),
(597, 104, 'title', 'NextGEN Basic ImageBrowser'),
(598, 104, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_imagebrowser\\static\\preview.jpg'),
(599, 104, 'default_source', 'galleries'),
(600, 104, 'view_order', '10020'),
(601, 104, 'name', 'photocrati-nextgen_basic_imagebrowser'),
(602, 104, 'installed_at_version', '3.0.1'),
(603, 104, 'hidden_from_ui', ''),
(604, 104, 'hidden_from_igw', ''),
(605, 104, '__defaults_set', '1'),
(606, 104, 'filter', 'raw'),
(607, 104, 'entity_types', 'WyJpbWFnZSJd'),
(608, 104, 'aliases', 'WyJiYXNpY19pbWFnZWJyb3dzZXIiLCJpbWFnZWJyb3dzZXIiLCJuZXh0Z2VuX2Jhc2ljX2ltYWdlYnJvd3NlciJd'),
(609, 104, 'id_field', 'ID'),
(610, 104, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJ0ZW1wbGF0ZSI6IiIsIm5nZ190cmlnZ2Vyc19kaXNwbGF5IjoibmV2ZXIiLCJfZXJyb3JzIjpbXX0='),
(611, 105, 'title', 'NextGEN Basic SinglePic'),
(612, 105, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_singlepic\\static\\preview.gif'),
(613, 105, 'default_source', 'galleries'),
(614, 105, 'view_order', '10060'),
(615, 105, 'hidden_from_ui', '1'),
(616, 105, 'hidden_from_igw', '1'),
(617, 105, 'name', 'photocrati-nextgen_basic_singlepic'),
(618, 105, 'installed_at_version', '3.0.1'),
(619, 105, '__defaults_set', '1'),
(620, 105, 'filter', 'raw'),
(621, 105, 'entity_types', 'WyJpbWFnZSJd'),
(622, 105, 'aliases', 'WyJiYXNpY19zaW5nbGVwaWMiLCJzaW5nbGVwaWMiLCJuZXh0Z2VuX2Jhc2ljX3NpbmdsZXBpYyJd'),
(623, 105, 'id_field', 'ID'),
(624, 105, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJ3aWR0aCI6IiIsImhlaWdodCI6IiIsIm1vZGUiOiIiLCJkaXNwbGF5X3dhdGVybWFyayI6MCwiZGlzcGxheV9yZWZsZWN0aW9uIjowLCJmbG9hdCI6IiIsImxpbmsiOiIiLCJsaW5rX3RhcmdldCI6Il9ibGFuayIsInF1YWxpdHkiOjEwMCwiY3JvcCI6MCwidGVtcGxhdGUiOiIiLCJuZ2dfdHJpZ2dlcnNfZGlzcGxheSI6Im5ldmVyIiwiX2Vycm9ycyI6W119'),
(625, 106, 'title', 'NextGEN Basic TagCloud'),
(626, 106, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_tagcloud\\static\\preview.gif'),
(627, 106, 'default_source', 'tags'),
(628, 106, 'view_order', '10100'),
(629, 106, 'name', 'photocrati-nextgen_basic_tagcloud'),
(630, 106, 'installed_at_version', '3.0.1'),
(631, 106, 'hidden_from_ui', ''),
(632, 106, 'hidden_from_igw', ''),
(633, 106, '__defaults_set', '1'),
(634, 106, 'filter', 'raw'),
(635, 106, 'entity_types', 'WyJpbWFnZSJd'),
(636, 106, 'aliases', 'WyJiYXNpY190YWdjbG91ZCIsInRhZ2Nsb3VkIiwibmV4dGdlbl9iYXNpY190YWdjbG91ZCJd'),
(637, 106, 'id_field', 'ID'),
(638, 106, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJnYWxsZXJ5X2Rpc3BsYXlfdHlwZSI6InBob3RvY3JhdGktbmV4dGdlbl9iYXNpY190aHVtYm5haWxzIiwibnVtYmVyIjo0NSwibmdnX3RyaWdnZXJzX2Rpc3BsYXkiOiJuZXZlciIsIl9lcnJvcnMiOltdfQ=='),
(639, 107, 'title', 'NextGEN Basic Compact Album'),
(640, 107, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_album\\static\\compact_preview.jpg'),
(641, 107, 'default_source', 'albums'),
(642, 107, 'view_order', '10200'),
(643, 107, 'name', 'photocrati-nextgen_basic_compact_album'),
(644, 107, 'installed_at_version', '3.0.1'),
(645, 107, 'hidden_from_ui', ''),
(646, 107, 'hidden_from_igw', ''),
(647, 107, '__defaults_set', '1'),
(648, 107, 'filter', 'raw'),
(649, 107, 'entity_types', 'WyJhbGJ1bSIsImdhbGxlcnkiXQ=='),
(650, 107, 'aliases', 'WyJiYXNpY19jb21wYWN0X2FsYnVtIiwibmV4dGdlbl9iYXNpY19hbGJ1bSIsImJhc2ljX2FsYnVtX2NvbXBhY3QiLCJjb21wYWN0X2FsYnVtIl0='),
(651, 107, 'id_field', 'ID'),
(652, 107, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJnYWxsZXJpZXNfcGVyX3BhZ2UiOjAsImVuYWJsZV9icmVhZGNydW1icyI6MSwiZGlzYWJsZV9wYWdpbmF0aW9uIjowLCJlbmFibGVfZGVzY3JpcHRpb25zIjowLCJ0ZW1wbGF0ZSI6IiIsIm9wZW5fZ2FsbGVyeV9pbl9saWdodGJveCI6MCwiZ2FsbGVyeV9kaXNwbGF5X3R5cGUiOiJwaG90b2NyYXRpLW5leHRnZW5fYmFzaWNfdGh1bWJuYWlscyIsImdhbGxlcnlfZGlzcGxheV90ZW1wbGF0ZSI6IiIsIm5nZ190cmlnZ2Vyc19kaXNwbGF5IjoibmV2ZXIiLCJfZXJyb3JzIjpbXX0='),
(653, 108, 'title', 'NextGEN Basic Extended Album'),
(654, 108, 'preview_image_relpath', '\\nextgen-gallery\\products\\photocrati_nextgen\\modules\\nextgen_basic_album\\static\\extended_preview.jpg'),
(655, 108, 'default_source', 'albums'),
(656, 108, 'view_order', '10210'),
(657, 108, 'name', 'photocrati-nextgen_basic_extended_album'),
(658, 108, 'installed_at_version', '3.0.1'),
(659, 108, 'hidden_from_ui', ''),
(660, 108, 'hidden_from_igw', ''),
(661, 108, '__defaults_set', '1'),
(662, 108, 'filter', 'raw'),
(663, 108, 'entity_types', 'WyJhbGJ1bSIsImdhbGxlcnkiXQ=='),
(664, 108, 'aliases', 'WyJiYXNpY19leHRlbmRlZF9hbGJ1bSIsIm5leHRnZW5fYmFzaWNfZXh0ZW5kZWRfYWxidW0iLCJleHRlbmRlZF9hbGJ1bSJd'),
(665, 108, 'id_field', 'ID'),
(666, 108, 'settings', 'eyJ1c2VfbGlnaHRib3hfZWZmZWN0Ijp0cnVlLCJnYWxsZXJpZXNfcGVyX3BhZ2UiOjAsImVuYWJsZV9icmVhZGNydW1icyI6MSwiZGlzYWJsZV9wYWdpbmF0aW9uIjowLCJlbmFibGVfZGVzY3JpcHRpb25zIjowLCJ0ZW1wbGF0ZSI6IiIsIm9wZW5fZ2FsbGVyeV9pbl9saWdodGJveCI6MCwib3ZlcnJpZGVfdGh1bWJuYWlsX3NldHRpbmdzIjowLCJ0aHVtYm5haWxfd2lkdGgiOjI0MCwidGh1bWJuYWlsX2hlaWdodCI6MTYwLCJ0aHVtYm5haWxfcXVhbGl0eSI6MTAwLCJ0aHVtYm5haWxfY3JvcCI6dHJ1ZSwidGh1bWJuYWlsX3dhdGVybWFyayI6MCwiZ2FsbGVyeV9kaXNwbGF5X3R5cGUiOiJwaG90b2NyYXRpLW5leHRnZW5fYmFzaWNfdGh1bWJuYWlscyIsImdhbGxlcnlfZGlzcGxheV90ZW1wbGF0ZSI6IiIsIm5nZ190cmlnZ2Vyc19kaXNwbGF5IjoibmV2ZXIiLCJfZXJyb3JzIjpbXX0='),
(667, 22, '__defaults_set', '1'),
(668, 22, 'filter', 'raw'),
(669, 22, 'id_field', 'ID'),
(688, 109, '__defaults_set', '1'),
(689, 109, 'filter', 'raw'),
(690, 109, 'id_field', 'ID'),
(691, 110, '__defaults_set', '1'),
(692, 110, 'filter', 'raw'),
(693, 110, 'id_field', 'ID'),
(700, 111, '__defaults_set', '1'),
(701, 111, 'filter', 'raw'),
(702, 111, 'id_field', 'ID'),
(703, 112, '__defaults_set', '1'),
(704, 112, 'filter', 'raw'),
(705, 112, 'id_field', 'ID'),
(706, 113, '__defaults_set', '1'),
(707, 113, 'filter', 'raw'),
(708, 113, 'id_field', 'ID'),
(715, 114, '__defaults_set', '1'),
(716, 114, 'filter', 'raw'),
(717, 114, 'id_field', 'ID'),
(718, 115, '__defaults_set', '1'),
(719, 115, 'filter', 'raw'),
(720, 115, 'id_field', 'ID'),
(727, 116, '__defaults_set', '1'),
(728, 116, 'filter', 'raw'),
(729, 116, 'id_field', 'ID'),
(730, 117, '__defaults_set', '1'),
(731, 117, 'filter', 'raw'),
(732, 117, 'id_field', 'ID'),
(739, 118, '__defaults_set', '1'),
(740, 118, 'filter', 'raw'),
(741, 118, 'id_field', 'ID'),
(742, 119, '__defaults_set', '1'),
(743, 119, 'filter', 'raw'),
(744, 119, 'id_field', 'ID'),
(751, 120, '__defaults_set', '1'),
(752, 120, 'filter', 'raw'),
(753, 120, 'id_field', 'ID'),
(754, 121, '__defaults_set', '1'),
(755, 121, 'filter', 'raw'),
(756, 121, 'id_field', 'ID'),
(763, 122, '__defaults_set', '1'),
(764, 122, 'filter', 'raw'),
(765, 122, 'id_field', 'ID'),
(766, 123, '__defaults_set', '1'),
(767, 123, 'filter', 'raw'),
(768, 123, 'id_field', 'ID'),
(775, 124, '__defaults_set', '1'),
(776, 124, 'filter', 'raw'),
(777, 124, 'id_field', 'ID'),
(778, 125, '__defaults_set', '1'),
(779, 125, 'filter', 'raw'),
(780, 125, 'id_field', 'ID'),
(787, 126, '__defaults_set', '1'),
(788, 126, 'filter', 'raw'),
(789, 126, 'id_field', 'ID'),
(790, 127, '__defaults_set', '1'),
(791, 127, 'filter', 'raw'),
(792, 127, 'id_field', 'ID'),
(799, 128, '__defaults_set', '1'),
(800, 128, 'filter', 'raw'),
(801, 128, 'id_field', 'ID'),
(802, 129, '__defaults_set', '1'),
(803, 129, 'filter', 'raw'),
(804, 129, 'id_field', 'ID'),
(811, 130, '__defaults_set', '1'),
(812, 130, 'filter', 'raw'),
(813, 130, 'id_field', 'ID'),
(814, 131, '__defaults_set', '1'),
(815, 131, 'filter', 'raw'),
(816, 131, 'id_field', 'ID'),
(823, 132, '__defaults_set', '1'),
(824, 132, 'filter', 'raw'),
(825, 132, 'id_field', 'ID'),
(826, 133, '__defaults_set', '1'),
(827, 133, 'filter', 'raw'),
(828, 133, 'id_field', 'ID'),
(835, 134, '__defaults_set', '1'),
(836, 134, 'filter', 'raw'),
(837, 134, 'id_field', 'ID'),
(838, 135, '__defaults_set', '1'),
(839, 135, 'filter', 'raw'),
(840, 135, 'id_field', 'ID'),
(847, 136, '__defaults_set', '1'),
(848, 136, 'filter', 'raw'),
(849, 136, 'id_field', 'ID'),
(850, 137, '__defaults_set', '1'),
(851, 137, 'filter', 'raw'),
(852, 137, 'id_field', 'ID'),
(859, 138, '__defaults_set', '1'),
(860, 138, 'filter', 'raw'),
(861, 138, 'id_field', 'ID'),
(862, 139, '__defaults_set', '1'),
(863, 139, 'filter', 'raw'),
(864, 139, 'id_field', 'ID'),
(871, 140, '__defaults_set', '1'),
(872, 140, 'filter', 'raw'),
(873, 140, 'id_field', 'ID'),
(874, 141, '__defaults_set', '1'),
(875, 141, 'filter', 'raw'),
(876, 141, 'id_field', 'ID'),
(883, 142, '__defaults_set', '1'),
(884, 142, 'filter', 'raw'),
(885, 142, 'id_field', 'ID'),
(886, 143, '__defaults_set', '1'),
(887, 143, 'filter', 'raw'),
(888, 143, 'id_field', 'ID'),
(895, 144, '__defaults_set', '1'),
(896, 144, 'filter', 'raw'),
(897, 144, 'id_field', 'ID'),
(898, 145, '__defaults_set', '1'),
(899, 145, 'filter', 'raw'),
(900, 145, 'id_field', 'ID'),
(907, 146, '__defaults_set', '1'),
(908, 146, 'filter', 'raw'),
(909, 146, 'id_field', 'ID'),
(910, 147, '__defaults_set', '1'),
(911, 147, 'filter', 'raw'),
(912, 147, 'id_field', 'ID'),
(919, 148, '__defaults_set', '1'),
(920, 148, 'filter', 'raw'),
(921, 148, 'id_field', 'ID'),
(922, 149, '__defaults_set', '1'),
(923, 149, 'filter', 'raw'),
(924, 149, 'id_field', 'ID'),
(931, 150, '__defaults_set', '1'),
(932, 150, 'filter', 'raw'),
(933, 150, 'id_field', 'ID'),
(934, 151, '__defaults_set', '1'),
(935, 151, 'filter', 'raw'),
(936, 151, 'id_field', 'ID'),
(943, 152, '__defaults_set', '1'),
(944, 152, 'filter', 'raw'),
(945, 152, 'id_field', 'ID'),
(946, 153, '__defaults_set', '1'),
(947, 153, 'filter', 'raw'),
(948, 153, 'id_field', 'ID'),
(955, 154, '__defaults_set', '1'),
(956, 154, 'filter', 'raw'),
(957, 154, 'id_field', 'ID'),
(958, 155, '__defaults_set', '1'),
(959, 155, 'filter', 'raw'),
(960, 155, 'id_field', 'ID'),
(967, 156, '__defaults_set', '1'),
(968, 156, 'filter', 'raw'),
(969, 156, 'id_field', 'ID'),
(970, 157, '__defaults_set', '1'),
(971, 157, 'filter', 'raw'),
(972, 157, 'id_field', 'ID'),
(979, 158, '__defaults_set', '1');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES 
(980, 158, 'filter', 'raw'),
(981, 158, 'id_field', 'ID'),
(982, 159, '__defaults_set', '1'),
(983, 159, 'filter', 'raw'),
(984, 159, 'id_field', 'ID'),
(991, 160, '__defaults_set', '1'),
(992, 160, 'filter', 'raw'),
(993, 160, 'id_field', 'ID'),
(994, 161, '__defaults_set', '1'),
(995, 161, 'filter', 'raw'),
(996, 161, 'id_field', 'ID'),
(1003, 162, '__defaults_set', '1'),
(1004, 162, 'filter', 'raw'),
(1005, 162, 'id_field', 'ID'),
(1006, 163, '__defaults_set', '1'),
(1007, 163, 'filter', 'raw'),
(1008, 163, 'id_field', 'ID'),
(1015, 164, '__defaults_set', '1'),
(1016, 164, 'filter', 'raw'),
(1017, 164, 'id_field', 'ID'),
(1018, 165, '__defaults_set', '1'),
(1019, 165, 'filter', 'raw'),
(1020, 165, 'id_field', 'ID'),
(1027, 166, '__defaults_set', '1'),
(1028, 166, 'filter', 'raw'),
(1029, 166, 'id_field', 'ID'),
(1030, 167, '__defaults_set', '1'),
(1031, 167, 'filter', 'raw'),
(1032, 167, 'id_field', 'ID'),
(1039, 168, '__defaults_set', '1'),
(1040, 168, 'filter', 'raw'),
(1041, 168, 'id_field', 'ID'),
(1042, 169, '__defaults_set', '1'),
(1043, 169, 'filter', 'raw'),
(1044, 169, 'id_field', 'ID'),
(1051, 170, '__defaults_set', '1'),
(1052, 170, 'filter', 'raw'),
(1053, 170, 'id_field', 'ID'),
(1054, 171, '__defaults_set', '1'),
(1055, 171, 'filter', 'raw'),
(1056, 171, 'id_field', 'ID'),
(1063, 172, '__defaults_set', '1'),
(1064, 172, 'filter', 'raw'),
(1065, 172, 'id_field', 'ID'),
(1066, 173, '__defaults_set', '1'),
(1067, 173, 'filter', 'raw'),
(1068, 173, 'id_field', 'ID'),
(1075, 174, '__defaults_set', '1'),
(1076, 174, 'filter', 'raw'),
(1077, 174, 'id_field', 'ID'),
(1078, 175, '__defaults_set', '1'),
(1079, 175, 'filter', 'raw'),
(1080, 175, 'id_field', 'ID'),
(1084, 176, '__defaults_set', '1'),
(1085, 176, 'filter', 'raw'),
(1086, 176, 'id_field', 'ID'),
(1087, 178, '__defaults_set', '1'),
(1088, 178, 'filter', 'raw'),
(1089, 178, 'id_field', 'ID'),
(1090, 177, '__defaults_set', '1'),
(1091, 177, 'filter', 'raw'),
(1092, 177, 'id_field', 'ID'),
(1105, 180, '__defaults_set', '1'),
(1106, 180, 'filter', 'raw'),
(1107, 180, 'id_field', 'ID'),
(1108, 179, '__defaults_set', '1'),
(1109, 179, 'filter', 'raw'),
(1110, 179, 'id_field', 'ID'),
(1111, 181, '__defaults_set', '1'),
(1112, 181, 'filter', 'raw'),
(1113, 181, 'id_field', 'ID'),
(1114, 182, '__defaults_set', '1'),
(1115, 182, 'filter', 'raw'),
(1116, 182, 'id_field', 'ID'),
(1129, 184, '__defaults_set', '1'),
(1130, 184, 'filter', 'raw'),
(1131, 184, 'id_field', 'ID'),
(1132, 183, '__defaults_set', '1'),
(1133, 183, 'filter', 'raw'),
(1134, 183, 'id_field', 'ID'),
(1135, 185, '__defaults_set', '1'),
(1136, 185, 'filter', 'raw'),
(1137, 185, 'id_field', 'ID'),
(1138, 186, '__defaults_set', '1'),
(1139, 186, 'filter', 'raw'),
(1140, 186, 'id_field', 'ID'),
(1153, 188, '__defaults_set', '1'),
(1154, 188, 'filter', 'raw'),
(1155, 188, 'id_field', 'ID'),
(1156, 187, '__defaults_set', '1'),
(1157, 187, 'filter', 'raw'),
(1158, 187, 'id_field', 'ID'),
(1159, 189, '__defaults_set', '1'),
(1160, 189, 'filter', 'raw'),
(1161, 189, 'id_field', 'ID'),
(1168, 190, '__defaults_set', '1'),
(1169, 190, 'filter', 'raw'),
(1170, 190, 'id_field', 'ID'),
(1171, 191, '__defaults_set', '1'),
(1172, 191, 'filter', 'raw'),
(1173, 191, 'id_field', 'ID'),
(1180, 192, '__defaults_set', '1'),
(1181, 192, 'filter', 'raw'),
(1182, 192, 'id_field', 'ID'),
(1183, 193, '__defaults_set', '1'),
(1184, 193, 'filter', 'raw'),
(1185, 193, 'id_field', 'ID'),
(1192, 194, '__defaults_set', '1'),
(1193, 194, 'filter', 'raw'),
(1194, 194, 'id_field', 'ID'),
(1195, 195, '__defaults_set', '1'),
(1196, 195, 'filter', 'raw'),
(1197, 195, 'id_field', 'ID'),
(1204, 196, '__defaults_set', '1'),
(1205, 196, 'filter', 'raw'),
(1206, 196, 'id_field', 'ID'),
(1207, 197, '__defaults_set', '1'),
(1208, 197, 'filter', 'raw'),
(1209, 197, 'id_field', 'ID'),
(1216, 198, '__defaults_set', '1'),
(1217, 198, 'filter', 'raw'),
(1218, 198, 'id_field', 'ID'),
(1219, 199, '__defaults_set', '1'),
(1220, 199, 'filter', 'raw'),
(1221, 199, 'id_field', 'ID'),
(1228, 200, '__defaults_set', '1'),
(1229, 200, 'filter', 'raw'),
(1230, 200, 'id_field', 'ID'),
(1231, 201, '__defaults_set', '1'),
(1232, 201, 'filter', 'raw'),
(1233, 201, 'id_field', 'ID'),
(1240, 202, '__defaults_set', '1'),
(1241, 202, 'filter', 'raw'),
(1242, 202, 'id_field', 'ID'),
(1243, 203, '__defaults_set', '1'),
(1244, 203, 'filter', 'raw'),
(1245, 203, 'id_field', 'ID'),
(1252, 204, '__defaults_set', '1'),
(1253, 204, 'filter', 'raw'),
(1254, 204, 'id_field', 'ID'),
(1255, 205, '__defaults_set', '1'),
(1256, 205, 'filter', 'raw'),
(1257, 205, 'id_field', 'ID'),
(1264, 206, '__defaults_set', '1'),
(1265, 206, 'filter', 'raw'),
(1266, 206, 'id_field', 'ID'),
(1267, 207, '__defaults_set', '1'),
(1268, 207, 'filter', 'raw'),
(1269, 207, 'id_field', 'ID'),
(1276, 208, '__defaults_set', '1'),
(1277, 208, 'filter', 'raw'),
(1278, 208, 'id_field', 'ID'),
(1279, 209, '__defaults_set', '1'),
(1280, 209, 'filter', 'raw'),
(1281, 209, 'id_field', 'ID'),
(1288, 210, '__defaults_set', '1'),
(1289, 210, 'filter', 'raw'),
(1290, 210, 'id_field', 'ID'),
(1291, 211, '__defaults_set', '1'),
(1292, 211, 'filter', 'raw'),
(1293, 211, 'id_field', 'ID'),
(1300, 212, '__defaults_set', '1'),
(1301, 212, 'filter', 'raw'),
(1302, 212, 'id_field', 'ID'),
(1303, 35, '_wp_old_date', '2018-07-18'),
(1304, 35, '_eg_just_published', '1'),
(1455, 263, '_edit_last', '1'),
(1456, 263, '_edit_lock', '1533908979:1'),
(1457, 263, 'rsg_galleryImages', 'a:1:{i:0;s:3:\"258\";}'),
(1458, 263, 'rsg_pretext', '  '),
(1459, 263, 'rsg_aftertext', '  '),
(1460, 263, 'rsg_width-size', 'a:1:{s:5:\"width\";s:3:\"100\";}'),
(1461, 263, 'rsg_thumb-size-options', 'a:4:{s:7:\"orderby\";s:9:\"categoryD\";s:6:\"source\";s:6:\"medium\";s:5:\"width\";s:3:\"240\";s:6:\"height\";s:3:\"140\";}'),
(1462, 263, 'rsg_colums', 'a:8:{s:9:\"autowidth\";s:4:\"auto\";s:6:\"colums\";s:1:\"3\";s:10:\"autowidth1\";s:4:\"auto\";s:7:\"colums1\";s:1:\"3\";s:10:\"autowidth2\";s:4:\"auto\";s:7:\"colums2\";s:1:\"2\";s:10:\"autowidth3\";s:4:\"auto\";s:7:\"colums3\";s:1:\"1\";}'),
(1463, 263, 'rsg_radius', '5'),
(1464, 263, 'rsg_horizontalSpaceBetweenBoxes', '15'),
(1465, 263, 'rsg_verticalSpaceBetweenBoxes', '15'),
(1466, 263, 'rsg_shadow', '1'),
(1467, 263, 'rsg_shadow-options', 'a:4:{s:7:\"hshadow\";s:1:\"0\";s:7:\"vshadow\";s:1:\"5\";s:7:\"bshadow\";s:1:\"7\";s:5:\"color\";s:21:\"rgba(34, 25, 25, 0.4)\";}'),
(1468, 263, 'rsg_hover-shadow-options', 'a:4:{s:7:\"hshadow\";s:1:\"1\";s:7:\"vshadow\";s:1:\"3\";s:7:\"bshadow\";s:1:\"3\";s:5:\"color\";s:21:\"rgba(34, 25, 25, 0.4)\";}'),
(1469, 263, 'rsg_border-options', 'a:3:{s:5:\"width\";s:1:\"5\";s:5:\"style\";s:5:\"solid\";s:5:\"color\";s:16:\"rgb(229, 64, 40)\";}'),
(1470, 263, 'rsg_hover-border-options', 'a:3:{s:5:\"width\";s:1:\"5\";s:5:\"style\";s:5:\"solid\";s:5:\"color\";s:16:\"rgb(229, 64, 40)\";}'),
(1471, 263, 'rsg_thumbClick', '0'),
(1472, 263, 'rsg_hover', '1'),
(1473, 263, 'rsg_background', 'rgba(7, 7, 7, 0.5)'),
(1474, 263, 'rsg_overlayEffect', 'direction-aware-fade'),
(1475, 263, 'rsg_showTitle', 'a:6:{s:7:\"enabled\";s:1:\"1\";s:8:\"fontBold\";s:4:\"bold\";s:8:\"fontSize\";s:2:\"12\";s:14:\"fontLineHeight\";s:3:\"100\";s:5:\"color\";s:18:\"rgb(255, 255, 255)\";s:10:\"colorHover\";s:18:\"rgb(255, 255, 255)\";}'),
(1476, 263, 'rsg_linkIcon', 'a:8:{s:10:\"iconSelect\";s:7:\"fa-link\";s:8:\"fontSize\";s:2:\"20\";s:14:\"fontLineHeight\";s:3:\"100\";s:10:\"borderSize\";s:1:\"0\";s:5:\"color\";s:18:\"rgb(255, 255, 255)\";s:10:\"colorHover\";s:18:\"rgb(255, 255, 255)\";s:7:\"colorBg\";s:16:\"rgb(229, 64, 40)\";s:12:\"colorBgHover\";s:16:\"rgb(183, 55, 37)\";}'),
(1477, 263, 'rsg_zoomIcon', 'a:9:{s:7:\"enabled\";s:1:\"1\";s:10:\"iconSelect\";s:14:\"fa-search-plus\";s:8:\"fontSize\";s:2:\"16\";s:14:\"fontLineHeight\";s:3:\"100\";s:10:\"borderSize\";s:1:\"2\";s:5:\"color\";s:18:\"rgb(255, 255, 255)\";s:10:\"colorHover\";s:18:\"rgb(255, 255, 255)\";s:7:\"colorBg\";s:17:\"rgb(13, 130, 241)\";s:12:\"colorBgHover\";s:15:\"rgb(6, 70, 130)\";}'),
(1478, 263, 'rsg_showDesc', 'a:4:{s:8:\"fontSize\";s:2:\"24\";s:14:\"fontLineHeight\";s:3:\"100\";s:5:\"color\";s:12:\"rgb(0, 0, 0)\";s:10:\"colorHover\";s:12:\"rgb(0, 0, 0)\";}'),
(1479, 263, 'rsg_desc_template', '  <div class=\"rbs-hover-title\">@TITLE@</div>\r\n<div class=\"rbs-hover-caption\">@CAPTION@</div>\r\n<div class=\"rbs-hover-text\">@DESC@</div>\r\n<div class=\"rbs-hover-more\"><a href=\"@LINK@\">Read more</a></div>\r\n'),
(1480, 263, 'rsg_menu', '1'),
(1481, 263, 'rsg_menuSelfImages', '1'),
(1482, 263, 'rsg_menuRoot', '1'),
(1483, 263, 'rsg_menuRootLabel', 'All'),
(1484, 263, 'rsg_menuSelf', '1'),
(1485, 263, 'rsg_buttonFill', 'border'),
(1486, 263, 'rsg_buttonColor', 'red'),
(1487, 263, 'rsg_buttonType', 'normal'),
(1488, 263, 'rsg_buttonSize', 'normal'),
(1489, 263, 'rsg_buttonAlign', 'left'),
(1490, 263, 'rsg_paddingLeft', '5'),
(1491, 263, 'rsg_paddingBottom', '10'),
(1492, 263, 'rsg_searchColor', 'rgb(218, 139, 163)'),
(1493, 263, 'rsg_searchLabel', 'search'),
(1494, 263, 'rsg_lazyLoad', '1'),
(1495, 263, 'rsg_boxesToLoadStart', '12'),
(1496, 263, 'rsg_boxesToLoad', '8'),
(1497, 263, 'rsg_waitUntilThumbLoads', '1'),
(1498, 263, 'rsg_loadingBgColor', 'rgb(255, 255, 255)'),
(1499, 263, 'rsg_LoadingWord', 'Loading...'),
(1500, 263, 'rsg_loadMoreWord', 'Load More'),
(1501, 263, 'rsg_noMoreEntriesWord', 'No More Entries'),
(1502, 263, 'rsg_polaroidSource', 'desc'),
(1503, 263, 'rsg_polaroidBackground', 'rgb(255, 255, 255)'),
(1504, 263, 'rsg_polaroidAlign', 'center'),
(1505, 263, 'rsg_lightboxTitle', '1'),
(1506, 263, 'rsg_lightboxSource', 'title'),
(1507, 263, 'rsg_lightboxColor', 'rgb(243, 243, 243)'),
(1508, 263, 'rsg_lightboxBackground', 'rgba(11, 11, 11, 0.8)'),
(1509, 263, 'rsg_lightboxCounter', '1'),
(1510, 263, 'rsg_lightboxCounterText', 'of'),
(1511, 263, 'rsg_lightboxSwipe', '1'),
(1512, 263, 'rsg_lightboxSwipeDirection', 'left'),
(1513, 263, 'rsg_lightboxClose', '1'),
(1514, 263, 'rsg_lightboxArrow', '1'),
(1515, 263, 'rsg_lightboxDescSource', 'title'),
(1516, 263, 'rsg_lightboxDescClass', 'light'),
(1517, 263, 'rsg_lightboxSocialFacebook', '1'),
(1518, 263, 'rsg_lightboxSocialTwitter', '1'),
(1519, 263, 'rsg_lightboxSocialGoogleplus', '1'),
(1520, 263, 'rsg_lightboxSocialPinterest', '1'),
(1521, 263, 'rsg_lightboxSocialVK', '1'),
(1522, 264, '_edit_last', '1'),
(1523, 264, 'mb_items_array', '1'),
(1524, 264, 'mb_skinname', ''),
(1525, 264, 'mb_scode', '[flagallery gid=1 w=100% h=100% fullwindow=true]'),
(1526, 264, 'mb_button', ''),
(1527, 264, 'mb_button_link', ''),
(1528, 264, 'mb_bg_link', ''),
(1529, 264, 'mb_bg_pos', 'center center'),
(1530, 264, 'mb_bg_repeat', 'repeat'),
(1531, 264, 'mb_bg_size', 'auto'),
(1532, 264, '_edit_lock', '1531959856:1'),
(1533, 263, 'gallery_views_count', '1'),
(1534, 263, 'rsg_options', '263'),
(1535, 265, '_edit_last', '1'),
(1536, 265, '_edit_lock', '1531961392:1'),
(1537, 265, 'foogallery_attachments', 'a:21:{i:0;s:3:\"262\";i:1;s:3:\"260\";i:2;s:3:\"261\";i:3;s:3:\"259\";i:4;s:3:\"258\";i:5;s:3:\"257\";i:6;s:3:\"256\";i:7;s:3:\"255\";i:8;s:3:\"254\";i:9;s:3:\"244\";i:10;s:3:\"245\";i:11;s:3:\"247\";i:12;s:3:\"248\";i:13;s:3:\"249\";i:14;s:3:\"250\";i:15;s:3:\"251\";i:16;s:3:\"252\";i:17;s:3:\"242\";i:18;s:3:\"240\";i:19;s:3:\"241\";i:20;s:3:\"239\";}'),
(1538, 265, 'foogallery_template', 'default'),
(1539, 265, '_foogallery_settings', 'a:23:{s:21:\"foogallery_items_view\";s:6:\"manage\";s:28:\"default_thumbnail_dimensions\";a:2:{s:5:\"width\";s:3:\"400\";s:6:\"height\";s:3:\"400\";}s:22:\"default_thumbnail_link\";s:4:\"page\";s:16:\"default_lightbox\";s:8:\"dfactory\";s:15:\"default_spacing\";s:12:\"fg-gutter-10\";s:17:\"default_alignment\";s:9:\"fg-center\";s:13:\"default_theme\";s:8:\"fg-light\";s:19:\"default_border_size\";s:14:\"fg-border-thin\";s:23:\"default_rounded_corners\";s:0:\"\";s:19:\"default_drop_shadow\";s:17:\"fg-shadow-outline\";s:20:\"default_inner_shadow\";s:0:\"\";s:20:\"default_loading_icon\";s:18:\"fg-loading-default\";s:21:\"default_loaded_effect\";s:17:\"fg-loaded-fade-in\";s:26:\"default_hover_effect_color\";s:0:\"\";s:26:\"default_hover_effect_scale\";s:0:\"\";s:39:\"default_hover_effect_caption_visibility\";s:16:\"fg-caption-hover\";s:31:\"default_hover_effect_transition\";s:13:\"fg-hover-fade\";s:25:\"default_hover_effect_icon\";s:13:\"fg-hover-zoom\";s:28:\"default_caption_title_source\";s:0:\"\";s:27:\"default_caption_desc_source\";s:0:\"\";s:29:\"default_captions_limit_length\";s:0:\"\";s:19:\"default_paging_type\";s:0:\"\";s:16:\"default_lazyload\";s:0:\"\";}'),
(1540, 265, 'foogallery_sort', ''),
(1615, 297, '_wp_attached_file', '2018/07/1.png'),
(1616, 297, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:301;s:6:\"height\";i:302;s:4:\"file\";s:13:\"2018/07/1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1617, 298, '_wp_attached_file', '2018/07/2.png'),
(1618, 298, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:298;s:6:\"height\";i:301;s:4:\"file\";s:13:\"2018/07/2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"2-297x300.png\";s:5:\"width\";i:297;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1619, 299, '_wp_attached_file', '2018/07/3.png'),
(1620, 299, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:303;s:4:\"file\";s:13:\"2018/07/3.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"3-297x300.png\";s:5:\"width\";i:297;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1621, 300, '_wp_attached_file', '2018/07/7.png'),
(1622, 300, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:301;s:6:\"height\";i:300;s:4:\"file\";s:13:\"2018/07/7.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:13:\"7-300x299.png\";s:5:\"width\";i:300;s:6:\"height\";i:299;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1623, 297, '_wp_attachment_image_alt', 'img'),
(1629, 302, '_wp_attached_file', '2018/07/55.png'),
(1630, 302, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:299;s:6:\"height\";i:303;s:4:\"file\";s:14:\"2018/07/55.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"55-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"55-296x300.png\";s:5:\"width\";i:296;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1631, 303, '_wp_attached_file', '2018/07/66.png'),
(1632, 303, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:300;s:4:\"file\";s:14:\"2018/07/66.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"66-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1633, 304, '_wp_attached_file', '2018/07/88.png'),
(1634, 304, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:301;s:6:\"height\";i:300;s:4:\"file\";s:14:\"2018/07/88.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"88-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"88-300x299.png\";s:5:\"width\";i:300;s:6:\"height\";i:299;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1635, 305, '_wp_attached_file', '2018/07/99.png'),
(1636, 305, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:298;s:6:\"height\";i:300;s:4:\"file\";s:14:\"2018/07/99.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"99-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"99-298x300.png\";s:5:\"width\";i:298;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1637, 305, '_wp_attachment_image_alt', 'img'),
(1641, 298, '_wp_attachment_image_alt', 'img'),
(1644, 299, '_wp_attachment_image_alt', 'img'),
(1648, 300, '_wp_attachment_image_alt', 'img'),
(1652, 302, '_wp_attachment_image_alt', 'img'),
(1656, 303, '_wp_attachment_image_alt', 'img'),
(1660, 304, '_wp_attachment_image_alt', 'img'),
(1665, 306, '_edit_last', '1'),
(1666, 306, '_edit_lock', '1572943414:1'),
(1667, 308, '_wp_attached_file', '2018/07/admin_rights.gif'),
(1668, 308, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:320;s:6:\"height\";i:320;s:4:\"file\";s:24:\"2018/07/admin_rights.gif\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"admin_rights-150x150.gif\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/gif\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"admin_rights-300x300.gif\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/gif\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1669, 308, '_wp_attachment_image_alt', 'admin'),
(1670, 309, '_wp_attached_file', '2018/07/fallout-4-bethesda-game-5913.jpg'),
(1671, 309, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:338;s:6:\"height\";i:355;s:4:\"file\";s:40:\"2018/07/fallout-4-bethesda-game-5913.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:40:\"fallout-4-bethesda-game-5913-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:40:\"fallout-4-bethesda-game-5913-286x300.jpg\";s:5:\"width\";i:286;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1672, 309, '_wp_attachment_image_alt', 'img'),
(1673, 310, '_edit_last', '1'),
(1674, 310, '_edit_lock', '1573382277:1'),
(1677, 313, '_wp_attached_file', '2018/07/1457023723_tt.jpg'),
(1678, 313, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:479;s:4:\"file\";s:25:\"2018/07/1457023723_tt.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"1457023723_tt-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"1457023723_tt-300x240.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:240;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1679, 313, '_wp_attachment_image_alt', 'img'),
(1683, 315, '_edit_last', '1'),
(1684, 315, '_edit_lock', '1532645401:1'),
(1687, 319, '_wp_attached_file', '2018/07/59ba7c1511604.jpeg'),
(1688, 319, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:750;s:4:\"file\";s:26:\"2018/07/59ba7c1511604.jpeg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"59ba7c1511604-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"59ba7c1511604-300x300.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1689, 319, '_wp_attachment_image_alt', 'img'),
(1695, 322, '_wp_attached_file', '2018/07/1511030296_13.jpg'),
(1696, 322, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:479;s:4:\"file\";s:25:\"2018/07/1511030296_13.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"1511030296_13-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"1511030296_13-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1697, 322, '_wp_attachment_image_alt', 'img'),
(1703, 325, '_wp_attached_file', '2018/07/ya47.png'),
(1704, 325, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:517;s:4:\"file\";s:16:\"2018/07/ya47.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"ya47-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"ya47-300x259.png\";s:5:\"width\";i:300;s:6:\"height\";i:259;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1705, 325, '_wp_attachment_image_alt', 'img'),
(1716, 330, '_wp_attached_file', '2018/07/787.jpg'),
(1717, 330, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:225;s:6:\"height\";i:225;s:4:\"file\";s:15:\"2018/07/787.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"787-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1718, 330, '_wp_attachment_image_alt', 'img'),
(1724, 333, '_wp_attached_file', '2018/07/orig.jpg'),
(1725, 333, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:1001;s:4:\"file\";s:16:\"2018/07/orig.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"orig-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"orig-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"orig-768x769.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:769;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1726, 333, '_wp_attachment_image_alt', 'img'),
(1732, 336, '_wp_attached_file', '2018/07/eFYlajY0PQ4.jpg'),
(1733, 336, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:640;s:4:\"file\";s:23:\"2018/07/eFYlajY0PQ4.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"eFYlajY0PQ4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"eFYlajY0PQ4-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1734, 336, '_wp_attachment_image_alt', 'img'),
(1742, 339, '_wp_attached_file', '2018/07/orig-1.jpg'),
(1743, 339, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:591;s:6:\"height\";i:446;s:4:\"file\";s:18:\"2018/07/orig-1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"orig-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"orig-1-300x226.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:226;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1744, 339, '_wp_attachment_image_alt', 'img'),
(1750, 342, '_wp_attached_file', '2018/07/manicure-trendy-3.jpg'),
(1751, 342, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:29:\"2018/07/manicure-trendy-3.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"manicure-trendy-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"manicure-trendy-3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1752, 342, '_wp_attachment_image_alt', 'img'),
(1756, 345, '_wp_attached_file', '2018/07/manikur_leto_80.jpg'),
(1757, 345, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:27:\"2018/07/manikur_leto_80.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"manikur_leto_80-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"manikur_leto_80-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1758, 345, '_wp_attachment_image_alt', 'img'),
(1763, 347, '_wp_attached_file', '2018/07/manicure-trendy-3-1.jpg'),
(1764, 347, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:600;s:6:\"height\";i:600;s:4:\"file\";s:31:\"2018/07/manicure-trendy-3-1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"manicure-trendy-3-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"manicure-trendy-3-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1765, 347, '_wp_attachment_image_alt', 'img'),
(1771, 350, '_wp_attached_file', '2018/07/images.jpg'),
(1772, 350, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:219;s:6:\"height\";i:230;s:4:\"file\";s:18:\"2018/07/images.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"images-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1773, 350, '_wp_attachment_image_alt', 'img'),
(1780, 374, '_wp_attached_file', '2018/07/s-l300-e1543303260417.jpg'),
(1781, 374, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:150;s:6:\"height\";i:150;s:4:\"file\";s:33:\"2018/07/s-l300-e1543303260417.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"s-l300-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1782, 374, '_wp_attachment_image_alt', 'img'),
(1798, 380, '_wp_attached_file', '2018/07/permanentnyj-makiyazh-nizhnego-veka.jpg'),
(1799, 380, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:362;s:4:\"file\";s:47:\"2018/07/permanentnyj-makiyazh-nizhnego-veka.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:47:\"permanentnyj-makiyazh-nizhnego-veka-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:47:\"permanentnyj-makiyazh-nizhnego-veka-300x217.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:217;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1800, 380, '_wp_attachment_image_alt', 'img'),
(1804, 382, '_wp_attached_file', '2018/07/4545.jpg'),
(1805, 382, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:500;s:4:\"file\";s:16:\"2018/07/4545.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"4545-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"4545-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1806, 382, '_wp_attachment_image_alt', 'img'),
(1814, 384, '_wp_attached_file', '2018/07/300px-Negr-e1543303566712.jpg'),
(1815, 384, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:150;s:6:\"height\";i:119;s:4:\"file\";s:37:\"2018/07/300px-Negr-e1543303566712.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"300px-Negr-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"300px-Negr-300x238.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:238;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1816, 384, '_wp_attachment_image_alt', 'img'),
(1822, 387, '_wp_attached_file', '2018/07/images-1.jpg'),
(1823, 387, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:210;s:6:\"height\";i:240;s:4:\"file\";s:20:\"2018/07/images-1.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"images-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1824, 387, '_wp_attachment_image_alt', 'img'),
(1830, 390, '_wp_attached_file', '2018/07/images-1-1.jpg'),
(1831, 390, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:265;s:6:\"height\";i:191;s:4:\"file\";s:22:\"2018/07/images-1-1.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"images-1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1832, 390, '_wp_attachment_image_alt', 'img'),
(1836, 392, '_wp_attached_file', '2018/07/1chas_posle.jpg'),
(1837, 392, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:870;s:6:\"height\";i:768;s:4:\"file\";s:23:\"2018/07/1chas_posle.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"1chas_posle-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"1chas_posle-300x265.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:265;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"1chas_posle-768x678.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:678;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1838, 392, '_wp_attachment_image_alt', 'img'),
(1865, 395, 'ecwd_added_shortcode', '1'),
(1881, 398, '_form', '<label> Ваше имя </label>\n[text your-name]\n\n<label> Телефон<span class=\"red\">*</span> </label>\n    [tel* phone]\n\n<label> e-mail<span class=\"red\">*</span> </label>\n[email* your-email]\n\n<br>\n\n<label> Введите желаемую дату и время </label>\n[datetime datetime-889 date-format:mm/dd/yy time-format:HH:mm]\n\n[acceptance acceptance-439] <span class=\"normal\"> Согласен с <a href=\"[main_url]/private-policy/\">политикой конфиденциальности</a></span>\n\n<br>\n\n[recaptcha]\n\n[submit \"Перезвоните мне\"]'),
(1882, 398, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:39:\"Письмо с сайта stylist.site\";s:6:\"sender\";s:36:\"[your-name] <wordpress@stylist.site>\";s:9:\"recipient\";s:18:\"gck92462@eoopy.com\";s:4:\"body\";s:178:\"Сообщение от клиента с stylist.site\n\nИмя: [your-name] \nEmail: <[your-email]>\nТелефон: [phone]\n\nЖелаемая дата записи: [datetime-889]\";s:18:\"additional_headers\";s:29:\"Reply-To:<eva34456@eveav.com>\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(1883, 398, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:24:\"stylist \"[your-subject]\"\";s:6:\"sender\";s:39:\"stylist <wordpress@q90241xj.beget.tech>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:137:\"Сообщение:\n[your-message]\n\n-- \nЭто сообщение отправлено с сайта stylist (http://q90241xj.beget.tech)\";s:18:\"additional_headers\";s:32:\"Reply-To: capitan.flin@yandex.ru\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(1884, 398, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:54:\"Сообщение успешно отправлено\";s:12:\"mail_sent_ng\";s:144:\"При отправке сообщения произошла ошибка. Пожалуйста, попробуйте ещё раз позже.\";s:16:\"validation_error\";s:52:\"Проверьте еще раз поля формы\";s:4:\"spam\";s:51:\"Подтвердите, что вы не робот\";s:12:\"accept_terms\";s:132:\"Вы должны принять условия и положения перед отправкой вашего сообщения.\";s:16:\"invalid_required\";s:60:\"Поле обязательно для заполнения.\";s:16:\"invalid_too_long\";s:39:\"Поле слишком длинное.\";s:17:\"invalid_too_short\";s:41:\"Поле слишком короткое.\";s:13:\"upload_failed\";s:90:\"При загрузке файла произошла неизвестная ошибка.\";s:24:\"upload_file_type_invalid\";s:81:\"Вам не разрешено загружать файлы этого типа.\";s:21:\"upload_file_too_large\";s:39:\"Файл слишком большой.\";s:23:\"upload_failed_php_error\";s:67:\"При загрузке файла произошла ошибка.\";s:14:\"invalid_number\";s:47:\"Формат числа некорректен.\";s:16:\"number_too_small\";s:68:\"Число меньше минимально допустимого.\";s:16:\"number_too_large\";s:70:\"Число больше максимально допустимого.\";s:23:\"quiz_answer_not_correct\";s:69:\"Неверный ответ на проверочный вопрос.\";s:17:\"captcha_not_match\";s:35:\"Код введен неверно.\";s:13:\"invalid_email\";s:62:\"Неверно введён электронный адрес.\";s:11:\"invalid_url\";s:53:\"Введён некорректный URL адрес.\";s:11:\"invalid_tel\";s:70:\"Введён некорректный телефонный номер.\";s:16:\"invalid_datetime\";s:31:\"Invalid date and time supplied.\";s:12:\"invalid_date\";s:22:\"Invalid date supplied.\";s:12:\"invalid_time\";s:22:\"Invalid time supplied.\";}'),
(1885, 398, '_additional_settings', ''),
(1886, 398, '_locale', 'ru_RU'),
(1895, 395, 'ecwd_calendar_theme', 'calendar'),
(1896, 395, '_edit_lock', '1543294157:1'),
(1897, 399, '_edit_last', '1'),
(1898, 399, '_edit_lock', '1533722380:1'),
(1899, 399, 'ecwd_event_date_from', '2018/08/08 13:00'),
(1900, 399, 'ecwd_event_date_to', '2018/08/08 15:00'),
(1901, 399, 'ecwd_event_calendars', 'a:1:{i:0;s:3:\"395\";}'),
(1902, 399, 'ecwd_event_organizers', 'a:1:{i:0;s:14:\"{organizer_id}\";}'),
(1903, 399, 'ecwd_event_url', ''),
(1904, 399, 'ecwd_event_video', ''),
(1905, 399, 'ecwd_event_repeat_event', 'no_repeat'),
(1906, 399, 'ecwd_event_day', 'a:0:{}'),
(1907, 399, 'ecwd_event_repeat_how', ''),
(1908, 399, 'ecwd_event_repeat_month_on_days', '1'),
(1909, 399, 'ecwd_event_repeat_year_on_days', '1'),
(1910, 399, 'ecwd_event_repeat_repeat_until', ''),
(1911, 401, '_edit_last', '1'),
(1912, 401, '_edit_lock', '1533722437:1'),
(1913, 401, 'ecwd_event_date_from', '2018/08/09'),
(1914, 401, 'ecwd_event_date_to', '2018/08/09'),
(1915, 401, 'ecwd_all_day_event', '1'),
(1916, 401, 'ecwd_event_calendars', 'a:1:{i:0;s:3:\"395\";}'),
(1917, 401, 'ecwd_event_organizers', 'a:1:{i:0;s:14:\"{organizer_id}\";}'),
(1918, 401, 'ecwd_event_url', ''),
(1919, 401, 'ecwd_event_video', ''),
(1920, 401, 'ecwd_event_repeat_event', 'no_repeat'),
(1921, 401, 'ecwd_event_day', 'a:0:{}'),
(1922, 401, 'ecwd_event_repeat_how', ''),
(1923, 401, 'ecwd_event_repeat_month_on_days', '1'),
(1924, 401, 'ecwd_event_repeat_year_on_days', '1'),
(1925, 401, 'ecwd_event_repeat_repeat_until', ''),
(1926, 402, '_edit_last', '1'),
(1927, 402, '_edit_lock', '1533722549:1'),
(1928, 402, 'ecwd_event_date_from', '2018/08/08 17:00'),
(1929, 402, 'ecwd_event_date_to', '2018/08/08 18:00'),
(1930, 402, 'ecwd_event_calendars', 'a:1:{i:0;s:3:\"395\";}'),
(1931, 402, 'ecwd_event_organizers', 'a:1:{i:0;s:14:\"{organizer_id}\";}'),
(1932, 402, 'ecwd_event_url', ''),
(1933, 402, 'ecwd_event_video', ''),
(1934, 402, 'ecwd_event_repeat_event', 'no_repeat'),
(1935, 402, 'ecwd_event_day', 'a:0:{}'),
(1936, 402, 'ecwd_event_repeat_how', ''),
(1937, 402, 'ecwd_event_repeat_month_on_days', '1'),
(1938, 402, 'ecwd_event_repeat_year_on_days', '1'),
(1939, 402, 'ecwd_event_repeat_repeat_until', ''),
(1942, 404, '_edit_last', '1'),
(1943, 404, '_edit_lock', '1533722675:1'),
(1944, 404, 'ecwd_event_date_from', '2018/08/11'),
(1945, 404, 'ecwd_event_date_to', '2018/08/12'),
(1946, 404, 'ecwd_all_day_event', '1'),
(1947, 404, 'ecwd_event_calendars', 'a:1:{i:0;s:3:\"395\";}'),
(1948, 404, 'ecwd_event_organizers', 'a:1:{i:0;s:14:\"{organizer_id}\";}'),
(1949, 404, 'ecwd_event_url', ''),
(1950, 404, 'ecwd_event_video', ''),
(1951, 404, 'ecwd_event_repeat_event', 'no_repeat'),
(1952, 404, 'ecwd_event_day', 'a:0:{}'),
(1953, 404, 'ecwd_event_repeat_how', ''),
(1954, 404, 'ecwd_event_repeat_month_on_days', '1'),
(1955, 404, 'ecwd_event_repeat_year_on_days', '1'),
(1956, 404, 'ecwd_event_repeat_repeat_until', ''),
(1960, 406, '_edit_last', '1'),
(1961, 406, '_edit_lock', '1533722730:1'),
(1962, 406, 'ecwd_event_date_from', '2018/08/08 18:00'),
(1963, 406, 'ecwd_event_date_to', '2018/08/08 19:00'),
(1964, 406, 'ecwd_event_calendars', 'a:1:{i:0;s:3:\"395\";}'),
(1965, 406, 'ecwd_event_organizers', 'a:1:{i:0;s:14:\"{organizer_id}\";}'),
(1966, 406, 'ecwd_event_url', ''),
(1967, 406, 'ecwd_event_video', ''),
(1968, 406, 'ecwd_event_repeat_event', 'no_repeat'),
(1969, 406, 'ecwd_event_day', 'a:0:{}'),
(1970, 406, 'ecwd_event_repeat_how', ''),
(1971, 406, 'ecwd_event_repeat_month_on_days', '1'),
(1972, 406, 'ecwd_event_repeat_year_on_days', '1'),
(1973, 406, 'ecwd_event_repeat_repeat_until', ''),
(1974, 407, '_edit_last', '1'),
(1975, 407, '_edit_lock', '1533723559:1'),
(1976, 407, 'ecwd_event_date_from', '2018/08/08 16:00'),
(1977, 407, 'ecwd_event_date_to', '2018/08/08 17:00'),
(1978, 407, 'ecwd_event_calendars', 'a:1:{i:0;s:3:\"395\";}'),
(1979, 407, 'ecwd_event_organizers', 'a:1:{i:0;s:14:\"{organizer_id}\";}'),
(1980, 407, 'ecwd_event_url', ''),
(1981, 407, 'ecwd_event_video', ''),
(1982, 407, 'ecwd_event_repeat_event', 'no_repeat'),
(1983, 407, 'ecwd_event_day', 'a:0:{}'),
(1984, 407, 'ecwd_event_repeat_how', ''),
(1985, 407, 'ecwd_event_repeat_month_on_days', '1'),
(1986, 407, 'ecwd_event_repeat_year_on_days', '1'),
(1987, 407, 'ecwd_event_repeat_repeat_until', ''),
(2028, 424, '_wp_attached_file', '2018/08/v-rossii-mozhet-poyavitsya-eshche-odin-vyhodnoy-133513.jpg'),
(2029, 424, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:700;s:6:\"height\";i:466;s:4:\"file\";s:66:\"2018/08/v-rossii-mozhet-poyavitsya-eshche-odin-vyhodnoy-133513.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:66:\"v-rossii-mozhet-poyavitsya-eshche-odin-vyhodnoy-133513-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:66:\"v-rossii-mozhet-poyavitsya-eshche-odin-vyhodnoy-133513-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2030, 424, '_wp_attachment_image_alt', 'img'),
(2045, 428, '_form', '<label> Ваше имя\n    [text your-name akismet:author] </label>\n\n<label> Ваш e-mail <span class=\"red\">*</span>\n    [email* your-email] </label>\n\n<label class=\"label_text\"> Сообщение\n    [textarea your-message] </label>\n\n[acceptance acceptance-59] <span class=\"normal\"> Согласен с <a href=\"[main_url]/private-policy/\">политикой конфиденциальности</a></span>\n\n<br>\n\n[recaptcha]\n\n[submit \"Отправить\"]'),
(2046, 428, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:41:\"Письмо с сайта kaza4enkova.ru\";s:6:\"sender\";s:36:\"[your-name] <wordpress@stylist.host>\";s:9:\"recipient\";s:18:\"gck92462@eoopy.com\";s:4:\"body\";s:106:\"Сообщение с сайта kaza4enkova.ru\n\nИмя: [your-name]\nemail: <[your-email]>\n\n[your-message]\";s:18:\"additional_headers\";s:28:\"Reply-To: irene17_07@mail.ru\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(2047, 428, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:24:\"stylist \"[your-subject]\"\";s:6:\"sender\";s:25:\"stylist <wordpress@wsite>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:123:\"Сообщение:\n[your-message]\n\n-- \nЭто сообщение отправлено с сайта stylist (http://wsite)\";s:18:\"additional_headers\";s:32:\"Reply-To: capitan.flin@yandex.ru\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(2048, 428, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:55:\"Сообщение успешно отправлено.\";s:12:\"mail_sent_ng\";s:144:\"При отправке сообщения произошла ошибка. Пожалуйста, попробуйте ещё раз позже.\";s:16:\"validation_error\";s:53:\"Проверьте еще раз поля формы.\";s:4:\"spam\";s:30:\"Проверьте капчу.\";s:12:\"accept_terms\";s:132:\"Вы должны принять условия и положения перед отправкой вашего сообщения.\";s:16:\"invalid_required\";s:60:\"Поле обязательно для заполнения.\";s:16:\"invalid_too_long\";s:39:\"Поле слишком длинное.\";s:17:\"invalid_too_short\";s:41:\"Поле слишком короткое.\";s:13:\"upload_failed\";s:90:\"При загрузке файла произошла неизвестная ошибка.\";s:24:\"upload_file_type_invalid\";s:81:\"Вам не разрешено загружать файлы этого типа.\";s:21:\"upload_file_too_large\";s:39:\"Файл слишком большой.\";s:23:\"upload_failed_php_error\";s:67:\"При загрузке файла произошла ошибка.\";s:14:\"invalid_number\";s:47:\"Формат числа некорректен.\";s:16:\"number_too_small\";s:68:\"Число меньше минимально допустимого.\";s:16:\"number_too_large\";s:70:\"Число больше максимально допустимого.\";s:23:\"quiz_answer_not_correct\";s:69:\"Неверный ответ на проверочный вопрос.\";s:17:\"captcha_not_match\";s:35:\"Код введен неверно.\";s:13:\"invalid_email\";s:62:\"Неверно введён электронный адрес.\";s:11:\"invalid_url\";s:53:\"Введён некорректный URL адрес.\";s:11:\"invalid_tel\";s:70:\"Введён некорректный телефонный номер.\";s:16:\"invalid_datetime\";s:31:\"Invalid date and time supplied.\";s:12:\"invalid_date\";s:22:\"Invalid date supplied.\";s:12:\"invalid_time\";s:22:\"Invalid time supplied.\";}'),
(2049, 428, '_additional_settings', '');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES 
(2050, 428, '_locale', 'ru_RU'),
(2053, 429, '_edit_last', '1'),
(2054, 429, '_edit_lock', '1573240763:1'),
(2083, 433, '_tablepress_table_options', '{\"last_editor\":1,\"table_head\":true,\"table_foot\":false,\"alternating_row_colors\":true,\"row_hover\":true,\"print_name\":false,\"print_name_position\":\"above\",\"print_description\":false,\"print_description_position\":\"below\",\"extra_css_classes\":\"\",\"use_datatables\":true,\"datatables_sort\":false,\"datatables_filter\":false,\"datatables_paginate\":false,\"datatables_lengthchange\":false,\"datatables_paginate_entries\":10,\"datatables_info\":false,\"datatables_scrollx\":false,\"datatables_custom_commands\":\"\"}'),
(2084, 433, '_tablepress_table_visibility', '{\"rows\":[1,1,1,1,1],\"columns\":[1,1,1]}'),
(2107, 35, '_wp_old_slug', '%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b5%d1%8f'),
(2108, 43, '_wp_old_slug', '%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b5%d1%8f'),
(2109, 399, '_wp_old_slug', '%d0%bc%d0%b0%d0%bd%d0%b8%d0%ba%d1%8e%d1%80'),
(2110, 401, '_wp_old_slug', '%d0%b7%d0%b0%d0%bd%d1%8f%d1%82%d0%b0'),
(2111, 402, '_wp_old_slug', '%d0%b8%d1%80%d0%b0-%d1%81%d1%82%d1%80%d0%b8%d0%b6%d0%ba%d0%b0'),
(2112, 404, '_wp_old_slug', '%d0%b7%d0%b0%d0%bf%d0%b8%d1%81%d0%b8-%d0%bd%d0%b5%d1%82'),
(2113, 406, '_wp_old_slug', '%d0%b1%d0%be%d1%80%d1%8f-%d1%81%d1%82%d1%80%d0%b8%d0%b6%d0%ba%d0%b0'),
(2114, 407, '_wp_old_slug', '%d0%bc%d0%b0%d0%bd%d0%b8%d0%ba%d1%8e%d1%80-2'),
(2126, 428, '_wp_old_slug', '%d1%84%d0%be%d1%80%d0%bc%d0%b0-%d0%bc%d0%be%d0%b4%d0%b0%d0%bb%d1%8c%d0%bd%d0%be%d0%b3%d0%be-%d0%be%d0%ba%d0%bd%d0%b0'),
(2127, 433, '_wp_old_slug', '%d0%bf%d1%80%d0%b0%d0%b9%d1%81'),
(2130, 450, '_edit_last', '1'),
(2131, 450, '_edit_lock', '1573062543:1'),
(2184, 475, '_edit_last', '1'),
(2185, 475, '_edit_lock', '1558376925:1'),
(2186, 477, '_edit_last', '1'),
(2187, 477, '_edit_lock', '1573329361:1'),
(2190, 496, '_wp_attached_file', '2018/11/1468242807_stilist-380h210.jpg'),
(2191, 496, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:380;s:6:\"height\";i:210;s:4:\"file\";s:38:\"2018/11/1468242807_stilist-380h210.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:38:\"1468242807_stilist-380h210-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:38:\"1468242807_stilist-380h210-300x166.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:166;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(2192, 496, '_wp_attachment_image_alt', 'img'),
(2193, 299, '_wp_old_slug', '3'),
(2200, 514, '_wp_attached_file', '2018/11/21042370_1181331868633623_1302576685098467328_n.jpg'),
(2201, 514, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:808;s:6:\"height\";i:1078;s:4:\"file\";s:59:\"2018/11/21042370_1181331868633623_1302576685098467328_n.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:59:\"21042370_1181331868633623_1302576685098467328_n-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:59:\"21042370_1181331868633623_1302576685098467328_n-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:60:\"21042370_1181331868633623_1302576685098467328_n-768x1025.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1025;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:60:\"21042370_1181331868633623_1302576685098467328_n-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(2202, 514, '_wp_attachment_image_alt', 'мое фото'),
(2203, 515, '_wp_attached_file', '2018/11/20398178_445919659140861_3772480860834496512_n.jpg'),
(2204, 515, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:808;s:6:\"height\";i:1080;s:4:\"file\";s:58:\"2018/11/20398178_445919659140861_3772480860834496512_n.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:58:\"20398178_445919659140861_3772480860834496512_n-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:58:\"20398178_445919659140861_3772480860834496512_n-224x300.jpg\";s:5:\"width\";i:224;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:59:\"20398178_445919659140861_3772480860834496512_n-768x1027.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1027;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:59:\"20398178_445919659140861_3772480860834496512_n-766x1024.jpg\";s:5:\"width\";i:766;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(2205, 515, '_wp_attachment_image_alt', 'мое фото'),
(2208, 527, '_edit_last', '1'),
(2209, 527, '_edit_lock', '1543291365:1'),
(2210, 527, 'sub_title', ''),
(2211, 527, 'color', ''),
(2212, 527, 'hover_color', ''),
(2213, 527, 'text_color', ''),
(2214, 527, 'hover_text_color', ''),
(2215, 527, 'timetable_custom_url', ''),
(2216, 527, 'timetable_disable_url', '0'),
(2217, 529, '_edit_last', '1'),
(2218, 529, '_edit_lock', '1543292440:1'),
(2219, 529, 'organizer', NULL);
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES 
(2220, 529, 'places', NULL),
(2221, 529, 'calendar', NULL),
(2222, 529, 'ev-from', '2018-11-28'),
(2223, 529, 'ev-from-h', ''),
(2224, 529, 'ev-from-m', ''),
(2225, 529, 'ev-to', '2018-11-28'),
(2226, 529, 'ev-to-h', ''),
(2227, 529, 'ev-to-m', ''),
(2228, 529, 'ev-repeat', ''),
(2229, 529, 'ev-repeat-every', ''),
(2230, 529, 'ev-repeat-option', 'never'),
(2231, 529, 'ev-until', ''),
(2232, 529, 'ev-end_count', ''),
(2233, 529, 'featured', NULL),
(2234, 529, 'order', ''),
(2235, 529, 'tickets', ''),
(2236, 529, 'events', NULL),
(2237, 529, 'seasons', NULL),
(2238, 529, 'custom_fields', NULL),
(2239, 529, 'dad1', 'template-default'),
(2242, 533, '_edit_last', '1'),
(2243, 533, '_edit_lock', '1543294034:1'),
(2244, 533, 'ecwd_event_date_from', '2018/11/27 07:00'),
(2245, 533, 'ecwd_event_date_to', '2018/11/27 08:00'),
(2246, 533, 'ecwd_event_calendars', 'a:1:{i:0;s:3:\"395\";}'),
(2247, 533, 'ecwd_event_organizers', 'a:1:{i:0;s:14:\"{organizer_id}\";}'),
(2248, 533, 'ecwd_event_url', ''),
(2249, 533, 'ecwd_event_video', ''),
(2250, 533, 'ecwd_event_repeat_event', 'no_repeat'),
(2251, 533, 'ecwd_event_day', 'a:0:{}'),
(2252, 533, 'ecwd_event_repeat_how', ''),
(2253, 533, 'ecwd_event_repeat_month_on_days', '1'),
(2254, 533, 'ecwd_event_repeat_year_on_days', '1'),
(2255, 533, 'ecwd_event_repeat_repeat_until', ''),
(2256, 534, '_edit_last', '1'),
(2257, 534, '_edit_lock', '1543294239:1'),
(2258, 534, 'ecwd_event_date_from', '2018/11/30 16:00'),
(2259, 534, 'ecwd_event_date_to', '2018/11/30 19:00'),
(2260, 534, 'ecwd_event_calendars', 'a:1:{i:0;s:3:\"395\";}'),
(2261, 534, 'ecwd_event_organizers', 'a:1:{i:0;s:14:\"{organizer_id}\";}'),
(2262, 534, 'ecwd_event_url', ''),
(2263, 534, 'ecwd_event_video', ''),
(2264, 534, 'ecwd_event_repeat_event', 'no_repeat'),
(2265, 534, 'ecwd_event_day', 'a:0:{}'),
(2266, 534, 'ecwd_event_repeat_how', ''),
(2267, 534, 'ecwd_event_repeat_month_on_days', '1'),
(2268, 534, 'ecwd_event_repeat_year_on_days', '1'),
(2269, 534, 'ecwd_event_repeat_repeat_until', ''),
(2270, 534, '_wp_old_slug', '%d0%b1%d0%be%d1%80%d1%8f-%d1%81%d1%82%d1%80%d0%b8%d0%b6%d0%ba%d0%b0'),
(2275, 537, '_mc_guid', '0e485f3cb80ed947bc429ed3c38f5dfc'),
(2276, 537, '_mc_event_shortcode', '[my_calendar_event event=\'7\' template=\'details\' list=\'\']'),
(2277, 537, '_mc_event_access', 'a:1:{s:5:\"notes\";s:0:\"\";}'),
(2278, 537, '_mc_event_id', '7'),
(2279, 537, '_mc_event_desc', 'Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.'),
(2280, 537, '_mc_event_image', ''),
(2281, 537, '_event_time_label', 'Весь день'),
(2282, 537, '_mc_event_data', 'a:40:{s:11:\"event_begin\";s:10:\"2018-11-28\";s:9:\"event_end\";s:10:\"2018-11-28\";s:11:\"event_title\";s:11:\"Lorem Ipsum\";s:10:\"event_desc\";s:497:\"Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.\";s:11:\"event_short\";s:0:\"\";s:10:\"event_time\";s:8:\"11:00:00\";s:13:\"event_endtime\";s:8:\"11:45:00\";s:10:\"event_link\";s:0:\"\";s:11:\"event_label\";s:0:\"\";s:12:\"event_street\";s:0:\"\";s:13:\"event_street2\";s:0:\"\";s:10:\"event_city\";s:0:\"\";s:11:\"event_state\";s:0:\"\";s:14:\"event_postcode\";s:0:\"\";s:12:\"event_region\";s:0:\"\";s:13:\"event_country\";s:0:\"\";s:9:\"event_url\";s:0:\"\";s:11:\"event_recur\";s:2:\"S1\";s:11:\"event_image\";s:0:\"\";s:11:\"event_phone\";s:0:\"\";s:12:\"event_phone2\";s:0:\"\";s:12:\"event_access\";s:0:\"\";s:13:\"event_tickets\";s:0:\"\";s:18:\"event_registration\";s:0:\"\";s:13:\"event_repeats\";s:1:\"0\";s:12:\"event_author\";s:1:\"1\";s:14:\"event_category\";s:1:\"1\";s:18:\"event_link_expires\";s:1:\"0\";s:10:\"event_zoom\";s:2:\"16\";s:14:\"event_approved\";i:1;s:10:\"event_host\";s:1:\"1\";s:13:\"event_flagged\";i:0;s:16:\"event_fifth_week\";i:1;s:13:\"event_holiday\";i:0;s:14:\"event_group_id\";i:0;s:10:\"event_span\";i:0;s:14:\"event_hide_end\";i:0;s:15:\"event_longitude\";s:8:\"0.000000\";s:14:\"event_latitude\";s:8:\"0.000000\";s:9:\"shortcode\";s:56:\"[my_calendar_event event=\'7\' template=\'details\' list=\'\']\";}'),
(2283, 538, '_mc_guid', '5d50ac26e7c46e03ab863617e386f636'),
(2284, 538, '_mc_event_shortcode', '[my_calendar_event event=\'8\' template=\'details\' list=\'\']'),
(2285, 538, '_mc_event_access', 'a:1:{s:5:\"notes\";s:0:\"\";}'),
(2286, 538, '_mc_event_id', '8'),
(2287, 538, '_mc_event_desc', 'Побрить Васю на лысо.'),
(2288, 538, '_mc_event_image', 'http://stylist/wp-content/uploads/2018/07/s-l300.jpg'),
(2289, 538, '_event_time_label', 'Весь день'),
(2290, 538, '_mc_event_data', 'a:40:{s:11:\"event_begin\";s:10:\"2018-11-29\";s:9:\"event_end\";s:10:\"2018-11-29\";s:11:\"event_title\";s:26:\"Стрижка у Васи\";s:10:\"event_desc\";s:38:\"Побрить Васю на лысо.\";s:11:\"event_short\";s:0:\"\";s:10:\"event_time\";s:8:\"08:00:00\";s:13:\"event_endtime\";s:8:\"09:00:00\";s:10:\"event_link\";s:0:\"\";s:11:\"event_label\";s:0:\"\";s:12:\"event_street\";s:0:\"\";s:13:\"event_street2\";s:0:\"\";s:10:\"event_city\";s:0:\"\";s:11:\"event_state\";s:0:\"\";s:14:\"event_postcode\";s:0:\"\";s:12:\"event_region\";s:0:\"\";s:13:\"event_country\";s:0:\"\";s:9:\"event_url\";s:0:\"\";s:11:\"event_recur\";s:2:\"S1\";s:11:\"event_image\";s:52:\"http://stylist/wp-content/uploads/2018/07/s-l300.jpg\";s:11:\"event_phone\";s:0:\"\";s:12:\"event_phone2\";s:0:\"\";s:12:\"event_access\";s:0:\"\";s:13:\"event_tickets\";s:0:\"\";s:18:\"event_registration\";s:0:\"\";s:13:\"event_repeats\";s:1:\"0\";s:12:\"event_author\";s:1:\"1\";s:14:\"event_category\";s:1:\"1\";s:18:\"event_link_expires\";s:1:\"0\";s:10:\"event_zoom\";s:2:\"16\";s:14:\"event_approved\";i:1;s:10:\"event_host\";s:1:\"1\";s:13:\"event_flagged\";i:0;s:16:\"event_fifth_week\";i:1;s:13:\"event_holiday\";i:0;s:14:\"event_group_id\";i:0;s:10:\"event_span\";i:0;s:14:\"event_hide_end\";i:0;s:15:\"event_longitude\";s:8:\"0.000000\";s:14:\"event_latitude\";s:8:\"0.000000\";s:9:\"shortcode\";s:56:\"[my_calendar_event event=\'8\' template=\'details\' list=\'\']\";}'),
(2291, 538, '_thumbnail_id', '374'),
(2292, 374, '_edit_lock', '1543303247:1'),
(2293, 374, '_wp_attachment_backup_sizes', 'a:1:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:300;s:6:\"height\";i:300;s:4:\"file\";s:10:\"s-l300.jpg\";}}'),
(2294, 374, '_edit_last', '1'),
(2295, 384, '_edit_lock', '1543303457:1'),
(2296, 384, '_wp_attachment_backup_sizes', 'a:1:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:300;s:6:\"height\";i:238;s:4:\"file\";s:14:\"300px-Negr.jpg\";}}'),
(2297, 384, '_edit_last', '1'),
(2298, 539, '_thumbnail_id', '384'),
(2299, 539, '_mc_guid', '8839a3809a825d40a0bc837a84e05418'),
(2300, 539, '_mc_event_shortcode', '[my_calendar_event event=\'9\' template=\'details\' list=\'\']'),
(2301, 539, '_mc_event_access', 'a:1:{s:5:\"notes\";s:0:\"\";}'),
(2302, 539, '_mc_event_id', '9'),
(2303, 539, '_mc_event_desc', 'Майкла надо постричь.'),
(2304, 539, '_mc_event_image', 'http://stylist/wp-content/uploads/2018/07/300px-Negr.jpg'),
(2305, 539, '_event_time_label', 'Весь день'),
(2306, 539, '_mc_event_data', 'a:40:{s:11:\"event_begin\";s:10:\"2018-12-13\";s:9:\"event_end\";s:10:\"2018-12-13\";s:11:\"event_title\";s:29:\"Постричь Майкла\";s:10:\"event_desc\";s:39:\"Майкла надо постричь.\";s:11:\"event_short\";s:0:\"\";s:10:\"event_time\";s:8:\"10:45:00\";s:13:\"event_endtime\";s:8:\"11:30:00\";s:10:\"event_link\";s:0:\"\";s:11:\"event_label\";s:0:\"\";s:12:\"event_street\";s:0:\"\";s:13:\"event_street2\";s:0:\"\";s:10:\"event_city\";s:0:\"\";s:11:\"event_state\";s:0:\"\";s:14:\"event_postcode\";s:0:\"\";s:12:\"event_region\";s:0:\"\";s:13:\"event_country\";s:0:\"\";s:9:\"event_url\";s:0:\"\";s:11:\"event_recur\";s:2:\"S1\";s:11:\"event_image\";s:56:\"http://stylist/wp-content/uploads/2018/07/300px-Negr.jpg\";s:11:\"event_phone\";s:0:\"\";s:12:\"event_phone2\";s:0:\"\";s:12:\"event_access\";s:0:\"\";s:13:\"event_tickets\";s:0:\"\";s:18:\"event_registration\";s:0:\"\";s:13:\"event_repeats\";s:1:\"0\";s:12:\"event_author\";s:1:\"1\";s:14:\"event_category\";s:1:\"1\";s:18:\"event_link_expires\";s:1:\"0\";s:10:\"event_zoom\";s:2:\"16\";s:14:\"event_approved\";i:1;s:10:\"event_host\";s:1:\"1\";s:13:\"event_flagged\";i:0;s:16:\"event_fifth_week\";i:1;s:13:\"event_holiday\";i:0;s:14:\"event_group_id\";i:0;s:10:\"event_span\";i:0;s:14:\"event_hide_end\";i:0;s:15:\"event_longitude\";s:0:\"\";s:14:\"event_latitude\";s:0:\"\";s:9:\"shortcode\";s:56:\"[my_calendar_event event=\'9\' template=\'details\' list=\'\']\";}'),
(2312, 475, '_aioseop_description', 'Стилист в Калининграде по доступным ценам. Полный комплекс работ с бровями. Профессионально и быстро. Звоните: +7 (909) 79 60 445'),
(2313, 475, '_aioseop_title', 'Стилист в Калининграде'),
(2314, 5, '_aioseop_title', 'Прайс'),
(2315, 5, '_aioseop_description', 'Цены на наши услуги. Недорого. Профессионально и быстро. Звоните: +7 (909) 79 60 445'),
(2316, 429, '_aioseop_title', 'Политика конфиденциальности'),
(2317, 429, '_aioseop_description', 'Политика конфиденциальности. Условия сбора и обработки персональных данных пользователей, возможности использования и передачи, сроки хранения информации'),
(2318, 10, '_aioseop_title', 'Галерея фотоснимков'),
(2319, 10, '_aioseop_description', 'Фото примеров выполненных работ. Инстаграм-галерея'),
(2322, 315, '_aioseop_title', 'Запись на прием'),
(2323, 315, '_aioseop_description', 'Запись на прием и бесплатные консультации по всем вопросам. Звоните: +7 (909) 79 60 445'),
(2332, 306, '_aioseop_description', 'Контакты, социальные сети, телефон, email. Звоните: +7 (909) 79 60 445'),
(2333, 306, '_aioseop_title', 'Контакты'),
(2354, 450, '_aioseop_description', 'Создание вашего имиджа. Полный спектр работ по преображению вашей внешности.Профессионально и быстро. Звоните: +7 (909) 79 60 445'),
(2355, 450, '_aioseop_title', 'Салон красоты в Калининграде'),
(2360, 477, '_aioseop_description', 'Бровист в Калининграде по доступным ценам. Полный комплекс работ с бровями. Профессионально и быстро. Звоните: +7 (909) 79 60 445'),
(2361, 477, '_aioseop_title', 'Бровист в Калининграде'),
(2364, 310, '_aioseop_description', 'Небольшая история становления меня как специалиста в своей области.'),
(2365, 310, '_aioseop_title', 'Личная история'),
(2368, 428, '_config_errors', 'a:1:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:103;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:70:\"https://contactform7.com/configuration-errors/email-not-in-site-domain\";}}}}'),
(2369, 398, '_config_errors', 'a:1:{s:11:\"mail.sender\";a:1:{i:0;a:2:{s:4:\"code\";i:103;s:4:\"args\";a:3:{s:7:\"message\";s:0:\"\";s:6:\"params\";a:0:{}s:4:\"link\";s:70:\"https://contactform7.com/configuration-errors/email-not-in-site-domain\";}}}}');
/*!40000 ALTER TABLE `wp_postmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=562 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_posts`
--

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES 
(5, 1, 0x323031382d30372d31362032313a35393a3238, 0x323031382d30372d31362031383a35393a3238, '[table id=1]', 'Прайс', '', 'publish', 'closed', 'closed', '', 'price', '', '', 0x323031382d31312d32362030383a35363a3430, 0x323031382d31312d32362030363a35363a3430, '', 0, 'http://wsite/?page_id=5', 0, 'page', '', 0),
(6, 1, 0x323031382d30372d31362032313a35393a3238, 0x323031382d30372d31362031383a35393a3238, '', 'Услуги', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', 0x323031382d30372d31362032313a35393a3238, 0x323031382d30372d31362031383a35393a3238, '', 5, 'http://wsite/2018/07/16/5-revision-v1/', 0, 'revision', '', 0),
(10, 1, 0x323031382d30372d31372030333a32353a3039, 0x323031382d30372d31372030303a32353a3039, '', 'Галерея', '', 'publish', 'closed', 'closed', '', 'gallery', '', '', 0x323031382d30372d32342030333a31353a3436, 0x323031382d30372d32342030303a31353a3436, '', 0, 'http://wsite/?page_id=10', 0, 'page', '', 0),
(11, 1, 0x323031382d30372d31372030333a32353a3039, 0x323031382d30372d31372030303a32353a3039, '', 'Галерея', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', 0x323031382d30372d31372030333a32353a3039, 0x323031382d30372d31372030303a32353a3039, '', 10, 'http://wsite/2018/07/17/10-revision-v1/', 0, 'revision', '', 0),
(19, 1, 0x323031382d30372d31382032323a33343a3436, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_gallery', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31382032323a33343a3436, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=19', 0, 'ngg_gallery', '', 0),
(20, 1, 0x323031382d30372d31382032323a33343a3436, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31382032323a33343a3436, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=20', 0, 'ngg_pictures', '', 0),
(22, 1, 0x323031382d30372d31392030323a34383a3433, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_gallery', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a34383a3433, 0x323031382d30372d31382032333a34383a3433, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=22', 0, 'ngg_gallery', '', 0),
(23, 1, 0x323031382d30372d31382032323a33343a3530, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31382032323a33343a3530, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=23', 0, 'ngg_pictures', '', 0),
(25, 1, 0x323031382d30372d31382032323a33343a3531, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31382032323a33343a3531, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=25', 0, 'ngg_pictures', '', 0),
(27, 1, 0x323031382d30372d31382032323a33343a3531, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31382032323a33343a3531, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=27', 0, 'ngg_pictures', '', 0),
(29, 1, 0x323031382d30372d31382032323a33343a3532, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31382032323a33343a3532, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=29', 0, 'ngg_pictures', '', 0),
(31, 1, 0x323031382d30372d31382032323a33343a3532, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31382032323a33343a3532, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=31', 0, 'ngg_pictures', '', 0),
(33, 1, 0x323031382d30372d31382032323a33383a3432, 0x323031382d30372d31382031393a33383a3432, '[ngg_images source=\"galleries\" display_type=\"photocrati-nextgen_basic_thumbnails\" override_thumbnail_settings=\"0\" thumbnail_width=\"240\" thumbnail_height=\"160\" thumbnail_crop=\"1\" images_per_page=\"20\" number_of_columns=\"0\" ajax_pagination=\"1\" show_all_in_lightbox=\"0\" use_imagebrowser_effect=\"0\" show_slideshow_link=\"1\" slideshow_link_text=\"[Показать слайдшоу]\" order_by=\"sortorder\" order_direction=\"ASC\" returns=\"included\" maximum_entity_count=\"500\"]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', 0x323031382d30372d31382032323a33383a3432, 0x323031382d30372d31382031393a33383a3432, '', 10, 'http://wsite/2018/07/18/10-revision-v1/', 0, 'revision', '', 0),
(34, 1, 0x323031382d30372d31382032323a33393a3135, 0x323031382d30372d31382031393a33393a3135, '', 'Галерея', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', 0x323031382d30372d31382032323a33393a3135, 0x323031382d30372d31382031393a33393a3135, '', 10, 'http://wsite/2018/07/18/10-revision-v1/', 0, 'revision', '', 0),
(35, 1, 0x323031382d30372d31392030333a30333a3438, 0x323031382d30372d31392030303a30333a3438, '', 'Галерея', '', 'publish', 'closed', 'closed', '', 'galereya', '', '', 0x323031382d30372d31392030333a30393a3231, 0x323031382d30372d31392030303a30393a3231, '', 0, 'http://wsite/?post_type=envira&#038;p=35', 0, 'envira', '', 0),
(42, 1, 0x323031382d30372d31382032333a32383a3039, 0x323031382d30372d31382032303a32383a3039, '', '', '', 'publish', 'closed', 'closed', '', '42', '', '', 0x323031382d30372d31382032333a33303a3438, 0x323031382d30372d31382032303a33303a3438, '', 0, 'http://wsite/?post_type=foogallery&#038;p=42', 0, 'foogallery', '', 0),
(43, 1, 0x323031382d30372d31382032333a33383a3433, 0x323031382d30372d31382032303a33383a3433, '[Best_Wordpress_Gallery id=\"1\" gal_title=\"Галерея\"]', 'Галерея', '', 'publish', 'closed', 'closed', '', 'galereya', '', '', 0x323031382d30372d31392030323a32363a3130, 0x323031382d30372d31382032333a32363a3130, '', 0, 'http://wsite/bwg_gallery/%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b5%d1%8f/', 0, 'bwg_gallery', '', 0),
(109, 1, 0x323031382d30372d31392030323a35313a3132, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_gallery', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3132, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=109', 0, 'ngg_gallery', '', 0),
(110, 1, 0x323031382d30372d31392030323a35313a3132, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3132, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=110', 0, 'ngg_pictures', '', 0),
(111, 1, 0x323031382d30372d31392030323a35313a3133, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3133, 0x323031382d30372d31382032333a35313a3133, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=111', 0, 'ngg_pictures', '', 0),
(112, 1, 0x323031382d30372d31392030323a35313a3133, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_gallery', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3133, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=112', 0, 'ngg_gallery', '', 0),
(113, 1, 0x323031382d30372d31392030323a35313a3133, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3133, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=113', 0, 'ngg_pictures', '', 0),
(114, 1, 0x323031382d30372d31392030323a35313a3133, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3133, 0x323031382d30372d31382032333a35313a3133, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=114', 0, 'ngg_pictures', '', 0),
(115, 1, 0x323031382d30372d31392030323a35313a3134, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3134, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=115', 0, 'ngg_pictures', '', 0),
(116, 1, 0x323031382d30372d31392030323a35313a3134, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3134, 0x323031382d30372d31382032333a35313a3134, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=116', 0, 'ngg_pictures', '', 0),
(117, 1, 0x323031382d30372d31392030323a35313a3134, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3134, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=117', 0, 'ngg_pictures', '', 0),
(118, 1, 0x323031382d30372d31392030323a35313a3134, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3134, 0x323031382d30372d31382032333a35313a3134, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=118', 0, 'ngg_pictures', '', 0),
(119, 1, 0x323031382d30372d31392030323a35313a3135, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3135, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=119', 0, 'ngg_pictures', '', 0),
(120, 1, 0x323031382d30372d31392030323a35313a3135, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3135, 0x323031382d30372d31382032333a35313a3135, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=120', 0, 'ngg_pictures', '', 0),
(121, 1, 0x323031382d30372d31392030323a35313a3135, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3135, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=121', 0, 'ngg_pictures', '', 0),
(122, 1, 0x323031382d30372d31392030323a35313a3136, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3136, 0x323031382d30372d31382032333a35313a3136, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=122', 0, 'ngg_pictures', '', 0),
(123, 1, 0x323031382d30372d31392030323a35313a3136, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3136, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=123', 0, 'ngg_pictures', '', 0),
(124, 1, 0x323031382d30372d31392030323a35313a3136, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3136, 0x323031382d30372d31382032333a35313a3136, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=124', 0, 'ngg_pictures', '', 0),
(125, 1, 0x323031382d30372d31392030323a35313a3137, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3137, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=125', 0, 'ngg_pictures', '', 0),
(126, 1, 0x323031382d30372d31392030323a35313a3137, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3137, 0x323031382d30372d31382032333a35313a3137, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=126', 0, 'ngg_pictures', '', 0),
(127, 1, 0x323031382d30372d31392030323a35313a3137, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3137, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=127', 0, 'ngg_pictures', '', 0),
(128, 1, 0x323031382d30372d31392030323a35313a3138, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3138, 0x323031382d30372d31382032333a35313a3138, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=128', 0, 'ngg_pictures', '', 0),
(129, 1, 0x323031382d30372d31392030323a35313a3138, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3138, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=129', 0, 'ngg_pictures', '', 0),
(130, 1, 0x323031382d30372d31392030323a35313a3138, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3138, 0x323031382d30372d31382032333a35313a3138, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=130', 0, 'ngg_pictures', '', 0),
(131, 1, 0x323031382d30372d31392030323a35313a3139, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3139, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=131', 0, 'ngg_pictures', '', 0),
(132, 1, 0x323031382d30372d31392030323a35313a3139, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3139, 0x323031382d30372d31382032333a35313a3139, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=132', 0, 'ngg_pictures', '', 0),
(133, 1, 0x323031382d30372d31392030323a35313a3230, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3230, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=133', 0, 'ngg_pictures', '', 0),
(134, 1, 0x323031382d30372d31392030323a35313a3230, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3230, 0x323031382d30372d31382032333a35313a3230, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=134', 0, 'ngg_pictures', '', 0),
(135, 1, 0x323031382d30372d31392030323a35313a3230, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3230, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=135', 0, 'ngg_pictures', '', 0),
(136, 1, 0x323031382d30372d31392030323a35313a3231, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3231, 0x323031382d30372d31382032333a35313a3231, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=136', 0, 'ngg_pictures', '', 0),
(137, 1, 0x323031382d30372d31392030323a35313a3231, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3231, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=137', 0, 'ngg_pictures', '', 0),
(138, 1, 0x323031382d30372d31392030323a35313a3231, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3231, 0x323031382d30372d31382032333a35313a3231, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=138', 0, 'ngg_pictures', '', 0),
(139, 1, 0x323031382d30372d31392030323a35313a3232, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3232, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=139', 0, 'ngg_pictures', '', 0),
(140, 1, 0x323031382d30372d31392030323a35313a3232, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3232, 0x323031382d30372d31382032333a35313a3232, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=140', 0, 'ngg_pictures', '', 0),
(141, 1, 0x323031382d30372d31392030323a35313a3232, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3232, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=141', 0, 'ngg_pictures', '', 0),
(142, 1, 0x323031382d30372d31392030323a35313a3233, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3233, 0x323031382d30372d31382032333a35313a3233, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=142', 0, 'ngg_pictures', '', 0),
(143, 1, 0x323031382d30372d31392030323a35313a3233, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3233, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=143', 0, 'ngg_pictures', '', 0),
(144, 1, 0x323031382d30372d31392030323a35313a3233, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3233, 0x323031382d30372d31382032333a35313a3233, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=144', 0, 'ngg_pictures', '', 0),
(145, 1, 0x323031382d30372d31392030323a35313a3234, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3234, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=145', 0, 'ngg_pictures', '', 0),
(146, 1, 0x323031382d30372d31392030323a35313a3234, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3234, 0x323031382d30372d31382032333a35313a3234, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=146', 0, 'ngg_pictures', '', 0),
(147, 1, 0x323031382d30372d31392030323a35313a3234, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3234, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=147', 0, 'ngg_pictures', '', 0),
(148, 1, 0x323031382d30372d31392030323a35313a3235, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3235, 0x323031382d30372d31382032333a35313a3235, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=148', 0, 'ngg_pictures', '', 0),
(149, 1, 0x323031382d30372d31392030323a35313a3236, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3236, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=149', 0, 'ngg_pictures', '', 0),
(150, 1, 0x323031382d30372d31392030323a35313a3236, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3236, 0x323031382d30372d31382032333a35313a3236, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=150', 0, 'ngg_pictures', '', 0),
(151, 1, 0x323031382d30372d31392030323a35313a3237, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3237, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=151', 0, 'ngg_pictures', '', 0),
(152, 1, 0x323031382d30372d31392030323a35313a3237, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3237, 0x323031382d30372d31382032333a35313a3237, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=152', 0, 'ngg_pictures', '', 0),
(153, 1, 0x323031382d30372d31392030323a35313a3238, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3238, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=153', 0, 'ngg_pictures', '', 0),
(154, 1, 0x323031382d30372d31392030323a35313a3238, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3238, 0x323031382d30372d31382032333a35313a3238, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=154', 0, 'ngg_pictures', '', 0),
(155, 1, 0x323031382d30372d31392030323a35313a3239, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3239, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=155', 0, 'ngg_pictures', '', 0),
(156, 1, 0x323031382d30372d31392030323a35313a3239, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3239, 0x323031382d30372d31382032333a35313a3239, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=156', 0, 'ngg_pictures', '', 0),
(157, 1, 0x323031382d30372d31392030323a35313a3330, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3330, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=157', 0, 'ngg_pictures', '', 0),
(158, 1, 0x323031382d30372d31392030323a35313a3330, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3330, 0x323031382d30372d31382032333a35313a3330, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=158', 0, 'ngg_pictures', '', 0),
(159, 1, 0x323031382d30372d31392030323a35313a3331, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3331, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=159', 0, 'ngg_pictures', '', 0),
(160, 1, 0x323031382d30372d31392030323a35313a3331, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3331, 0x323031382d30372d31382032333a35313a3331, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=160', 0, 'ngg_pictures', '', 0),
(161, 1, 0x323031382d30372d31392030323a35313a3332, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3332, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=161', 0, 'ngg_pictures', '', 0),
(162, 1, 0x323031382d30372d31392030323a35313a3332, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3332, 0x323031382d30372d31382032333a35313a3332, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=162', 0, 'ngg_pictures', '', 0),
(163, 1, 0x323031382d30372d31392030323a35313a3333, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3333, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=163', 0, 'ngg_pictures', '', 0),
(164, 1, 0x323031382d30372d31392030323a35313a3333, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3333, 0x323031382d30372d31382032333a35313a3333, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=164', 0, 'ngg_pictures', '', 0),
(165, 1, 0x323031382d30372d31392030323a35313a3334, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3334, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=165', 0, 'ngg_pictures', '', 0),
(166, 1, 0x323031382d30372d31392030323a35313a3334, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3334, 0x323031382d30372d31382032333a35313a3334, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=166', 0, 'ngg_pictures', '', 0),
(167, 1, 0x323031382d30372d31392030323a35313a3335, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3335, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=167', 0, 'ngg_pictures', '', 0),
(168, 1, 0x323031382d30372d31392030323a35313a3335, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3335, 0x323031382d30372d31382032333a35313a3335, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=168', 0, 'ngg_pictures', '', 0),
(169, 1, 0x323031382d30372d31392030323a35313a3336, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3336, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=169', 0, 'ngg_pictures', '', 0),
(170, 1, 0x323031382d30372d31392030323a35313a3336, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3336, 0x323031382d30372d31382032333a35313a3336, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=170', 0, 'ngg_pictures', '', 0),
(171, 1, 0x323031382d30372d31392030323a35313a3337, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3337, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=171', 0, 'ngg_pictures', '', 0),
(172, 1, 0x323031382d30372d31392030323a35313a3337, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3337, 0x323031382d30372d31382032333a35313a3337, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=172', 0, 'ngg_pictures', '', 0),
(173, 1, 0x323031382d30372d31392030323a35313a3338, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3338, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=173', 0, 'ngg_pictures', '', 0),
(174, 1, 0x323031382d30372d31392030323a35313a3338, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3338, 0x323031382d30372d31382032333a35313a3338, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=174', 0, 'ngg_pictures', '', 0),
(175, 1, 0x323031382d30372d31392030323a35313a3339, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3339, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=175', 0, 'ngg_pictures', '', 0),
(176, 1, 0x323031382d30372d31392030323a35313a3339, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3339, 0x323031382d30372d31382032333a35313a3339, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=176', 0, 'ngg_pictures', '', 0),
(177, 1, 0x323031382d30372d31392030323a35313a3430, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3430, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=177', 0, 'ngg_pictures', '', 0),
(178, 1, 0x323031382d30372d31392030323a35313a3430, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3430, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=178', 0, 'ngg_pictures', '', 0),
(179, 1, 0x323031382d30372d31392030323a35313a3430, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3430, 0x323031382d30372d31382032333a35313a3430, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=179', 0, 'ngg_pictures', '', 0),
(180, 1, 0x323031382d30372d31392030323a35313a3430, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3430, 0x323031382d30372d31382032333a35313a3430, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=180', 0, 'ngg_pictures', '', 0),
(181, 1, 0x323031382d30372d31392030323a35313a3431, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3431, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=181', 0, 'ngg_pictures', '', 0),
(182, 1, 0x323031382d30372d31392030323a35313a3431, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3431, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=182', 0, 'ngg_pictures', '', 0),
(183, 1, 0x323031382d30372d31392030323a35313a3431, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3431, 0x323031382d30372d31382032333a35313a3431, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=183', 0, 'ngg_pictures', '', 0),
(184, 1, 0x323031382d30372d31392030323a35313a3431, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3431, 0x323031382d30372d31382032333a35313a3431, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=184', 0, 'ngg_pictures', '', 0),
(185, 1, 0x323031382d30372d31392030323a35313a3432, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3432, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=185', 0, 'ngg_pictures', '', 0),
(186, 1, 0x323031382d30372d31392030323a35313a3432, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3432, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=186', 0, 'ngg_pictures', '', 0),
(187, 1, 0x323031382d30372d31392030323a35313a3432, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3432, 0x323031382d30372d31382032333a35313a3432, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=187', 0, 'ngg_pictures', '', 0),
(188, 1, 0x323031382d30372d31392030323a35313a3432, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3432, 0x323031382d30372d31382032333a35313a3432, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=188', 0, 'ngg_pictures', '', 0),
(189, 1, 0x323031382d30372d31392030323a35313a3433, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3433, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=189', 0, 'ngg_pictures', '', 0),
(190, 1, 0x323031382d30372d31392030323a35313a3433, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3433, 0x323031382d30372d31382032333a35313a3433, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=190', 0, 'ngg_pictures', '', 0),
(191, 1, 0x323031382d30372d31392030323a35313a3434, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3434, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=191', 0, 'ngg_pictures', '', 0),
(192, 1, 0x323031382d30372d31392030323a35313a3434, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3434, 0x323031382d30372d31382032333a35313a3434, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=192', 0, 'ngg_pictures', '', 0),
(193, 1, 0x323031382d30372d31392030323a35313a3435, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3435, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=193', 0, 'ngg_pictures', '', 0),
(194, 1, 0x323031382d30372d31392030323a35313a3435, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3435, 0x323031382d30372d31382032333a35313a3435, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=194', 0, 'ngg_pictures', '', 0),
(195, 1, 0x323031382d30372d31392030323a35313a3436, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3436, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=195', 0, 'ngg_pictures', '', 0),
(196, 1, 0x323031382d30372d31392030323a35313a3436, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3436, 0x323031382d30372d31382032333a35313a3436, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=196', 0, 'ngg_pictures', '', 0),
(197, 1, 0x323031382d30372d31392030323a35313a3438, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3438, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=197', 0, 'ngg_pictures', '', 0),
(198, 1, 0x323031382d30372d31392030323a35313a3438, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3438, 0x323031382d30372d31382032333a35313a3438, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=198', 0, 'ngg_pictures', '', 0),
(199, 1, 0x323031382d30372d31392030323a35313a3439, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3439, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=199', 0, 'ngg_pictures', '', 0),
(200, 1, 0x323031382d30372d31392030323a35313a3439, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3439, 0x323031382d30372d31382032333a35313a3439, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=200', 0, 'ngg_pictures', '', 0),
(201, 1, 0x323031382d30372d31392030323a35313a3530, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3530, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=201', 0, 'ngg_pictures', '', 0),
(202, 1, 0x323031382d30372d31392030323a35313a3530, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3530, 0x323031382d30372d31382032333a35313a3530, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=202', 0, 'ngg_pictures', '', 0),
(203, 1, 0x323031382d30372d31392030323a35313a3531, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3531, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=203', 0, 'ngg_pictures', '', 0),
(204, 1, 0x323031382d30372d31392030323a35313a3531, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3531, 0x323031382d30372d31382032333a35313a3531, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=204', 0, 'ngg_pictures', '', 0),
(205, 1, 0x323031382d30372d31392030323a35313a3532, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3532, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=205', 0, 'ngg_pictures', '', 0),
(206, 1, 0x323031382d30372d31392030323a35313a3532, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3532, 0x323031382d30372d31382032333a35313a3532, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=206', 0, 'ngg_pictures', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES 
(207, 1, 0x323031382d30372d31392030323a35313a3533, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3533, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=207', 0, 'ngg_pictures', '', 0),
(208, 1, 0x323031382d30372d31392030323a35313a3533, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3533, 0x323031382d30372d31382032333a35313a3533, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=208', 0, 'ngg_pictures', '', 0),
(209, 1, 0x323031382d30372d31392030323a35313a3534, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3534, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=209', 0, 'ngg_pictures', '', 0),
(210, 1, 0x323031382d30372d31392030323a35313a3534, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35313a3534, 0x323031382d30372d31382032333a35313a3534, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=210', 0, 'ngg_pictures', '', 0),
(211, 1, 0x323031382d30372d31392030323a35323a3239, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35323a3239, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=211', 0, 'ngg_pictures', '', 0),
(212, 1, 0x323031382d30372d31392030323a35323a3330, 0x303030302d30302d30302030303a30303a3030, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 'Untitled ngg_pictures', '', 'draft', 'closed', 'closed', '', 'mixin_nextgen_table_extras', '', '', 0x323031382d30372d31392030323a35323a3330, 0x323031382d30372d31382032333a35323a3330, 'eyJpZF9maWVsZCI6IklEIiwiX19kZWZhdWx0c19zZXQiOnRydWV9', 0, 'http://wsite/?p=212', 0, 'ngg_pictures', '', 0),
(263, 1, 0x323031382d30372d31392030333a32363a3033, 0x323031382d30372d31392030303a32363a3033, '', '33333', '', 'publish', 'open', 'closed', '', '33333-2', '', '', 0x323031382d30372d31392030333a32383a3038, 0x323031382d30372d31392030303a32383a3038, '', 0, 'http://wsite/?post_type=robo_gallery_table&#038;p=263', 0, 'robo_gallery_table', '', 0),
(264, 1, 0x323031382d30372d31392030333a32363a3234, 0x323031382d30372d31392030303a32363a3234, '', '', '', 'publish', 'closed', 'closed', '', '264-2', '', '', 0x323031382d30372d31392030333a32363a3234, 0x323031382d30372d31392030303a32363a3234, '', 0, 'http://wsite/?post_type=flagallery&#038;p=264', 0, 'flagallery', '', 0),
(265, 1, 0x323031382d30372d31392030333a34363a3131, 0x323031382d30372d31392030303a34363a3131, '', '777', '', 'publish', 'closed', 'closed', '', '777', '', '', 0x323031382d30372d31392030333a34373a3235, 0x323031382d30372d31392030303a34373a3235, '', 0, 'http://wsite/?post_type=foogallery&#038;p=265', 0, 'foogallery', '', 0),
(266, 1, 0x323031382d31312d32372030373a31383a3031, 0x323031382d31312d32372030353a31383a3031, '[bookingcalendar]', 'Галерея', '', 'inherit', 'closed', 'closed', '', '10-autosave-v1', '', '', 0x323031382d31312d32372030373a31383a3031, 0x323031382d31312d32372030353a31383a3031, '', 10, 'http://wsite/2018/07/19/10-autosave-v1/', 0, 'revision', '', 0),
(297, 1, 0x323031382d30372d31392032303a32343a3134, 0x323031382d30372d31392031373a32343a3134, '', '1', '', 'inherit', 'open', 'closed', '', '1', '', '', 0x323031382d31312d32372030353a30363a3038, 0x323031382d31312d32372030333a30363a3038, '', 310, 'http://wsite/wp-content/uploads/2018/07/1.png', 0, 'attachment', 'image/png', 0),
(298, 1, 0x323031382d30372d31392032303a32343a3134, 0x323031382d30372d31392031373a32343a3134, '', '2', '', 'inherit', 'open', 'closed', '', '2', '', '', 0x323031382d31312d32372030353a30373a3539, 0x323031382d31312d32372030333a30373a3539, '', 310, 'http://wsite/wp-content/uploads/2018/07/2.png', 0, 'attachment', 'image/png', 0),
(299, 1, 0x323031382d30372d31392032303a32343a3135, 0x323031382d30372d31392031373a32343a3135, '', '3', '', 'inherit', 'open', 'closed', '', '3-2', '', '', 0x323031382d31312d32372030353a30353a3436, 0x323031382d31312d32372030333a30353a3436, '', 310, 'http://wsite/wp-content/uploads/2018/07/3.png', 0, 'attachment', 'image/png', 0),
(300, 1, 0x323031382d30372d31392032303a32343a3135, 0x323031382d30372d31392031373a32343a3135, '', '7', '', 'inherit', 'open', 'closed', '', '7', '', '', 0x323031382d30372d31392032313a34373a3030, 0x323031382d30372d31392031383a34373a3030, '', 0, 'http://wsite/wp-content/uploads/2018/07/7.png', 0, 'attachment', 'image/png', 0),
(302, 1, 0x323031382d30372d31392032303a32343a3532, 0x323031382d30372d31392031373a32343a3532, '', '55', '', 'inherit', 'open', 'closed', '', '55', '', '', 0x323031382d31312d32372030343a33333a3530, 0x323031382d31312d32372030323a33333a3530, '', 475, 'http://wsite/wp-content/uploads/2018/07/55.png', 0, 'attachment', 'image/png', 0),
(303, 1, 0x323031382d30372d31392032303a32343a3533, 0x323031382d30372d31392031373a32343a3533, '', '66', '', 'inherit', 'open', 'closed', '', '66', '', '', 0x323031382d31312d32362032313a34333a3532, 0x323031382d31312d32362031393a34333a3532, '', 477, 'http://wsite/wp-content/uploads/2018/07/66.png', 0, 'attachment', 'image/png', 0),
(304, 1, 0x323031382d30372d31392032303a32343a3533, 0x323031382d30372d31392031373a32343a3533, '', '88', '', 'inherit', 'open', 'closed', '', '88', '', '', 0x323031382d31312d32372030353a30363a3236, 0x323031382d31312d32372030333a30363a3236, '', 310, 'http://wsite/wp-content/uploads/2018/07/88.png', 0, 'attachment', 'image/png', 0),
(305, 1, 0x323031382d30372d31392032303a32343a3534, 0x323031382d30372d31392031373a32343a3534, '', '99', '', 'inherit', 'open', 'closed', '', '99', '', '', 0x323031382d31312d32372030353a31323a3533, 0x323031382d31312d32372030333a31323a3533, '', 310, 'http://wsite/wp-content/uploads/2018/07/99.png', 0, 'attachment', 'image/png', 0),
(306, 1, 0x323031382d30372d32302031353a31363a3538, 0x323031382d30372d32302031323a31363a3538, 'Вы всегда можете задать любой интересующий вопрос и мы с радостью постараемся вам помочь.', 'Контакты', '', 'publish', 'closed', 'closed', '', 'contacts', '', '', 0x323031392d31312d30352031303a34353a3531, 0x323031392d31312d30352030383a34353a3531, '', 0, 'http://wsite/?page_id=306', 0, 'page', '', 0),
(307, 1, 0x323031382d30372d32302031353a31363a3538, 0x323031382d30372d32302031323a31363a3538, '', 'Контакты', '', 'inherit', 'closed', 'closed', '', '306-revision-v1', '', '', 0x323031382d30372d32302031353a31363a3538, 0x323031382d30372d32302031323a31363a3538, '', 306, 'http://wsite/2018/07/20/306-revision-v1/', 0, 'revision', '', 0),
(308, 1, 0x323031382d30372d32302032303a32333a3336, 0x323031382d30372d32302031373a32333a3336, '', 'admin_rights', '', 'inherit', 'open', 'closed', '', 'admin_rights', '', '', 0x323031382d30372d32302032303a32333a3438, 0x323031382d30372d32302031373a32333a3438, '', 0, 'http://wsite/wp-content/uploads/2018/07/admin_rights.gif', 0, 'attachment', 'image/gif', 0),
(309, 1, 0x323031382d30372d32322032333a33393a3431, 0x323031382d30372d32322032303a33393a3431, '', 'fallout-4-bethesda-game-5913', '', 'inherit', 'open', 'closed', '', 'fallout-4-bethesda-game-5913', '', '', 0x323031382d30372d32322032333a33393a3531, 0x323031382d30372d32322032303a33393a3531, '', 0, 'http://wsite/wp-content/uploads/2018/07/fallout-4-bethesda-game-5913.jpg', 0, 'attachment', 'image/jpeg', 0),
(310, 1, 0x323031382d30372d32332030303a33373a3531, 0x323031382d30372d32322032313a33373a3531, '<img class=\"alignnone size-medium wp-image-514\" src=\"http://stylist/wp-content/uploads/2018/11/21042370_1181331868633623_1302576685098467328_n-225x300.jpg\" alt=\"мое фото\" width=\"225\" height=\"300\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше!\r\n\r\n<img class=\"alignnone size-medium wp-image-515\" src=\"http://stylist/wp-content/uploads/2018/11/20398178_445919659140861_3772480860834496512_n-224x300.jpg\" alt=\"мое фото\" width=\"224\" height=\"300\" />\r\n\r\nСледующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.\r\n\r\nОбучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'publish', 'closed', 'closed', '', 'about', '', '', 0x323031392d31312d31302031323a32363a3439, 0x323031392d31312d31302031303a32363a3439, '', 0, 'http://wsite/?page_id=310', 0, 'page', '', 0),
(311, 1, 0x323031382d30372d32332030303a33373a3531, 0x323031382d30372d32322032313a33373a3531, '', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d30372d32332030303a33373a3531, 0x323031382d30372d32322032313a33373a3531, '', 310, 'http://wsite/2018/07/23/310-revision-v1/', 0, 'revision', '', 0),
(313, 1, 0x323031382d30372d32332031323a33383a3536, 0x323031382d30372d32332030393a33383a3536, '', '1457023723_tt', '', 'inherit', 'open', 'closed', '', '1457023723_tt', '', '', 0x323031382d30372d32332031323a33393a3033, 0x323031382d30372d32332030393a33393a3033, '', 0, 'http://wsite/wp-content/uploads/2018/07/1457023723_tt.jpg', 0, 'attachment', 'image/jpeg', 0),
(315, 1, 0x323031382d30372d32332031373a32313a3030, 0x323031382d30372d32332031343a32313a3030, '', 'Запись', '', 'publish', 'closed', 'closed', '', 'entry', '', '', 0x323031382d31312d31372031303a34393a3135, 0x323031382d31312d31372030383a34393a3135, '', 0, 'http://wsite/?page_id=315', 0, 'page', '', 0),
(316, 1, 0x323031382d30372d32332031373a32313a3030, 0x323031382d30372d32332031343a32313a3030, '', 'Запись', '', 'inherit', 'closed', 'closed', '', '315-revision-v1', '', '', 0x323031382d30372d32332031373a32313a3030, 0x323031382d30372d32332031343a32313a3030, '', 315, 'http://wsite/2018/07/23/315-revision-v1/', 0, 'revision', '', 0),
(319, 1, 0x323031382d30372d32332031373a33393a3437, 0x323031382d30372d32332031343a33393a3437, '', '59ba7c1511604', '', 'inherit', 'open', 'closed', '', '59ba7c1511604', '', '', 0x323031382d30372d32332031373a33393a3533, 0x323031382d30372d32332031343a33393a3533, '', 0, 'http://wsite/wp-content/uploads/2018/07/59ba7c1511604.jpeg', 0, 'attachment', 'image/jpeg', 0),
(322, 1, 0x323031382d30372d32332031373a34313a3038, 0x323031382d30372d32332031343a34313a3038, '', '1511030296_13', '', 'inherit', 'open', 'closed', '', '1511030296_13', '', '', 0x323031382d30372d32332031373a34313a3133, 0x323031382d30372d32332031343a34313a3133, '', 0, 'http://wsite/wp-content/uploads/2018/07/1511030296_13.jpg', 0, 'attachment', 'image/jpeg', 0),
(325, 1, 0x323031382d30372d32332031373a35353a3431, 0x323031382d30372d32332031343a35353a3431, '', 'ya47', '', 'inherit', 'open', 'closed', '', 'ya47', '', '', 0x323031382d30372d32332031373a35353a3438, 0x323031382d30372d32332031343a35353a3438, '', 0, 'http://wsite/wp-content/uploads/2018/07/ya47.png', 0, 'attachment', 'image/png', 0),
(330, 1, 0x323031382d30372d32332031373a35363a3530, 0x323031382d30372d32332031343a35363a3530, '', '787', '', 'inherit', 'open', 'closed', '', '787', '', '', 0x323031382d30372d32332031373a35363a3534, 0x323031382d30372d32332031343a35363a3534, '', 0, 'http://wsite/wp-content/uploads/2018/07/787.jpg', 0, 'attachment', 'image/jpeg', 0),
(333, 1, 0x323031382d30372d32332031373a35373a3237, 0x323031382d30372d32332031343a35373a3237, '', 'orig', '', 'inherit', 'open', 'closed', '', 'orig', '', '', 0x323031382d30372d32332031373a35373a3333, 0x323031382d30372d32332031343a35373a3333, '', 0, 'http://wsite/wp-content/uploads/2018/07/orig.jpg', 0, 'attachment', 'image/jpeg', 0),
(336, 1, 0x323031382d30372d32332031373a35393a3131, 0x323031382d30372d32332031343a35393a3131, '', 'eFYlajY0PQ4', '', 'inherit', 'open', 'closed', '', 'efylajy0pq4', '', '', 0x323031382d30372d32332031373a35393a3136, 0x323031382d30372d32332031343a35393a3136, '', 0, 'http://wsite/wp-content/uploads/2018/07/eFYlajY0PQ4.jpg', 0, 'attachment', 'image/jpeg', 0),
(339, 1, 0x323031382d30372d32332031373a35393a3438, 0x323031382d30372d32332031343a35393a3438, '', 'orig (1)', '', 'inherit', 'open', 'closed', '', 'orig-1', '', '', 0x323031382d31312d32362032313a33333a3337, 0x323031382d31312d32362031393a33333a3337, '', 477, 'http://wsite/wp-content/uploads/2018/07/orig-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(342, 1, 0x323031382d30372d32332031383a30313a3032, 0x323031382d30372d32332031353a30313a3032, '', 'manicure-trendy-3', '', 'inherit', 'open', 'closed', '', 'manicure-trendy-3', '', '', 0x323031382d30372d32332031383a30313a3130, 0x323031382d30372d32332031353a30313a3130, '', 0, 'http://wsite/wp-content/uploads/2018/07/manicure-trendy-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(345, 1, 0x323031382d30372d32332031383a30323a3435, 0x323031382d30372d32332031353a30323a3435, '', 'manikur_leto_80', '', 'inherit', 'open', 'closed', '', 'manikur_leto_80', '', '', 0x323031382d30372d32332031383a30353a3537, 0x323031382d30372d32332031353a30353a3537, '', 0, 'http://wsite/wp-content/uploads/2018/07/manikur_leto_80.jpg', 0, 'attachment', 'image/jpeg', 0),
(347, 1, 0x323031382d30372d32332031383a30363a3233, 0x323031382d30372d32332031353a30363a3233, '', 'manicure-trendy-3', '', 'inherit', 'open', 'closed', '', 'manicure-trendy-3-2', '', '', 0x323031382d30372d32332031383a30363a3237, 0x323031382d30372d32332031353a30363a3237, '', 0, 'http://wsite/wp-content/uploads/2018/07/manicure-trendy-3-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(350, 1, 0x323031382d30372d32332031383a30363a3439, 0x323031382d30372d32332031353a30363a3439, '', 'images', '', 'inherit', 'open', 'closed', '', 'images', '', '', 0x323031382d30372d32332031383a30363a3534, 0x323031382d30372d32332031353a30363a3534, '', 0, 'http://wsite/wp-content/uploads/2018/07/images.jpg', 0, 'attachment', 'image/jpeg', 0),
(374, 1, 0x323031382d30372d32362030313a35383a3036, 0x323031382d30372d32352032323a35383a3036, '', 's-l300', '', 'inherit', 'open', 'closed', '', 's-l300', '', '', 0x323031382d31312d32372030393a32313a3035, 0x323031382d31312d32372030373a32313a3035, '', 0, 'http://wsite/wp-content/uploads/2018/07/s-l300.jpg', 0, 'attachment', 'image/jpeg', 0),
(380, 1, 0x323031382d30372d32362030313a35393a3535, 0x323031382d30372d32352032323a35393a3535, '', 'permanentnyj-makiyazh-nizhnego-veka', '', 'inherit', 'open', 'closed', '', 'permanentnyj-makiyazh-nizhnego-veka', '', '', 0x323031382d31312d32372030343a33313a3530, 0x323031382d31312d32372030323a33313a3530, '', 475, 'http://wsite/wp-content/uploads/2018/07/permanentnyj-makiyazh-nizhnego-veka.jpg', 0, 'attachment', 'image/jpeg', 0),
(382, 1, 0x323031382d30372d32362030323a30303a3435, 0x323031382d30372d32352032333a30303a3435, '', '4545', '', 'inherit', 'open', 'closed', '', '4545', '', '', 0x323031382d30372d32362030323a30303a3530, 0x323031382d30372d32352032333a30303a3530, '', 0, 'http://wsite/wp-content/uploads/2018/07/4545.jpg', 0, 'attachment', 'image/jpeg', 0),
(384, 1, 0x323031382d30372d32362030323a30313a3238, 0x323031382d30372d32352032333a30313a3238, '', '300px-Negr', '', 'inherit', 'open', 'closed', '', '300px-negr', '', '', 0x323031382d31312d32372030393a32363a3039, 0x323031382d31312d32372030373a32363a3039, '', 0, 'http://wsite/wp-content/uploads/2018/07/300px-Negr.jpg', 0, 'attachment', 'image/jpeg', 0),
(387, 1, 0x323031382d30372d32362030323a30373a3436, 0x323031382d30372d32352032333a30373a3436, '', 'images', '', 'inherit', 'open', 'closed', '', 'images-2', '', '', 0x323031382d30372d32362030323a30373a3531, 0x323031382d30372d32352032333a30373a3531, '', 0, 'http://wsite/wp-content/uploads/2018/07/images-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(390, 1, 0x323031382d30372d32362030323a30383a3232, 0x323031382d30372d32352032333a30383a3232, '', 'images (1)', '', 'inherit', 'open', 'closed', '', 'images-1', '', '', 0x323031382d31312d32372030343a33323a3034, 0x323031382d31312d32372030323a33323a3034, '', 475, 'http://wsite/wp-content/uploads/2018/07/images-1-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(392, 1, 0x323031382d30372d32362030323a31303a3030, 0x323031382d30372d32352032333a31303a3030, '', '1chas_posle', '', 'inherit', 'open', 'closed', '', '1chas_posle', '', '', 0x323031382d30372d32362030323a31303a3035, 0x323031382d30372d32352032333a31303a3035, '', 0, 'http://wsite/wp-content/uploads/2018/07/1chas_posle.jpg', 0, 'attachment', 'image/jpeg', 0),
(395, 1, 0x323031382d30372d32392030313a34353a3435, 0x323031382d30372d32382032323a34353a3435, '[ecwd id=\"395\" type=\"full\" page_items=\"5\" event_search=\"yes\" display=\"full\" displays=\"full,list,week,day\" filters=\"\"]', 'Calendar', '', 'publish', 'closed', 'closed', '', 'calendar', '', '', 0x323031382d30372d32392030313a34353a3435, 0x323031382d30372d32382032323a34353a3435, '', 0, 'http://wsite/2018/07/29/calendar/', 0, 'ecwd_calendar', '', 0),
(398, 1, 0x323031382d30382d30312032303a35303a3237, 0x323031382d30382d30312031373a35303a3237, '<label> Ваше имя </label>\r\n[text your-name]\r\n\r\n<label> Телефон<span class=\"red\">*</span> </label>\r\n    [tel* phone]\r\n\r\n<label> e-mail<span class=\"red\">*</span> </label>\r\n[email* your-email]\r\n\r\n<br>\r\n\r\n<label> Введите желаемую дату и время </label>\r\n[datetime datetime-889 date-format:mm/dd/yy time-format:HH:mm]\r\n\r\n[acceptance acceptance-439] <span class=\"normal\"> Согласен с <a href=\"[main_url]/private-policy/\">политикой конфиденциальности</a></span>\r\n\r\n<br>\r\n\r\n[recaptcha]\r\n\r\n[submit \"Перезвоните мне\"]\n1\nПисьмо с сайта stylist.site\n[your-name] <wordpress@stylist.site>\ngck92462@eoopy.com\nСообщение от клиента с stylist.site\r\n\r\nИмя: [your-name] \r\nEmail: <[your-email]>\r\nТелефон: [phone]\r\n\r\nЖелаемая дата записи: [datetime-889]\nReply-To:<eva34456@eveav.com>\n\n\n\n\nstylist \"[your-subject]\"\nstylist <wordpress@q90241xj.beget.tech>\n[your-email]\nСообщение:\r\n[your-message]\r\n\r\n-- \r\nЭто сообщение отправлено с сайта stylist (http://q90241xj.beget.tech)\nReply-To: capitan.flin@yandex.ru\n\n\n\nСообщение успешно отправлено\nПри отправке сообщения произошла ошибка. Пожалуйста, попробуйте ещё раз позже.\nПроверьте еще раз поля формы\nПодтвердите, что вы не робот\nВы должны принять условия и положения перед отправкой вашего сообщения.\nПоле обязательно для заполнения.\nПоле слишком длинное.\nПоле слишком короткое.\nПри загрузке файла произошла неизвестная ошибка.\nВам не разрешено загружать файлы этого типа.\nФайл слишком большой.\nПри загрузке файла произошла ошибка.\nФормат числа некорректен.\nЧисло меньше минимально допустимого.\nЧисло больше максимально допустимого.\nНеверный ответ на проверочный вопрос.\nКод введен неверно.\nНеверно введён электронный адрес.\nВведён некорректный URL адрес.\nВведён некорректный телефонный номер.\nInvalid date and time supplied.\nInvalid date supplied.\nInvalid time supplied.', 'Форма записи', '', 'publish', 'closed', 'closed', '', 'yyy', '', '', 0x323032302d30372d30372031343a35363a3433, 0x323032302d30372d30372031323a35363a3433, '', 0, 'http://q90241xj.beget.tech/?post_type=wpcf7_contact_form&#038;p=398', 0, 'wpcf7_contact_form', '', 0),
(399, 1, 0x323031382d30382d30382031313a35333a3437, 0x323031382d30382d30382030393a35333a3437, 'Сбрить брови и покрасить волосы в красный цвет', 'Света-брови', '', 'publish', 'closed', 'closed', '', 'manikyur', '', '', 0x323031382d30382d30382031313a35383a3231, 0x323031382d30382d30382030393a35383a3231, '', 0, 'http://wsite/?post_type=ecwd_event&#038;p=399', 0, 'ecwd_event', '', 0),
(401, 1, 0x323031382d30382d30382031323a30323a3339, 0x323031382d30382d30382031303a30323a3339, '', 'Занята', '', 'publish', 'closed', 'closed', '', 'zanyata', '', '', 0x323031382d30382d30382031323a30323a3339, 0x323031382d30382d30382031303a30323a3339, '', 0, 'http://wsite/?post_type=ecwd_event&#038;p=401', 0, 'ecwd_event', '', 0),
(402, 1, 0x323031382d30382d30382031323a30333a3338, 0x323031382d30382d30382031303a30333a3338, 'Побрить налысо Иру', 'Ира-стрижка', '', 'publish', 'closed', 'closed', '', 'ira-strizhka', '', '', 0x323031382d30382d30382031323a30333a3338, 0x323031382d30382d30382031303a30333a3338, '', 0, 'http://wsite/?post_type=ecwd_event&#038;p=402', 0, 'ecwd_event', '', 0),
(404, 1, 0x323031382d30382d30382031323a30363a3336, 0x323031382d30382d30382031303a30363a3336, '', 'Записи нет', '', 'publish', 'closed', 'closed', '', 'zapisi-net', '', '', 0x323031382d30382d30382031323a30363a3336, 0x323031382d30382d30382031303a30363a3336, '', 0, 'http://wsite/?post_type=ecwd_event&#038;p=404', 0, 'ecwd_event', '', 0),
(406, 1, 0x323031382d30382d30382031323a30373a3339, 0x323031382d30382d30382031303a30373a3339, '', 'Боря. Стрижка', '', 'publish', 'closed', 'closed', '', 'borya-strizhka', '', '', 0x323031382d30382d30382031323a30373a3339, 0x323031382d30382d30382031303a30373a3339, '', 0, 'http://wsite/?post_type=ecwd_event&#038;p=406', 0, 'ecwd_event', '', 0),
(407, 1, 0x323031382d30382d30382031323a30383a3137, 0x323031382d30382d30382031303a30383a3137, '', 'Маникюр', '', 'publish', 'closed', 'closed', '', 'manikyur-2', '', '', 0x323031382d30382d30382031323a30383a3137, 0x323031382d30382d30382031303a30383a3137, '', 0, 'http://wsite/?post_type=ecwd_event&#038;p=407', 0, 'ecwd_event', '', 0),
(424, 1, 0x323031382d30382d31312030323a34303a3032, 0x323031382d30382d31312030303a34303a3032, '', 'v-rossii-mozhet-poyavitsya-eshche-odin-vyhodnoy-133513', '', 'inherit', 'open', 'closed', '', 'v-rossii-mozhet-poyavitsya-eshche-odin-vyhodnoy-133513', '', '', 0x323031382d30382d31312030323a34303a3131, 0x323031382d30382d31312030303a34303a3131, '', 0, 'http://wsite/wp-content/uploads/2018/08/v-rossii-mozhet-poyavitsya-eshche-odin-vyhodnoy-133513.jpg', 0, 'attachment', 'image/jpeg', 0),
(428, 1, 0x323031382d30382d31312032303a35353a3033, 0x323031382d30382d31312031383a35353a3033, '<label> Ваше имя\r\n    [text your-name akismet:author] </label>\r\n\r\n<label> Ваш e-mail <span class=\"red\">*</span>\r\n    [email* your-email] </label>\r\n\r\n<label class=\"label_text\"> Сообщение\r\n    [textarea your-message] </label>\r\n\r\n[acceptance acceptance-59] <span class=\"normal\"> Согласен с <a href=\"[main_url]/private-policy/\">политикой конфиденциальности</a></span>\r\n\r\n<br>\r\n\r\n[recaptcha]\r\n\r\n[submit \"Отправить\"]\n1\nПисьмо с сайта kaza4enkova.ru\n[your-name] <wordpress@stylist.host>\ngck92462@eoopy.com\nСообщение с сайта kaza4enkova.ru\r\n\r\nИмя: [your-name]\r\nemail: <[your-email]>\r\n\r\n[your-message]\nReply-To: irene17_07@mail.ru\n\n\n\n\nstylist \"[your-subject]\"\nstylist <wordpress@wsite>\n[your-email]\nСообщение:\r\n[your-message]\r\n\r\n-- \r\nЭто сообщение отправлено с сайта stylist (http://wsite)\nReply-To: capitan.flin@yandex.ru\n\n\n\nСообщение успешно отправлено.\nПри отправке сообщения произошла ошибка. Пожалуйста, попробуйте ещё раз позже.\nПроверьте еще раз поля формы.\nПроверьте капчу.\nВы должны принять условия и положения перед отправкой вашего сообщения.\nПоле обязательно для заполнения.\nПоле слишком длинное.\nПоле слишком короткое.\nПри загрузке файла произошла неизвестная ошибка.\nВам не разрешено загружать файлы этого типа.\nФайл слишком большой.\nПри загрузке файла произошла ошибка.\nФормат числа некорректен.\nЧисло меньше минимально допустимого.\nЧисло больше максимально допустимого.\nНеверный ответ на проверочный вопрос.\nКод введен неверно.\nНеверно введён электронный адрес.\nВведён некорректный URL адрес.\nВведён некорректный телефонный номер.\nInvalid date and time supplied.\nInvalid date supplied.\nInvalid time supplied.', 'Форма модального окна', '', 'publish', 'closed', 'closed', '', 'forma-modalnogo-okna', '', '', 0x323032302d30372d30372031343a35363a3234, 0x323032302d30372d30372031323a35363a3234, '', 0, 'http://wsite/?post_type=wpcf7_contact_form&#038;p=428', 0, 'wpcf7_contact_form', '', 0),
(429, 1, 0x323031382d30382d31312032313a33333a3537, 0x323031382d30382d31312031393a33333a3537, '<h2 align=\"justify\">1. Общие положения</h2>\r\n1.1 <span lang=\"ru-RU\">Администрация сайта </span><a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a> (далее по тексту – Оператор) ставит соблюдение прав и свобод граждан одним из важнейших условий осуществления своей деятельности.\r\n\r\n1.2 Политика Оператора в отношении обработки персональных данных (далее по тексту — Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>. Персональные данные обрабатывается в соответствии с ФЗ «О персональных данных» № 152-ФЗ.\r\n<h2 align=\"justify\">2. Основные понятия, используемые в Политике:</h2>\r\n2.1 Веб-сайт — совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>;\r\n\r\n2.2 Пользователь – любой посетитель веб-сайта <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>;\r\n\r\n2.3 Персональные данные – любая информация, относящаяся к Пользователю веб-сайта <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>;\r\n\r\n2.4 Обработка персональных данных — любое действие с персональными данными, совершаемые с использованием ЭВМ, равно как и без их использования;\r\n\r\n2.5 Обезличивание персональных данных – действия, результатом которых является невозможность без использования дополнительной информации определить принадлежность персональных данных конкретному Пользователю или лицу;\r\n\r\n2.6 Распространение персональных данных – любые действия, результатом которых является раскрытие персональных данных неопределенному кругу лиц;\r\n\r\n2.7 Предоставление персональных данных – любые действия, результатом которых является раскрытие персональных данных определенному кругу лиц;\r\n\r\n2.8 Уничтожение персональных данных – любые действия, результатом которых является безвозвратное уничтожение персональных на ЭВМ или любых других носителях.\r\n<h2 class=\"western\" align=\"justify\">3. Оператор может обрабатывать следующие персональные данные:</h2>\r\n<p align=\"justify\">3.1 Адрес электронной почты Пользователя.</p>\r\n<p align=\"justify\">3.2 Номер телефона Пользователя.</p>\r\n<p align=\"justify\">3.3. <span lang=\"ru-RU\">Имя</span> <span lang=\"ru-RU\">Пользователя.</span></p>\r\n<p align=\"justify\">3.4. Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов «cookie») с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">4. Цели обработки персональных данных</h2>\r\n4.1 Цель обработки адреса электронной почты, номера телефона, имени Пользователя — уточнение деталей заказа.\r\n\r\n4.2 Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания.\r\n<h2 class=\"western\" align=\"justify\">5. Правовые основания обработки персональных данных</h2>\r\n<p align=\"justify\">5.1 Оператор обрабатывает персональные данные Пользователя только в случае их отправки Пользователем через формы, расположенные на сайте <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>; отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.</p>\r\n<p align=\"justify\">5.2 Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов «cookie» и использование технологии JavaScript).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">6. Порядок сбора, хранения, передачи и других видов обработки персональных данных</h2>\r\n<p align=\"justify\">6.1 Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.</p>\r\n<p align=\"justify\">6.2 Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства.</p>\r\n<p align=\"justify\">6.3. В случае выявления неточностей в персональных данных, Пользователь может актуализировать их, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a>, с пометкой «Актуализация персональных данных»</p>\r\n<p align=\"justify\">6.3 Срок обработки персональных данных является неограниченным. Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a> с пометкой «Отзыв согласия на обработку персональных данных».</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">7. Заключительные положения</h2>\r\n<p align=\"justify\">7.1. Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a>.</p>\r\n<p align=\"justify\">7.2. В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. В случае существенных изменений Пользователю может быть выслана информация на указанный им электронный адрес.</p>', 'Политика конфиденциальности', '', 'publish', 'closed', 'closed', '', 'private-policy', '', '', 0x323031382d31312d33302031343a31333a3337, 0x323031382d31312d33302031323a31333a3337, '', 0, 'http://wsite/?page_id=429', 0, 'page', '', 0),
(430, 1, 0x323031382d30382d31312032313a33333a3537, 0x323031382d30382d31312031393a33333a3537, '<h2 align=\"justify\">1. Общие положения</h2>\r\n1.1 <span lang=\"ru-RU\">Администрация сайта </span><span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span> (далее по тексту – Оператор) ставит соблюдение прав и свобод граждан одним из важнейших условий осуществления своей деятельности.\r\n\r\n1.2 Политика Оператора в отношении обработки персональных данных (далее по тексту — Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта <span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span>. Персональные данные обрабатывается в соответствии с ФЗ «О персональных данных» № 152-ФЗ.\r\n<h2 align=\"justify\">2. Основные понятия, используемые в Политике:</h2>\r\n2.1 Веб-сайт — совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу<span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span>;\r\n\r\n2.2 Пользователь – любой посетитель веб-сайта <span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span>;\r\n\r\n2.3 Персональные данные – любая информация, относящаяся к Пользователю веб-сайта<span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span>;\r\n\r\n2.4 Обработка персональных данных — любое действие с персональными данными, совершаемые с использованием ЭВМ, равно как и без их использования;\r\n\r\n2.5 Обезличивание персональных данных – действия, результатом которых является невозможность без использования дополнительной информации определить принадлежность персональных данных конкретному Пользователю или лицу;\r\n\r\n2.6 Распространение персональных данных – любые действия, результатом которых является раскрытие персональных данных неопределенному кругу лиц;\r\n\r\n2.7 Предоставление персональных данных – любые действия, результатом которых является раскрытие персональных данных определенному кругу лиц;\r\n\r\n2.8 Уничтожение персональных данных – любые действия, результатом которых является безвозвратное уничтожение персональных на ЭВМ или любых других носителях.\r\n<h2 class=\"western\" align=\"justify\">3. Оператор может обрабатывать следующие персональные данные:</h2>\r\n<p align=\"justify\">3.1 Адрес электронной почты Пользователя</p>\r\n<p align=\"justify\">3.2 Номер телефона Пользователя</p>\r\n<p align=\"justify\">3.3. <span lang=\"ru-RU\">Имя</span> <span lang=\"ru-RU\">Пользователя</span></p>\r\n<p align=\"justify\">3.4. Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов «cookie») с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">4. Цели обработки персональных данных</h2>\r\n4.1 Цель обработки адреса электронной почты, номера телефона, имени Пользователя — уточнение деталей заказа.\r\n\r\n4.2 Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания.\r\n<h2 class=\"western\" align=\"justify\">5. Правовые основания обработки персональных данных</h2>\r\n<p align=\"justify\">5.1 Оператор обрабатывает персональные данные Пользователя только в случае их отправки Пользователем через формы, расположенные на сайте <a href=\"http://www.konigcards.ru/\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span></a><b> </b>Отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.</p>\r\n<p align=\"justify\">5.2 Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов «cookie» и использование технологии JavaScript).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">6. Порядок сбора, хранения, передачи и других видов обработки персональных данных</h2>\r\n<p align=\"justify\">6.1 Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.</p>\r\n<p align=\"justify\">6.2 Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства.</p>\r\n<p align=\"justify\">6.3. В случае выявления неточностей в персональных данных, Пользователь может актуализировать их, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <span lang=\"en-US\">aslepow@gmail.com</span>, с пометкой «Актуализация персональных данных»</p>\r\n<p align=\"justify\">6.3 Срок обработки персональных данных является неограниченным. Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:aslepow@gmail.com\"><span lang=\"en-US\">aslepow@gmail.com</span></a><b> </b>с пометкой «Отзыв согласия на обработку персональных данных».</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">7. Заключительные положения</h2>\r\n<p align=\"justify\">7.1. Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты <span lang=\"en-US\">aslepow@gmail.com</span>.</p>\r\n<p align=\"justify\">7.2. В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. В случае существенных изменений Пользователю может быть выслана информация на указанный им электронный адрес.</p>', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '429-revision-v1', '', '', 0x323031382d30382d31312032313a33333a3537, 0x323031382d30382d31312031393a33333a3537, '', 429, 'http://wsite/429-revision-v1/', 0, 'revision', '', 0),
(431, 1, 0x323031382d31312d33302031343a31333a3132, 0x323031382d31312d33302031323a31333a3132, '<h2 align=\"justify\">1. Общие положения</h2>\n1.1 <span lang=\"ru-RU\">Администрация сайта </span><a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a> (далее по тексту – Оператор) ставит соблюдение прав и свобод граждан одним из важнейших условий осуществления своей деятельности.\n\n1.2 Политика Оператора в отношении обработки персональных данных (далее по тексту — Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>. Персональные данные обрабатывается в соответствии с ФЗ «О персональных данных» № 152-ФЗ.\n<h2 align=\"justify\">2. Основные понятия, используемые в Политике:</h2>\n2.1 Веб-сайт — совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>;\n\n2.2 Пользователь – любой посетитель веб-сайта <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>;\n\n2.3 Персональные данные – любая информация, относящаяся к Пользователю веб-сайта <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>;\n\n2.4 Обработка персональных данных — любое действие с персональными данными, совершаемые с использованием ЭВМ, равно как и без их использования;\n\n2.5 Обезличивание персональных данных – действия, результатом которых является невозможность без использования дополнительной информации определить принадлежность персональных данных конкретному Пользователю или лицу;\n\n2.6 Распространение персональных данных – любые действия, результатом которых является раскрытие персональных данных неопределенному кругу лиц;\n\n2.7 Предоставление персональных данных – любые действия, результатом которых является раскрытие персональных данных определенному кругу лиц;\n\n2.8 Уничтожение персональных данных – любые действия, результатом которых является безвозвратное уничтожение персональных на ЭВМ или любых других носителях.\n<h2 class=\"western\" align=\"justify\">3. Оператор может обрабатывать следующие персональные данные:</h2>\n<p align=\"justify\">3.1 Адрес электронной почты Пользователя.</p>\n<p align=\"justify\">3.2 Номер телефона Пользователя.</p>\n<p align=\"justify\">3.3. <span lang=\"ru-RU\">Имя</span> <span lang=\"ru-RU\">Пользователя.</span></p>\n<p align=\"justify\">3.4. Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов «cookie») с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).</p>\n\n<h2 class=\"western\" align=\"justify\">4. Цели обработки персональных данных</h2>\n4.1 Цель обработки адреса электронной почты, номера телефона, имени Пользователя — уточнение деталей заказа.\n\n4.2 Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания.\n<h2 class=\"western\" align=\"justify\">5. Правовые основания обработки персональных данных</h2>\n<p align=\"justify\">5.1 Оператор обрабатывает персональные данные Пользователя только в случае их отправки Пользователем через формы, расположенные на сайте <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>; отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.</p>\n<p align=\"justify\">5.2 Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов «cookie» и использование технологии JavaScript).</p>\n\n<h2 class=\"western\" align=\"justify\">6. Порядок сбора, хранения, передачи и других видов обработки персональных данных</h2>\n<p align=\"justify\">6.1 Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.</p>\n<p align=\"justify\">6.2 Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства.</p>\n<p align=\"justify\">6.3. В случае выявления неточностей в персональных данных, Пользователь может актуализировать их, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a>, с пометкой «Актуализация персональных данных»</p>\n<p align=\"justify\">6.3 Срок обработки персональных данных является неограниченным. Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a> с пометкой «Отзыв согласия на обработку персональных данных».</p>\n\n<h2 class=\"western\" align=\"justify\">7. Заключительные положения</h2>\n<p align=\"justify\">7.1. Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a>.</p>\n<p align=\"justify\">7.2. В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. В случае существенных изменений Пользователю может быть выслана информация на указанный им электронный адрес.</p>', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '429-autosave-v1', '', '', 0x323031382d31312d33302031343a31333a3132, 0x323031382d31312d33302031323a31333a3132, '', 429, 'http://wsite/429-autosave-v1/', 0, 'revision', '', 0),
(432, 1, 0x323031382d30382d31322030313a30343a3130, 0x323031382d30382d31312032333a30343a3130, '<h2 align=\"justify\">1. Общие положения</h2>\r\n1.1 <span lang=\"ru-RU\">Администрация сайта </span><span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span> (далее по тексту – Оператор) ставит соблюдение прав и свобод граждан одним из важнейших условий осуществления своей деятельности.\r\n\r\n1.2 Политика Оператора в отношении обработки персональных данных (далее по тексту — Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта <span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span>. Персональные данные обрабатывается в соответствии с ФЗ «О персональных данных» № 152-ФЗ.\r\n<h2 align=\"justify\">2. Основные понятия, используемые в Политике:</h2>\r\n2.1 Веб-сайт — совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу <span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span>;\r\n\r\n2.2 Пользователь – любой посетитель веб-сайта <span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span>;\r\n\r\n2.3 Персональные данные – любая информация, относящаяся к Пользователю веб-сайта <span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span>;\r\n\r\n2.4 Обработка персональных данных — любое действие с персональными данными, совершаемые с использованием ЭВМ, равно как и без их использования;\r\n\r\n2.5 Обезличивание персональных данных – действия, результатом которых является невозможность без использования дополнительной информации определить принадлежность персональных данных конкретному Пользователю или лицу;\r\n\r\n2.6 Распространение персональных данных – любые действия, результатом которых является раскрытие персональных данных неопределенному кругу лиц;\r\n\r\n2.7 Предоставление персональных данных – любые действия, результатом которых является раскрытие персональных данных определенному кругу лиц;\r\n\r\n2.8 Уничтожение персональных данных – любые действия, результатом которых является безвозвратное уничтожение персональных на ЭВМ или любых других носителях.\r\n<h2 class=\"western\" align=\"justify\">3. Оператор может обрабатывать следующие персональные данные:</h2>\r\n<p align=\"justify\">3.1 Адрес электронной почты Пользователя</p>\r\n<p align=\"justify\">3.2 Номер телефона Пользователя</p>\r\n<p align=\"justify\">3.3. <span lang=\"ru-RU\">Имя</span> <span lang=\"ru-RU\">Пользователя</span></p>\r\n<p align=\"justify\">3.4. Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов «cookie») с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">4. Цели обработки персональных данных</h2>\r\n4.1 Цель обработки адреса электронной почты, номера телефона, имени Пользователя — уточнение деталей заказа.\r\n\r\n4.2 Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания.\r\n<h2 class=\"western\" align=\"justify\">5. Правовые основания обработки персональных данных</h2>\r\n<p align=\"justify\">5.1 Оператор обрабатывает персональные данные Пользователя только в случае их отправки Пользователем через формы, расположенные на сайте <a href=\"http://www.konigcards.ru/\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span></a><b> </b>Отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.</p>\r\n<p align=\"justify\">5.2 Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов «cookie» и использование технологии JavaScript).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">6. Порядок сбора, хранения, передачи и других видов обработки персональных данных</h2>\r\n<p align=\"justify\">6.1 Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.</p>\r\n<p align=\"justify\">6.2 Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства.</p>\r\n<p align=\"justify\">6.3. В случае выявления неточностей в персональных данных, Пользователь может актуализировать их, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <span lang=\"en-US\">aslepow@gmail.com </span>, с пометкой «Актуализация персональных данных»</p>\r\n<p align=\"justify\">6.3 Срок обработки персональных данных является неограниченным. Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:aslepow@gmail.com\"><span lang=\"en-US\">aslepow@gmail.com</span></a><b> </b>с пометкой «Отзыв согласия на обработку персональных данных».</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">7. Заключительные положения</h2>\r\n<p align=\"justify\">7.1. Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты <span lang=\"en-US\">aslepow@gmail.com </span>.</p>\r\n<p align=\"justify\">7.2. В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. В случае существенных изменений Пользователю может быть выслана информация на указанный им электронный адрес.</p>', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '429-revision-v1', '', '', 0x323031382d30382d31322030313a30343a3130, 0x323031382d30382d31312032333a30343a3130, '', 429, 'http://wsite/429-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES 
(433, 1, 0x323031382d30382d31352031333a33393a3439, 0x323031382d30382d31352031313a33393a3439, '[[\"\\u041c\\u0430\\u043a\\u0438\\u044f\\u0436\",\"\\u0423\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438\",\"\\u0411\\u0440\\u043e\\u0432\\u0438\"],[\"\\u0434\\u043d\\u0435\\u0432\\u043d\\u043e\\u0439 - 1200 \\u20bd\",\"\\u043b\\u043e\\u043a\\u043e\\u043d\\u044b - 700 \\u20bd\",\"\\u043a\\u043e\\u0440\\u0440\\u0435\\u043a\\u0446\\u0438\\u044f - 200 \\u20bd\"],[\"\\u0432\\u0435\\u0447\\u0435\\u0440\\u043d\\u0438\\u0439- 1400 \\u20bd\",\"\\u043e\\u0431\\u044a\\u0435\\u043c\\u043d\\u044b\\u0435 \\u043b\\u043e\\u043a\\u043e\\u043d\\u044b - 1000 \\u20bd\",\"\\u043e\\u043a\\u0440\\u0430\\u0448\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043a\\u0440\\u0430\\u0441\\u043a\\u043e\\u0439 - 150 \\u20bd\"],[\"\\u0441\\u0432\\u0430\\u0434\\u0435\\u0431\\u043d\\u044b\\u0439 - 1500 \\u20bd\",\"\\u043f\\u0443\\u0447\\u043a\\u0438 - 800 \\u20bd\",\"\\u043e\\u043a\\u0440\\u0430\\u0448\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0445\\u043d\\u043e\\u0439 - 300 \\u20bd\"],[\"\",\"\\u0442\\u0435\\u043a\\u0441\\u0442\\u0443\\u0440\\u043d\\u044b\\u0435 \\u0443\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438 - 1200 \\u20bd\",\"\"]]', 'Прайс', '', 'publish', 'closed', 'closed', '', 'prajs', '', '', 0x323031382d31312d32362030393a32303a3131, 0x323031382d31312d32362030373a32303a3131, '', 0, 'http://wsite/?post_type=tablepress_table&#038;p=433', 0, 'tablepress_table', 'application/json', 0),
(434, 1, 0x323031382d30382d31352031333a34313a3032, 0x323031382d30382d31352031313a34313a3032, '[[\"\\u041c\\u0430\\u043a\\u0438\\u044f\\u0436\",\"\\u043e\",\"\",\"\",\"\\u043d\"],[\"\",\"\",\"\",\"\",\"\\u043d\"],[\"\\u043d\",\"\\u043e\",\"\",\"\\u043d\",\"\"],[\"\",\"\",\"\",\"\",\"\"],[\"\\u043e\",\"\\u043d\",\"\\u043e\",\"\",\"\\u043e\"]]', 'Прайс', '', 'inherit', 'closed', 'closed', '', '433-revision-v1', '', '', 0x323031382d30382d31352031333a34313a3032, 0x323031382d30382d31352031313a34313a3032, '', 433, 'http://wsite/433-revision-v1/', 0, 'revision', '', 0),
(435, 1, 0x323031382d30382d31352031343a30303a3338, 0x323031382d30382d31352031323a30303a3338, '[[\"\\u041c\\u0430\\u043a\\u0438\\u044f\\u0436\",\"\\u0414\\u043d\\u0435\\u0432\\u043d\\u043e\\u0439\",\"\\u0412\\u0435\\u0447\\u0435\\u0440\\u043d\\u0438\\u0439\",\"\\u0421\\u0432\\u0430\\u0434\\u0435\\u0431\\u043d\\u044b\\u0439\"],[\"\",\"1200 \\u20bd\",\"1400 \\u20bd\",\"1500 \\u20bd\"],[\"\\u0423\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438\",\"\\u041b\\u043e\\u043a\\u043e\\u043d\\u044b\",\"\\u041f\\u0443\\u0447\\u043a\\u0438\",\"\\u0422\\u0435\\u043a\\u0441\\u0442\\u0443\\u0440\\u043d\\u044b\\u0435 \\u0443\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438\"],[\"\",\"700 \\u20bd\",\"800 \\u20bd\",\"1200 \\u20bd\"],[\"\\u0411\\u0440\\u043e\\u0432\\u0438\",\"\\u043e\",\"\",\"\\u043e\"]]', 'Прайс', '', 'inherit', 'closed', 'closed', '', '433-revision-v1', '', '', 0x323031382d30382d31352031343a30303a3338, 0x323031382d30382d31352031323a30303a3338, '', 433, 'http://wsite/433-revision-v1/', 0, 'revision', '', 0),
(436, 1, 0x323031382d30382d31352031343a30333a3435, 0x323031382d30382d31352031323a30333a3435, '[[\"\\u041c\\u0430\\u043a\\u0438\\u044f\\u0436\",\"\\u0423\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438\",\"\\u0411\\u0440\\u043e\\u0432\\u0438\"],[\"\\u0434\\u043d\\u0435\\u0432\\u043d\\u043e\\u0439 - 1200 \\u20bd\",\"\",\"\"],[\"\\u0432\\u0435\\u0447\\u0435\\u0440\\u043d\\u0438\\u0439 - 1400 \\u20bd\",\"\",\"\"],[\"\\u0441\\u0432\\u0430\\u0434\\u0435\\u0431\\u043d\\u044b\\u0439 - 1500 \\u20bd\",\"\",\"\"],[\"\",\"\",\"\"]]', 'Прайс', '', 'inherit', 'closed', 'closed', '', '433-revision-v1', '', '', 0x323031382d30382d31352031343a30333a3435, 0x323031382d30382d31352031323a30333a3435, '', 433, 'http://wsite/433-revision-v1/', 0, 'revision', '', 0),
(437, 1, 0x323031382d30382d31352031343a30393a3137, 0x323031382d30382d31352031323a30393a3137, '[[\"\\u041c\\u0430\\u043a\\u0438\\u044f\\u0436\",\"\\u0423\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438\",\"\\u0411\\u0440\\u043e\\u0432\\u0438\"],[\"\\u0434\\u043d\\u0435\\u0432\\u043d\\u043e\\u0439 - 1200 \\u20bd\",\"\\u043b\\u043e\\u043a\\u043e\\u043d\\u044b - 700 \\u20bd\",\"\\u043a\\u043e\\u0440\\u0440\\u0435\\u043a\\u0446\\u0438\\u044f - 200 \\u20bd\"],[\"\\u0432\\u0435\\u0447\\u0435\\u0440\\u043d\\u0438\\u0439 - 1400 \\u20bd\",\"\\u043e\\u0431\\u044a\\u0435\\u043c\\u043d\\u044b\\u0435 \\u043b\\u043e\\u043a\\u043e\\u043d\\u044b - 1000 \\u20bd\",\"\\u043e\\u043a\\u0440\\u0430\\u0448\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043a\\u0440\\u0430\\u0441\\u043a\\u043e\\u0439 150 \\u20bd\"],[\"\\u0441\\u0432\\u0430\\u0434\\u0435\\u0431\\u043d\\u044b\\u0439 - 1500 \\u20bd\",\"\\u043f\\u0443\\u0447\\u043a\\u0438 - 800 \\u20bd\",\"\\u043e\\u043a\\u0440\\u0430\\u0448\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0445\\u043d\\u043e\\u0439 300 \\u20bd\"],[\"\",\"\\u0442\\u0435\\u043a\\u0441\\u0442\\u0443\\u0440\\u043d\\u044b\\u0435 \\u0443\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438 - 1200 \\u20bd\",\"\"]]', 'Прайс', '', 'inherit', 'closed', 'closed', '', '433-revision-v1', '', '', 0x323031382d30382d31352031343a30393a3137, 0x323031382d30382d31352031323a30393a3137, '', 433, 'http://wsite/433-revision-v1/', 0, 'revision', '', 0),
(438, 1, 0x323031382d30382d31352031343a31333a3336, 0x323031382d30382d31352031323a31333a3336, '[[\"\\u041c\\u0430\\u043a\\u0438\\u044f\\u0436\",\"\\u0423\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438\",\"\\u0411\\u0440\\u043e\\u0432\\u0438\"],[\"\\u0434\\u043d\\u0435\\u0432\\u043d\\u043e\\u0439 - 1200 \\u20bd\",\"\\u043b\\u043e\\u043a\\u043e\\u043d\\u044b - 700 \\u20bd\",\"\\u043a\\u043e\\u0440\\u0440\\u0435\\u043a\\u0446\\u0438\\u044f - 200 \\u20bd\"],[\"\\u0432\\u0435\\u0447\\u0435\\u0440\\u043d\\u0438\\u0439 - 1400 \\u20bd\",\"\\u043e\\u0431\\u044a\\u0435\\u043c\\u043d\\u044b\\u0435 \\u043b\\u043e\\u043a\\u043e\\u043d\\u044b - 1000 \\u20bd\",\"\\u043e\\u043a\\u0440\\u0430\\u0448\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043a\\u0440\\u0430\\u0441\\u043a\\u043e\\u0439 - 150 \\u20bd\"],[\"\\u0441\\u0432\\u0430\\u0434\\u0435\\u0431\\u043d\\u044b\\u0439 - 1500 \\u20bd\",\"\\u043f\\u0443\\u0447\\u043a\\u0438 - 800 \\u20bd\",\"\\u043e\\u043a\\u0440\\u0430\\u0448\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0445\\u043d\\u043e\\u0439 - 300 \\u20bd\"],[\"\",\"\\u0442\\u0435\\u043a\\u0441\\u0442\\u0443\\u0440\\u043d\\u044b\\u0435 \\u0443\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438 - 1200 \\u20bd\",\"\"]]', 'Прайс', '', 'inherit', 'closed', 'closed', '', '433-revision-v1', '', '', 0x323031382d30382d31352031343a31333a3336, 0x323031382d30382d31352031323a31333a3336, '', 433, 'http://wsite/433-revision-v1/', 0, 'revision', '', 0),
(441, 1, 0x323031382d31302d31352031363a30313a3038, 0x323031382d31302d31352031343a30313a3038, '<h2 align=\"justify\">1. Общие положения</h2>\r\n1.1 <span lang=\"ru-RU\">Администрация сайта </span><a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">www.kaza4enkova.ru</span></a> (далее по тексту – Оператор) ставит соблюдение прав и свобод граждан одним из важнейших условий осуществления своей деятельности.\r\n\r\n1.2 Политика Оператора в отношении обработки персональных данных (далее по тексту — Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта <span lang=\"en-US\">www.</span><span lang=\"en-US\">www.kaza4enkova.ru</span>. Персональные данные обрабатывается в соответствии с ФЗ «О персональных данных» № 152-ФЗ.\r\n<h2 align=\"justify\">2. Основные понятия, используемые в Политике:</h2>\r\n2.1 Веб-сайт — совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу <span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span>;\r\n\r\n2.2 Пользователь – любой посетитель веб-сайта <span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span>;\r\n\r\n2.3 Персональные данные – любая информация, относящаяся к Пользователю веб-сайта <span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span>;\r\n\r\n2.4 Обработка персональных данных — любое действие с персональными данными, совершаемые с использованием ЭВМ, равно как и без их использования;\r\n\r\n2.5 Обезличивание персональных данных – действия, результатом которых является невозможность без использования дополнительной информации определить принадлежность персональных данных конкретному Пользователю или лицу;\r\n\r\n2.6 Распространение персональных данных – любые действия, результатом которых является раскрытие персональных данных неопределенному кругу лиц;\r\n\r\n2.7 Предоставление персональных данных – любые действия, результатом которых является раскрытие персональных данных определенному кругу лиц;\r\n\r\n2.8 Уничтожение персональных данных – любые действия, результатом которых является безвозвратное уничтожение персональных на ЭВМ или любых других носителях.\r\n<h2 class=\"western\" align=\"justify\">3. Оператор может обрабатывать следующие персональные данные:</h2>\r\n<p align=\"justify\">3.1 Адрес электронной почты Пользователя</p>\r\n<p align=\"justify\">3.2 Номер телефона Пользователя</p>\r\n<p align=\"justify\">3.3. <span lang=\"ru-RU\">Имя</span> <span lang=\"ru-RU\">Пользователя</span></p>\r\n<p align=\"justify\">3.4. Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов «cookie») с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">4. Цели обработки персональных данных</h2>\r\n4.1 Цель обработки адреса электронной почты, номера телефона, имени Пользователя — уточнение деталей заказа.\r\n\r\n4.2 Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания.\r\n<h2 class=\"western\" align=\"justify\">5. Правовые основания обработки персональных данных</h2>\r\n<p align=\"justify\">5.1 Оператор обрабатывает персональные данные Пользователя только в случае их отправки Пользователем через формы, расположенные на сайте <a href=\"http://www.konigcards.ru/\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span></a><b> </b>Отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.</p>\r\n<p align=\"justify\">5.2 Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов «cookie» и использование технологии JavaScript).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">6. Порядок сбора, хранения, передачи и других видов обработки персональных данных</h2>\r\n<p align=\"justify\">6.1 Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.</p>\r\n<p align=\"justify\">6.2 Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства.</p>\r\n<p align=\"justify\">6.3. В случае выявления неточностей в персональных данных, Пользователь может актуализировать их, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <span lang=\"en-US\">lagrigorio@gmail.com </span>, с пометкой «Актуализация персональных данных»</p>\r\n<p align=\"justify\">6.3 Срок обработки персональных данных является неограниченным. Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:lagrigorio@gmail.com\"><span lang=\"en-US\">lagrigorio@gmail.com</span></a><b> </b>с пометкой «Отзыв согласия на обработку персональных данных».</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">7. Заключительные положения</h2>\r\n<p align=\"justify\">7.1. Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты <span lang=\"en-US\">lagrigorio@gmail.com </span>.</p>\r\n<p align=\"justify\">7.2. В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. В случае существенных изменений Пользователю может быть выслана информация на указанный им электронный адрес.</p>', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '429-revision-v1', '', '', 0x323031382d31302d31352031363a30313a3038, 0x323031382d31302d31352031343a30313a3038, '', 429, 'http://wsite/429-revision-v1/', 0, 'revision', '', 0),
(442, 1, 0x323031382d31302d31352031363a30313a3338, 0x323031382d31302d31352031343a30313a3338, '<h2 align=\"justify\">1. Общие положения</h2>\r\n1.1 <span lang=\"ru-RU\">Администрация сайта </span><a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a> (далее по тексту – Оператор) ставит соблюдение прав и свобод граждан одним из важнейших условий осуществления своей деятельности.\r\n\r\n1.2 Политика Оператора в отношении обработки персональных данных (далее по тексту — Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта <span lang=\"en-US\">www.</span><span lang=\"en-US\">www.kaza4enkova.ru</span>. Персональные данные обрабатывается в соответствии с ФЗ «О персональных данных» № 152-ФЗ.\r\n<h2 align=\"justify\">2. Основные понятия, используемые в Политике:</h2>\r\n2.1 Веб-сайт — совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу <span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span>;\r\n\r\n2.2 Пользователь – любой посетитель веб-сайта <span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span>;\r\n\r\n2.3 Персональные данные – любая информация, относящаяся к Пользователю веб-сайта <span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span>;\r\n\r\n2.4 Обработка персональных данных — любое действие с персональными данными, совершаемые с использованием ЭВМ, равно как и без их использования;\r\n\r\n2.5 Обезличивание персональных данных – действия, результатом которых является невозможность без использования дополнительной информации определить принадлежность персональных данных конкретному Пользователю или лицу;\r\n\r\n2.6 Распространение персональных данных – любые действия, результатом которых является раскрытие персональных данных неопределенному кругу лиц;\r\n\r\n2.7 Предоставление персональных данных – любые действия, результатом которых является раскрытие персональных данных определенному кругу лиц;\r\n\r\n2.8 Уничтожение персональных данных – любые действия, результатом которых является безвозвратное уничтожение персональных на ЭВМ или любых других носителях.\r\n<h2 class=\"western\" align=\"justify\">3. Оператор может обрабатывать следующие персональные данные:</h2>\r\n<p align=\"justify\">3.1 Адрес электронной почты Пользователя</p>\r\n<p align=\"justify\">3.2 Номер телефона Пользователя</p>\r\n<p align=\"justify\">3.3. <span lang=\"ru-RU\">Имя</span> <span lang=\"ru-RU\">Пользователя</span></p>\r\n<p align=\"justify\">3.4. Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов «cookie») с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">4. Цели обработки персональных данных</h2>\r\n4.1 Цель обработки адреса электронной почты, номера телефона, имени Пользователя — уточнение деталей заказа.\r\n\r\n4.2 Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания.\r\n<h2 class=\"western\" align=\"justify\">5. Правовые основания обработки персональных данных</h2>\r\n<p align=\"justify\">5.1 Оператор обрабатывает персональные данные Пользователя только в случае их отправки Пользователем через формы, расположенные на сайте <a href=\"http://www.konigcards.ru/\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">konigcards.ru</span></a><b> </b>Отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.</p>\r\n<p align=\"justify\">5.2 Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов «cookie» и использование технологии JavaScript).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">6. Порядок сбора, хранения, передачи и других видов обработки персональных данных</h2>\r\n<p align=\"justify\">6.1 Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.</p>\r\n<p align=\"justify\">6.2 Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства.</p>\r\n<p align=\"justify\">6.3. В случае выявления неточностей в персональных данных, Пользователь может актуализировать их, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <span lang=\"en-US\">lagrigorio@gmail.com </span>, с пометкой «Актуализация персональных данных»</p>\r\n<p align=\"justify\">6.3 Срок обработки персональных данных является неограниченным. Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:lagrigorio@gmail.com\"><span lang=\"en-US\">lagrigorio@gmail.com</span></a><b> </b>с пометкой «Отзыв согласия на обработку персональных данных».</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">7. Заключительные положения</h2>\r\n<p align=\"justify\">7.1. Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты <span lang=\"en-US\">lagrigorio@gmail.com </span>.</p>\r\n<p align=\"justify\">7.2. В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. В случае существенных изменений Пользователю может быть выслана информация на указанный им электронный адрес.</p>', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '429-revision-v1', '', '', 0x323031382d31302d31352031363a30313a3338, 0x323031382d31302d31352031343a30313a3338, '', 429, 'http://wsite/429-revision-v1/', 0, 'revision', '', 0),
(443, 1, 0x323031382d31302d31352031363a30373a3436, 0x323031382d31302d31352031343a30373a3436, '<h2 align=\"justify\">1. Общие положения</h2>\r\n1.1 <span lang=\"ru-RU\">Администрация сайта </span><a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a> (далее по тексту – Оператор) ставит соблюдение прав и свобод граждан одним из важнейших условий осуществления своей деятельности.\r\n\r\n1.2 Политика Оператора в отношении обработки персональных данных (далее по тексту — Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>. Персональные данные обрабатывается в соответствии с ФЗ «О персональных данных» № 152-ФЗ.\r\n<h2 align=\"justify\">2. Основные понятия, используемые в Политике:</h2>\r\n2.1 Веб-сайт — совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>;\r\n\r\n2.2 Пользователь – любой посетитель веб-сайта <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>;\r\n\r\n2.3 Персональные данные – любая информация, относящаяся к Пользователю веб-сайта <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>;\r\n\r\n2.4 Обработка персональных данных — любое действие с персональными данными, совершаемые с использованием ЭВМ, равно как и без их использования;\r\n\r\n2.5 Обезличивание персональных данных – действия, результатом которых является невозможность без использования дополнительной информации определить принадлежность персональных данных конкретному Пользователю или лицу;\r\n\r\n2.6 Распространение персональных данных – любые действия, результатом которых является раскрытие персональных данных неопределенному кругу лиц;\r\n\r\n2.7 Предоставление персональных данных – любые действия, результатом которых является раскрытие персональных данных определенному кругу лиц;\r\n\r\n2.8 Уничтожение персональных данных – любые действия, результатом которых является безвозвратное уничтожение персональных на ЭВМ или любых других носителях.\r\n<h2 class=\"western\" align=\"justify\">3. Оператор может обрабатывать следующие персональные данные:</h2>\r\n<p align=\"justify\">3.1 Адрес электронной почты Пользователя</p>\r\n<p align=\"justify\">3.2 Номер телефона Пользователя</p>\r\n<p align=\"justify\">3.3. <span lang=\"ru-RU\">Имя</span> <span lang=\"ru-RU\">Пользователя</span></p>\r\n<p align=\"justify\">3.4. Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов «cookie») с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">4. Цели обработки персональных данных</h2>\r\n4.1 Цель обработки адреса электронной почты, номера телефона, имени Пользователя — уточнение деталей заказа.\r\n\r\n4.2 Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания.\r\n<h2 class=\"western\" align=\"justify\">5. Правовые основания обработки персональных данных</h2>\r\n<p align=\"justify\">5.1 Оператор обрабатывает персональные данные Пользователя только в случае их отправки Пользователем через формы, расположенные на сайте <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>; отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.</p>\r\n<p align=\"justify\">5.2 Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов «cookie» и использование технологии JavaScript).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">6. Порядок сбора, хранения, передачи и других видов обработки персональных данных</h2>\r\n<p align=\"justify\">6.1 Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.</p>\r\n<p align=\"justify\">6.2 Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства.</p>\r\n<p align=\"justify\">6.3. В случае выявления неточностей в персональных данных, Пользователь может актуализировать их, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:lagrigorio@gmail.com\"><span lang=\"en-US\">lagrigorio@gmail.com</a>, с пометкой «Актуализация персональных данных»</p>\r\n<p align=\"justify\">6.3 Срок обработки персональных данных является неограниченным. Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:lagrigorio@gmail.com\"><span lang=\"en-US\">lagrigorio@gmail.com</span></a> с пометкой «Отзыв согласия на обработку персональных данных».</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">7. Заключительные положения</h2>\r\n<p align=\"justify\">7.1. Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты a href=\"mailto:lagrigorio@gmail.com\"><span lang=\"en-US\">lagrigorio@gmail.com</a>.</p>\r\n<p align=\"justify\">7.2. В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. В случае существенных изменений Пользователю может быть выслана информация на указанный им электронный адрес.</p>', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '429-revision-v1', '', '', 0x323031382d31302d31352031363a30373a3436, 0x323031382d31302d31352031343a30373a3436, '', 429, 'http://wsite/429-revision-v1/', 0, 'revision', '', 0),
(444, 1, 0x323031382d31302d31352031363a31313a3334, 0x323031382d31302d31352031343a31313a3334, '<h2 align=\"justify\">1. Общие положения</h2>\r\n1.1 <span lang=\"ru-RU\">Администрация сайта </span><a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a> (далее по тексту – Оператор) ставит соблюдение прав и свобод граждан одним из важнейших условий осуществления своей деятельности.\r\n\r\n1.2 Политика Оператора в отношении обработки персональных данных (далее по тексту — Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>. Персональные данные обрабатывается в соответствии с ФЗ «О персональных данных» № 152-ФЗ.\r\n<h2 align=\"justify\">2. Основные понятия, используемые в Политике:</h2>\r\n2.1 Веб-сайт — совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>;\r\n\r\n2.2 Пользователь – любой посетитель веб-сайта <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>;\r\n\r\n2.3 Персональные данные – любая информация, относящаяся к Пользователю веб-сайта <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>;\r\n\r\n2.4 Обработка персональных данных — любое действие с персональными данными, совершаемые с использованием ЭВМ, равно как и без их использования;\r\n\r\n2.5 Обезличивание персональных данных – действия, результатом которых является невозможность без использования дополнительной информации определить принадлежность персональных данных конкретному Пользователю или лицу;\r\n\r\n2.6 Распространение персональных данных – любые действия, результатом которых является раскрытие персональных данных неопределенному кругу лиц;\r\n\r\n2.7 Предоставление персональных данных – любые действия, результатом которых является раскрытие персональных данных определенному кругу лиц;\r\n\r\n2.8 Уничтожение персональных данных – любые действия, результатом которых является безвозвратное уничтожение персональных на ЭВМ или любых других носителях.\r\n<h2 class=\"western\" align=\"justify\">3. Оператор может обрабатывать следующие персональные данные:</h2>\r\n<p align=\"justify\">3.1 Адрес электронной почты Пользователя.</p>\r\n<p align=\"justify\">3.2 Номер телефона Пользователя.</p>\r\n<p align=\"justify\">3.3. <span lang=\"ru-RU\">Имя</span> <span lang=\"ru-RU\">Пользователя.</span></p>\r\n<p align=\"justify\">3.4. Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов «cookie») с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">4. Цели обработки персональных данных</h2>\r\n4.1 Цель обработки адреса электронной почты, номера телефона, имени Пользователя — уточнение деталей заказа.\r\n\r\n4.2 Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания.\r\n<h2 class=\"western\" align=\"justify\">5. Правовые основания обработки персональных данных</h2>\r\n<p align=\"justify\">5.1 Оператор обрабатывает персональные данные Пользователя только в случае их отправки Пользователем через формы, расположенные на сайте <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>; отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.</p>\r\n<p align=\"justify\">5.2 Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов «cookie» и использование технологии JavaScript).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">6. Порядок сбора, хранения, передачи и других видов обработки персональных данных</h2>\r\n<p align=\"justify\">6.1 Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.</p>\r\n<p align=\"justify\">6.2 Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства.</p>\r\n<p align=\"justify\">6.3. В случае выявления неточностей в персональных данных, Пользователь может актуализировать их, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:lagrigorio@gmail.com\"><span lang=\"en-US\">lagrigorio@gmail.com</a>, с пометкой «Актуализация персональных данных»</p>\r\n<p align=\"justify\">6.3 Срок обработки персональных данных является неограниченным. Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:lagrigorio@gmail.com\"><span lang=\"en-US\">lagrigorio@gmail.com</span></a> с пометкой «Отзыв согласия на обработку персональных данных».</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">7. Заключительные положения</h2>\r\n<p align=\"justify\">7.1. Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты <a href=\"mailto:lagrigorio@gmail.com\"><span lang=\"en-US\">lagrigorio@gmail.com</a>.</p>\r\n<p align=\"justify\">7.2. В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. В случае существенных изменений Пользователю может быть выслана информация на указанный им электронный адрес.</p>', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '429-revision-v1', '', '', 0x323031382d31302d31352031363a31313a3334, 0x323031382d31302d31352031343a31313a3334, '', 429, 'http://wsite/429-revision-v1/', 0, 'revision', '', 0),
(450, 1, 0x323031382d31312d31362031393a30363a3339, 0x323031382d31312d31362031373a30363a3339, '<h1>Наш салон красоты преобразит вас!</h1>\r\nНаш салон - это штат профессиональных и квалифицированных специалистов, которые готовы решить любые ваши проблемы, связанные с кожей тела или лица. Мы гарантируем превосходный результат, который достигается благодаря колоссальному опыту наших мастеров. Наша цель - это найти наиболее оптимальное решение вашей проблемы, а не навязать свои услуги. Индивидуальный подход к каждому клиенту.', 'Главная', '', 'publish', 'closed', 'closed', '', 'glavnaya', '', '', 0x323031392d31312d30362031393a35313a3234, 0x323031392d31312d30362031373a35313a3234, '', 0, 'http://stylist/?page_id=450', 0, 'page', '', 0),
(451, 1, 0x323031382d31312d31362031393a30363a3339, 0x323031382d31312d31362031373a30363a3339, '', 'Главная', '', 'inherit', 'closed', 'closed', '', '450-revision-v1', '', '', 0x323031382d31312d31362031393a30363a3339, 0x323031382d31312d31362031373a30363a3339, '', 450, 'http://stylist/450-revision-v1/', 0, 'revision', '', 0),
(452, 1, 0x323031382d31312d31362031393a31343a3334, 0x323031382d31312d31362031373a31343a3334, '<h1>Наш салон красоты - №1 в Калининграде!</h1>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore pariatur, nesciunt iste, enim aperiam assumenda. Illo quaerat numquam accusantium ipsam laudantium commodi, tempore expedita, sed a amet provident ab nulla exercitationem, fuga suscipit iusto illum, soluta fugit repellat ducimus. Omnis recusandae labore reprehenderit minima ducimus placeat, maiores similique vero dolorum autem nesciunt alias consequuntur, voluptate dolor aspernatur fuga voluptas tempore, velit quis. Dicta ab est voluptates tempore facilis non velit repellendus, autem omnis. Officiis dignissimos debitis enim at, inventore! Deleniti modi atque voluptatibus. Eius voluptates ipsam ad alias illo iusto, excepturi possimus voluptatem quasi, ab numquam, inventore rem consequuntur molestiae?', 'Главная', '', 'inherit', 'closed', 'closed', '', '450-revision-v1', '', '', 0x323031382d31312d31362031393a31343a3334, 0x323031382d31312d31362031373a31343a3334, '', 450, 'http://stylist/450-revision-v1/', 0, 'revision', '', 0),
(453, 1, 0x323031382d31312d31362032313a30313a3439, 0x323031382d31312d31362031393a30313a3439, '<h1>Наш салон красоты - №1 в мире!</h1>\r\nСупер салон!!!!!!!!!', 'Главная', '', 'inherit', 'closed', 'closed', '', '450-revision-v1', '', '', 0x323031382d31312d31362032313a30313a3439, 0x323031382d31312d31362031393a30313a3439, '', 450, 'http://stylist/450-revision-v1/', 0, 'revision', '', 0),
(454, 1, 0x323031382d31312d31362032313a30343a3533, 0x323031382d31312d31362031393a30343a3533, '<h1>Наш салон красоты - №1 в мире!</h1>\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', 'Главная', '', 'inherit', 'closed', 'closed', '', '450-revision-v1', '', '', 0x323031382d31312d31362032313a30343a3533, 0x323031382d31312d31362031393a30343a3533, '', 450, 'http://stylist/450-revision-v1/', 0, 'revision', '', 0),
(455, 1, 0x323031382d31312d31362032313a30353a3039, 0x323031382d31312d31362031393a30353a3039, '<h1>Наш салон красоты - №1 в Калининграде!</h1>\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', 'Главная', '', 'inherit', 'closed', 'closed', '', '450-revision-v1', '', '', 0x323031382d31312d31362032313a30353a3039, 0x323031382d31312d31362031393a30353a3039, '', 450, 'http://stylist/450-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES 
(456, 1, 0x323031382d31312d31362032313a31323a3535, 0x323031382d31312d31362031393a31323a3535, '<h1>Наш салон красоты - №1 в Мире!</h1>\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum!!!!!!!!!!!!!!!!!!!!', 'Главная', '', 'inherit', 'closed', 'closed', '', '450-revision-v1', '', '', 0x323031382d31312d31362032313a31323a3535, 0x323031382d31312d31362031393a31323a3535, '', 450, 'http://stylist/450-revision-v1/', 0, 'revision', '', 0),
(458, 1, 0x323031382d31312d31372030323a32323a3139, 0x323031382d31312d31372030303a32323a3139, '<h1>Наш салон красоты - №1 в Калининграде!</h1>\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', 'Главная', '', 'inherit', 'closed', 'closed', '', '450-revision-v1', '', '', 0x323031382d31312d31372030323a32323a3139, 0x323031382d31312d31372030303a32323a3139, '', 450, 'http://stylist/450-revision-v1/', 0, 'revision', '', 0),
(468, 1, 0x323031382d31312d32362030383a32383a3534, 0x323031382d31312d32362030363a32383a3534, '', 'Прайс', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', 0x323031382d31312d32362030383a32383a3534, 0x323031382d31312d32362030363a32383a3534, '', 5, 'http://stylist/5-revision-v1/', 0, 'revision', '', 0),
(469, 1, 0x323031382d31312d32362030383a35333a3332, 0x323031382d31312d32362030363a35333a3332, '[table id=1]', 'Прайс', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', 0x323031382d31312d32362030383a35333a3332, 0x323031382d31312d32362030363a35333a3332, '', 5, 'http://stylist/5-revision-v1/', 0, 'revision', '', 0),
(470, 1, 0x323031382d31312d32362030383a35343a3437, 0x323031382d31312d32362030363a35343a3437, '[table id=1]\r\n\r\n&nbsp;\r\n\r\n!!!!!!!!!', 'Прайс', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', 0x323031382d31312d32362030383a35343a3437, 0x323031382d31312d32362030363a35343a3437, '', 5, 'http://stylist/5-revision-v1/', 0, 'revision', '', 0),
(471, 1, 0x323031382d31312d32362030383a35363a3430, 0x323031382d31312d32362030363a35363a3430, '[table id=1]', 'Прайс', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', 0x323031382d31312d32362030383a35363a3430, 0x323031382d31312d32362030363a35363a3430, '', 5, 'http://stylist/5-revision-v1/', 0, 'revision', '', 0),
(472, 1, 0x323031382d31312d32362030393a31353a3435, 0x323031382d31312d32362030373a31353a3435, '[[\"\\u041c\\u0430\\u043a\\u0438\\u044f\\u0436\",\"\\u0423\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438\",\"\\u0411\\u0440\\u043e\\u0432\\u0438\"],[\"\\u0434\\u043d\\u0435\\u0432\\u043d\\u043e\\u0439 - 1200 \\u20bd\",\"\\u043b\\u043e\\u043a\\u043e\\u043d\\u044b - 700 \\u20bd\",\"\\u043a\\u043e\\u0440\\u0440\\u0435\\u043a\\u0446\\u0438\\u044f - 200 \\u20bd\"],[\"\\u0432\\u0435\\u0447\\u0435\\u0440\\u043d\\u0438\\u0439- 1400 \\u20bd\",\"\\u043e\\u0431\\u044a\\u0435\\u043c\\u043d\\u044b\\u0435 \\u043b\\u043e\\u043a\\u043e\\u043d\\u044b - 1000 \\u20bd\",\"\\u043e\\u043a\\u0440\\u0430\\u0448\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043a\\u0440\\u0430\\u0441\\u043a\\u043e\\u0439 - 150 \\u20bd\"],[\"\\u0441\\u0432\\u0430\\u0434\\u0435\\u0431\\u043d\\u044b\\u0439 - 1500 \\u20bd\",\"\\u043f\\u0443\\u0447\\u043a\\u0438 - 800 \\u20bd\",\"\\u043e\\u043a\\u0440\\u0430\\u0448\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0445\\u043d\\u043e\\u0439 - 300 \\u20bd\"],[\"\",\"\\u0442\\u0435\\u043a\\u0441\\u0442\\u0443\\u0440\\u043d\\u044b\\u0435 \\u0443\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438 - 1200 \\u20bd\",\"\"]]', 'Прайс', '', 'inherit', 'closed', 'closed', '', '433-revision-v1', '', '', 0x323031382d31312d32362030393a31353a3435, 0x323031382d31312d32362030373a31353a3435, '', 433, 'http://stylist/433-revision-v1/', 0, 'revision', '', 0),
(473, 1, 0x323031382d31312d32362030393a31393a3532, 0x323031382d31312d32362030373a31393a3532, '[[\"\\u041c\\u0430\\u043a\\u0438\\u044f\\u0436\",\"\\u0423\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438\",\"\\u0411\\u0440\\u043e\\u0432\\u0438\"],[\"\\u0434\\u043d\\u0435\\u0432\\u043d\\u043e\\u0439 - 1200 \\u20bd\",\"\\u043b\\u043e\\u043a\\u043e\\u043d\\u044b - 700 \\u20bd\",\"\\u043a\\u043e\\u0440\\u0440\\u0435\\u043a\\u0446\\u0438\\u044f - 300 \\u20bd\"],[\"\\u0432\\u0435\\u0447\\u0435\\u0440\\u043d\\u0438\\u0439- 1400 \\u20bd\",\"\\u043e\\u0431\\u044a\\u0435\\u043c\\u043d\\u044b\\u0435 \\u043b\\u043e\\u043a\\u043e\\u043d\\u044b - 1000 \\u20bd\",\"\\u043e\\u043a\\u0440\\u0430\\u0448\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043a\\u0440\\u0430\\u0441\\u043a\\u043e\\u0439 - 150 \\u20bd\"],[\"\\u0441\\u0432\\u0430\\u0434\\u0435\\u0431\\u043d\\u044b\\u0439 - 1500 \\u20bd\",\"\\u043f\\u0443\\u0447\\u043a\\u0438 - 800 \\u20bd\",\"\\u043e\\u043a\\u0440\\u0430\\u0448\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0445\\u043d\\u043e\\u0439 - 300 \\u20bd\"],[\"\",\"\\u0442\\u0435\\u043a\\u0441\\u0442\\u0443\\u0440\\u043d\\u044b\\u0435 \\u0443\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438 - 1200 \\u20bd\",\"\"]]', 'Прайс', '', 'inherit', 'closed', 'closed', '', '433-revision-v1', '', '', 0x323031382d31312d32362030393a31393a3532, 0x323031382d31312d32362030373a31393a3532, '', 433, 'http://stylist/433-revision-v1/', 0, 'revision', '', 0),
(474, 1, 0x323031382d31312d32362030393a32303a3131, 0x323031382d31312d32362030373a32303a3131, '[[\"\\u041c\\u0430\\u043a\\u0438\\u044f\\u0436\",\"\\u0423\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438\",\"\\u0411\\u0440\\u043e\\u0432\\u0438\"],[\"\\u0434\\u043d\\u0435\\u0432\\u043d\\u043e\\u0439 - 1200 \\u20bd\",\"\\u043b\\u043e\\u043a\\u043e\\u043d\\u044b - 700 \\u20bd\",\"\\u043a\\u043e\\u0440\\u0440\\u0435\\u043a\\u0446\\u0438\\u044f - 200 \\u20bd\"],[\"\\u0432\\u0435\\u0447\\u0435\\u0440\\u043d\\u0438\\u0439- 1400 \\u20bd\",\"\\u043e\\u0431\\u044a\\u0435\\u043c\\u043d\\u044b\\u0435 \\u043b\\u043e\\u043a\\u043e\\u043d\\u044b - 1000 \\u20bd\",\"\\u043e\\u043a\\u0440\\u0430\\u0448\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043a\\u0440\\u0430\\u0441\\u043a\\u043e\\u0439 - 150 \\u20bd\"],[\"\\u0441\\u0432\\u0430\\u0434\\u0435\\u0431\\u043d\\u044b\\u0439 - 1500 \\u20bd\",\"\\u043f\\u0443\\u0447\\u043a\\u0438 - 800 \\u20bd\",\"\\u043e\\u043a\\u0440\\u0430\\u0448\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0445\\u043d\\u043e\\u0439 - 300 \\u20bd\"],[\"\",\"\\u0442\\u0435\\u043a\\u0441\\u0442\\u0443\\u0440\\u043d\\u044b\\u0435 \\u0443\\u043a\\u043b\\u0430\\u0434\\u043a\\u0438 - 1200 \\u20bd\",\"\"]]', 'Прайс', '', 'inherit', 'closed', 'closed', '', '433-revision-v1', '', '', 0x323031382d31312d32362030393a32303a3131, 0x323031382d31312d32362030373a32303a3131, '', 433, 'http://stylist/433-revision-v1/', 0, 'revision', '', 0),
(475, 1, 0x323031382d31312d32362030393a32333a3030, 0x323031382d31312d32362030373a32333a3030, 'Стилист – это специалист в сфере создания стиля и образа человека. Делает он это с помощью всех доступных методов: прическа, макияж, одежда и т.д. Профессия стилиста заключается в создании имиджа. Несмотря на, казалось бы, современную тенденцию сотворения образов, данная работа своими корнями уходит далеко в прошлое. Профессия появилась вместе с модой. Самыми первыми ее законодателями стали царицы в Древней Греции. Они меняли свои прически, а все подданные подражали их стилю. Яркими представителями стилистической разницы стали племена индейцев, в которых вождь выделялся среди соотечественников особым головным убором. Стиль и стилистика долгое время развивались как нечто само собой разумеющееся. Они были частью моды и считались неотъемлемыми ее составляющими. Отделяться они начали с появлением искусства кино. Публичным людям нужно было всегда выглядеть хорошо, но не у всех это получалось, так и появились персональные ассистенты, а впоследствии и стилисты. В современное время понятие стилиста уже имеет четкие очертания. Это персональный помощник, который создает имидж клиента от кончиков волос до кончиков пальцев.', 'Стилист', '', 'publish', 'closed', 'closed', '', 'stylist', '', '', 0x323031392d30352d32302032303a33303a3533, 0x323031392d30352d32302031383a33303a3533, '', 0, 'http://stylist/?page_id=475', 0, 'page', '', 0),
(476, 1, 0x323031382d31312d32362030393a32333a3030, 0x323031382d31312d32362030373a32333a3030, '', 'Стилист', '', 'inherit', 'closed', 'closed', '', '475-revision-v1', '', '', 0x323031382d31312d32362030393a32333a3030, 0x323031382d31312d32362030373a32333a3030, '', 475, 'http://stylist/475-revision-v1/', 0, 'revision', '', 0),
(477, 1, 0x323031382d31312d32362030393a32393a3338, 0x323031382d31312d32362030373a32393a3338, 'Брови – важная часть на лице каждого. Красивое и ухоженное лицо состоит не только из качественного макияжа, аккуратной прически и мягкой улыбки… но и из правильно оформленных бровей. В этом Вам может помочь профессиональный мастер-бровист!\r\nНа данный момент, профессия «Мастер-бровист» является очень востребованной.\r\nБровист не щипает и не красит брови! Он занимается моделированием, художественным оформлением, или дизайном бровей (как вам больше нравится).\r\nС Вашими бровями работает именно визажист-стилист, бровист.\r\nПрофессиональный бровист работает с лицом. Он учитывает форму лица и его анатомические особенности, умеет с помощью подбора правильной формы бровей подчеркнуть достоинства и скорректировать недостатки клиента.\r\nНапример, бровист может с помощью правильно подобранной формы и цвета бровей:\r\n- визуально убрать отёчность век,\r\n- сузить широкий нос,\r\n- скорректировать овал лица,\r\n- сделать вас моложе на пару лет,\r\n- придать лицу нужной настроение, сделать вас страстной соблазнительницей или нежной милашкой,\r\n- придать выразительность взгляду и более ухоженный вид лицу в целом.\r\nПрофессиональный бровист обращает внимание на особенности характера и поведения клиентки и придает характер бровям.\r\nУ бровиста каждый волосок брови на счету. Сталкиваясь с проблемой перещипанных и перестриженных бровей, а вследствие этого их отсутствием бровист использует индивидуальный подход к каждому клиенту. Также мастер-бровист дает рекомендации по уходу за бровями, что очень важно, особенно в случае отращивания бровей.', 'Бровист', '', 'publish', 'closed', 'closed', '', 'brow', '', '', 0x323031392d31312d30392032313a35383a3233, 0x323031392d31312d30392031393a35383a3233, '', 0, 'http://stylist/?page_id=477', 0, 'page', '', 0),
(478, 1, 0x323031382d31312d32362030393a32393a3338, 0x323031382d31312d32362030373a32393a3338, '', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031382d31312d32362030393a32393a3338, 0x323031382d31312d32362030373a32393a3338, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(479, 1, 0x323031382d31312d32362032313a33323a3234, 0x323031382d31312d32362031393a33323a3234, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\n\n&nbsp;\n\n&nbsp;', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-autosave-v1', '', '', 0x323031382d31312d32362032313a33323a3234, 0x323031382d31312d32362031393a33323a3234, '', 477, 'http://stylist/477-autosave-v1/', 0, 'revision', '', 0),
(480, 1, 0x323031382d31312d32362032313a33323a3431, 0x323031382d31312d32362031393a33323a3431, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031382d31312d32362032313a33323a3431, 0x323031382d31312d32362031393a33323a3431, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(481, 1, 0x323031382d31312d32362032313a33333a3430, 0x323031382d31312d32362031393a33333a3430, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-339\" src=\"http://stylist/wp-content/uploads/2018/07/orig-1-300x226.jpg\" alt=\"img\" width=\"300\" height=\"226\" />\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031382d31312d32362032313a33333a3430, 0x323031382d31312d32362031393a33333a3430, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(482, 1, 0x323031382d31312d32362032313a33343a3530, 0x323031382d31312d32362031393a33343a3530, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-339\" src=\"http://stylist/wp-content/uploads/2018/07/orig-1-300x226.jpg\" alt=\"img\" width=\"300\" height=\"226\" />  <strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031382d31312d32362032313a33343a3530, 0x323031382d31312d32362031393a33343a3530, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(483, 1, 0x323031382d31312d32362032313a34323a3235, 0x323031382d31312d32362031393a34323a3235, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-339\" src=\"http://stylist/wp-content/uploads/2018/07/orig-1-300x226.jpg\" alt=\"img\" width=\"300\" height=\"226\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031382d31312d32362032313a34323a3235, 0x323031382d31312d32362031393a34323a3235, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(485, 1, 0x323031382d31312d32362032313a34333a3537, 0x323031382d31312d32362031393a34333a3537, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-339\" src=\"http://stylist/wp-content/uploads/2018/07/orig-1-300x226.jpg\" alt=\"img\" width=\"300\" height=\"226\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-full wp-image-303\" src=\"http://stylist/wp-content/uploads/2018/07/66.png\" alt=\"img\" width=\"300\" height=\"300\" />\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031382d31312d32362032313a34333a3537, 0x323031382d31312d32362031393a34333a3537, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(486, 1, 0x323031382d31312d32362032313a35313a3038, 0x323031382d31312d32362031393a35313a3038, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-339\" src=\"http://stylist/wp-content/uploads/2018/07/orig-1-300x226.jpg\" alt=\"img\" width=\"300\" height=\"226\">\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-full wp-image-303\" src=\"http://stylist/wp-content/uploads/2018/07/66.png\" alt=\"img\" width=\"300\" height=\"300\">', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031382d31312d32362032313a35313a3038, 0x323031382d31312d32362031393a35313a3038, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(487, 1, 0x323031382d31312d32362032313a35373a3436, 0x323031382d31312d32362031393a35373a3436, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-339\" src=\"http://stylist/wp-content/uploads/2018/07/orig-1-300x226.jpg\" alt=\"img\" width=\"300\" height=\"226\" />\r\n\r\n<img class=\"alignnone size-full wp-image-303\" src=\"http://stylist/wp-content/uploads/2018/07/66.png\" alt=\"img\" width=\"300\" height=\"300\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031382d31312d32362032313a35373a3436, 0x323031382d31312d32362031393a35373a3436, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(488, 1, 0x323031382d31312d32362032323a30303a3435, 0x323031382d31312d32362032303a30303a3435, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-339\" src=\"http://stylist/wp-content/uploads/2018/07/orig-1-300x226.jpg\" alt=\"img\" width=\"300\" height=\"226\" />    <strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-full wp-image-303\" src=\"http://stylist/wp-content/uploads/2018/07/66.png\" alt=\"img\" width=\"300\" height=\"300\" />\r\n\r\n&nbsp;', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031382d31312d32362032323a30303a3435, 0x323031382d31312d32362032303a30303a3435, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(489, 1, 0x323031382d31312d32362032323a30303a3539, 0x323031382d31312d32362032303a30303a3539, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-339\" src=\"http://stylist/wp-content/uploads/2018/07/orig-1-300x226.jpg\" alt=\"img\" width=\"300\" height=\"226\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-full wp-image-303\" src=\"http://stylist/wp-content/uploads/2018/07/66.png\" alt=\"img\" width=\"300\" height=\"300\" />\r\n\r\n&nbsp;', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031382d31312d32362032323a30303a3539, 0x323031382d31312d32362032303a30303a3539, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(490, 1, 0x323031382d31312d32362032323a30313a3137, 0x323031382d31312d32362032303a30313a3137, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-339\" src=\"http://stylist/wp-content/uploads/2018/07/orig-1-300x226.jpg\" alt=\"img\" width=\"300\" height=\"226\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-full wp-image-303\" src=\"http://stylist/wp-content/uploads/2018/07/66.png\" alt=\"img\" width=\"300\" height=\"300\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n&nbsp;', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031382d31312d32362032323a30313a3137, 0x323031382d31312d32362032303a30313a3137, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(491, 1, 0x323031382d31312d32362032323a30353a3039, 0x323031382d31312d32362032303a30353a3039, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-339\" src=\"http://stylist/wp-content/uploads/2018/07/orig-1-300x226.jpg\" alt=\"img\" width=\"300\" height=\"226\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-full wp-image-303\" src=\"http://stylist/wp-content/uploads/2018/07/66.png\" alt=\"img\" width=\"300\" height=\"300\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n&nbsp;', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031382d31312d32362032323a30353a3039, 0x323031382d31312d32362032303a30353a3039, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(492, 1, 0x323031382d31312d32372030343a33323a3037, 0x323031382d31312d32372030323a33323a3037, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-380\" src=\"http://stylist/wp-content/uploads/2018/07/permanentnyj-makiyazh-nizhnego-veka-300x217.jpg\" alt=\"img\" width=\"300\" height=\"217\" /><strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.<img class=\"alignnone size-full wp-image-390\" src=\"http://stylist/wp-content/uploads/2018/07/images-1-1.jpg\" alt=\"img\" width=\"265\" height=\"191\" />', 'Стилист', '', 'inherit', 'closed', 'closed', '', '475-revision-v1', '', '', 0x323031382d31312d32372030343a33323a3037, 0x323031382d31312d32372030323a33323a3037, '', 475, 'http://stylist/475-revision-v1/', 0, 'revision', '', 0),
(493, 1, 0x323031382d31312d32372030343a33333a3039, 0x323031382d31312d32372030323a33333a3039, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\n\n<img class=\"alignnone size-medium wp-image-380\" src=\"http://stylist/wp-content/uploads/2018/07/permanentnyj-makiyazh-nizhnego-veka-300x217.jpg\" alt=\"img\" width=\"300\" height=\"217\" />\n\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.<img class=\"alignnone size-full wp-image-390\" src=\"http://stylist/wp-content/uploads/2018/07/images-1-1.jpg\" alt=\"img\" width=\"265\" height=\"191\" />', 'Стилист', '', 'inherit', 'closed', 'closed', '', '475-autosave-v1', '', '', 0x323031382d31312d32372030343a33333a3039, 0x323031382d31312d32372030323a33333a3039, '', 475, 'http://stylist/475-autosave-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES 
(494, 1, 0x323031382d31312d32372030343a33333a3536, 0x323031382d31312d32372030323a33333a3536, '<img class=\"alignnone size-full wp-image-390\" src=\"http://stylist/wp-content/uploads/2018/07/images-1-1.jpg\" alt=\"img\" width=\"265\" height=\"191\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-302\" src=\"http://stylist/wp-content/uploads/2018/07/55-296x300.png\" alt=\"img\" width=\"296\" height=\"300\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', 'Стилист', '', 'inherit', 'closed', 'closed', '', '475-revision-v1', '', '', 0x323031382d31312d32372030343a33333a3536, 0x323031382d31312d32372030323a33333a3536, '', 475, 'http://stylist/475-revision-v1/', 0, 'revision', '', 0),
(495, 1, 0x323031382d31312d32372030343a33363a3236, 0x323031382d31312d32372030323a33363a3236, '', 'Стилист', '', 'inherit', 'closed', 'closed', '', '475-revision-v1', '', '', 0x323031382d31312d32372030343a33363a3236, 0x323031382d31312d32372030323a33363a3236, '', 475, 'http://stylist/475-revision-v1/', 0, 'revision', '', 0),
(496, 1, 0x323031382d31312d32372030343a33393a3039, 0x323031382d31312d32372030323a33393a3039, '', '1468242807_stilist-380h210', '', 'inherit', 'open', 'closed', '', '1468242807_stilist-380h210', '', '', 0x323031382d31312d32372030343a33393a3135, 0x323031382d31312d32372030323a33393a3135, '', 475, 'http://stylist/wp-content/uploads/2018/11/1468242807_stilist-380h210.jpg', 0, 'attachment', 'image/jpeg', 0),
(497, 1, 0x323031382d31312d32372030343a33393a3232, 0x323031382d31312d32372030323a33393a3232, '<img class=\"alignnone size-full wp-image-390\" src=\"http://stylist/wp-content/uploads/2018/07/images-1-1.jpg\" alt=\"img\" width=\"265\" height=\"191\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-496\" src=\"http://stylist/wp-content/uploads/2018/11/1468242807_stilist-380h210-300x166.jpg\" alt=\"img\" width=\"300\" height=\"166\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', 'Стилист', '', 'inherit', 'closed', 'closed', '', '475-revision-v1', '', '', 0x323031382d31312d32372030343a33393a3232, 0x323031382d31312d32372030323a33393a3232, '', 475, 'http://stylist/475-revision-v1/', 0, 'revision', '', 0),
(498, 1, 0x323031382d31312d32372030343a34393a3230, 0x323031382d31312d32372030323a34393a3230, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', 'Контакты', '', 'inherit', 'closed', 'closed', '', '306-revision-v1', '', '', 0x323031382d31312d32372030343a34393a3230, 0x323031382d31312d32372030323a34393a3230, '', 306, 'http://stylist/306-revision-v1/', 0, 'revision', '', 0),
(499, 1, 0x323031382d31312d32372030353a30323a3235, 0x323031382d31312d32372030333a30323a3235, 'В один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше! Следующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей. Обучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a30323a3235, 0x323031382d31312d32372030333a30323a3235, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(500, 1, 0x323031382d31312d32372030353a30343a3230, 0x323031382d31312d32372030333a30343a3230, 'В один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше! Следующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей. Обучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!\r\n\r\n<div class=\"row\">\r\n	    			<div class=\"offset-lg-1 col-lg-10\">\r\n	    				<div class=\"aboutMe_block-img\">\r\n	    					<img  class=\"img-fluid\" src=\"<?php echo get_template_directory_uri();?>/assets/images/work1.png\" alt=\"img\">\r\n	    					<img class=\"img-fluid\" src=\"<?php echo get_template_directory_uri();?>/assets/images/work2.png\" alt=\"img\">\r\n	    					<img class=\"img-fluid\" src=\"<?php echo get_template_directory_uri();?>/assets/images/work3.png\" alt=\"img\">\r\n	    				</div>\r\n	    			</div>	\r\n	    		</div>', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a30343a3230, 0x323031382d31312d32372030333a30343a3230, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(501, 1, 0x323031382d31312d32372030353a32363a3131, 0x323031382d31312d32372030333a32363a3131, '<img class=\"alignnone size-medium wp-image-514\" src=\"http://stylist/wp-content/uploads/2018/11/21042370_1181331868633623_1302576685098467328_n-225x300.jpg\" alt=\"мое фото\" width=\"225\" height=\"300\" />\n\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше!\n\n<img class=\"alignnone size-medium wp-image-515\" src=\"http://stylist/wp-content/uploads/2018/11/20398178_445919659140861_3772480860834496512_n-224x300.jpg\" alt=\"мое фото\" width=\"224\" height=\"300\" />\n\nСледующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.Обучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-autosave-v1', '', '', 0x323031382d31312d32372030353a32363a3131, 0x323031382d31312d32372030333a32363a3131, '', 310, 'http://stylist/310-autosave-v1/', 0, 'revision', '', 0),
(502, 1, 0x323031382d31312d32372030353a30363a3333, 0x323031382d31312d32372030333a30363a3333, 'В один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше! Следующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей. Обучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!\r\n\r\n<div class=\"offset-lg-1 col-lg-10\">\r\n    <div class=\"aboutMe_block-img\">\r\n        <img src=\"http://stylist/wp-content/uploads/2018/07/3-297x300.png\" alt=\"img\" width=\"297\" height=\"300\" class=\"alignnone size-medium wp-image-299\">\r\n        <img src=\"http://stylist/wp-content/uploads/2018/07/1-300x300.png\" alt=\"img\" width=\"300\" height=\"300\" class=\"alignnone size-medium wp-image-297\">\r\n        <img src=\"http://stylist/wp-content/uploads/2018/07/88-300x299.png\" alt=\"img\" width=\"300\" height=\"299\" class=\"alignnone size-medium wp-image-304\" />     \r\n    </div>\r\n</div>	\r\n	    		', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a30363a3333, 0x323031382d31312d32372030333a30363a3333, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(503, 1, 0x323031382d31312d32372030353a30383a3230, 0x323031382d31312d32372030333a30383a3230, '<img class=\"alignnone size-medium wp-image-298\" src=\"http://stylist/wp-content/uploads/2018/07/2-297x300.png\" alt=\"img\" width=\"297\" height=\"300\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше! Следующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.\r\n\r\n<img class=\"alignnone size-medium wp-image-304\" src=\"http://stylist/wp-content/uploads/2018/07/88-300x299.png\" alt=\"img\" width=\"300\" height=\"299\" />\r\n\r\nОбучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a30383a3230, 0x323031382d31312d32372030333a30383a3230, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(504, 1, 0x323031382d31312d32372030353a31323a3237, 0x323031382d31312d32372030333a31323a3237, '<img class=\"alignnone wp-image-298 size-thumbnail\" src=\"http://stylist/wp-content/uploads/2018/07/2-150x150.png\" alt=\"img\" width=\"150\" height=\"150\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше! Следующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.\r\n\r\nОбучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a31323a3237, 0x323031382d31312d32372030333a31323a3237, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(505, 1, 0x323031382d31312d32372030353a31333a3033, 0x323031382d31312d32372030333a31333a3033, '<img class=\"alignnone wp-image-298 size-thumbnail\" src=\"http://stylist/wp-content/uploads/2018/07/2-150x150.png\" alt=\"img\" width=\"150\" height=\"150\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше! Следующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.\r\n\r\n<img class=\"alignnone wp-image-305 size-thumbnail\" src=\"http://stylist/wp-content/uploads/2018/07/99-150x150.png\" alt=\"img\" width=\"150\" height=\"150\" />\r\n\r\nОбучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a31333a3033, 0x323031382d31312d32372030333a31333a3033, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(506, 1, 0x323031382d31312d32372030353a31333a3333, 0x323031382d31312d32372030333a31333a3333, '<img class=\"alignnone wp-image-298 size-thumbnail\" src=\"http://stylist/wp-content/uploads/2018/07/2-150x150.png\" alt=\"img\" width=\"150\" height=\"150\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше!\r\n\r\n<img class=\"alignnone wp-image-305 size-thumbnail\" src=\"http://stylist/wp-content/uploads/2018/07/99-150x150.png\" alt=\"img\" width=\"150\" height=\"150\" />\r\n\r\nСледующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.Обучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a31333a3333, 0x323031382d31312d32372030333a31333a3333, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(509, 1, 0x323031382d31312d32372030353a31363a3337, 0x323031382d31312d32372030333a31363a3337, '<img class=\"alignnone size-thumbnail wp-image-507\" src=\"http://stylist/wp-content/uploads/2018/11/21042370_1181331868633623_1302576685098467328_n-150x150.jpg\" alt=\"мое фото\" width=\"150\" height=\"150\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше!\r\n\r\n<img class=\"alignnone size-thumbnail wp-image-508\" src=\"http://stylist/wp-content/uploads/2018/11/20398178_445919659140861_3772480860834496512_n-150x150.jpg\" alt=\"мое фото\" width=\"150\" height=\"150\" />\r\n\r\nСледующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.Обучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a31363a3337, 0x323031382d31312d32372030333a31363a3337, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(510, 1, 0x323031382d31312d32372030353a31363a3535, 0x323031382d31312d32372030333a31363a3535, '<img class=\"alignnone wp-image-507 size-medium\" src=\"http://stylist/wp-content/uploads/2018/11/21042370_1181331868633623_1302576685098467328_n-300x300.jpg\" alt=\"мое фото\" width=\"300\" height=\"300\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше!\r\n\r\n<img class=\"alignnone size-thumbnail wp-image-508\" src=\"http://stylist/wp-content/uploads/2018/11/20398178_445919659140861_3772480860834496512_n-150x150.jpg\" alt=\"мое фото\" width=\"150\" height=\"150\" />\r\n\r\nСледующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.Обучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a31363a3535, 0x323031382d31312d32372030333a31363a3535, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(511, 1, 0x323031382d31312d32372030353a31373a3430, 0x323031382d31312d32372030333a31373a3430, '<img class=\"alignnone wp-image-507 size-thumbnail\" src=\"http://stylist/wp-content/uploads/2018/11/21042370_1181331868633623_1302576685098467328_n-150x150.jpg\" alt=\"мое фото\" width=\"150\" height=\"150\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше!\r\n\r\n<img class=\"alignnone size-thumbnail wp-image-508\" src=\"http://stylist/wp-content/uploads/2018/11/20398178_445919659140861_3772480860834496512_n-150x150.jpg\" alt=\"мое фото\" width=\"150\" height=\"150\" />\r\n\r\nСледующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.Обучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a31373a3430, 0x323031382d31312d32372030333a31373a3430, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(512, 1, 0x323031382d31312d32372030353a31393a3131, 0x323031382d31312d32372030333a31393a3131, '<img class=\"alignnone wp-image-507 size-medium\" src=\"http://stylist/wp-content/uploads/2018/11/21042370_1181331868633623_1302576685098467328_n-300x300.jpg\" alt=\"мое фото\" width=\"300\" height=\"300\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше!<br><br><br><br><br><br>\r\n\r\n<img class=\"alignnone size-thumbnail wp-image-508\" src=\"http://stylist/wp-content/uploads/2018/11/20398178_445919659140861_3772480860834496512_n-150x150.jpg\" alt=\"мое фото\" width=\"150\" height=\"150\" />\r\n\r\nСледующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.Обучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a31393a3131, 0x323031382d31312d32372030333a31393a3131, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(513, 1, 0x323031382d31312d32372030353a31393a3339, 0x323031382d31312d32372030333a31393a3339, '<img class=\"alignnone wp-image-507 size-medium\" src=\"http://stylist/wp-content/uploads/2018/11/21042370_1181331868633623_1302576685098467328_n-300x300.jpg\" alt=\"мое фото\" width=\"300\" height=\"300\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше!\r\n\r\n\r\n\r\n\r\n\r\n<img class=\"alignnone wp-image-508 size-medium\" src=\"http://stylist/wp-content/uploads/2018/11/20398178_445919659140861_3772480860834496512_n-300x300.jpg\" alt=\"мое фото\" width=\"300\" height=\"300\" />\r\n\r\nСледующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.Обучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a31393a3339, 0x323031382d31312d32372030333a31393a3339, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES 
(514, 1, 0x323031382d31312d32372030353a32323a3332, 0x323031382d31312d32372030333a32323a3332, '', '21042370_1181331868633623_1302576685098467328_n', '', 'inherit', 'open', 'closed', '', '21042370_1181331868633623_1302576685098467328_n', '', '', 0x323031382d31312d32372030353a32323a3339, 0x323031382d31312d32372030333a32323a3339, '', 310, 'http://stylist/wp-content/uploads/2018/11/21042370_1181331868633623_1302576685098467328_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(515, 1, 0x323031382d31312d32372030353a32323a3534, 0x323031382d31312d32372030333a32323a3534, '', '20398178_445919659140861_3772480860834496512_n', '', 'inherit', 'open', 'closed', '', '20398178_445919659140861_3772480860834496512_n', '', '', 0x323031382d31312d32372030353a32333a3031, 0x323031382d31312d32372030333a32333a3031, '', 310, 'http://stylist/wp-content/uploads/2018/11/20398178_445919659140861_3772480860834496512_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(516, 1, 0x323031382d31312d32372030353a32333a3034, 0x323031382d31312d32372030333a32333a3034, '<img class=\"alignnone size-medium wp-image-514\" src=\"http://stylist/wp-content/uploads/2018/11/21042370_1181331868633623_1302576685098467328_n-225x300.jpg\" alt=\"мое фото\" width=\"225\" height=\"300\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше!\r\n\r\n<img class=\"alignnone size-medium wp-image-515\" src=\"http://stylist/wp-content/uploads/2018/11/20398178_445919659140861_3772480860834496512_n-224x300.jpg\" alt=\"мое фото\" width=\"224\" height=\"300\" />\r\n\r\nСледующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.Обучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a32333a3034, 0x323031382d31312d32372030333a32333a3034, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(517, 1, 0x323031382d31312d32372030353a32333a3435, 0x323031382d31312d32372030333a32333a3435, '<img class=\"alignnone size-medium wp-image-514\" src=\"http://stylist/wp-content/uploads/2018/11/21042370_1181331868633623_1302576685098467328_n-225x300.jpg\" alt=\"мое фото\" width=\"225\" height=\"300\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше!\r\n\r\n<img class=\"alignnone size-medium wp-image-515\" src=\"http://stylist/wp-content/uploads/2018/11/20398178_445919659140861_3772480860834496512_n-224x300.jpg\" alt=\"мое фото\" width=\"224\" height=\"300\" /> <br><br><br><br><br><br><br><br><br><br><br>\r\n\r\nСледующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.Обучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a32333a3435, 0x323031382d31312d32372030333a32333a3435, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(518, 1, 0x323031382d31312d32372030353a32343a3039, 0x323031382d31312d32372030333a32343a3039, '<img class=\"alignnone size-medium wp-image-514\" src=\"http://stylist/wp-content/uploads/2018/11/21042370_1181331868633623_1302576685098467328_n-225x300.jpg\" alt=\"мое фото\" width=\"225\" height=\"300\">\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше!\r\n\r\n<br><br><br><br><br><br><br><br><br><br><br>\r\n\r\n<img class=\"alignnone size-medium wp-image-515\" src=\"http://stylist/wp-content/uploads/2018/11/20398178_445919659140861_3772480860834496512_n-224x300.jpg\" alt=\"мое фото\" width=\"224\" height=\"300\"> \r\n\r\nСледующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.Обучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a32343a3039, 0x323031382d31312d32372030333a32343a3039, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(519, 1, 0x323031382d31312d32372030353a32363a3239, 0x323031382d31312d32372030333a32363a3239, '<img class=\"alignnone size-medium wp-image-514\" src=\"http://stylist/wp-content/uploads/2018/11/21042370_1181331868633623_1302576685098467328_n-225x300.jpg\" alt=\"мое фото\" width=\"225\" height=\"300\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше!\r\n\r\nСледующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.\r\n\r\n<img class=\"alignnone size-medium wp-image-515\" src=\"http://stylist/wp-content/uploads/2018/11/20398178_445919659140861_3772480860834496512_n-224x300.jpg\" alt=\"мое фото\" width=\"224\" height=\"300\" />\r\n\r\nОбучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a32363a3239, 0x323031382d31312d32372030333a32363a3239, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(520, 1, 0x323031382d31312d32372030353a32373a3237, 0x323031382d31312d32372030333a32373a3237, '<img class=\"alignnone size-medium wp-image-514\" src=\"http://stylist/wp-content/uploads/2018/11/21042370_1181331868633623_1302576685098467328_n-225x300.jpg\" alt=\"мое фото\" width=\"225\" height=\"300\" />\r\n\r\nВ один момент моя жизнь перевернулась. Я решила эту любовь превратить в профессию, начался мой путь с академии макияжа Гоар Аветисян, где мне дали не только базовые знания и навыки, но и неиссякаемый поток вдохновения и мотивации к развитию. Участие в проектах с замиранием сердца, первые клиенты и первые улыбки радости на лицах. И те бабочки в груди подсказывали, что я на правильном пути. Хотелось еще и больше, и дальше!\r\n\r\n<img class=\"alignnone size-medium wp-image-515\" src=\"http://stylist/wp-content/uploads/2018/11/20398178_445919659140861_3772480860834496512_n-224x300.jpg\" alt=\"мое фото\" width=\"224\" height=\"300\" />\r\n\r\nСледующим глотком знаний стала Академия макияжа Make up Kaliningrad. Совершенно новое видение процесса, другие техники, мир красоты раскрылся для меня с другой стороны. Балансируя между различными форматами макияжа, начинаешь вырабатывать для себя совершенно иной подход, где имеют место и нежность в образе, и роковая красотка, и даже азиатская красота найдет свое отражение. Создавая макияж, хотелось завершить образ, чтобы отражение соответствовало внутренним ощущениям девушек, и я расширила круг своих возможностей.\r\n\r\nОбучение укладкам и первое знакомство с таким желанным и далеким на тот момент миром началось с арт-академии Елены Павловой. Не останавливаясь, продолжила в Минске, Санкт-Петербурге в школе красоты El Stile, международная Академия Lash&amp;Brow помогла улучшить навыки в архитектуре бровей. И я не останавливаюсь, это ведь только начало. И по настоящий день моя главная задача показывать насколько вы прекрасны и индивидуальны. Давайте любить себя! И я в этом вам помогу с большим удовольствием!', 'О нас', '', 'inherit', 'closed', 'closed', '', '310-revision-v1', '', '', 0x323031382d31312d32372030353a32373a3237, 0x323031382d31312d32372030333a32373a3237, '', 310, 'http://stylist/310-revision-v1/', 0, 'revision', '', 0),
(521, 1, 0x323031382d31312d32372030353a34393a3334, 0x323031382d31312d32372030333a34393a3334, '<h2 align=\"justify\">1. Общие положения</h2>\r\n1.1 <span lang=\"ru-RU\">Администрация сайта </span><a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a> (далее по тексту – Оператор) ставит соблюдение прав и свобод граждан одним из важнейших условий осуществления своей деятельности.\r\n\r\n1.2 Политика Оператора в отношении обработки персональных данных (далее по тексту — Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>. Персональные данные обрабатывается в соответствии с ФЗ «О персональных данных» № 152-ФЗ.\r\n<h2 align=\"justify\">2. Основные понятия, используемые в Политике:</h2>\r\n2.1 Веб-сайт — совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>;\r\n\r\n2.2 Пользователь – любой посетитель веб-сайта <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>;\r\n\r\n2.3 Персональные данные – любая информация, относящаяся к Пользователю веб-сайта <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>;\r\n\r\n2.4 Обработка персональных данных — любое действие с персональными данными, совершаемые с использованием ЭВМ, равно как и без их использования;\r\n\r\n2.5 Обезличивание персональных данных – действия, результатом которых является невозможность без использования дополнительной информации определить принадлежность персональных данных конкретному Пользователю или лицу;\r\n\r\n2.6 Распространение персональных данных – любые действия, результатом которых является раскрытие персональных данных неопределенному кругу лиц;\r\n\r\n2.7 Предоставление персональных данных – любые действия, результатом которых является раскрытие персональных данных определенному кругу лиц;\r\n\r\n2.8 Уничтожение персональных данных – любые действия, результатом которых является безвозвратное уничтожение персональных на ЭВМ или любых других носителях.\r\n<h2 class=\"western\" align=\"justify\">3. Оператор может обрабатывать следующие персональные данные:</h2>\r\n<p align=\"justify\">3.1 Адрес электронной почты Пользователя.</p>\r\n<p align=\"justify\">3.2 Номер телефона Пользователя.</p>\r\n<p align=\"justify\">3.3. <span lang=\"ru-RU\">Имя</span> <span lang=\"ru-RU\">Пользователя.</span></p>\r\n<p align=\"justify\">3.4. Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов «cookie») с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">4. Цели обработки персональных данных</h2>\r\n4.1 Цель обработки адреса электронной почты, номера телефона, имени Пользователя — уточнение деталей заказа.\r\n\r\n4.2 Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания.\r\n<h2 class=\"western\" align=\"justify\">5. Правовые основания обработки персональных данных</h2>\r\n<p align=\"justify\">5.1 Оператор обрабатывает персональные данные Пользователя только в случае их отправки Пользователем через формы, расположенные на сайте <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>; отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.</p>\r\n<p align=\"justify\">5.2 Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов «cookie» и использование технологии JavaScript).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">6. Порядок сбора, хранения, передачи и других видов обработки персональных данных</h2>\r\n<p align=\"justify\">6.1 Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.</p>\r\n<p align=\"justify\">6.2 Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства.</p>\r\n<p align=\"justify\">6.3. В случае выявления неточностей в персональных данных, Пользователь может актуализировать их, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a>, с пометкой «Актуализация персональных данных»</p>\r\n<p align=\"justify\">6.3 Срок обработки персональных данных является неограниченным. Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a> с пометкой «Отзыв согласия на обработку персональных данных».</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">7. Заключительные положения</h2>\r\n<p align=\"justify\">7.1. Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a>.</p>\r\n<p align=\"justify\">7.2. В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. В случае существенных изменений Пользователю может быть выслана информация на указанный им электронный адрес.</p>', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '429-revision-v1', '', '', 0x323031382d31312d32372030353a34393a3334, 0x323031382d31312d32372030333a34393a3334, '', 429, 'http://stylist/429-revision-v1/', 0, 'revision', '', 0),
(522, 1, 0x323031382d31312d32372030353a35343a3035, 0x323031382d31312d32372030333a35343a3035, '<h2 align=\"justify\">1. Общие положения</h2>\r\n1.1 <span lang=\"ru-RU\">Администрация сайта </span><a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a> (далее по тексту – Оператор) ставит соблюдение прав и свобод граждан одним из важнейших условий осуществления своей деятельности.\r\n\r\n1.2 Политика Оператора в отношении обработки персональных данных (далее по тексту — Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>. Персональные данные обрабатывается в соответствии с ФЗ «О персональных данных» № 152-ФЗ.\r\n<h2 align=\"justify\">2. Основные понятия, используемые в Политике:</h2>\r\n2.1 Веб-сайт — совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>;\r\n\r\n2.2 Пользователь – любой посетитель веб-сайта <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>;\r\n\r\n2.3 Персональные данные – любая информация, относящаяся к Пользователю веб-сайта <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>;\r\n\r\n2.4 Обработка персональных данных — любое действие с персональными данными, совершаемые с использованием ЭВМ, равно как и без их использования;\r\n\r\n2.5 Обезличивание персональных данных – действия, результатом которых является невозможность без использования дополнительной информации определить принадлежность персональных данных конкретному Пользователю или лицу;\r\n\r\n2.6 Распространение персональных данных – любые действия, результатом которых является раскрытие персональных данных неопределенному кругу лиц;\r\n\r\n2.7 Предоставление персональных данных – любые действия, результатом которых является раскрытие персональных данных определенному кругу лиц;\r\n\r\n2.8 Уничтожение персональных данных – любые действия, результатом которых является безвозвратное уничтожение персональных на ЭВМ или любых других носителях.\r\n<h2 class=\"western\" align=\"justify\">3. Оператор может обрабатывать следующие персональные данные:</h2>\r\n<p align=\"justify\">3.1 Адрес электронной почты Пользователя.</p>\r\n<p align=\"justify\">3.2 Номер телефона Пользователя.</p>\r\n<p align=\"justify\">3.3. <span lang=\"ru-RU\">Имя</span> <span lang=\"ru-RU\">Пользователя.</span></p>\r\n<p align=\"justify\">3.4. Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов «cookie») с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">4. Цели обработки персональных данных</h2>\r\n4.1 Цель обработки адреса электронной почты, номера телефона, имени Пользователя — уточнение деталей заказа.\r\n\r\n4.2 Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания.\r\n<h2 class=\"western\" align=\"justify\">5. Правовые основания обработки персональных данных</h2>\r\n<p align=\"justify\">5.1 Оператор обрабатывает персональные данные Пользователя только в случае их отправки Пользователем через формы, расположенные на сайте <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>; отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.</p>\r\n<p align=\"justify\">5.2 Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов «cookie» и использование технологии JavaScript).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">6. Порядок сбора, хранения, передачи и других видов обработки персональных данных</h2>\r\n<p align=\"justify\">6.1 Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.</p>\r\n<p align=\"justify\">6.2 Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства.</p>\r\n<p align=\"justify\">6.3. В случае выявления неточностей в персональных данных, Пользователь может актуализировать их, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a>, с пометкой «Актуализация персональных данных»</p>\r\n<p align=\"justify\">6.3 Срок обработки персональных данных является неограниченным. Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a> с пометкой «Отзыв согласия на обработку персональных данных».</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">7. Заключительные положения</h2>\r\n<p align=\"justify\">7.1. Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a>.</p>\r\n<p align=\"justify\">7.2. В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. В случае существенных изменений Пользователю может быть выслана информация на указанный им электронный адрес.</p>', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '429-revision-v1', '', '', 0x323031382d31312d32372030353a35343a3035, 0x323031382d31312d32372030333a35343a3035, '', 429, 'http://stylist/429-revision-v1/', 0, 'revision', '', 0),
(523, 1, 0x323031382d31312d32372030353a35353a3235, 0x323031382d31312d32372030333a35353a3235, '<h2 align=\"justify\">1. Общие положения</h2>\r\n1.1 <span lang=\"ru-RU\">Администрация сайта </span><a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a> (далее по тексту – Оператор) ставит соблюдение прав и свобод граждан одним из важнейших условий осуществления своей деятельности.\r\n\r\n1.2 Политика Оператора в отношении обработки персональных данных (далее по тексту — Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>. Персональные данные обрабатывается в соответствии с ФЗ «О персональных данных» № 152-ФЗ.\r\n<h2 align=\"justify\">2. Основные понятия, используемые в Политике:</h2>\r\n2.1 Веб-сайт — совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>;\r\n\r\n2.2 Пользователь – любой посетитель веб-сайта <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>;\r\n\r\n2.3 Персональные данные – любая информация, относящаяся к Пользователю веб-сайта <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>;\r\n\r\n2.4 Обработка персональных данных — любое действие с персональными данными, совершаемые с использованием ЭВМ, равно как и без их использования;\r\n\r\n2.5 Обезличивание персональных данных – действия, результатом которых является невозможность без использования дополнительной информации определить принадлежность персональных данных конкретному Пользователю или лицу;\r\n\r\n2.6 Распространение персональных данных – любые действия, результатом которых является раскрытие персональных данных неопределенному кругу лиц;\r\n\r\n2.7 Предоставление персональных данных – любые действия, результатом которых является раскрытие персональных данных определенному кругу лиц;\r\n\r\n2.8 Уничтожение персональных данных – любые действия, результатом которых является безвозвратное уничтожение персональных на ЭВМ или любых других носителях.\r\n<h2 class=\"western\" align=\"justify\">3. Оператор может обрабатывать следующие персональные данные:</h2>\r\n<p align=\"justify\">3.1 Адрес электронной почты Пользователя.</p>\r\n<p align=\"justify\">3.2 Номер телефона Пользователя.</p>\r\n<p align=\"justify\">3.3. <span lang=\"ru-RU\">Имя</span> <span lang=\"ru-RU\">Пользователя.</span></p>\r\n<p align=\"justify\">3.4. Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов «cookie») с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">4. Цели обработки персональных данных</h2>\r\n4.1 Цель обработки адреса электронной почты, номера телефона, имени Пользователя — уточнение деталей заказа.\r\n\r\n4.2 Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания.\r\n<h2 class=\"western\" align=\"justify\">5. Правовые основания обработки персональных данных</h2>\r\n<p align=\"justify\">5.1 Оператор обрабатывает персональные данные Пользователя только в случае их отправки Пользователем через формы, расположенные на сайте <a href=\"http://kaza4enkova.ru\"><span lang=\"en-US\">www.</span><span lang=\"en-US\">kaza4enkova.ru</span></a>; отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.</p>\r\n<p align=\"justify\">5.2 Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов «cookie» и использование технологии JavaScript).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">6. Порядок сбора, хранения, передачи и других видов обработки персональных данных</h2>\r\n<p align=\"justify\">6.1 Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.</p>\r\n<p align=\"justify\">6.2 Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства.</p>\r\n<p align=\"justify\">6.3. В случае выявления неточностей в персональных данных, Пользователь может актуализировать их, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a>, с пометкой «Актуализация персональных данных»</p>\r\n<p align=\"justify\">6.3 Срок обработки персональных данных является неограниченным. Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a> с пометкой «Отзыв согласия на обработку персональных данных».</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">7. Заключительные положения</h2>\r\n<p align=\"justify\">7.1. Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a>.</p>\r\n<p align=\"justify\">7.2. В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. В случае существенных изменений Пользователю может быть выслана информация на указанный им электронный адрес.</p>', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '429-revision-v1', '', '', 0x323031382d31312d32372030353a35353a3235, 0x323031382d31312d32372030333a35353a3235, '', 429, 'http://stylist/429-revision-v1/', 0, 'revision', '', 0),
(527, 1, 0x323031382d31312d32372030363a30343a3332, 0x323031382d31312d32372030343a30343a3332, 'Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', '1', '', 'publish', 'open', 'closed', '', '1', '', '', 0x323031382d31312d32372030363a30343a3332, 0x323031382d31312d32372030343a30343a3332, '', 0, 'http://stylist/?post_type=mp-event&#038;p=527', 0, 'mp-event', '', 0),
(528, 0, 0x323031382d31312d32372030363a31313a3137, 0x323031382d31312d32372030343a31313a3137, '', 'calendar', '', 'publish', 'closed', 'closed', '', 'calendar', '', '', 0x323031382d31312d32372030363a31313a3137, 0x323031382d31312d32372030343a31313a3137, '', 0, 'http://stylist/chronosly-calendar/calendar/', 0, 'chronosly_calendar', '', 0),
(529, 1, 0x323031382d31312d32372030363a31383a3130, 0x323031382d31312d32372030343a31383a3130, 'Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', '1', 'Lorem Ipsum - это текст-\"рыба\"', 'publish', 'closed', 'closed', '', '1-2', '', '', 0x323031382d31312d32372030363a32303a3336, 0x323031382d31312d32372030343a32303a3336, '', 0, 'http://stylist/?post_type=chronosly&#038;p=529', 0, 'chronosly', '', 0),
(533, 1, 0x323031382d31312d32372030363a34363a3432, 0x323031382d31312d32372030343a34363a3432, 'jjjjjjjjj', '1', '', 'publish', 'closed', 'closed', '', '1', '', '', 0x323031382d31312d32372030363a34363a3432, 0x323031382d31312d32372030343a34363a3432, '', 0, 'http://stylist/?post_type=ecwd_event&#038;p=533', 0, 'ecwd_event', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES 
(534, 1, 0x323031382d31312d32372030363a35313a3532, 0x323031382d31312d32372030343a35313a3532, '', 'Боря. Стрижка', '', 'publish', 'closed', 'closed', '', 'borya-strizhka-2', '', '', 0x323031382d31312d32372030363a35323a3232, 0x323031382d31312d32372030343a35323a3232, '', 0, 'http://stylist/event/borya-strizhka-2/', 0, 'ecwd_event', '', 0),
(537, 1, 0x323031382d31312d32372030383a35383a3339, 0x323031382d31312d32372030363a35383a3339, 'Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов.', 'Lorem Ipsum', '', 'publish', 'closed', 'closed', '', 'lorem-ipsum', '', '', 0x323031382d31312d32372030393a31393a3436, 0x323031382d31312d32372030373a31393a3436, '', 0, 'http://stylist/mc-events/lorem-ipsum/', 0, 'mc-events', '', 0),
(538, 1, 0x323031382d31312d32372030393a31363a3039, 0x323031382d31312d32372030373a31363a3039, 'Побрить Васю на лысо.', 'Стрижка у Васи', '', 'publish', 'closed', 'closed', '', 'strizhka-u-vasi', '', '', 0x323031382d31312d32372030393a32303a3139, 0x323031382d31312d32372030373a32303a3139, '', 0, 'http://stylist/mc-events/strizhka-u-vasi/', 0, 'mc-events', '', 0),
(539, 1, 0x323031382d31312d32372030393a32363a3335, 0x323031382d31312d32372030373a32363a3335, 'Майкла надо постричь.', 'Постричь Майкла', '', 'publish', 'closed', 'closed', '', 'postrich-majkla', '', '', 0x323031382d31312d32372030393a32363a3335, 0x323031382d31312d32372030373a32363a3335, '', 0, 'http://stylist/mc-events/postrich-majkla/', 0, 'mc-events', '', 0),
(540, 1, 0x323031382d31312d32372030393a32393a3534, 0x323031382d31312d32372030373a32393a3534, '<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-medium wp-image-339\" src=\"http://stylist/wp-content/uploads/2018/07/orig-1-300x226.jpg\" alt=\"img\" width=\"300\" height=\"226\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<img class=\"alignnone size-full wp-image-303\" src=\"http://stylist/wp-content/uploads/2018/07/66.png\" alt=\"img\" width=\"300\" height=\"300\" />\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n<strong>Lorem Ipsum</strong> - это текст-\"рыба\", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной \"рыбой\" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031382d31312d32372030393a32393a3534, 0x323031382d31312d32372030373a32393a3534, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(541, 1, 0x323031382d31312d33302031333a34383a3231, 0x323031382d31312d33302031313a34383a3231, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima nulla numquam ipsum cupiditate deleniti, alias. Dolore, dolorem nulla reiciendis debitis fugit deserunt iure repellat, nihil modi magni provident aliquam, vel nam ea culpa unde veritatis deleniti nisi. Amet id debitis dignissimos asperiores facilis deserunt omnis, esse, quaerat in, maiores totam quas consectetur? Sapiente dolores, aliquam labore fugiat temporibus recusandae repudiandae eum nihil consectetur ratione dicta, vero! Labore quasi officiis libero quo modi, eos esse quisquam dolorum. Perferendis fugit eligendi minus amet repudiandae, dolorum rerum, quam voluptatem optio sequi veniam aliquid possimus nostrum et, ex, ab similique. Corporis error perferendis nulla.', 'Контакты', '', 'inherit', 'closed', 'closed', '', '306-revision-v1', '', '', 0x323031382d31312d33302031333a34383a3231, 0x323031382d31312d33302031313a34383a3231, '', 306, 'http://stylist/306-revision-v1/', 0, 'revision', '', 0),
(542, 1, 0x323031382d31312d33302031343a31333a3137, 0x323031382d31312d33302031323a31333a3137, '<h2 align=\"justify\">1. Общие положения</h2>\r\n1.1 <span lang=\"ru-RU\">Администрация сайта </span><a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a> (далее по тексту – Оператор) ставит соблюдение прав и свобод граждан одним из важнейших условий осуществления своей деятельности.\r\n\r\n1.2 Политика Оператора в отношении обработки персональных данных (далее по тексту — Политика) применяется ко всей информации, которую Оператор может получить о посетителях веб-сайта <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>. Персональные данные обрабатывается в соответствии с ФЗ «О персональных данных» № 152-ФЗ.\r\n<h2 align=\"justify\">2. Основные понятия, используемые в Политике:</h2>\r\n2.1 Веб-сайт — совокупность графических и информационных материалов, а также программ для ЭВМ и баз данных, обеспечивающих их доступность в сети интернет по сетевому адресу <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>;\r\n\r\n2.2 Пользователь – любой посетитель веб-сайта <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>;\r\n\r\n2.3 Персональные данные – любая информация, относящаяся к Пользователю веб-сайта <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>;\r\n\r\n2.4 Обработка персональных данных — любое действие с персональными данными, совершаемые с использованием ЭВМ, равно как и без их использования;\r\n\r\n2.5 Обезличивание персональных данных – действия, результатом которых является невозможность без использования дополнительной информации определить принадлежность персональных данных конкретному Пользователю или лицу;\r\n\r\n2.6 Распространение персональных данных – любые действия, результатом которых является раскрытие персональных данных неопределенному кругу лиц;\r\n\r\n2.7 Предоставление персональных данных – любые действия, результатом которых является раскрытие персональных данных определенному кругу лиц;\r\n\r\n2.8 Уничтожение персональных данных – любые действия, результатом которых является безвозвратное уничтожение персональных на ЭВМ или любых других носителях.\r\n<h2 class=\"western\" align=\"justify\">3. Оператор может обрабатывать следующие персональные данные:</h2>\r\n<p align=\"justify\">3.1 Адрес электронной почты Пользователя.</p>\r\n<p align=\"justify\">3.2 Номер телефона Пользователя.</p>\r\n<p align=\"justify\">3.3. <span lang=\"ru-RU\">Имя</span> <span lang=\"ru-RU\">Пользователя.</span></p>\r\n<p align=\"justify\">3.4. Также на сайте происходит сбор и обработка обезличенных данных о посетителях (в т.ч. файлов «cookie») с помощью сервисов интернет-статистики (Яндекс Метрика и Гугл Аналитика и других).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">4. Цели обработки персональных данных</h2>\r\n4.1 Цель обработки адреса электронной почты, номера телефона, имени Пользователя — уточнение деталей заказа.\r\n\r\n4.2 Обезличенные данные Пользователей, собираемые с помощью сервисов интернет-статистики, служат для сбора информации о действиях Пользователей на сайте, улучшения качества сайта и его содержания.\r\n<h2 class=\"western\" align=\"justify\">5. Правовые основания обработки персональных данных</h2>\r\n<p align=\"justify\">5.1 Оператор обрабатывает персональные данные Пользователя только в случае их отправки Пользователем через формы, расположенные на сайте <a href=\"[main_url]\"><span lang=\"en-US\">[site_name]</span></a>; отправляя свои персональные данные Оператору, Пользователь выражает свое согласие с данной Политикой.</p>\r\n<p align=\"justify\">5.2 Оператор обрабатывает обезличенные данные о Пользователе в случае, если это разрешено в настройках браузера Пользователя (включено сохранение файлов «cookie» и использование технологии JavaScript).</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">6. Порядок сбора, хранения, передачи и других видов обработки персональных данных</h2>\r\n<p align=\"justify\">6.1 Оператор обеспечивает сохранность персональных данных и принимает все возможные меры, исключающие доступ к персональным данным неуполномоченных лиц.</p>\r\n<p align=\"justify\">6.2 Персональные данные Пользователя никогда, ни при каких условиях не будут переданы третьим лицам, за исключением случаев, связанных с исполнением действующего законодательства.</p>\r\n<p align=\"justify\">6.3. В случае выявления неточностей в персональных данных, Пользователь может актуализировать их, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a>, с пометкой «Актуализация персональных данных»</p>\r\n<p align=\"justify\">6.3 Срок обработки персональных данных является неограниченным. Пользователь может в любой момент отозвать свое согласие на обработку персональных данных, направив Оператору уведомление с помощью электронной почты на электронный адрес Оператора <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a> с пометкой «Отзыв согласия на обработку персональных данных».</p>\r\n\r\n<h2 class=\"western\" align=\"justify\">7. Заключительные положения</h2>\r\n<p align=\"justify\">7.1. Пользователь может получить любые разъяснения по интересующим вопросам, касающимся обработки его персональных данных, обратившись к Оператору с помощью электронной почты <a href=\"mailto:[userEmail]\"><span lang=\"en-US\">[userEmail]</span></a>.</p>\r\n<p align=\"justify\">7.2. В данном документе будут отражены любые изменения политики обработки персональных данных Оператором. В случае существенных изменений Пользователю может быть выслана информация на указанный им электронный адрес.</p>', 'Политика конфиденциальности', '', 'inherit', 'closed', 'closed', '', '429-revision-v1', '', '', 0x323031382d31312d33302031343a31333a3137, 0x323031382d31312d33302031323a31333a3137, '', 429, 'http://stylist/429-revision-v1/', 0, 'revision', '', 0),
(545, 1, 0x323031392d30352d31312031343a34353a3434, 0x323031392d30352d31312031323a34353a3434, '<h1>Наш салон красоты - №1 в Калининграде!</h1>\r\nПолный спектр парикмахерских услуг в Калининграде: мужские и женские стрижки в Калининграде; укладки; бережное и грамотное окрашивание волос; прически из длинных волос в Калининграде; правильная стрижка горячими ножницами. Перманентное выпрямление и завивка волос в Калининграде.', 'Главная', '', 'inherit', 'closed', 'closed', '', '450-revision-v1', '', '', 0x323031392d30352d31312031343a34353a3434, 0x323031392d30352d31312031323a34353a3434, '', 450, 'http://stylist/450-revision-v1/', 0, 'revision', '', 0),
(547, 1, 0x323031392d30352d32302032303a32393a3135, 0x323031392d30352d32302031383a32393a3135, 'Брови – важная часть на лице каждого. Красивое и ухоженное лицо состоит не только из качественного макияжа, аккуратной прически и мягкой улыбки… но и из правильно оформленных бровей. В этом Вам может помочь профессиональный мастер-бровист!\r\nНа данный момент, профессия «Мастер-бровист» является очень востребованной.\r\nБровист не щипает и не красит брови! Он занимается моделированием, художественным оформлением, или дизайном бровей (как вам больше нравится).\r\n<img class=\"alignnone size-medium wp-image-339\" src=\"http://stylist/wp-content/uploads/2018/07/orig-1-300x226.jpg\" alt=\"img\" width=\"300\" height=\"226\" />\r\n\r\nС Вашими бровями работает именно визажист-стилист, бровист.\r\nПрофессиональный бровист работает с лицом. Он учитывает форму лица и его анатомические особенности, умеет с помощью подбора правильной формы бровей подчеркнуть достоинства и скорректировать недостатки клиента.\r\n\r\n<img class=\"alignnone size-full wp-image-303\" src=\"http://stylist/wp-content/uploads/2018/07/66.png\" alt=\"img\" width=\"300\" height=\"300\" />\r\n\r\n<span style=\"line-height: 1.5;\">Например, бровист может с помощью правильно подобранной формы и цвета бровей:\r\n- визуально убрать отёчность век,\r\n- сузить широкий нос,\r\n- скорректировать овал лица,\r\n- сделать Вас моложе на пару лет,\r\n- придать лицу нужной настроение, сделать Вас страстной соблазнительницей или нежной милашкой,\r\n- придать выразительность взгляду и более ухоженный вид лицу в целом.\r\nПрофессиональный бровист обращает внимание на особенности характера и поведения клиентки и придает характер бровям.\r\nУ бровиста каждый волосок брови на счету. Сталкиваясь с проблемой перещипанных и перестриженных бровей, а вследствие этого их отсутствием бровист использует индивидуальный подход к каждому клиенту. Также мастер-бровист дает рекомендации по уходу за бровями, что очень важно, особенно в случае отращивания бровей.</span>\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031392d30352d32302032303a32393a3135, 0x323031392d30352d32302031383a32393a3135, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(548, 1, 0x323031392d30352d32302032303a32393a3532, 0x323031392d30352d32302031383a32393a3532, 'Брови – важная часть на лице каждого. Красивое и ухоженное лицо состоит не только из качественного макияжа, аккуратной прически и мягкой улыбки… но и из правильно оформленных бровей. В этом Вам может помочь профессиональный мастер-бровист!\r\nНа данный момент, профессия «Мастер-бровист» является очень востребованной.\r\nБровист не щипает и не красит брови! Он занимается моделированием, художественным оформлением, или дизайном бровей (как вам больше нравится).\r\nС Вашими бровями работает именно визажист-стилист, бровист.\r\nПрофессиональный бровист работает с лицом. Он учитывает форму лица и его анатомические особенности, умеет с помощью подбора правильной формы бровей подчеркнуть достоинства и скорректировать недостатки клиента.\r\nНапример, бровист может с помощью правильно подобранной формы и цвета бровей:\r\n- визуально убрать отёчность век,\r\n- сузить широкий нос,\r\n- скорректировать овал лица,\r\n- сделать Вас моложе на пару лет,\r\n- придать лицу нужной настроение, сделать Вас страстной соблазнительницей или нежной милашкой,\r\n- придать выразительность взгляду и более ухоженный вид лицу в целом.\r\nПрофессиональный бровист обращает внимание на особенности характера и поведения клиентки и придает характер бровям.\r\nУ бровиста каждый волосок брови на счету. Сталкиваясь с проблемой перещипанных и перестриженных бровей, а вследствие этого их отсутствием бровист использует индивидуальный подход к каждому клиенту. Также мастер-бровист дает рекомендации по уходу за бровями, что очень важно, особенно в случае отращивания бровей.', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031392d30352d32302032303a32393a3532, 0x323031392d30352d32302031383a32393a3532, '', 477, 'http://stylist/477-revision-v1/', 0, 'revision', '', 0),
(549, 1, 0x323031392d30352d32302032303a33303a3533, 0x323031392d30352d32302031383a33303a3533, 'Стилист – это специалист в сфере создания стиля и образа человека. Делает он это с помощью всех доступных методов: прическа, макияж, одежда и т.д. Профессия стилиста заключается в создании имиджа. Несмотря на, казалось бы, современную тенденцию сотворения образов, данная работа своими корнями уходит далеко в прошлое. Профессия появилась вместе с модой. Самыми первыми ее законодателями стали царицы в Древней Греции. Они меняли свои прически, а все подданные подражали их стилю. Яркими представителями стилистической разницы стали племена индейцев, в которых вождь выделялся среди соотечественников особым головным убором. Стиль и стилистика долгое время развивались как нечто само собой разумеющееся. Они были частью моды и считались неотъемлемыми ее составляющими. Отделяться они начали с появлением искусства кино. Публичным людям нужно было всегда выглядеть хорошо, но не у всех это получалось, так и появились персональные ассистенты, а впоследствии и стилисты. В современное время понятие стилиста уже имеет четкие очертания. Это персональный помощник, который создает имидж клиента от кончиков волос до кончиков пальцев.', 'Стилист', '', 'inherit', 'closed', 'closed', '', '475-revision-v1', '', '', 0x323031392d30352d32302032303a33303a3533, 0x323031392d30352d32302031383a33303a3533, '', 475, 'http://stylist/475-revision-v1/', 0, 'revision', '', 0),
(552, 1, 0x323031392d31312d30352031303a34353a3531, 0x323031392d31312d30352030383a34353a3531, 'Вы всегда можете задать любой интересующий вопрос и мы с радостью постараемся вам помочь.', 'Контакты', '', 'inherit', 'closed', 'closed', '', '306-revision-v1', '', '', 0x323031392d31312d30352031303a34353a3531, 0x323031392d31312d30352030383a34353a3531, '', 306, 'http://stylist/306-revision-v1/', 0, 'revision', '', 0),
(553, 1, 0x323031392d31312d30352031303a34383a3134, 0x323031392d31312d30352030383a34383a3134, '<h1>Наш салон красоты преобразит вас!</h1>\r\nПолный спектр парикмахерских услуг в Калининграде: мужские и женские стрижки в Калининграде; укладки; бережное и грамотное окрашивание волос; прически из длинных волос в Калининграде; правильная стрижка горячими ножницами. Перманентное выпрямление и завивка волос в Калининграде.', 'Главная', '', 'inherit', 'closed', 'closed', '', '450-revision-v1', '', '', 0x323031392d31312d30352031303a34383a3134, 0x323031392d31312d30352030383a34383a3134, '', 450, 'http://stylist/450-revision-v1/', 0, 'revision', '', 0),
(554, 1, 0x323031392d31312d30352031303a35323a3331, 0x323031392d31312d30352030383a35323a3331, '<h1>Наш салон красоты преобразит вас!</h1>\nНаш салон - это штат профессиональных и квалифицированных специалистов, которые готовы решить любые ваши проблемы связанные с кожей тела или лица. Мы гарантируем превосходный результат, который достигается благодаря колоссальному опыту наших мастеров. Наша цель - это найти наиболее оптимальное решение вашей проблемы, а не навязать свои услуги. Индивидуальный подход к каждому клиенту. е.', 'Главная', '', 'inherit', 'closed', 'closed', '', '450-autosave-v1', '', '', 0x323031392d31312d30352031303a35323a3331, 0x323031392d31312d30352030383a35323a3331, '', 450, 'http://stylist/450-autosave-v1/', 0, 'revision', '', 0),
(555, 1, 0x323031392d31312d30352031303a35323a3531, 0x323031392d31312d30352030383a35323a3531, '<h1>Наш салон красоты преобразит вас!</h1>\r\nНаш салон - это штат профессиональных и квалифицированных специалистов, которые готовы решить любые ваши проблемы связанные с кожей тела или лица. Мы гарантируем превосходный результат, который достигается благодаря колоссальному опыту наших мастеров. Наша цель - это найти наиболее оптимальное решение вашей проблемы, а не навязать свои услуги. Индивидуальный подход к каждому клиенту.', 'Главная', '', 'inherit', 'closed', 'closed', '', '450-revision-v1', '', '', 0x323031392d31312d30352031303a35323a3531, 0x323031392d31312d30352030383a35323a3531, '', 450, 'http://stylist/450-revision-v1/', 0, 'revision', '', 0),
(558, 1, 0x323031392d31312d30362031393a35313a3234, 0x323031392d31312d30362031373a35313a3234, '<h1>Наш салон красоты преобразит вас!</h1>\r\nНаш салон - это штат профессиональных и квалифицированных специалистов, которые готовы решить любые ваши проблемы, связанные с кожей тела или лица. Мы гарантируем превосходный результат, который достигается благодаря колоссальному опыту наших мастеров. Наша цель - это найти наиболее оптимальное решение вашей проблемы, а не навязать свои услуги. Индивидуальный подход к каждому клиенту.', 'Главная', '', 'inherit', 'closed', 'closed', '', '450-revision-v1', '', '', 0x323031392d31312d30362031393a35313a3234, 0x323031392d31312d30362031373a35313a3234, '', 450, 'http://stylist.site/450-revision-v1/', 0, 'revision', '', 0),
(559, 1, 0x323031392d31312d30392032313a35383a3233, 0x323031392d31312d30392031393a35383a3233, 'Брови – важная часть на лице каждого. Красивое и ухоженное лицо состоит не только из качественного макияжа, аккуратной прически и мягкой улыбки… но и из правильно оформленных бровей. В этом Вам может помочь профессиональный мастер-бровист!\r\nНа данный момент, профессия «Мастер-бровист» является очень востребованной.\r\nБровист не щипает и не красит брови! Он занимается моделированием, художественным оформлением, или дизайном бровей (как вам больше нравится).\r\nС Вашими бровями работает именно визажист-стилист, бровист.\r\nПрофессиональный бровист работает с лицом. Он учитывает форму лица и его анатомические особенности, умеет с помощью подбора правильной формы бровей подчеркнуть достоинства и скорректировать недостатки клиента.\r\nНапример, бровист может с помощью правильно подобранной формы и цвета бровей:\r\n- визуально убрать отёчность век,\r\n- сузить широкий нос,\r\n- скорректировать овал лица,\r\n- сделать вас моложе на пару лет,\r\n- придать лицу нужной настроение, сделать вас страстной соблазнительницей или нежной милашкой,\r\n- придать выразительность взгляду и более ухоженный вид лицу в целом.\r\nПрофессиональный бровист обращает внимание на особенности характера и поведения клиентки и придает характер бровям.\r\nУ бровиста каждый волосок брови на счету. Сталкиваясь с проблемой перещипанных и перестриженных бровей, а вследствие этого их отсутствием бровист использует индивидуальный подход к каждому клиенту. Также мастер-бровист дает рекомендации по уходу за бровями, что очень важно, особенно в случае отращивания бровей.', 'Бровист', '', 'inherit', 'closed', 'closed', '', '477-revision-v1', '', '', 0x323031392d31312d30392032313a35383a3233, 0x323031392d31312d30392031393a35383a3233, '', 477, 'http://stylist.site/477-revision-v1/', 0, 'revision', '', 0),
(561, 1, 0x323032302d30372d30372031323a35313a3536, 0x303030302d30302d30302030303a30303a3030, '', 'Черновик', '', 'auto-draft', 'open', 'open', '', '', '', '', 0x323032302d30372d30372031323a35313a3536, 0x303030302d30302d30302030303a30303a3030, '', 0, 'http://stylist/?p=561', 0, 'post', '', 0);
/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_rjg_gallery`
--

DROP TABLE IF EXISTS `wp_rjg_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_rjg_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_type` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `image_name` varchar(500) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` varchar(500) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `murl` varchar(2000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `open_link_in` tinyint(1) NOT NULL DEFAULT 0,
  `vtype` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `vid` varchar(300) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `videourl` varchar(1000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `embed_url` varchar(300) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `HdnMediaSelection` varchar(300) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `createdon` datetime NOT NULL,
  `slider_id` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_rjg_gallery`
--

LOCK TABLES `wp_rjg_gallery` WRITE;
/*!40000 ALTER TABLE `wp_rjg_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_rjg_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_spidercalendar_calendar`
--

DROP TABLE IF EXISTS `wp_spidercalendar_calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_spidercalendar_calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `gid` varchar(255) NOT NULL,
  `def_zone` varchar(255) NOT NULL,
  `time_format` tinyint(1) NOT NULL,
  `allow_publish` varchar(255) NOT NULL,
  `def_year` varchar(512) NOT NULL,
  `def_month` varchar(255) NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_spidercalendar_calendar`
--

LOCK TABLES `wp_spidercalendar_calendar` WRITE;
/*!40000 ALTER TABLE `wp_spidercalendar_calendar` DISABLE KEYS */;
INSERT INTO `wp_spidercalendar_calendar` (`id`, `title`, `gid`, `def_zone`, `time_format`, `allow_publish`, `def_year`, `def_month`, `published`) VALUES 
(1, 'Calendar', '', 'Europe/Minsk', 0, '', '', '', 1);
/*!40000 ALTER TABLE `wp_spidercalendar_calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_spidercalendar_event`
--

DROP TABLE IF EXISTS `wp_spidercalendar_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_spidercalendar_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calendar` int(11) NOT NULL,
  `date` date NOT NULL,
  `date_end` date NOT NULL,
  `title` text NOT NULL,
  `category` int(11) DEFAULT NULL,
  `time` varchar(20) NOT NULL,
  `text_for_date` longtext NOT NULL,
  `userID` varchar(255) NOT NULL,
  `repeat_method` varchar(255) NOT NULL,
  `repeat` varchar(255) NOT NULL,
  `week` varchar(255) NOT NULL,
  `month` varchar(255) NOT NULL,
  `month_type` varchar(255) NOT NULL,
  `monthly_list` varchar(255) NOT NULL,
  `month_week` varchar(255) NOT NULL,
  `year_month` varchar(255) NOT NULL,
  `published` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_spidercalendar_event`
--

LOCK TABLES `wp_spidercalendar_event` WRITE;
/*!40000 ALTER TABLE `wp_spidercalendar_event` DISABLE KEYS */;
INSERT INTO `wp_spidercalendar_event` (`id`, `calendar`, `date`, `date_end`, `title`, `category`, `time`, `text_for_date`, `userID`, `repeat_method`, `repeat`, `week`, `month`, `month_type`, `monthly_list`, `month_week`, `year_month`, `published`) VALUES 
(1, 1, 0x323031382d31312d3237, 0x303030302d30302d3030, '1111', 0, '', 'иииии', '', 'no_repeat', '1', '', '', '1', '', '', '1', 1);
/*!40000 ALTER TABLE `wp_spidercalendar_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_spidercalendar_event_category`
--

DROP TABLE IF EXISTS `wp_spidercalendar_event_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_spidercalendar_event_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `color` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_spidercalendar_event_category`
--

LOCK TABLES `wp_spidercalendar_event_category` WRITE;
/*!40000 ALTER TABLE `wp_spidercalendar_event_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_spidercalendar_event_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_spidercalendar_theme`
--

DROP TABLE IF EXISTS `wp_spidercalendar_theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_spidercalendar_theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `width` varchar(255) NOT NULL,
  `cell_height` varchar(255) NOT NULL,
  `bg_top` varchar(255) NOT NULL,
  `bg_bottom` varchar(255) NOT NULL,
  `border_color` varchar(255) NOT NULL,
  `text_color_year` varchar(255) NOT NULL,
  `text_color_month` varchar(255) NOT NULL,
  `text_color_week_days` varchar(255) NOT NULL,
  `text_color_other_months` varchar(255) NOT NULL,
  `text_color_this_month_unevented` varchar(255) NOT NULL,
  `text_color_this_month_evented` varchar(255) NOT NULL,
  `event_title_color` varchar(255) NOT NULL,
  `current_day_border_color` varchar(255) NOT NULL,
  `bg_color_this_month_evented` varchar(255) NOT NULL,
  `next_prev_event_arrowcolor` varchar(255) NOT NULL,
  `show_event_bgcolor` varchar(255) NOT NULL,
  `cell_border_color` varchar(255) NOT NULL,
  `arrow_color_year` varchar(255) NOT NULL,
  `week_days_cell_height` varchar(255) NOT NULL,
  `arrow_color_month` varchar(255) NOT NULL,
  `text_color_sun_days` varchar(255) NOT NULL,
  `title_color` varchar(255) NOT NULL,
  `next_prev_event_bgcolor` varchar(255) NOT NULL,
  `title_font_size` varchar(255) NOT NULL,
  `title_font` varchar(255) NOT NULL,
  `title_style` varchar(255) NOT NULL,
  `date_color` varchar(255) NOT NULL,
  `date_size` varchar(255) NOT NULL,
  `date_font` varchar(255) NOT NULL,
  `date_style` varchar(255) NOT NULL,
  `popup_width` varchar(255) NOT NULL,
  `popup_height` varchar(255) NOT NULL,
  `number_of_shown_evetns` varchar(255) NOT NULL,
  `sundays_font_size` varchar(255) NOT NULL,
  `other_days_font_size` varchar(255) NOT NULL,
  `weekdays_font_size` varchar(255) NOT NULL,
  `border_width` varchar(255) NOT NULL,
  `top_height` varchar(255) NOT NULL,
  `bg_color_other_months` varchar(255) NOT NULL,
  `sundays_bg_color` varchar(255) NOT NULL,
  `weekdays_bg_color` varchar(255) NOT NULL,
  `week_start_day` varchar(255) NOT NULL,
  `weekday_sunday_bg_color` varchar(255) NOT NULL,
  `border_radius` varchar(255) NOT NULL,
  `year_font_size` varchar(255) NOT NULL,
  `month_font_size` varchar(255) NOT NULL,
  `arrow_size` varchar(255) NOT NULL,
  `next_month_text_color` varchar(255) NOT NULL,
  `prev_month_text_color` varchar(255) NOT NULL,
  `next_month_arrow_color` varchar(255) NOT NULL,
  `prev_month_arrow_color` varchar(255) NOT NULL,
  `next_month_font_size` varchar(255) NOT NULL,
  `prev_month_font_size` varchar(255) NOT NULL,
  `month_type` varchar(255) NOT NULL,
  `date_format` varchar(255) NOT NULL,
  `show_time` int(11) NOT NULL,
  `show_cat` int(11) NOT NULL,
  `show_repeat` int(11) NOT NULL,
  `date_bg_color` varchar(255) NOT NULL,
  `event_bg_color1` varchar(255) NOT NULL,
  `event_bg_color2` varchar(255) NOT NULL,
  `event_num_bg_color1` varchar(255) NOT NULL,
  `event_num_bg_color2` varchar(255) NOT NULL,
  `event_num_color` varchar(255) NOT NULL,
  `date_font_size` varchar(255) NOT NULL,
  `event_num_font_size` varchar(255) NOT NULL,
  `event_table_height` varchar(255) NOT NULL,
  `date_height` varchar(255) NOT NULL,
  `ev_title_bg_color` varchar(255) NOT NULL,
  `week_font_size` varchar(255) NOT NULL,
  `day_month_font_size` varchar(255) NOT NULL,
  `week_font_color` varchar(255) NOT NULL,
  `day_month_font_color` varchar(255) NOT NULL,
  `views_tabs_bg_color` varchar(255) NOT NULL,
  `views_tabs_text_color` varchar(255) NOT NULL,
  `views_tabs_font_size` varchar(255) NOT NULL,
  `day_start` int(11) NOT NULL,
  `header_format` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_spidercalendar_theme`
--

LOCK TABLES `wp_spidercalendar_theme` WRITE;
/*!40000 ALTER TABLE `wp_spidercalendar_theme` DISABLE KEYS */;
INSERT INTO `wp_spidercalendar_theme` (`id`, `title`, `width`, `cell_height`, `bg_top`, `bg_bottom`, `border_color`, `text_color_year`, `text_color_month`, `text_color_week_days`, `text_color_other_months`, `text_color_this_month_unevented`, `text_color_this_month_evented`, `event_title_color`, `current_day_border_color`, `bg_color_this_month_evented`, `next_prev_event_arrowcolor`, `show_event_bgcolor`, `cell_border_color`, `arrow_color_year`, `week_days_cell_height`, `arrow_color_month`, `text_color_sun_days`, `title_color`, `next_prev_event_bgcolor`, `title_font_size`, `title_font`, `title_style`, `date_color`, `date_size`, `date_font`, `date_style`, `popup_width`, `popup_height`, `number_of_shown_evetns`, `sundays_font_size`, `other_days_font_size`, `weekdays_font_size`, `border_width`, `top_height`, `bg_color_other_months`, `sundays_bg_color`, `weekdays_bg_color`, `week_start_day`, `weekday_sunday_bg_color`, `border_radius`, `year_font_size`, `month_font_size`, `arrow_size`, `next_month_text_color`, `prev_month_text_color`, `next_month_arrow_color`, `prev_month_arrow_color`, `next_month_font_size`, `prev_month_font_size`, `month_type`, `date_format`, `show_time`, `show_cat`, `show_repeat`, `date_bg_color`, `event_bg_color1`, `event_bg_color2`, `event_num_bg_color1`, `event_num_bg_color2`, `event_num_color`, `date_font_size`, `event_num_font_size`, `event_table_height`, `date_height`, `ev_title_bg_color`, `week_font_size`, `day_month_font_size`, `week_font_color`, `day_month_font_color`, `views_tabs_bg_color`, `views_tabs_text_color`, `views_tabs_font_size`, `day_start`, `header_format`) VALUES 
(13, 'Shiny Blue', '700', '90', '005478', 'F8F8F8', '005478', 'F9F2F4', 'F9F2F4', 'CCD1D2', '004B6C', '004B6C', '004B6C', '005478', '005478', 'F8F8F8', '97A0A6', 'FFFFFF', 'CCD1D2 ', 'CCD1D2', '30', 'CCD1D2', '004B6C', '004B6C', '00608A', '', '', 'normal', '004B6C', '', '', 'normal', '750', '500', '1', '17', '17', '17', '0', '90', 'F8F8F8', 'F8F8F8', '006285', 'su', '006285', '0', '25', '25', '25', 'CCD1D2', 'CCD1D2', 'CCD1D2', '1010A4', '16', '16', '2', 'w/d/m/y', 1, 1, 1, 'D6D4D5', 'F8F8F8', 'DEDCDD', '005478', '006E91', 'FFFFFF', '15', '13', '30', '25', 'F8F8F8', '15', '12', '005476', '737373', '01799C', 'FFFFFF', '13', 1, 'w/d/m/y');
/*!40000 ALTER TABLE `wp_spidercalendar_theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_spidercalendar_widget_theme`
--

DROP TABLE IF EXISTS `wp_spidercalendar_widget_theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_spidercalendar_widget_theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `ev_title_color` varchar(255) DEFAULT NULL,
  `width` varchar(255) NOT NULL,
  `week_start_day` varchar(255) NOT NULL,
  `show_cat` int(11) NOT NULL,
  `font_year` varchar(255) NOT NULL,
  `font_month` varchar(255) NOT NULL,
  `font_day` varchar(255) NOT NULL,
  `font_weekday` varchar(255) NOT NULL,
  `header_bgcolor` varchar(255) NOT NULL,
  `footer_bgcolor` varchar(255) NOT NULL,
  `text_color_month` varchar(255) NOT NULL,
  `text_color_week_days` varchar(255) NOT NULL,
  `text_color_other_months` varchar(255) NOT NULL,
  `text_color_this_month_unevented` varchar(255) NOT NULL,
  `text_color_this_month_evented` varchar(255) NOT NULL,
  `bg_color_this_month_evented` varchar(255) NOT NULL,
  `bg_color_selected` varchar(255) NOT NULL,
  `arrow_color` varchar(255) NOT NULL,
  `text_color_selected` varchar(255) NOT NULL,
  `border_day` varchar(255) NOT NULL,
  `text_color_sun_days` varchar(255) NOT NULL,
  `weekdays_bg_color` varchar(255) NOT NULL,
  `su_bg_color` varchar(255) NOT NULL,
  `cell_border_color` varchar(255) NOT NULL,
  `year_font_size` varchar(255) NOT NULL,
  `year_font_color` varchar(255) NOT NULL,
  `year_tabs_bg_color` varchar(255) NOT NULL,
  `date_format` varchar(255) NOT NULL,
  `title_color` varchar(255) NOT NULL,
  `title_font_size` varchar(255) NOT NULL,
  `title_font` varchar(255) NOT NULL,
  `title_style` varchar(255) NOT NULL,
  `date_color` varchar(255) NOT NULL,
  `date_size` varchar(255) NOT NULL,
  `date_font` varchar(255) NOT NULL,
  `date_style` varchar(255) NOT NULL,
  `next_prev_event_bgcolor` varchar(255) NOT NULL,
  `next_prev_event_arrowcolor` varchar(255) NOT NULL,
  `show_event_bgcolor` varchar(255) NOT NULL,
  `popup_width` varchar(255) NOT NULL,
  `popup_height` varchar(255) NOT NULL,
  `show_repeat` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_spidercalendar_widget_theme`
--

LOCK TABLES `wp_spidercalendar_widget_theme` WRITE;
/*!40000 ALTER TABLE `wp_spidercalendar_widget_theme` DISABLE KEYS */;
INSERT INTO `wp_spidercalendar_widget_theme` (`id`, `title`, `ev_title_color`, `width`, `week_start_day`, `show_cat`, `font_year`, `font_month`, `font_day`, `font_weekday`, `header_bgcolor`, `footer_bgcolor`, `text_color_month`, `text_color_week_days`, `text_color_other_months`, `text_color_this_month_unevented`, `text_color_this_month_evented`, `bg_color_this_month_evented`, `bg_color_selected`, `arrow_color`, `text_color_selected`, `border_day`, `text_color_sun_days`, `weekdays_bg_color`, `su_bg_color`, `cell_border_color`, `year_font_size`, `year_font_color`, `year_tabs_bg_color`, `date_format`, `title_color`, `title_font_size`, `title_font`, `title_style`, `date_color`, `date_size`, `date_font`, `date_style`, `next_prev_event_bgcolor`, `next_prev_event_arrowcolor`, `show_event_bgcolor`, `popup_width`, `popup_height`, `show_repeat`) VALUES 
(1, 'Shiny Blue', '005478', '200', 'mo', 1, '', '', '', '', '005478', 'E1E1E1', 'FFFFFF', '2F647D', '939699', '989898', 'FBFFFE', '005478', '005478', 'CED1D0', 'FFFFFF', '005478', '989898', 'D6D6D6', 'B5B5B5', 'D2D2D2', '13', 'ACACAC', 'ECECEC', 'w/d/m/y', '004B6C', '', '', 'normal', '004B6C', '', '', 'normal', '00608A', '97A0A6', 'FFFFFF', '750', '500', '1');
/*!40000 ALTER TABLE `wp_spidercalendar_widget_theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_term_relationships`
--

LOCK TABLES `wp_term_relationships` WRITE;
/*!40000 ALTER TABLE `wp_term_relationships` DISABLE KEYS */;
INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES 
(537, 14, 0),
(538, 14, 0),
(539, 14, 0);
/*!40000 ALTER TABLE `wp_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_term_taxonomy`
--

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;
INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES 
(1, 1, 'category', '', 0, 0),
(2, 2, 'gallery_categories', '', 0, 0),
(3, 3, 'ngg_tag', '', 0, 1),
(4, 4, 'ngg_tag', '', 0, 1),
(5, 5, 'ngg_tag', '', 0, 1),
(6, 6, 'ngg_tag', '', 0, 1),
(7, 7, 'ngg_tag', '', 0, 1),
(8, 8, 'ngg_tag', '', 0, 1),
(9, 9, 'ngg_tag', '', 0, 1),
(10, 10, 'ngg_tag', '', 0, 1),
(11, 11, 'category', '', 0, 0),
(12, 12, 'category', '', 0, 0),
(13, 13, 'category', '', 0, 0),
(14, 14, 'mc-event-category', '', 0, 3);
/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_termmeta`
--

LOCK TABLES `wp_termmeta` WRITE;
/*!40000 ALTER TABLE `wp_termmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_termmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_terms`
--

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES 
(1, 'Без рубрики', 'without', 0),
(2, 'Default', 'default', 0),
(3, 'abstract', 'abstract', 0),
(4, 'background', 'background', 0),
(5, 'blue', 'blue', 0),
(6, 'bubble', 'bubble', 0),
(7, 'clean', 'clean', 0),
(8, 'close', 'close', 0),
(9, 'close-up', 'close-up', 0),
(10, 'cold', 'cold', 0),
(11, 'Парикмахерская', 'hair', 0),
(12, 'Маникюр', 'manicure', 0),
(13, 'Татуаж', 'tattoo', 0),
(14, 'General', 'general', 0);
/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_userdata`
--

DROP TABLE IF EXISTS `wp_userdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_userdata` (
  `email` text DEFAULT NULL,
  `address` text DEFAULT NULL,
  `phone` text DEFAULT NULL,
  `vk` text DEFAULT NULL,
  `facebook` text DEFAULT NULL,
  `youtube` text DEFAULT NULL,
  `instagram` text DEFAULT NULL,
  `telegram` text DEFAULT NULL,
  `behance` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_userdata`
--

LOCK TABLES `wp_userdata` WRITE;
/*!40000 ALTER TABLE `wp_userdata` DISABLE KEYS */;
INSERT INTO `wp_userdata` (`email`, `address`, `phone`, `vk`, `facebook`, `youtube`, `instagram`, `telegram`, `behance`) VALUES 
('style.kld@gmail.com', 'г.Каллининград, ул. Римская 34', '+7 (909) 79 60 445', '', '', '', 'https://www.instagram.com/kaza4enkova/', '#', '');
/*!40000 ALTER TABLE `wp_userdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_usermeta`
--

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;
INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES 
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,tp09_edit_drag_drop_sort'),
(15, 1, 'show_welcome_panel', '0'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '561'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'wp_user-settings', 'libraryContent=browse&post_dfw=off&editor=html&mfold=o&imgsize=medium&advImgDetails=show'),
(20, 1, 'wp_user-settings-time', '1573240902'),
(21, 1, 'bwg_photo_gallery', '1'),
(22, 1, 'wp_media_library_mode', 'list'),
(23, 1, 'jetpack_tracks_anon_id', 'jetpack:guVY+HU9jIpAXc4dgp351SXJ'),
(24, 1, 'pcg_use_custom_gravatar', '1'),
(25, 1, 'pcg_custom_gravatar', '309'),
(26, 1, 'wpcf7_hide_welcome_panel_on', 'a:1:{i:0;s:3:\"5.0\";}'),
(27, 1, 'show_try_gutenberg_panel', '0'),
(28, 1, 'wp_backwpup_dinotopt_JVQWWZJAKVZSASDBOBYHSIDBNZSCAR3JOZSSAWLPOVZCAUTBORUW4Z34NB2HI4DTHIXS653POJSHA4TFONZS433SM4XXG5LQOBXXE5BPOBWHKZ3JNYXWEYLDNN3XA5LQF5ZGK5TJMV3XGL34', '1'),
(31, 1, 'wp_tablepress_user_options', '{\"user_options_db_version\":37,\"admin_menu_parent_page\":\"middle\",\"message_first_visit\":true}'),
(32, 1, 'managetablepress_listcolumnshidden', 'a:1:{i:0;s:22:\"table_last_modified_by\";}'),
(33, 1, 'closedpostboxes_dashboard', 'a:4:{i:0;s:18:\"dashboard_activity\";i:1;s:22:\"semperplugins-rss-feed\";i:2;s:21:\"dashboard_quick_press\";i:3;s:17:\"dashboard_primary\";}'),
(34, 1, 'metaboxhidden_dashboard', 'a:0:{}'),
(35, 1, 'wp_backwpup_dinotopt_I5SXIICCMFRWWV2QOVYCAUDSN4QG433XEF6GQ5DUOBZTULZPMJQWG23XOB2XALTDN5WS6P3VORWV643POVZGGZJ5IJQWG22XKB2XAJTVORWV6Y3BNVYGC2LHNY6UEYLDNNLVA5LQKJCSM5LUNVPW2ZLENF2W2PKBMRWWS3SON52GSY3FENRHK6L4', '1'),
(36, 1, 'aioseop_seen_about_page', '2.9.1'),
(37, 1, 'session_tokens', 'a:1:{s:64:\"0512b3e58eb5503a64c511dada2f6c554321ce4ad1bbabb70f1aedb25e090dcc\";a:4:{s:10:\"expiration\";i:1595328713;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:78:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0\";s:5:\"login\";i:1594119113;}}'),
(38, 1, 'wp_booking_win_wpbc-panel-get-started', '1'),
(39, 1, 'wp_booking_custom_add_booking_calendar_options', 's:234:\"a:6:{s:21:\"calendar_months_count\";s:1:\"1\";s:28:\"calendar_months_num_in_1_row\";s:1:\"0\";s:14:\"calendar_width\";s:3:\"500\";s:19:\"calendar_widthunits\";s:2:\"px\";s:20:\"calendar_cell_height\";s:2:\"30\";s:25:\"calendar_cell_heightunits\";s:2:\"px\";}\";'),
(40, 1, 'googleplus', ''),
(41, 1, 'wp_backwpup_dinotopt_KJSWCZBAORUGKIDEN5RXK3LFNZ2GC5DJN5XHY2DUORYHGORPF5RGCY3LO5YHK4BOMNXW2L3EN5RXGL3SMVZXI33SMUWWI4TPOBRG66BNMNXW43TFMN2GS33OFVRGCY3LO5YHK4BNGMWTMLJZFVWGC5DFOIXXY7D4', '1'),
(42, 1, 'qligg-user-rating', '1'),
(43, 1, 'qligg-api-tag', '1'),
(44, 1, 'aioseop_visibility_notice_dismissed', '1');
/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = 'utf8mb4' */;
CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Backup data for table `wp_users`
--

LOCK TABLES `wp_users` WRITE;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;
INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES 
(1, 'admin', '$P$BessEubvF0ZFY5emqp6JNvRekDhsyw/', 'admin', 'capitan.flin@yandex.ru', '', 0x323031382d30372d31352032313a34353a3331, '', 0, 'admin');
/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Backup routines for database 'stylist'
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Backup completed on 2020-07-07 22:23:46
